# Registro de cambios en el núcleo

- **[2022-09-13] v2022.0913:**
  - Añadida funcionalidad para recordar el estado de los desplegables de filtros en los listados

- **[2022-08-18] v2022.0818:**
  - Solución error al tratar de guardar un agente con codpais ""
  - Se modifica el modelo de agente para incluir foto del mismo

- **[2022-07-18] v2022.0718:**
  - Agregado campo codpais al método save del modelo agente.
  - Modificado Empleado de Serie a "Nombre Empleado"
  - Modificado Empleado de serie para que ponga como fecha de alta, la fecha de la instalacion.

- **[2022-06-15] v2022.0615:**
  - Primera versión subida a la comunidad beta usando el motor de plantillas BLADE y SKOTE.

- **[2022-04-03] v2022.0203:**
  - Se recibe en la comunidad la lista completa de plugins (incluyendo desactivados). Se quitan los desactivas de la pestaña de instalación, pero sí que se siguen actualizando.

- **[2022-02-03] v2022.0203:**
  - Se amplía el tamaño del campo fs_page de 30 a 90 caracteres.

- **[2021-12-28] v2021.3620:**
  - Se añade soporte para Blade.

- **[2021-12-24] v2021.3580:**
  - Se pasan los campos de la tabla agentes de contratos a facturación base y núcleo.

- **[2021-09-17] v2021.2602:**
  - Se añaden nuevos paquetes al composer para eliminar duplicidades en los plugins.

- **[2021-09-15] v2021.2580:**
  - Se cambian los campos de tipo TEXT por MEDIUMTEXT.
 
- **[2021-09-8] v2021.2510:**
  - Agregada la posibilidad de incluir extensiones de botones en el edit controller y plantillas en cabecera y pie

- **[2021-09-09] v2021.2520:**
  - Se añade soporte inicial para "Actualizar todo" con 1 solo click
  - Se añade integración con los wizards de todos los plugins
  - Se añade en algunos modelos el método "fix_model_table()" para poder corregir al vuelo algunos problemas

- **[2021-09-01] v2021.2441:**
  - Agregada la posibilidad de incluir campos jquery autocomplete en las columnas de un edit controler.

- **[2021-09-1] v2021.2440:**
  - Se agregan las extradata en los add_colum del fs_edit_form y se implementa maxlength tanto para text 
    como para default y min, max y step para los number y money 

- **[2021-07-17] v2021.1980:**
  - Eliminados los archivos excluidos en .gitignore de los propios repositorios
  - Añadidos algunos composer.json que faltaban
  - Movidos algunos falsos vendor a extras
  - Subida versión mínima de PHP de 5.6 a 7.0
  - Añadidos/corregidos bloques PHP faltantes
  - Código reformateado
  - Código reordenado
  - Añadidos .gitignore

- **[2021-06-21] v2021.1720:**
  - Se habilita la opción de plugins autoactivables, y se corrige error en plugins no desactivables.
  - Se fuerza a actualizar las dependencias antes que un plugin que dependa de ellas.
  - De existir un wizard, se ejecuta tras actualizar.

- **[2021-06-18] v2021.1670:**
  - Se incluye en fatal_handler la llamada a una función send_error (si existe), que está definida en ayuda_soporte.
  - Esa función, remite mediante la API de domaincontroller, información sobre cualquier error que ocurra.
