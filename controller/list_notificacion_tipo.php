<?php

/*
 * This file is part of MiFactura.eu
 * Copyright (C) 2021 X-Net Software Solutions S.L. <xnetsoftware@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Class list_notificacion_tipo
 */
class list_notificacion_tipo extends fs_list_controller
{
    /**
     * admin_kdb constructor.
     */
    public function __construct()
    {
        parent::__construct(
            'Mantenimiento de tipos de notificaciones',
            'Tipos de notificaciones', ['admin', 'company'], false, false
        );
    }

    /**
     * Crea las pestañas del controlador
     */
    protected function create_tabs($tab_name = 'tipos_notificaciones')
    {
        $this->add_tab($tab_name, 'Tipos de notificaciones', 'notificaciones_tipo');
        $this->add_search_columns($tab_name, ['nombre', 'estilo', 'icono',]);
        $this->add_sort_option($tab_name, ['id']);
        $this->add_sort_option($tab_name, ['nombre'], 1);
        $this->add_sort_option($tab_name, ['estilo']);

        $iconos = get_free_fa_icons();

        /// columnas a mostrar
        $this->decoration->add_column($tab_name, 'id', 'integer', '#', '', 'index.php?page=edit_notificacion_tipo&code=');
        $this->decoration->add_column($tab_name, 'nombre', 'string', 'Nombre');
        $this->decoration->add_column($tab_name, 'estilo', 'string', 'Estilo');
        $this->decoration->add_column($tab_name, 'icono', 'i', 'Icono', $iconos);

        /// click
        $this->decoration->add_row_url($tab_name, 'index.php?page=edit_notificacion_tipo&code=', 'id');

        /// botones
        $this->add_button($tab_name, 'Nuevo', 'index.php?page=edit_notificacion_tipo', 'fa-solid fa-plus fa-fw', 'btn-success flex-grow-1 flex-sm-grow-0');
    }

    /**
     * Devuelve el icono asociado al controlador
     *
     * @author  Daniel M. Hernández Vieira  <daniel.hernandez@x-netdigital.com>
     * @version 2022.0503
     *
     * @return string
     */
    public function get_icon_controller()
    {
        return '<i class="fa-solid fa-comment-dots fa-fw"></i>';
    }
}
