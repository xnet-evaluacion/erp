<?php

/*
 * This file is part of MiFactura.eu
 * Copyright (C) 2021 X-Net Software Solutions S.L. <xnetsoftware@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Class edit_notificacion_tipo
 */
class edit_notificacion_tipo extends fs_edit_controller
{
    /**
     * edit_kdb constructor.
     */
    public function __construct()
    {
        parent::__construct(
            'Edición de un tipo de notificaciones',
            'Tipo de notificaciones', ['admin', 'company'], false, false
        );
    }

    /**
     * Devuelve el nombre de la clase model relacionada.
     *
     * @return string
     */
    public function get_model_class_name()
    {
        return 'notificacion_tipo';
    }

    /**
     * Asigna las columnas de edición.
     */
    protected function set_edit_columns()
    {
        $iconos = get_free_fa_icons();
        $colores = get_text_color();

        $this->form->add_column('nombre', 'string', 'Nombre', 4, true);
        $this->form->add_column('estilo', 'select', 'Estilo', 4, true, $colores);
        $this->form->add_column('icono', 'select', 'Icono', 4, true, $iconos);
    }

    /**
     * Devuelve el icono asociado al controlador
     *
     * @return string
     * @version 2022.0503
     *
     * @author  Daniel M. Hernández Vieira  <daniel.hernandez@x-netdigital.com>
     */
    public function get_icon_controller()
    {
        return '<i class="fa-solid fa-bell fa-fw"></i>';
    }
}
