<?php

/*
 * This file is part of MiFactura.eu
 * Copyright (C) 2021 X-Net Software Solutions S.L. <xnetsoftware@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

use Xfs\base\xfs_file_manager;

require_once constant('BASE_PATH') . '/vendor/autoload.php';

/**
 * Controlador de admin -> empresa.
 */
class admin_empresa extends fs_controller
{
    /**
     * Objeto almacen
     *
     * @var almacen
     */
    public $almacen;

    /**
     * Objeto cuenta de banco
     *
     * @var cuenta_banco
     */
    public $cuenta_banco;

    /**
     * Objeto divisa.
     *
     * @var divisa
     */
    public $divisa;

    /**
     * Objeto ejercicio.
     *
     * @var ejercicio
     */
    public $ejercicio;

    /**
     * TODO: Missing documentation
     *
     * @var array
     */
    public $email_plantillas;

    /**
     * Objeto forma de pago.
     *
     * @var forma_pago
     */
    public $forma_pago;

    /**
     * TODO: Missing documentation
     *
     * @var array
     */
    public $impresion;

    /**
     * Objeto serie.
     *
     * @var serie
     */
    public $serie;

    /**
     * Objeto país
     *
     * @var pais
     */
    public $pais;

    /**
     * Devuelve una lista de plantillas.
     *
     * @var array
     */
    public $plantillas;

    /**
     * admin_empresa constructor.
     */
    public function __construct()
    {
        parent::__construct(
            'Personalice todos los datos de su empresa para ajustarla a su situación particular.',
            'Empresa / web', ['admin', 'company'], true, true
        );
    }

    /**
     * Devuelve las encriptaciones soportadas.
     *
     * @return string[]
     */
    public function encriptaciones()
    {
        return [
            'ssl' => 'SSL',
            'tls' => 'TLS',
            '' => 'Ninguna',
        ];
    }

    /**
     * Devuelve los mailers soportados.
     *
     * @return string[]
     */
    public function mailers()
    {
        return [
            'mail' => 'Mail',
            'sendmail' => 'SendMail',
            'smtp' => 'SMTP',
        ];
    }

    /**
     * Código que se ejecutará en la parte privada
     *
     * @throws phpmailerException
     */
    protected function private_core()
    {
        /// inicializamos para que se creen las tablas, aunque no vayamos a configurarlo aquí
        $this->almacen = new almacen();
        $this->cuenta_banco = new cuenta_banco();
        $this->divisa = new divisa();
        $this->ejercicio = new ejercicio();
        $this->forma_pago = new forma_pago();
        $this->serie = new serie();
        $this->pais = new pais();

        $fsvar = new fs_var();

        $this->plantillas = plantillas_login();

        $action = fs_filter_input_req('action');
        switch ($action) {
            case 'delete_dashboardbg':
                $this->delete_dashboardbg();
                break;
        }

        /// obtenemos los datos de las plantillas de emails
        $this->email_plantillas = [
            'mail_factura' => "Buenos días, le adjunto su #DOCUMENTO#.\n#FIRMA#",
            'mail_albaran' => "Buenos días, le adjunto su #DOCUMENTO#.\n#FIRMA#",
            'mail_pedido' => "Buenos días, le adjunto su #DOCUMENTO#.\n#FIRMA#",
            'mail_presupuesto' => "Buenos días, le adjunto su #DOCUMENTO#.\n#FIRMA#",
        ];
        $this->email_plantillas = $fsvar->array_get($this->email_plantillas, false);

        if (filter_input(INPUT_POST, 'nombre')) {
            /// guardamos solamente lo básico, ya que facturacion_base no está activado
            $this->empresa->nombre = filter_input(INPUT_POST, 'nombre');
            $this->empresa->nombrecorto = filter_input(INPUT_POST, 'nombrecorto');
            $this->empresa->web = filter_input(INPUT_POST, 'web');
            $this->empresa->email = filter_input(INPUT_POST, 'email');
            /// guardamos los datos de la empresa
            $fields = [
                'nombre',
                'nombrecorto',
                'web',
                'email',
                'plantilla_login',
            ];
            foreach ($fields as $field) {
                $this->empresa->{$field} = fs_filter_input_post($field);
            }

            /// configuración de email
            $this->empresa->email_config['mail_password'] = filter_input(INPUT_POST, 'mail_password');
            $this->empresa->email_config['mail_bcc'] = filter_input(INPUT_POST, 'mail_bcc');
            $this->empresa->email_config['mail_firma'] = filter_input(INPUT_POST, 'mail_firma');
            $this->empresa->email_config['mail_mailer'] = filter_input(INPUT_POST, 'mail_mailer');
            $this->empresa->email_config['mail_host'] = filter_input(INPUT_POST, 'mail_host');
            $this->empresa->email_config['mail_port'] = intval(filter_input(INPUT_POST, 'mail_port'));
            $this->empresa->email_config['mail_enc'] = mb_strtolower(filter_input(INPUT_POST, 'mail_enc'));
            $this->empresa->email_config['mail_user'] = filter_input(INPUT_POST, 'mail_user');
            $this->empresa->email_config['mail_low_security'] = (bool)filter_input(INPUT_POST, 'mail_low_security');

            if (isset($_FILES['dashboardbg']['tmp_name']) && !empty($_FILES['dashboardbg']['tmp_name'])) {
                $this->delete_dashboardbg();
                $this->cambiar_dashboardbg();
            }

            if ($this->empresa->save()) {
                $this->new_message('Datos guardados correctamente.');
                $this->mail_test();
            } else {
                $this->new_error_msg('Error al guardar los datos.');
            }
        }
    }

    /**
     * Eliminar fondo de pantalla personalizado
     */
    private function delete_dashboardbg()
    {
        $dashboardbg = constant('FS_FOLDER') . '/images/dashboardbg.jpg';
        if (file_exists($dashboardbg)) {
            unlink($dashboardbg);
            $this->new_message('Fondo de pantalla borrado correctamente.');
            $this->cache->clean();
        }
    }

    /**
     * Cambiar fondo de pantalla personalizado
     */
    private function cambiar_dashboardbg()
    {
        $images_folder = constant('FS_FOLDER') . '/images/';
        $dashboardbg = constant('FS_FOLDER') . '/images/dashboardbg.jpg';
        if (is_uploaded_file($_FILES['dashboardbg']['tmp_name'])) {
            if (!file_exists($images_folder)) {
                mkdir($images_folder, 0777, true);
            }
            $this->delete_dashboardbg();
            if (copy($_FILES['dashboardbg']['tmp_name'], $dashboardbg)) {
                $this->new_message('Fondo de pantalla aplicado correctamente.');
                $this->cache->clean();
            } else {
                $this->new_error_msg('Ha ocurrido un problema aplicando el fondo de pantalla.');
            }
        }
    }

    /**
     * Intenta realizar el envío de un email.
     */
    private function mail_test()
    {
        if (false === $this->empresa->can_send_mail()) {
            return;
        }

        /// Es imprescindible OpenSSL para enviar emails con los principales proveedores
        if (false === extension_loaded('openssl')) {
            $this->new_error_msg('No se encuentra la extensión OpenSSL, imprescindible para enviar emails.');
            return;
        }

        $mail = $this->empresa->new_mail();
        $mail->Timeout = 3;
        $mail->FromName = $this->user->nick;
        $mail->Subject = 'TEST';
        $mail->AltBody = 'TEST';
        $mail->msgHTML('TEST');
        $mail->isHTML(true);

        if ($this->empresa->mail_connect($mail)) {
            /// OK
            return;
        }

        $this->new_error_msg('No se ha podido conectar por email. ¿La contraseña es correcta?');
        if ($mail->Host == 'smtp.gmail.com') {
            $this->new_error_msg('Aunque la contraseña de gmail sea correcta, en ciertas ' . 'situaciones los servidores de gmail bloquean la conexión. ' . 'Para superar esta situación debes crear y usar una ' . '<a href="https://support.google.com/accounts/answer/185833?hl=es" ' . 'target="_blank">contraseña de aplicación</a>');
            return;
        }
        // $this->new_error_msg("¿<a href='" . constant('FS_COMMUNITY_URL') . "/contacto' target='_blank'>Necesitas ayuda</a>?");
    }
}
