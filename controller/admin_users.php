<?php

/*
 * This file is part of MiFactura.eu
 * Copyright (C) 2021 X-Net Software Solutions S.L. <xnetsoftware@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Controlador de admin -> users.
 */
class admin_users extends fs_controller
{
    /**
     * Objeto agente.
     *
     * @var agente
     */
    public $agente;

    /**
     * Lista de registros fs_log.
     *
     * @var fs_log[]
     */
    public $historial;

    /**
     * Objeto fs_rol.
     *
     * @var fs_rol
     */
    public $rol;

    /**
     * admin_users constructor.
     */
    public function __construct()
    {
        parent::__construct(
            'Creación y asignación de roles a usuarios',
            'Usuarios', ['admin', 'dashboard'], true, true
        );
    }

    /**
     * Devuelve todas las páginas a las que tiene acceso.
     *
     * @return array
     */
    public function all_pages()
    {
        $returnlist = [];

        /// Obtenemos la lista de páginas. Todas
        foreach ($this->menu as $m) {
            $m->enabled = false;
            $m->allow_delete = false;
            $m->users = [];
            if ($m->name == 'dashboard') {
                $m->enabled = true;
                $m->allow_delete = true;
            }
            $returnlist[] = $m;
        }

        $users = $this->user->all();
        /// colocamos a los administradores primero
        usort($users, function ($a, $b) {
            if ($a->admin) {
                return -1;
            } elseif ($b->admin) {
                return 1;
            }

            return 0;
        });

        /// completamos con los permisos de los usuarios
        foreach ($users as $user) {
            if ($user->admin) {
                foreach ($returnlist as $i => $value) {
                    $returnlist[$i]->users[$user->nick] = [
                        'modify' => true,
                        'delete' => true,
                    ];
                }
            } else {
                foreach ($returnlist as $i => $value) {
                    $returnlist[$i]->users[$user->nick] = [
                        'modify' => false,
                        'delete' => false,
                    ];
                }

                foreach ($user->get_accesses() as $a) {
                    foreach ($returnlist as $i => $value) {
                        if ($a->fs_page == $value->name || $value->name === 'dashboard') {
                            $returnlist[$i]->users[$user->nick]['modify'] = true;
                            $returnlist[$i]->users[$user->nick]['delete'] = $a->allow_delete;
                            break;
                        }
                    }
                }
            }
        }

        /// ordenamos por nombre
        usort($returnlist, function ($a, $b) {
            return strcmp($a->name, $b->name);
        });

        return $returnlist;
    }

    /**
     * Código que se ejecutará en la parte privada
     */
    protected function private_core()
    {
        $this->agente = new agente();
        $this->rol = new fs_rol();

        if (filter_input(INPUT_POST, 'nnick')) {
            $this->add_user();
        } elseif (filter_input(INPUT_GET, 'delete')) {
            $this->delete_user();
        } elseif (filter_input(INPUT_POST, 'nrol')) {
            $this->add_rol();
        } elseif (filter_input(INPUT_GET, 'delete_rol')) {
            $this->delete_rol();
        }

        /// cargamos el historial
        $fslog = new fs_log();
        $this->historial = $fslog->all_by('login');
    }

    /**
     * Se encarga de añadir un nuevo usuario con los datos del formulario.
     */
    private function add_user()
    {
        $nu = $this->user->get(filter_input(INPUT_POST, 'nnick'));
        if ($nu) {
            $this->new_error_msg('El usuario <a href="' . $nu->url() . '">ya existe</a>.');
        } elseif (!$this->user->admin) {
            $this->new_error_msg('Solamente un administrador puede crear usuarios.', 'login', true, true);
        } else {
            $nu = new fs_user();
            $nu->nick = filter_input(INPUT_POST, 'nnick');
            $nu->email = mb_strtolower(filter_input(INPUT_POST, 'nemail'));

            if ($nu->set_password(filter_input(INPUT_POST, 'npassword'))) {
                $nu->admin = (bool) filter_input(INPUT_POST, 'nadmin');
                if (filter_input(INPUT_POST, 'ncodagente') && filter_input(INPUT_POST, 'ncodagente') != '') {
                    $nu->codagente = filter_input(INPUT_POST, 'ncodagente');
                }

                if ($nu->save()) {
                    $this->new_message('Usuario ' . $nu->nick . ' creado correctamente.', true, 'login', true);

                    /// para cada página, comprobamos si hay que darle acceso o no
                    foreach ($this->all_pages() as $p) {
                        $a = new fs_access([
                            'fs_user' => $nu->nick,
                            'fs_page' => $p->name,
                            'allow_delete' => false,
                        ]);
                        if ($p->enabled) {
                            /// la página ha sido marcada como autorizada.
                            if (!$a->save()) {
                                $this->new_error_msg('No se ha podido dar acceso a la página ' . $p->name . ' al usuario ' . $nu->nick);
                            }

                            /// si no hay una página de inicio para el usuario, usamos esta
                            if (is_null($nu->fs_page)) {
                                $nu->fs_page = 'dashboard';
                                $nu->save();
                            }
                        }
                    }

                    /// algún rol marcado
                    if (!$nu->admin && filter_input(INPUT_POST, 'roles', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY)) {
                        foreach (filter_input(INPUT_POST, 'roles', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY) as $codrol) {
                            $rol = $this->rol->get($codrol);
                            if ($rol) {
                                $fru = new fs_rol_user();
                                $fru->codrol = $codrol;
                                $fru->fs_user = $nu->nick;

                                if ($fru->save()) {
                                    foreach ($rol->get_accesses() as $p) {
                                        $a = new fs_access();
                                        $a->fs_page = $p->fs_page;
                                        $a->fs_user = $nu->nick;
                                        $a->allow_delete = $p->allow_delete;
                                        $a->save();
                                    }
                                }
                            }
                        }
                    }

                    header('Location: index.php?page=admin_user&snick=' . $nu->nick);
                } else {
                    $this->new_error_msg("¡No ha sido posible guardar el usuario!");
                }
            }
        }
    }

    /**
     * Se encarga de eliminar el usuario indicado.
     */
    private function delete_user()
    {
        $nu = $this->user->get(filter_input(INPUT_GET, 'delete'));
        if ($nu) {
            if (FS_DEMO) {
                $this->new_error_msg('En el modo <b>demo</b> no se pueden eliminar usuarios.
               Esto es así para evitar malas prácticas entre usuarios que prueban la demo.');
            } elseif (!$this->user->admin) {
                $this->new_error_msg("Solamente un administrador puede eliminar usuarios.", 'login', true);
            } elseif ($nu->delete()) {
                $this->new_message("Usuario " . $nu->nick . " eliminado correctamente.", true, 'login', true);
            } else {
                $this->new_error_msg("¡Imposible eliminar al usuario!");
            }
        } else {
            $this->new_error_msg("¡Usuario no encontrado!");
        }
    }

    /**
     * Se encarga de añadir un nuevo rol.
     */
    private function add_rol()
    {
        $this->rol->codrol = filter_input(INPUT_POST, 'nrol');
        $this->rol->descripcion = filter_input(INPUT_POST, 'descripcion');

        if ($this->rol->save()) {
            $this->new_message('Datos guardados correctamente.');
            header('Location: ' . $this->rol->url());
        } else {
            $this->new_error_msg('Error al crear el rol.');
        }
    }

    /**
     * Se encarga de eliminar el rol indicado.
     */
    private function delete_rol()
    {
        $rol = $this->rol->get(filter_input(INPUT_GET, 'delete_rol'));
        if ($rol) {
            if ($rol->delete()) {
                $this->new_message('Rol ' . $rol->codrol . ' eliminado correctamente.');
            } else {
                $this->new_error_msg('No se ha podido eliminar el rol #' . $rol->codrol);
            }
        } else {
            $this->new_error_msg('Rol no encontrado.');
        }
    }

    /**
     * Devuelve el icono asociado al controlador
     *
     * @author  Daniel M. Hernández Vieira  <daniel.hernandez@x-netdigital.com>
     * @version 2022.0503
     *
     * @return string
     */
    public function get_icon_controller()
    {
        return '<i class="fa-solid fa-people-line fa-fw"></i>';
    }
}
