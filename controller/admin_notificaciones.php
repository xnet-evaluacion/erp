<?php

/*
 * This file is part of MiFactura.eu
 * Copyright (C) 2021 X-Net Software Solutions S.L. <xnetsoftware@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Class admin_notificaciones
 *
 * @author  Benis Carrillo <benis.carrillo@x-netdigital.com>
 * @version 2022.04
 */
class admin_notificaciones extends fs_list_controller
{
    /**
     * admin_notificaciones constructor.
     *
     * @author  Benis Carrillo <benis.carrillo@x-netdigital.com>
     * @version 2022.04
     */
    public function __construct()
    {
        parent::__construct(
            'Gestionar las notificaciones a empleados.',
            'Notificaciones', ['admin', 'company'], true, true
        );
    }


    protected function create_tabs()
    {
        $this->create_tabs_candidatos();
        $this->create_tabs_tipo_notificaciones();
    }

    /**
     * Crea la pestaña del controlador
     *
     * @author  Benis Carrillo <benis.carrillo@x-netdigital.com>
     * @version 2022.04
     */
    protected function create_tabs_candidatos($tab_name = 'notificacion_items')
    {
        $notificacion_tipo = new notificacion_tipo();
        $agente = new agente();
        $tipo_item = [];
        $agente_item = [];
        foreach ((new notificacion())->all() as $itemnotif) {
            $arraTipo = $notificacion_tipo->get($itemnotif->idtipo_notificacion);
            $valor_tipo = $arraTipo->nombre;

            $arrAgent = $agente->get($itemnotif->codagente_destino);
            $valor_agente = $arrAgent->nombre . ' ' . $arrAgent->apellidos;

            $tipo_item[$itemnotif->idtipo_notificacion] = $valor_tipo;
            $agente_item[$itemnotif->codagente_destino] = $valor_agente;
        }

        $iconos = get_free_fa_icons();

        $this->add_tab($tab_name, 'Notificaciones', 'notificaciones');
        $this->add_search_columns($tab_name, [
            'id',
            'codagente_destino',
            'mensaje',

        ]);

        $this->add_filter_checkbox($tab_name, 'f_lectura', 'Leido', '!=', true);
        $this->add_filter_select($tab_name, 'codagente_destino', 'Agente Destino', $agente_item);
        $this->add_filter_select($tab_name, 'idtipo_notificacion', 'Tipo ', $tipo_item);

        $this->add_sort_option($tab_name, ['id'], 1);
        $this->add_sort_option($tab_name, ['codagente_destino']);
        $this->add_sort_option($tab_name, ['idtipo_notificacion']);
        $this->add_sort_option($tab_name, ['mensaje']);
        $this->add_sort_option($tab_name, ['f_emision']);

        /// Columnas
        $this->decoration->add_column($tab_name, 'id', 'string', '#', '', 'index.php?page=admin_notificacion&action=edit-detalle&id=');
        $this->decoration->add_column($tab_name, 'codagente_destino', 'array', 'Agente', $agente_item);
        $this->decoration->add_column($tab_name, 'idtipo_notificacion', 'array', 'Tipo', $tipo_item);
        $this->decoration->add_column($tab_name, 'mensaje', '', 'Mensaje');
        $this->decoration->add_column($tab_name, 'icono', 'i', 'Icono', $iconos);
        $this->decoration->add_column($tab_name, 'f_emision', 'date', 'Fecha', 'text-end');

        // $this->decoration->add_row_url($tab_name, 'index.php?page=edit_pdd_item&code=', 'id');

        $this->add_button($tab_name, 'Añadir', 'index.php?page=edit_notificacion&id=', 'fa-solid fa-plus fa-fw', 'btn-success flex-grow-1 flex-sm-grow-0');
    }

    /**
     * Crea las pestañas del controlador
     */
    protected function create_tabs_tipo_notificaciones($tab_name = 'tipos_notificaciones')
    {
        $this->add_tab($tab_name, 'Tipos de notificaciones', 'notificaciones_tipo');
        $this->add_search_columns($tab_name, ['nombre', 'estilo', 'icono',]);
        $this->add_sort_option($tab_name, ['id']);
        $this->add_sort_option($tab_name, ['nombre'], 1);
        $this->add_sort_option($tab_name, ['estilo']);

        $iconos = get_free_fa_icons();

        /// columnas a mostrar
        $this->decoration->add_column($tab_name, 'id', 'integer', '#', '', 'index.php?page=edit_notificacion_tipo&code=');
        $this->decoration->add_column($tab_name, 'nombre', 'string', 'Nombre');
        $this->decoration->add_column($tab_name, 'estilo', 'string', 'Estilo');
        $this->decoration->add_column($tab_name, 'icono', 'i', 'Icono', $iconos);

        /// click
        $this->decoration->add_row_url($tab_name, 'index.php?page=edit_notificacion_tipo&code=', 'id');

        /// botones
        $this->add_button($tab_name, 'Nuevo', 'index.php?page=edit_notificacion_tipo', 'fa-solid fa-plus fa-fw', 'btn-success');
    }


    /**
     * Devuelve el icono asociado al controlador
     *
     * @author  Daniel M. Hernández Vieira  <daniel.hernandez@x-netdigital.com>
     * @version 2022.0503
     *
     * @return string
     */
    public function get_icon_controller()
    {
        return '<i class="fa-solid fa-bell fa-fw"></i>';
    }
}
