<?php

/*
 * This file is part of facturacion_base for MiFactura.eu
 * Copyright (C) 2021 X-Net Software Solutions S.L. <xnetsoftware@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Description of notificaciones
 */
class admin_notificacion extends fs_controller
{
    /**
     * Objeto notificación.
     *
     * @var notificacion
     */
    public $notificaciones;

    /**
     * Objeto agente.
     *
     * @var agente
     */
    public $agente;

    /**
     * Lista de agente.
     *
     * @var agente[]
     */
    public $list_empleado;

    /**
     * Objeto notificaciones lista.
     *
     * @var notificacion[]
     */
    public $lista_notificaciones;

    /**
     * Objeto notificaciones tipos lista.
     *
     * @var notificacion_tipo[]
     */
    public $lista_notificaciones_tipo;

    /**
     * TODO: Missing documentation
     *
     * @var false|notificacion[]
     */
    public $resultados;

    /**
     * Notificación cargada.
     *
     * @var false|notificacion
     */
    public $notificacion;

    /**
     * TODO: Missing documentation
     *
     * @var array
     */
    public $tipos_notificaciones;

    /**
     * notificacion constructor.
     */
    public function __construct()
    {
        parent::__construct(
            'Gestionar una notificación a un empleado.',
            'Notificación', ['admin', 'company'], false, false
        );
        $this->show_background = false;
    }

    /**
     * TODO: Missing documentation
     *
     * @author  Benis (rellena tus datos en File -> Settings -> Editor -> File and Code Templates)
     * @version 2022.0406
     *
     * @param $idtipo_notificacion
     * @param $mensaje
     * @param $enlace
     * @param $f_emision
     * @param $f_lectura
     *
     * @param $codagente
     * @param $codagente_destino
     */
    public function save($codagente, $codagente_destino, $idtipo_notificacion, $mensaje, $enlace, $f_emision, $f_lectura)
    {
        $this->notificaciones->codagente = $codagente;
        $this->notificaciones->codagente_destino = $codagente_destino;
        $this->notificaciones->idtipo_notificacion = $idtipo_notificacion;
        $this->notificaciones->mensaje = $mensaje;
        $this->notificaciones->enlace = $enlace;
        $this->notificaciones->f_emision = $f_emision;
        $this->notificaciones->f_lectura = !empty($f_lectura) ? $f_lectura : null;

        $this->notificaciones->save();
    }

    /**
     * Código que se ejecutará en la parte privada
     */
    protected function private_core()
    {
        $this->tipos_notificaciones = [];
        foreach ((new notificacion_tipo())->all() as $value) {
            $this->tipos_notificaciones[$value->id] = [
                'icono' => $value->icono,
                'estilo' => $value->estilo,
            ];
        }

        $this->notificaciones = new notificacion();
        $this->notificaciones_tipo = new notificacion_tipo();
        $this->notificaciones_tipo_m = new notificacion_tipo();
        $this->agente = new agente();

        $action = fs_filter_input_req('action');

        switch ($action) {
            case 'totalNotificaciones':
                $this->header_notificaciones_total(fs_filter_input_req('codagente'));
                break;
            case 'arrayNotificaciones':
                $this->header_notificaciones('', fs_filter_input_req('codagente'));
                break;
            case 'guardar_notificacion':
                $this->notificaciones->codagente = $this->user->codagente;
                $this->notificaciones->codagente_destino = filter_input(INPUT_POST, 'vcodagente_destino');
                $this->notificaciones->idtipo_notificacion = filter_input(INPUT_POST, 'vidtipo_notificacion');;
                $this->notificaciones->f_lectura = null;
                $this->notificaciones->mensaje = filter_input(INPUT_POST, 'vmensaje');
                $this->notificaciones->enlace = filter_input(INPUT_POST, 'venlace');
                $this->notificaciones->f_aviso = filter_input(INPUT_POST, 'f_aviso');
                $this->notificaciones->save();
                break;
            case 'enlace':
                $id = filter_input(INPUT_GET, 'id');
                $this->notificaciones->actualizarLectura($id);
                $this->notificacion = $this->notificaciones->get($id);
                if (!empty($this->notificacion->enlace)) {
                    header('Location: ' . $this->notificacion->enlace);
                } else {
                    $this->new_error_msg('La notificación #' . $id . ' no tiene enlace.', 'error', true);
                }
                break;
            case 'actualizar_leido':
                $id = filter_input(INPUT_POST, 'id');
                $sw = filter_input(INPUT_POST, 'f_lectura');
                if ($sw != 'leido') {
                    $sw = 'No';
                }
                $this->notificaciones->actualizarEstatusLeido($id, $sw);
                header('Location: ' . $this->url() . "&page=admin_notificaciones");
                break;

            case 'edit-detalle':
                $this->template = 'notificacion_detalle';
                $id = filter_input(INPUT_GET, 'id');
                $this->notificacion = $this->notificaciones->get($id);

                if (!$this->notificacion) {
                    $this->new_error_msg('El registro #' . $id . ' no existe.', 'error', true);
                }
                break;

            case 'eliminar':
                $id = filter_input(INPUT_GET, 'id');
                $this->notificaciones = $this->notificaciones->get($id);
                // var_dump($this->notificaciones);
                $resp = $this->notificaciones->delete();
                // die('--> '.$resp);
                header('Location: ' . $this->url() . "&page=admin_notificaciones");
                break;

            case 'detalle':
            default:
                $this->template = 'notificacion_detalle';
                $id = filter_input(INPUT_GET, 'id');
                if (!$id) {
                    $this->new_error_msg("¡Notificación no encontrada!", 'error', false, false);
                }
                $this->notificacion = $this->notificaciones->get($id);
                $this->notificaciones_tipo = $this->notificaciones_tipo_m->getById($this->notificacion->idtipo_notificacion);
                break;
        }

        $this->lista_notificaciones = $this->arrayNotificaciones('', '', '');
        $this->list_empleado = $this->agente->all_full();
        $this->lista_notificaciones_tipo = $this->notificaciones_tipo_m->all();
    }

    /**
     * TODO: Missing documentation
     *
     * @author  Benis (rellena tus datos en File -> Settings -> Editor -> File and Code Templates)
     * @version 2022.0406
     *
     * @param $codagente
     * @param $codagente_destino
     *
     * @throws Exception
     */
    public function header_notificaciones($codagente, $codagente_destino = null)
    {
        $this->template = false;
        $resultados = [
            'notificaciones' => $this->arrayNotificaciones($codagente, $codagente_destino),
            'tipos_notificaciones' => $this->tipos_notificaciones,
        ];
        json_response($resultados);
    }

    /**
     * TODO: Missing documentation
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2022.0902
     *
     * @param $codagente
     */
    public function header_notificaciones_total($codagente)
    {
        $this->template = false;
        $resultados = [
            'codagente' => $codagente,
            'total' => $this->notificaciones->get_total_unreaded($codagente, true),
        ];
        json_response($resultados);
    }

    /**
     * TODO: Missing documentation
     *
     * @author  Benis (rellena tus datos en File -> Settings -> Editor -> File and Code Templates)
     * @version 2022.0406
     *
     * @param $tipo_notificacion
     *
     * @param $codagente
     * @param $codagente_destino
     *
     * @return array|false|notificacion[]
     */
    public function arrayNotificaciones($codagente, $codagente_destino, $tipo_notificacion = null)
    {
        $ArrAgentes = [];
        $ArrNotificaciones = $this->notificaciones->get_by_id_sel($codagente, $codagente_destino, $tipo_notificacion, null, null, 'id DESC', '');
        foreach ($ArrNotificaciones as $pos => $item) {
            if (!in_array($item->codagente, $ArrAgentes)) {
                $ArrAgentes[$item->codagente] = (new agente())->get($item->codagente);
            }
            $ArrNotificaciones[$pos]->nombempleado = $ArrAgentes[$item->codagente] ? $ArrAgentes[$item->codagente]->get_fullname() : 'Agente no encontrado';
            $ArrNotificaciones[$pos]->tiempo_transcurrido = $item->get_tiempo_transcurrido();
        }
        return $ArrNotificaciones;
    }

    /**
     * Devuelve el icono asociado al controlador
     *
     * @author  Daniel M. Hernández Vieira  <daniel.hernandez@x-netdigital.com>
     * @version 2022.0503
     *
     * @return string
     */
    public function get_icon_controller()
    {
        return '<i class="fa-solid fa-bell fa-fw"></i>';
    }
}
