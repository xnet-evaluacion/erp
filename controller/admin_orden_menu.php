<?php

/*
 * This file is part of MiFactura.eu
 * Copyright (C) 2021 X-Net Software Solutions S.L. <xnetsoftware@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Description Ordenar menú
 *
 * @author alagoro
 */
class admin_orden_menu extends fs_controller
{
    /**
     * admin_orden_menu constructor.
     */
    public function __construct()
    {
        parent::__construct(
            'Reordenar las opciones del menú.',
            'Ordenar menú', ['admin', 'company'], true, true
        );
    }

    /**
     * Código que se ejecutará en la parte privada
     */
    protected function private_core()
    {
        /**
         * Si se va a subir a producción, subir con $multinivel = false hasta que esté testado.
         *
         * Una vez probado, eliminar $multinivel y el template admin_orden_menu_old
         */
        $multinivel = true;

        if ($multinivel) {
            if (filter_input(INPUT_POST, 'guardar')) {
                $this->guardar_orden();
            }
        } else {
            if (filter_input(INPUT_POST, 'guardar')) {
                $this->guardar_orden_old();
            }
            $this->template = 'admin_orden_menu_old';
        }
    }

    /**
     * Guarda la orden usando el menú multi-nivel
     *
     * @return void
     */
    private function guardar_orden()
    {
        $order = 1;
        foreach ($_POST as $controller => $name) {
            $page = $this->page->get($controller);
            if ($page === false) { // Será otra variable POST, como action...
                continue;
            }
            $page->orden = $order;
            if ($page->save()) {
                $order++;
            }
        }

        $this->new_message('Datos guardados.');
        //$this->array_menu = $this->user->get_array_menu(true);
        $this->array_menu = $this->getArrayMenu(true);
    }

    /**
     * Guarda el nuevo orden indicado.
     */
    private function guardar_orden_old()
    {
        foreach ($this->folders() as $folder) {
            $orden = 0;
            foreach (filter_input_array(INPUT_POST) as $key => $value) {
                if (strlen($key) > $folder) {
                    if (substr($key, 0, strlen($folder)) == $folder) {
                        $page = $this->page->get($value);
                        $page->orden = $orden;
                        if ($page->save()) {
                            $orden++;
                        }
                    }
                }
            }
        }

        $this->new_message('Datos guardados.');
        $this->menu = $this->user->get_menu(true);
    }

    /**
     * Devuelve el icono asociado al controlador
     *
     * @return string
     * @version 2022.0503
     *
     * @author  Daniel M. Hernández Vieira  <daniel.hernandez@x-netdigital.com>
     */
    public function get_icon_controller()
    {
        return '<i class="fa-solid fa-arrow-down-short-wide fa-fw"></i>';
    }
}
