<?php

/**
 * Class edit_notificacion
 */
class edit_notificacion extends fs_edit_controller
{

    /**
     * edit_notificacion constructor.
     */
    public function __construct()
    {
        parent::__construct(
            'Edición de una notificación',
            'Notificación', ['admin', 'company'], false, false
        );
    }

    /**
     * Devuelve el nombre de la clase model relacionada.
     *
     * @return string
     */
    public function get_model_class_name()
    {
        return 'notificacion';
    }

    /**
     * Asigna las columnas de edición.
     */
    protected function set_edit_columns()
    {
        $types_item = [];
        foreach ((new notificacion_tipo())->all() as $tipo) {
            $types_item[$tipo->id] = $tipo->nombre;
        }

        $agente_item = [];
        foreach ((new agente())->all() as $item_agente) {
            $agente_item[$item_agente->codagente] = $item_agente->nombre . ' ' . $item_agente->apellidos;
        }

        $agente_emisor = [
            $this->user->codagente => $agente_item[$this->user->codagente],
        ];
        if ($this->user->admin) {
            $agente_emisor = $agente_item;
        }

        $this->form->add_column_select('idtipo_notificacion', $types_item, 'Tipos', 4, true);
        $this->form->add_column_select('codagente', $agente_emisor, 'Agente Emisor', 4, true);
        $this->form->add_column_select('codagente_destino', $agente_item, 'Agente Destino', 4, true);
        $this->form->add_column('mensaje', 'textarea', 'Mensaje', 4, true);
        $this->form->add_column('enlace', 'text', 'Enlace', 4, true);
        $this->form->add_column('f_aviso', 'datetime-local', 'Fecha Aviso', 4, false);
    }
}
