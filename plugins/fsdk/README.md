# Plugin FSDK

__FSDK__ es un plugin que proporciona ayudas en el desarrollo y testeo de plugins:

* Generación de manera automática los archivos de estructura de datos, modelo y controlador a partir de una tabla.
* Generación de plugin 'Hola mundo' como ejemplo
* Generación de datos aleatorios de las tablas principales para pruebas
