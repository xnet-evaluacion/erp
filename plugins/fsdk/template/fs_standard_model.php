<?php

/*
 * This file is part of fsdk for MiFactura.eu
 * Copyright (C) 2021 X-Net Software Solutions S.L. <xnetsoftware@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Description of fs_standard_model
 */
abstract class fs_standard_model extends fs_model
{
    /**
     * TODO: Missing documentation
     *
     * @var mixed
     */
    protected $key_fields;

    /**
     * TODO: Missing documentation
     *
     * @var array
     */
    protected $required_fields;

    /**
     * fs_standard_model constructor.
     *
     * @param string $name
     */
    public function __construct($name = '')
    {
        parent::__construct($name);

        $this->fields_key = [];
        $this->required_fields = [];
    }

    /**
     * TODO: Missing documentation
     *
     * @param string $name
     *
     * @return mixed
     */
    public function __get($name)
    {
        return $this->$name;
    }

    /**
     * TODO: Missing documentation
     *
     * @param string $name
     * @param mixed  $value
     */
    public function __set($name, $value)
    {
        $this->$name = $value;
    }

    /**
     * TODO: Missing documentation
     *
     * @return false
     */
    public function save()
    {
        if ($this->test()) {
            $this->clean_cache();

            if ($this->exists()) {
                return $this->update();
            } else {
                return $this->insert();
            }
        } else {
            return false;
        }
    }

    /**
     * TODO: Missing documentation
     *
     * @return bool
     */
    protected function test()
    {
        return $this->test_requiredfields();
    }

    /**
     * TODO: Missing documentation
     *
     * @return bool
     */
    private function test_requiredfields()
    {
        $result = $this->test_keyfields();
        if ($result) {
            foreach ($this->required_fields as $field) {
                if (empty($this->$field)) {
                    $result = false;
                    break;
                }
            }
        }

        return $result;
    }

    /**
     * TODO: Missing documentation
     *
     * @return bool
     */
    private function test_keyfields()
    {
        $result = true;
        foreach ($this->key_fields as $key_field) {
            if (empty($this->$key_field)) {
                $result = false;
                break;
            }
        }

        return $result;
    }

    /**
     * Esta función devuelve TRUE si los datos del objeto se encuentran
     * en la base de datos.
     *
     * @return bool
     */
    public function exists()
    {
        $result = false;
        if ($this->test_keyfields()) {
            $sql = 'SELECT 1 FROM ' . $this->table_name() . ' WHERE 1 = 1';
            foreach ($this->key_fields as $key_field) {
                $sql .= ' AND ' . $key_field . ' = ' . $this->var2str($this->$key_field);
            }
            $result = $this->db->select($sql . ' LIMIT 1');
        }
        return $result;
    }

    /**
     * TODO: Missing documentation
     *
     * @return mixed
     */
    abstract protected function update();

    /**
     * TODO: Missing documentation
     *
     * @return mixed
     */
    abstract protected function insert();

    /**
     * Elimina el registro de la tabla, devuelve el resultado de la consulta.
     *
     * @return bool
     */
    public function delete()
    {
        if (!empty($this->key_fields)) {
            $sql = 'DELETE FROM ' . $this->table_name() . ' WHERE ';
            foreach ($this->key_fields as $key_field) {
                $sql .= $key_field . ' = ' . $this->var2str($this->$key_field);
            }

            $this->db->exec($sql);
        }
    }

    /**
     * TODO: Missing documentation
     *
     * @param array $data
     *
     * @return mixed
     */
    abstract public function load_from_data($data);

    /**
     * TODO: Missing documentation
     *
     * @return mixed
     */
    abstract public function clear();

    /**
     * TODO: Missing documentation
     *
     * @param string $fieldname
     *
     * @return int
     */
    protected function add_keyfield($fieldname)
    {
        return array_push($this->key_fields, $fieldname);
    }

    /**
     * TODO: Missing documentation
     *
     * @param string $fieldname
     *
     * @return int
     */
    protected function add_requiredfield($fieldname)
    {
        return array_push($this->required_fields, $fieldname);
    }
}