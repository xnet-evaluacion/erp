# Registro de cambios en el plugin.

- **[2022-06-15] v2022.0615:**
  - Primera versión subida a la comunidad beta usando el motor de plantillas BLADE y SKOTE.
- **[2021-07-17] v2021.1980:**
  - Eliminados los archivos excluidos en .gitignore de los propios repositorios
  - Añadidos algunos composer.json que faltaban
  - Movidos algunos falsos vendor a extras
  - Subida versión mínima de PHP de 5.6 a 7.0
  - Añadidos/corregidos bloques PHP faltantes
  - Código reformateado
  - Código reordenado
  - Añadidos .gitignore
- **[2021-04-27] v2021.1170:**
  - Se añade soporte para comprobar dependencias entre tablas, no solo al momento de crearlo por primera vez, sinó también al actualizar.
  - Se cambia la visibilidad del método install de public a protected
  - Corregida documentación para método exists de los modelos
  - Formateados los archivos modificados
