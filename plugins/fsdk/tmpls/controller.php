<?php

/**
 * Description of holamundo
 */
class holamundo extends fs_controller
{
    /**
     * TODO: Missing documentation
     *
     * @var null|string
     */
    public $texto;

    /**
     * TODO: Missing documentation
     *
     * @var null|string
     */
    public $texto2;

    /**
     * TODO: Missing documentation
     *
     * @var array
     */
    public $lista;

    /**
     * TODO: Missing documentation
     *
     * @var string
     */
    public $resultados_sql;

    /**
     * holamundo constructor.
     */
    public function __construct()
    {
        parent::__construct(__CLASS__, 'Hola mundo', ['DEV']);
    }

    /**
     * Código que se ejecutará en la parte privada
     */
    protected function private_core()
    {
        $this->texto = 'hola mundo';
        $this->texto2 = 'Bla, bla, bla, bla, bla, bla, bla, bla, bla, bla, bla, bla.';
        $this->lista = ['peras', 'manzanas', 'puerros', 'naranjas'];

        $this->resultados_sql = $this->db->select("SELECT *"
            . " FROM `paises`;");
    }
}
