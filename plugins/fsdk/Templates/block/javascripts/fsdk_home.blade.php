@if ($fsc->url_recarga)
    <script type="text/javascript">
        function recargar() {
            window.location.href = '{!! $fsc->url_recarga !!}';
        }

        $(document).ready(function () {
            setTimeout(recargar, 5000);
        });
    </script>
@endif