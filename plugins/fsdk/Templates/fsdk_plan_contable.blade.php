@extends('layouts/main')

@section('main-content')
    @include('master/template_header')

    @if ($fsc->ejercicio)
        <div class="container-fluid mt-3">
            <div class="row">
                <div class="col-12">
                    <div class="card border">
                        <div class="card-header mt-2 text-truncate">
                            <i class="fa-solid fa-balance-scale fa-fw"></i>
                            CSV =&gt; Plan contable
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12 text-justify">
                                    <p class="form-text">
                                        Permite generar un plan contable a partir de un CSV o
                                        <b>excel</b> con el listado
                                        de cuentas y sus descripciones.
                                    </p>
                                </div>
                            </div>
                            <form class="form" action="{!! $fsc->url() !!}#plugins-tab" enctype="multipart/form-data" method="post">
                                <input type="hidden" name="csv" value="TRUE"/>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="mb-2">
                                            <input class="form-control" type="file" name="fcsv" accept=".csv"/>
                                            <p class="form-text">
                                                El archivo debe tener las columnas <b>CODIGO</b> y <b>NOMBRE</b>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="mb-2">
                                            <label>
                                                Separador de columnas
                                            </label>
                                            <input type="text" name="separador" value="{!! $fsc->separador !!}" maxlength="1" class="form-control" autocomplete="off"/>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="mb-2">
                                            <label>
                                                <a href="{!! $fsc->ejercicio->url() !!}" class="link-primary">Ejercicio</a>
                                            </label>
                                            <select name="codejercicio" class="form-select select2">
                                                <option data-comment="Placeholder"></option>
                                                @foreach ($fsc->ejercicio->all() as $key1 => $value1)
                                                    @if ($value1->codejercicio==$fsc->codejercicio)
                                                        <option value="{!! $value1->codejercicio !!}" selected="">{!! $value1->nombre !!}</option>
                                                    @else
                                                        <option value="{!! $value1->codejercicio !!}">{!! $value1->nombre !!}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <br>
                                        <button type="submit" class="btn btn-primary flex-grow-1 flex-sm-grow-0" onclick="this.disabled=true;this.form.submit();">
                                            <i class="fa-solid fa-file-import fa-fw"></i>
                                            <span>Procesar</span>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection
