@extends('layouts/main')

@section('main-content')
    @include('master/template_header')

    @if ($fsc->tabla)
        <div class="container-fluid mt-3">
            <div class="row">
                <div class="col-12">
                    <div class="card border">
                        <div class="card-header mt-2 text-truncate">
                            <i class="fa-solid fa-database fa-fw"></i>
                            {!! $fsc->tabla !!}
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12 text-justify">
                                    <a href="index.php?page=fsdk_home" class="d-flex justify-content-center align-items-center btn btn-outline-secondary">
                                        <i class="fa-solid fa-arrow-left fa-fw"></i>
                                        <span>Atrás</span>
                                    </a>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 mt-3">
                                    <ul class="nav nav-tabs nav-tabs-custom nav-justified" role="tablist">
                                        <li class="nav-item" role="presentation">
                                            <a href="#xml" aria-controls="xml" role="tab" data-bs-toggle="tab" class="nav-link d-flex flex-column align-items-center active">
                                                <i class="fa-regular fa-file-excel fa-fw"></i>
                                                <span>model/table/{!! $fsc->tabla !!}.xml</span>
                                            </a>
                                        </li>

                                        <li class="nav-item" role="presentation">
                                            <a href="#modelo" aria-controls="modelo" role="tab" data-bs-toggle="tab" class="nav-link d-flex flex-column align-items-center">
                                                <i class="fa-solid fa-file-code fa-fw"></i>
                                                <span>model/{!! $fsc->nombre_modelo !!}.php</span>
                                            </a>
                                        </li>

                                        <li class="nav-item" role="presentation">
                                            <a href="#controlador" aria-controls="controlador" role="tab" data-bs-toggle="tab" class="nav-link d-flex flex-column align-items-center">
                                                <i class="fa-solid fa-code fa-fw"></i>
                                                <span>controller/{!! $fsc->tabla !!}.php</span>
                                            </a>
                                        </li>
                                    </ul>
                                    <div class="tab-content nav-content mt-3">
                                        <div class="tab-pane fade show active" id="xml" role="tabpanel" aria-labelledby="xml-tab">
                                            <div class="row">
                                                <div class="col-sm-10">
                                                    <p class="form-text">
                                                        Este archivo le dice a {!! $fsc->reseller_data->name !!} cómo es
                                                        la estructura
                                                        de esta tabla, para que el modelo correspondiente la genere
                                                        o adapte si es necesario. Recuerda que debe estar en la carpeta
                                                        <b>model/table</b> del plugin.
                                                        <br>
                                                        <b>Aviso</b>: a veces la detección no es perfecta y aparece
                                                        <b>(...)</b>
                                                        en el archivo, tendrás que llenar esos huecos con las columnas
                                                        que correspondan.
                                                    </p>
                                                </div>
                                                <div class="col-sm-2 text-end">
                                                    <button class="d-flex justify-content-center align-items-center btn btn-outline-secondary" data-clipboard-target="#txt_xml">
                                                        <i class="fa-solid fa-copy fa-fw"></i>
                                                        <span>Copiar</span>
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-12">
                                                    <br>
                                                    <div class="mb-2">
                                                        <textarea id='txt_xml' class="form-control">{!! $fsc->xml !!}</textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="modelo" role="tabpanel" aria-labelledby="modelo-tab">
                                            <div class="row">
                                                <div class="col-sm-10">
                                                    <p class="form-text">
                                                        Esta clase sirve para leer/modificar/eliminar los datos de la
                                                        tabla de la base de datos. Recuerda ponerlo en el directorio
                                                        <b>model</b> del plugin.
                                                    </p>
                                                </div>
                                                <div class="col-sm-2 text-end">
                                                    <button class="d-flex justify-content-center align-items-center btn btn-outline-secondary" data-clipboard-target="#txt_modelo">
                                                        <i class="fa-solid fa-copy fa-fw"></i>
                                                        <span>Copiar</span>
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-12">
                                                    <br>
                                                    <div class="mb-2">
                                                        <textarea id='txt_modelo' class="form-control">{!! $fsc->modelo !!}</textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="controlador" role="tabpanel" aria-labelledby="controlador-tab">
                                            <div class="row">
                                                <div class="col-sm-10">
                                                    <p class="form-text">
                                                        Esta clase sirve para comunicarse entre el modelo y la vista.
                                                        Recuerda ponerlo en el directorio <b>controller</b> del plugin.
                                                    </p>
                                                </div>
                                                <div class="col-sm-2 text-end">
                                                    <button class="d-flex justify-content-center align-items-center btn btn-outline-secondary" data-clipboard-target="#txt_controlador">
                                                        <i class="fa-solid fa-copy fa-fw"></i>
                                                        <span>Copiar</span>
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-12">
                                                    <br>
                                                    <div class="mb-2">
                                                        <textarea id='txt_controlador' class="form-control">{!! $fsc->controlador !!}</textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection
