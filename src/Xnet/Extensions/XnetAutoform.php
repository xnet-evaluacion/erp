<?php
/*
 * This file is part of plugin xnetcommonhelpers for MiFactura.eu
 * Copyright (C) 2022 X-Net Software Solutions S.L. <xnetsoftware@gmail.com>
 */

namespace Xnet\Extensions;

/**
 * Trait XnetAutoform
 *
 * TODO: ¿Es necesario?
 * Inicialmente se pensó porque podría hacerse un EditController desde varios
 * tipos de controladores, pero ahora mismo no termino de verlo claro.
 *
 * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
 * @version 2022.0812
 *
 * @package Xnet\Extensions
 */
trait XnetAutoform
{
    /**
     * Contiene un array con los datos obtenidos de la consulta SQL.
     *
     * @var array
     */
    public $resultados;

    abstract public function getHeaders();
    abstract public function getFields();

    public function anterior_url()
    {
        return '';
    }

    public function siguiente_url()
    {
        return '';
    }

    public function show($col_name, $col_config, $model)
    {
        $html = '<div class="form-group">'
            . ($col_config['type'] == 'bool' ? '' : '<label>' . $col_config['label'] . '</label>');
        $required = $col_config['required'] ? ' required' : '';
        $readonly = $col_config['readonly'] ? ' readonly' : '';
        if (isset($col_config['extradata'] ['maxlength'])) {
            $maxlength = ' maxlength="' . $col_config['extradata'] ['maxlength'] . '"';
        } else {
            $maxlength = '';
        }

        if (isset($col_config['extradata'] ['inputid'])) {
            $inputid = ' id="' . $col_config['extradata'] ['inputid'] . '"';
        } else {
            $inputid = '';
        }

        $name = $col_name;
        switch ($col_config['type']) {
            case 'bool':
                $checked = $model->{$col_name} ? ' checked=""' : '';
                $html .= '<div class="checkbox"><input type="checkbox" name="' . $name . '" value="TRUE"' . $checked . $readonly . $inputid . '/> ' . $col_config['label'] . '</div>';
                break;

            case 'date':
                $html .= '<div class="input-group">'
                    . '    <span class="input-group-text">'
                    . '        <i class="fa-solid fa-calendar fa-fw"></i>'
                    . '    </span>'
                    . '    <input class="form-control" type="date" name="' . $name . '" value="' . $model->{$col_name} . '" autocomplete="off"' . $required . $readonly . $inputid . '/>'
                    . '</div>';
                break;

            case 'datetime-local':
                $datetime_local = '';
                if ($model->{$col_name}) {
                    $datetime_local = substr(date(DATE_ATOM, strtotime($model->{$col_name})), 0, 16);
                }
                $html .= '<div class="input-group">'
                    . '    <span class="input-group-text">'
                    . '        <i class="fa-solid fa-calendar fa-fw"></i>'
                    . '    </span>'
                    . '    <input class="form-control" type="datetime-local" name="' . $name . '" value="' . $datetime_local . '" autocomplete="off"' . $required . $readonly . $inputid . '/>'
                    . '</div>';
                break;

            case 'money':
            case 'number':
                if (isset($col_config['extradata'] ['min'])) {
                    $min = ' min="' . $col_config['extradata'] ['min'] . '"';
                } else {
                    $min = '';
                }
                if (isset($col_config['extradata'] ['max'])) {
                    $max = ' max="' . $col_config['extradata'] ['max'] . '"';
                } else {
                    $max = '';
                }
                if (isset($col_config['extradata'] ['step'])) {
                    $step = ' step="' . $col_config['extradata'] ['step'] . '"';
                } else {
                    $step = '';
                }
                $html .= '<input class="form-control" type="number" step="any" name="' . $name . '" value="' . $model->{$col_name} . '" autocomplete="off"' . $required . $readonly . $min . $max . $step . $inputid . '/>';
                break;

            case 'hidden':
                $html .= '<input class="form-control" type="hidden" name="' . $name . '" value="' . $model->{$col_name} . '" autocomplete="off"  ' . $inputid . '/>';
                break;

            case 'select':
                $html .= $this->show_select($name, $col_config, $model);
                break;

            case 'textarea':
                $html .= '<textarea class="form-control" name="' . $name . '"' . $required . $readonly . $inputid . '>' . $model->{$col_name} . '</textarea>';
                break;

            case 'autocomplete':
                $html .= '<input class="form-control" type="hidden" name="' . $col_name . '" value="' . $model->{$col_name} . '" autocomplete="off"  id="' . $col_config['extradata'] ['inputid'] . '" />';
                if (isset($col_config['extradata'] ['ac_noSuggestion'])) { //el mensaje que se mostrará si no se encuentran coincidencias
                    $noSuggestion = "noSuggestionNotice: \"" . $col_config['extradata'] ['ac_noSuggestion'] . "\",";
                } else {
                    $noSuggestion = '';
                }
                $html .= '<script type="text/javascript">
                    $( document ).ready(function() {
                        $( "#ac_' . $col_name . '" ).autocomplete({
                            serviceUrl: "' . $col_config['extradata'] ['ac_url'] . '", //la url con el ajax de respuesta para los posibles valores
                            dataType: "json",
                            minLength: 2,
                            deferRequestBy: 200,
                            paramName: "query",
                            showNoSuggestionNotice: true,
                            onSearchStart: function(params) {
                                $("#' . $col_config['extradata'] ['inputid'] . '").val("");
                            }, 
                            ' . $noSuggestion . '
                            onSelect: function(suggestion) {
                               $("#' . $col_config['extradata'] ['inputid'] . '").val(suggestion.id);
                            }
                        })

                ';
                $html .= '
                    $.get( "' . $col_config['extradata'] ['ac_url'] . '",{ "get": "' . $model->{$col_name} . '" })  //pedimos el texto que deberia tener el campo con ese valor para ponerlo en el input correspondiente
                        .done(function (data) {
                            $( "#ac_' . $col_name . '" ).val(data);
                        });
                ';
                $html .= '
                    });
                    </script>
                ';
                $name = "ac_" . $col_name; //si es de tipo autocomplete, el input mostrado se le pone otro nombre
                $inputid = ' id="ac_' . $col_config['extradata'] ['inputid'] . '"'; //y algo similar hacemos con el id
                break;
            case 'range':
                //Por defecto max=100, min=0, step = 1
                if (isset($col_config['extradata'] ['min'])) {
                    $min = ' min="' . $col_config['extradata'] ['min'] . '"';
                } else {
                    $min = '0';
                }
                if (isset($col_config['extradata'] ['max'])) {
                    $max = ' max="' . $col_config['extradata'] ['max'] . '"';
                } else {
                    $max = '100';
                }
                if (isset($col_config['extradata'] ['step'])) {
                    $step = ' step="' . $col_config['extradata'] ['step'] . '"';
                } else {
                    $step = '1';
                }
                $html .= '<input class="form-range" type="range" step="' . $step . '" name="' . $name . '" value="' . $model->{$col_name} . '" autocomplete="off"' . $required . $readonly . $min . $max . $step . $inputid . '/>';

                break;

            default:
                $restrictions = '';
                if (isset($col_config['extradata'])) {
                    foreach ($col_config['extradata'] as $key => $value) {
                        $restrictions .= ' ' . $key . '="' . $value . '"';
                    }
                }

                $onChange = ' onChange="modifiedField()"';

                $html .= '<input class="form-control" type="text" name="' . $name . '" value="' . $model->{$col_name} . '" autocomplete="off"' . $onChange . $required . $readonly . $maxlength . $inputid . $restrictions . ' />';
        }

        $html .= '</div>';

        return $html;
    }

}
