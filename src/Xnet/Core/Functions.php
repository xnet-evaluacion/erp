<?php

/**
 * Alxarafe. Development of PHP applications in a flash!
 * Copyright (C) 2018-2020 Alxarafe <info@alxarafe.com>
 */

use Xnet\Core\XnetConfig;
use Xnet\Core\XnetDebugBar;
use Xnet\Model\Version;

if (!defined('BASE_PATH')) {
    /**
     * Base path for the app.
     */
    define('BASE_PATH', realpath(__DIR__ . constant('DIRECTORY_SEPARATOR') . '..' . constant('DIRECTORY_SEPARATOR') . '..' . constant('DIRECTORY_SEPARATOR') . '..'));
}

/**
 * Muestra un mensaje de error en caso de error fatal, aunque php tenga
 * desactivados los errores.
 */
function fatal_handler()
{
    $error = error_get_last();
    if (isset($error) && in_array($error["type"], [1, 64,])) {
        if (function_exists('send_error')) {
            send_error($error);
        }
        echo "<h1>Error fatal</h1>"
            . "<ul>"
            . "    <li><b>Tipo:</b> " . $error["type"] . "</li>"
            . "    <li><b>Archivo:</b> " . $error["file"] . "</li>"
            . "    <li><b>Línea:</b> " . $error["line"] . "</li>"
            . "    <li><b>Mensaje:</b> " . $error["message"] . "</li>"
            . "</ul>";
    }
}

/**
 * Returns the app base path.
 *
 * @param string $path
 *
 * @return string
 */
function basePath($path = '')
{
    return realpath(constant('BASE_PATH')) . (empty($path) ? $path : constant('DIRECTORY_SEPARATOR') . trim($path, constant('DIRECTORY_SEPARATOR')));
}

/**
 * Redirects to path.
 *
 * @param string $path
 */
function redirect($path)
{
    header('Location: ' . $path);
}

/**
 * Returns the base url.
 *
 * @param string $url
 *
 * @return string
 */
function baseUrl($url = '')
{
    $defaultPort = defined('SERVER_PORT') ? constant('SERVER_PORT') : 80;
    $defaultHost = defined('SERVER_NAME') ? constant('SERVER_NAME') : 'localhost';
    $path = $_SERVER['PHP_SELF'];
    // For PHPUnit tests, SERVER PHP_SELF contains 'vendor/bin/phpunit'
    if (isset($_SERVER['argv'][0]) && $_SERVER['PHP_SELF'] === $_SERVER['argv'][0]) {
        $path = '';
    }
    $folder = str_replace(['/index.php', constant('APP_URI')], '', $path);
    $port = '';
    if (!in_array($defaultPort, ['80', '443'], false)) {
        $port = ':' . $defaultPort;
    }
    $baseUrl = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' ? 'https' : 'http') . '://' . $defaultHost . $port . constant('APP_URI') . $folder;
    return empty($url) ? $baseUrl : trim($baseUrl, '/') . '/' . trim($url, '/');
}

/**
 * Create a randomString
 *
 * @param int $length
 *
 * @return string
 */
function randomString(int $length)
{
    $random = '';
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    while (strlen($random) < $length) {
        try {
            $pos = random_int(0, strlen($characters) - 1);
        } catch (Exception $e) {
            $pos = mt_rand(0, strlen($characters) - 1);
        }
        $random .= $characters[$pos];
    }
    return str_shuffle($random);
}

/**
 * Delete a directory with all its content
 *
 * @param string $dir
 *
 * @return bool
 */
function delTree($dir)
{
    if (!is_dir($dir)) {
        return true;
    }

    $files = scandir($dir);
    if ($files === false) {
        return false;
    }
    $files = array_diff($files, ['.', '..']);
    foreach ($files as $file) {
        (is_dir("$dir/$file")) ? delTree("$dir/$file") : unlink("$dir/$file");
    }
    return rmdir($dir);
}

function find_controller_and_path($name)
{
    $controller = constant('BASE_PATH') . '/base/fs_controller.php';
    $path = null;
    $namespace = null;

    $found = false;
    foreach ($GLOBALS['plugins'] as $plugin) {
        // Se busca con el sistema nuevo
        if (file_exists(constant('FS_FOLDER') . '/plugins/' . $plugin . '/Controllers/' . $name . '.php')) {
            $controller = constant('BASE_PATH') . '/plugins/' . $plugin . '/Controllers/' . $name . '.php';
            $path = '/plugins/' . $plugin;
            $namespace = 'Plugins\\' . $plugin . '\\Controllers\\';
            $found = true;
            break;
        }

        // Se busca con el sistema antiguo, a extinguir
        if (file_exists_case_sensitive(constant('FS_FOLDER') . '/plugins/' . $plugin . '/controller/' . $name . '.php')) {
            $controller = constant('BASE_PATH') . '/plugins/' . $plugin . '/controller/' . $name . '.php';
            $path = '/plugins/' . $plugin;
            $found = true;
            break;
        }
    }

    // Se busca en el nuevo núcleo
    if (!$found && file_exists(constant('FS_FOLDER') . '/src/Xnet/Controllers/' . $name . '.php')) {
        $controller = constant('BASE_PATH') . '/src/Xnet/Controllers/' . $name . '.php';
        $namespace = 'Xnet\\Controllers\\';
        $path = '/src/Xnet';
        $found = true;
    }

    // Se busca en el núcleo viejo, a extinguir
    if (!$found && file_exists_case_sensitive(constant('FS_FOLDER') . '/controller/' . $name . '.php')) {
        $controller = constant('BASE_PATH') . '/controller/' . $name . '.php';
        $path = '/';
    }

    return [
        'controller' => $controller,
        'namespace' => $namespace,
        'path' => $path,
    ];
}

function debug_message($msg)
{
    if (constant('FS_DEBUG')) {
        XnetDebugBar::addMessage('messages', $msg, 'debug');
    }
}

/**
 * Retorna un StdClass con los datos de personalización de cada reseller
 * TODO: Externalizar de alguna manera.
 *
 * @return stdClass
 */
function getResellerData()
{
    $result = new stdClass();
    $name = $_SERVER['SERVER_NAME'];
    $result->url = 'https://mifactura.eu';
    $result->url_contacto = 'https://mifactura.eu/contacto';
    $result->telefono = '+34822048526';
    $result->locked_controllers = [];
    if (strpos($name, 'localhost') !== false) {
        $result->name = 'XFS.Cloud (localhost)';
        $result->email = 'soporte@mifactura.eu';
        $result->logo_45 = 'resellers/mifactura/img/logo-45.png';
        $result->logo_200 = 'resellers/mifactura/img/logo-200.png';
        $result->locked_controllers = [
            'compras_albaran',
            'compras_albaranes',
            'compras_factura',
            'compras_facturas',
            'ventas_factura',
            'ventas_facturas',
        ];
    } elseif (strpos($name, 'cloudnubelia.cloud') !== false) {
        $result->name = 'CloudNubelia';
        $result->email = 'info@cloudnubelia.com';
        $result->logo_45 = 'resellers/nubelia/img/logo-45.png';
        $result->logo_200 = 'resellers/nubelia/img/logo-200.png';
    } elseif (strpos($name, 'akiai.es') !== false) {
        $result->name = 'Akiai';
        $result->email = 'info@akiai.es';
        $result->logo_45 = 'resellers/akiai/img/logo-45.png';
        $result->logo_200 = 'resellers/akiai/img/logo-200.png';
    } else {
        $result->name = 'XFS.Cloud';
        $result->email = 'soporte@mifactura.eu';
        $result->logo_45 = 'resellers/mifactura/img/logo-45.png';
        $result->logo_200 = 'resellers/mifactura/img/logo-200.png';
        $result->locked_controllers = [
            'compras_albaran',
            'compras_albaranes',
            'compras_factura',
            'compras_facturas',
            'ventas_factura',
            'ventas_facturas',
        ];
    }
    return $result;
}

function view_exists($name)
{
    $status = false;
    foreach (XnetConfig::getEnabledPlugins() as $plugin) {
        if (file_exists(constant('BASE_PATH') . '/plugins/' . $plugin . '/Templates/' . $name . '.blade.php')) {
            $status = true;
        }
    }

    if (file_exists(constant('BASE_PATH') . '/Templates/' . $name . '.blade.php')) {
        $status = true;
    }

    return $status;
}

/**
 * Redondeo bancario
 *
 * @staticvar real $dFuzz
 *
 * @param float   $dVal
 * @param integer $iDec
 *
 * @return float
 */
function bround($dVal, $iDec = 2)
{
    // banker's style rounding or round-half-even
    // (round down when even number is left of 5, otherwise round up)
    // $dVal is value to round
    // $iDec specifies number of decimal places to retain
    static $dFuzz = 0.00001; // to deal with floating-point precision loss

    $iSign = ($dVal != 0.0) ? intval($dVal / abs($dVal)) : 1;
    $dVal = abs($dVal);

    // get decimal digit in question and amount to right of it as a fraction
    $dWorking = $dVal * pow(10.0, $iDec + 1) - floor($dVal * pow(10.0, $iDec)) * 10.0;
    $iEvenOddDigit = floor($dVal * pow(10.0, $iDec)) - floor($dVal * pow(10.0, $iDec - 1)) * 10.0;

    if (abs($dWorking - 5.0) < $dFuzz) {
        $iRoundup = ($iEvenOddDigit & 1) ? 1 : 0;
    } else {
        $iRoundup = ($dWorking > 5.0) ? 1 : 0;
    }

    return $iSign * ((floor($dVal * pow(10.0, $iDec)) + $iRoundup) / pow(10.0, $iDec));
}

/**
 * Devuelve la ruta del controlador solicitado.
 *
 * @param string $name
 *
 * @return string
 */
function find_controller($name)
{
    foreach ($GLOBALS['plugins'] as $plugin) {
        if (file_exists(constant('FS_FOLDER') . '/plugins/' . $plugin . '/controller/' . $name . '.php')) {
            return constant('BASE_PATH') . '/plugins/' . $plugin . '/controller/' . $name . '.php';
        }
    }

    if (file_exists(constant('FS_FOLDER') . '/controller/' . $name . '.php')) {
        return constant('BASE_PATH') . '/controller/' . $name . '.php';
    }

    return constant('BASE_PATH') . '/base/fs_controller.php';
}

/**
 * Devuelve si la ruta de la vista con código CSS existe.
 *
 * @param string $name
 *
 * @return bool
 */
function style_exists($name)
{
    $status = false;
    foreach ($GLOBALS['plugins'] as $plugin) {
        if (file_exists(constant('FS_FOLDER') . '/plugins/' . $plugin . '/Templates/style/' . $name . '.blade.php')) {
            $status = true;
        }
    }

    if (file_exists(constant('FS_FOLDER') . '/Templates/style/' . $name . '.blade.php')) {
        $status = true;
    }

    return $status;
}

/**
 * Devuelve si la ruta de la vista con código JS existe.
 *
 * @param string $name
 *
 * @return bool
 */
function javascripts_exists($name)
{
    $status = false;
    foreach ($GLOBALS['plugins'] as $plugin) {
        if (file_exists(constant('FS_FOLDER') . '/plugins/' . $plugin . '/Templates/javascripts/' . $name . '.blade.php')) {
            $status = true;
        }
    }

    if (file_exists(constant('FS_FOLDER') . '/Templates/javascripts/' . $name . '.blade.php')) {
        $status = true;
    }

    return $status;
}

/**
 * TODO: Missing documentation
 *
 * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
 * @version 2021.07
 *
 * @param string $name
 *
 * @return false|mixed|string
 *
 */
function find_plugin($name)
{
    foreach ($GLOBALS['plugins'] as $plugin) {
        if (file_exists(constant('FS_FOLDER') . '/plugins/' . $plugin . '/schema')) {
            if (file_exists(constant('FS_FOLDER') . '/plugins/' . $plugin . '/controller/' . $name . '.php')) {
                return '*' . $plugin;
            }
        } elseif (file_exists(constant('FS_FOLDER') . '/plugins/' . $plugin . '/controller/' . $name . '.php')) {
            return $plugin;
        }
    }

    if (file_exists(constant('FS_FOLDER') . '/controller/' . $name . '.php')) {
        return '';
    }

    return false;
}

/**
 * Función alternativa para cuando el follow location falla.
 *
 * @param resource $ch
 * @param integer  $redirects
 * @param bool     $curlopt_header
 *
 * @return string
 */
function fs_curl_redirect_exec($ch, &$redirects, $curlopt_header = false)
{
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $data = curl_exec($ch);
    $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

    if ($http_code == 301 || $http_code == 302) {
        [$header] = explode("\r\n\r\n", $data, 2);
        $matches = [];
        preg_match("/(Location:|URI:)[^(\n)]*/i", $header, $matches);
        $url = trim(str_replace($matches[1], "", $matches[0]));
        $url_parsed = parse_url($url);
        if (isset($url_parsed)) {
            curl_setopt($ch, CURLOPT_URL, $url);
            $redirects++;
            return fs_curl_redirect_exec($ch, $redirects, $curlopt_header);
        }
    }

    if ($curlopt_header) {
        curl_close($ch);
        return $data;
    }

    if ($data === false) {
        return 'No data';
    }

    [, $body,] = explode("\r\n\r\n", $data, 2);
    curl_close($ch);
    return $body;
}

/**
 * Descarga el archivo de la url especificada
 *
 * @param string  $url
 * @param string  $filename
 * @param integer $timeout
 *
 * @return bool
 */
function fs_file_download($url, $filename, $timeout = 60)
{
    $ok = false;

    try {
        $data = fs_file_get_contents($url, $timeout);
        if ($data && $data != 'ERROR' && file_put_contents($filename, $data) !== false) {
            $ok = true;
        }
    } catch (Exception $e) {
        /// nada
    }

    return $ok;
}

/**
 * Centraliza las consultas a los JSON de la comunidad.
 *
 * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
 * @version 2022.0325
 *
 * @param string $url
 * @param int    $timeout
 *
 * @return mixed
 */
function get_from_community($url = '', $timeout = 10)
{
    $completeUrl = constant('FS_COMMUNITY_URL') . $url;
    $fileContent = fs_file_get_contents($completeUrl, $timeout);
    return json_decode($fileContent);
}

/**
 * Descarga el contenido con curl o file_get_contents.
 *
 * @param string  $url
 * @param integer $timeout
 *
 * @return string
 */
function fs_file_get_contents($url, $timeout = 10)
{
    if (function_exists('curl_init')) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        if (ini_get('open_basedir') === null) {
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        }

        /**
         * En algunas configuraciones de php es necesario desactivar estos flags,
         * en otras es necesario activarlos. habrá que buscar una solución mejor.
         */
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);

        if (defined('FS_PROXY_TYPE')) {
            curl_setopt($ch, CURLOPT_PROXYTYPE, FS_PROXY_TYPE);
            curl_setopt($ch, CURLOPT_PROXY, FS_PROXY_HOST);
            curl_setopt($ch, CURLOPT_PROXYPORT, FS_PROXY_PORT);
        }
        $data = curl_exec($ch);
        $info = curl_getinfo($ch);

        if ($info['http_code'] == 200) {
            curl_close($ch);
            return $data;
        } elseif ($info['http_code'] == 301 || $info['http_code'] == 302) {
            $redirs = 0;
            return fs_curl_redirect_exec($ch, $redirs);
        }

        /// guardamos en el log
        if (class_exists('fs_core_log') && $info['http_code'] != 404) {
            $error = curl_error($ch);
            if ($error == '') {
                $error = 'ERROR ' . $info['http_code'];
                // if (constant('FS_DB_HISTORY')) {
                // $error .= ' en fs_file_get_contents con url "' . $url . '"';
                // $error .= '<pre>' . print_r(debug_backtrace(), true) . '</pre>';
                // }
            }

            $core_log = new fs_core_log();
            $core_log->new_error($error);
            $core_log->save($url . ' - ' . $error);
        }

        curl_close($ch);
        return "ERROR ($url)";
    }

    return file_get_contents($url);
}

/**
 * Devuelve el equivalente a $_POST[$name], pero pudiendo definir un valor
 * por defecto si no encuentra nada.
 *
 * @param string $name
 * @param mixed  $default
 *
 * @return mixed
 */
function fs_filter_input_post($name, $default = false)
{
    return isset($_POST[$name]) ? $_POST[$name] : $default;
}

/**
 * Devuelve el equivalente a $_GET[$name], pero pudiendo definir un valor
 * por defecto si no encuentra nada.
 *
 * @param string $name
 * @param mixed  $default
 *
 * @return mixed
 */
function fs_filter_input_get($name, $default = false)
{
    return isset($_GET[$name]) ? $_GET[$name] : $default;
}

/**
 * Devuelve el equivalente a $_REQUEST[$name], pero pudiendo definir un valor
 * por defecto si no encuentra nada.
 *
 * @param string $name
 * @param mixed  $default
 *
 * @return mixed
 */
function fs_filter_input_req($name, $default = false)
{
    return isset($_REQUEST[$name]) ? $_REQUEST[$name] : $default;
}

/**
 * Deshace las conversiones realizadas por fs_model::no_html()
 *
 * @param string $txt
 *
 * @return string
 */
function fs_fix_html($txt)
{
    $original = ['&lt;', '&gt;', '&quot;', '&#39;',];
    $final = ['<', '>', "'", "'",];
    return trim(str_replace($original, $final, $txt));
}

/**
 * Devuelve la IP del usuario.
 *
 * @return string
 */
function fs_get_ip()
{
    $fields = [
        'HTTP_CF_CONNECTING_IP',
        'HTTP_X_FORWARDED_FOR',
        'REMOTE_ADDR',
    ];
    foreach ($fields as $field) {
        if (isset($_SERVER[$field])) {
            return $_SERVER[$field];
        }
    }

    return '';
}

/**
 * Devuelve el tamaño máximo de archivo que soporta el servidor para las subidas por formulario.
 *
 * @return int
 */
function fs_get_max_file_upload()
{
    $max = intval(ini_get('post_max_size'));
    if (intval(ini_get('upload_max_filesize')) < $max) {
        $max = intval(ini_get('upload_max_filesize'));
    }

    return $max;
}

/**
 * Devuelve el nombre de la clase del objeto, pero sin el namespace.
 *
 * @param object $object
 *
 * @return string
 */
function get_class_name($object = null)
{
    $name = get_class($object);
    $pos = strrpos($name, '\\');
    if ($pos !== false) {
        $name = substr($name, $pos + 1);
    }

    return $name;
}

/**
 * Carga todos los modelos disponibles en los plugins activados y el núcleo.
 */
function require_all_models()
{
    require_once constant('BASE_PATH') . '/base/fs_model.php';
    require_once constant('BASE_PATH') . '/base/fs_extended_model.php';

    if (!isset($GLOBALS['models'])) {
        $GLOBALS['models'] = [];
    }

    foreach ($GLOBALS['plugins'] as $plugin) {
        if (!file_exists('plugins/' . $plugin . '/model')) {
            continue;
        }

        foreach (scandir('plugins/' . $plugin . '/model') as $file_name) {
            if ($file_name != '.' && $file_name != '..' && substr($file_name, -4) == '.php' && !in_array($file_name, $GLOBALS['models'])) {
                require_once constant('BASE_PATH') . '/plugins/' . $plugin . '/model/' . $file_name;
                $GLOBALS['models'][] = $file_name;
            }
        }
    }

    /// ahora cargamos los del núcleo
    foreach (scandir('model') as $file_name) {
        if ($file_name != '.' && $file_name != '..' && substr($file_name, -4) == '.php' && !in_array($file_name, $GLOBALS['models'])) {
            require_once constant('BASE_PATH') . '/model/' . $file_name;
            $GLOBALS['models'][] = $file_name;
        }
    }
}

/**
 * Función obsoleta para cargar un modelo concreto.
 *
 * @param string $name
 *
 * @deprecated since version 2017.025
 */
function require_model($name)
{
    if (FS_DB_HISTORY) {
        $fichero = debug_backtrace()[0]['file'];
        $core_log = new fs_core_log();
        $core_log->new_error("require_model('$name') es innecesario en '$fichero'.");
    }
}

/**
 * Devuelve un string aleatorio de longitud $length
 *
 * @param int $length la longitud del string
 *
 * @return string la cadena aleatoria
 */
function random_string($length = 20)
{
    $random = '';
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    while (strlen($random) < $length) {
        $pos = mt_rand(0, strlen($characters) - 1);
        $random .= $characters[$pos];
    }
    return str_shuffle($random);
}

/**
 * Retorna un array con los registros que cumplirán años en los próximos $dias.
 * Si queremos que retorne también a quienes cumplen años el día de la fecha, $contarcumplidores ha de ser true.
 *
 * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
 * @version 2021.05
 *
 * @param bool   $contarcumplidores
 *
 * @param string $tabla
 * @param string $campo_fecha
 *
 * @param null   $fecha
 * @param int    $dias
 *
 * @return false|array
 *
 */
function get_aniversario($tabla, $campo_fecha, $fecha = null, $dias = 0, $contarcumplidores = true)
{
    if ($fecha === null) {
        $fecha = date('Y-m-d');
    }
    $desde = $contarcumplidores ? 0 : 1;
    $sql = "
        SELECT *, DATEDIFF(a.next_nac, '{$fecha}') AS difnext, DATEDIFF(a.this_nac, '{$fecha}') AS difthis
            FROM (
                SELECT *, 
                    DATE_ADD({$campo_fecha}, INTERVAL YEAR(FROM_DAYS(DATEDIFF('{$fecha}', f_nacimiento))) + 1 YEAR) as next_nac,
                    DATE_ADD({$campo_fecha}, INTERVAL YEAR(FROM_DAYS(DATEDIFF('{$fecha}', f_nacimiento))) - 0 YEAR) as this_nac
                FROM {$tabla}) a
        WHERE 
            DATEDIFF(a.next_nac, '{$fecha}') BETWEEN {$desde} AND {$dias} OR
            DATEDIFF(a.this_nac, '{$fecha}') BETWEEN {$desde} AND {$dias}";

    $db = new fs_db2();
    return $db->select($sql);
}

if (!function_exists('getResellerData')) {
    /**
     * Retorna un StdClass con los datos de personalización de cada reseller
     * TODO: Externalizar de alguna manera.
     *
     * @return stdClass
     */
    function getResellerData()
    {
        $result = new stdClass();
        $name = $_SERVER['SERVER_NAME'];
        $result->url = 'https://mifactura.eu';
        $result->url_contacto = 'https://mifactura.eu/contacto';
        $result->telefono = '+34822048526';
        $result->locked_controllers = [];
        if (strpos($name, 'localhost') !== false) {
            $result->name = 'XFS.Cloud (localhost)';
            $result->email = 'soporte@mifactura.eu';
            $result->logo_45 = 'resellers/mifactura/img/logo-45.png';
            $result->logo_200 = 'resellers/mifactura/img/logo-200.png';
            $result->locked_controllers = [
                'compras_albaran',
                'compras_albaranes',
                'compras_factura',
                'compras_facturas',
                'ventas_factura',
                'ventas_facturas',
            ];
        } elseif (strpos($name, 'cloudnubelia.cloud') !== false) {
            $result->name = 'CloudNubelia';
            $result->email = 'info@cloudnubelia.com';
            $result->logo_45 = 'resellers/nubelia/img/logo-45.png';
            $result->logo_200 = 'resellers/nubelia/img/logo-200.png';
        } elseif (strpos($name, 'akiai.es') !== false) {
            $result->name = 'Akiai';
            $result->email = 'info@akiai.es';
            $result->logo_45 = 'resellers/akiai/img/logo-45.png';
            $result->logo_200 = 'resellers/akiai/img/logo-200.png';
        } else {
            $result->name = 'XFS.Cloud';
            $result->email = 'soporte@mifactura.eu';
            $result->logo_45 = 'resellers/mifactura/img/logo-45.png';
            $result->logo_200 = 'resellers/mifactura/img/logo-200.png';
            $result->locked_controllers = [
                'compras_albaran',
                'compras_albaranes',
                'compras_factura',
                'compras_facturas',
                'ventas_factura',
                'ventas_facturas',
            ];
        }
        return $result;
    }
}

/**
 * Devuelve los tipos de logins.
 *
 * @return array
 */
function plantillas_login()
{
    if (function_exists('plantillas_login_plugin')) {
        return plantillas_login_plugin();
    }

    $anonimo = new stdClass();
    $anonimo->nombre = 'Con login anónimo';
    $anonimo->plantilla_login = 'default';

    $desplegable = new stdClass();
    $desplegable->nombre = 'Desplegable de usuarios';
    $desplegable->plantilla_login = 'desplegable';

    return [
        $anonimo,
        $desplegable,
    ];
}

/**
 * Devuelve la URL base de la instalación.
 *
 * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
 * @version 2021.06
 * @return string
 *
 */
function get_base_url()
{
    return (isset($_SERVER['HTTPS']) ? 'https://' : 'http://') . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'];
}

/**
 * Devuelve si el plugin está activo o no.
 *
 * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
 * @version 2021.09
 *
 * @param $plugin_name
 *
 * @return bool
 */
function is_plugin_enabled($plugin_name): bool
{
    $plugin = Version::getByName($plugin_name);
    if (isset($plugin)) {
        return isset($plugin->sequence);
    }
    return false;
}

/**
 * Devuelve una fecha en formato humano d-m-Y o en el formato que especifiquemos del tipo date
 *
 * @author  Manuel Miranda Melián <manuel@x-netdigital.com>
 * @version 2021.10
 *
 * @param string $diff_time
 *
 * @param string $date
 * @param string $format
 *
 * @return string
 */
function show_date($date = 'now', $format = "d-m-Y", $diff_time = ' + 0 days')
{
    if (empty($date)) {
        return '';
    }
    $date = $date . $diff_time;
    return $date ? date($format, strtotime($date)) : null;
}

/**
 * Devuelve una fecha en formato humano H:i o en el formato que especifiquemos del tipo date
 *
 * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
 * @version 2022.0427
 *
 * @param string $diff_time
 *
 * @param string $date
 * @param string $format
 *
 * @return string
 */
function show_hour($date = 'now', $format = "H:i:s", $diff_time = ' + 0 days')
{
    if (empty($date)) {
        return '';
    }
    $date = $date . $diff_time;
    return show_date($date, $format);
}

/**
 * Devuelve una fecha en formato humano Y-m-d o en el formato que especifiquemos del tipo date
 *
 * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
 * @version 2022.0427
 *
 * @param string $diff_time
 *
 * @param string $date
 * @param string $format
 *
 * @return string
 */
function show_datetime($date = 'now', $format = "d-m-Y H:i", $diff_time = ' + 0 days')
{
    if (empty($date)) {
        return '';
    }
    $date = $date . $diff_time;
    return show_date($date, $format);
}

/**
 * Devuelve una fecha en formato humano Y-m-d o en el formato que especifiquemos del tipo date
 *
 * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
 * @version 2022.0427
 *
 * @param string $diff_time
 *
 * @param string $date
 * @param string $format
 *
 * @return string
 */
function form_date($date = 'now', $format = "Y-m-d", $diff_time = ' + 0 days')
{
    if (empty($date)) {
        return '';
    }
    $date = $date . $diff_time;
    return show_date($date, $format);
}

/**
 * Devuelve una fecha en formato humano H:i o en el formato que especifiquemos del tipo date
 *
 * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
 * @version 2022.0427
 *
 * @param string $diff_time
 *
 * @param string $date
 * @param string $format
 *
 * @return string
 */
function form_hour($date = 'now', $format = "H:i", $diff_time = ' + 0 days')
{
    if (empty($date)) {
        return '';
    }
    $date = $date . $diff_time;
    return show_date($date, $format);
}

/**
 * Devuelve una fecha en formato humano Y-m-d o en el formato que especifiquemos del tipo date
 *
 * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
 * @version 2022.0427
 *
 * @param string $diff_time
 *
 * @param string $date
 * @param string $format
 *
 * @return string
 */
function form_datetime($date = 'now', $format = "Y-m-d\TH:i", $diff_time = ' + 0 days')
{
    if (empty($date)) {
        return '';
    }
    $date = $date . $diff_time;
    return show_date($date, $format);
}

/**
 * Devuelve una fecha en formato humano dd-mm-YYYY o en el formato que especifiquemos del tipo Date()
 *
 * @author     Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
 * @version    2022.0427
 *
 * @param $date
 * @param $format
 *
 * @return string|null
 * @deprecated 2022.0427 Reemplazo y unificación por función show_date
 */
function d2h($date = 'now', $format = "d-m-Y")
{
    return show_date($date, $format);
}

/**
 * Obtiene los datos de una opción de menú dada su ruta separada por pipelines.
 * En $state se le pasa si es la opción actual o no para retornar el icono cuando abierto.
 *
 * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
 * @version 2022.0613
 *
 * @param $page_folder
 * @param $state
 *
 * @return array
 */
function get_page_info($page_folder, $state = false)
{
    $result = [];

    $option_menu = (new fs_folder())->get($page_folder);
    if ($option_menu !== false) {
        $result['name'] = $option_menu->name;
        $result['icon'] = $state ? $option_menu->icon_open : $option_menu->icon;
        $result['icon_close'] = $option_menu->icon;
        $result['icon_open'] = $option_menu->icon_open;
        return $result;
    }
    if (str_contains($page_folder, "|")) {
        $page_folder = explode('|', $page_folder);
        $page_folder = end($page_folder);
    }
    $result['name'] = "$page_folder";
    $result['icon'] = $state ? 'fas fa-folder-open fa-fw' : 'fas fa-folder fa-fw';
    $result['icon_close'] = 'fas fa-folder fa-fw';
    $result['icon_open'] = 'fas fa-folder-open fa-fw';
    return $result;
}

/**
 * Devuelve una lista de iconos con su texto y color.
 *
 * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
 * @version 2022.0325
 *
 * @return array[]
 */
function get_icons_details()
{
    $documentos = 'fa-solid fa-file';
    $doc_proveedor = '#f46a6a';
    $doc_cliente = '#4e92bb';
    return [
        'default' => [
            'icon' => 'fa-solid fa-circle-question fa-fw',
            'text' => '',
            'color' => null,
        ],
        'pedido_proveedor' => [
            'icon' => $documentos,
            'text' => constant('FS_PEDIDO') ?? 'pedido',
            'color' => $doc_proveedor,
        ],
        'albaran_proveedor' => [
            'icon' => $documentos,
            'text' => constant('FS_ALBARAN') ?? 'albaran',
            'color' => $doc_proveedor,
        ],
        'factura_proveedor' => [
            'icon' => $documentos,
            'text' => constant('FS_FACTURA') ?? 'factura',
            'color' => $doc_proveedor,
        ],
        'presupuesto_cliente' => [
            'icon' => $documentos,
            'text' => constant('FS_PRESUPUESTO') ?? 'presupuesto',
            'color' => $doc_cliente,
        ],
        'pedido_cliente' => [
            'icon' => $documentos,
            'text' => constant('FS_PEDIDO') ?? 'pedido',
            'color' => $doc_cliente,
        ],
        'servicio_cliente' => [
            'icon' => $documentos,
            'text' => constant('FS_SERVICIO') ?? 'servicio',
            'color' => $doc_cliente,
        ],
        'albaran_cliente' => [
            'icon' => $documentos,
            'text' => constant('FS_ALBARAN') ?? 'albaran',
            'color' => $doc_cliente,
        ],
        'factura_cliente' => [
            'icon' => $documentos,
            'text' => constant('FS_FACTURA') ?? 'factura',
            'color' => $doc_cliente,
        ],
        'expediente_cliente' => [
            'icon' => $documentos,
            'text' => constant('FS_EXPEDIENTE') ?? 'expediente',
            'color' => $doc_cliente,
        ],
        'gasto' => [
            'icon' => $documentos,
            'text' => constant('FS_GASTO') ?? 'gasto',
            'color' => $doc_cliente,
        ],
        'ingreso' => [
            'icon' => $documentos,
            'text' => constant('FS_INGRESO') ?? 'ingreso',
            'color' => $doc_cliente,
        ],

    ];
}

/**
 * Devuelve el código HTML del icono, a partir de su nombre.
 *
 * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
 * @version 2022.0325
 *
 * @param $icon_name
 *
 * @return string
 */
function get_icon($icon_name)
{
    $icon = get_icons_details()[$icon_name] ?? get_icons_details()['default'];
    $color = isset($icon['color']) ? 'style="color: ' . $icon['color'] . '"' : '';
    return '<span class="fa-layers fa-fw">'
        . '<i class="' . $icon['icon'] . '" ' . $color . ' ></i>'
        . '<span class="fa-layers-text fa-inverse" data-fa-transform="shrink-11 down-3">' . strtoupper(substr($icon['text'], 0, 3)) . '</span>'
        . '</span>';
}

/**
 * Devuelve un icono compuesto con un badge.
 *
 * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
 * @version 2022.0325
 *
 * @param $icon_name
 * @param $badge_number
 *
 * @return string
 */
function get_icon_with_badge($icon_name, $badge_number)
{
    return '  <span class="fa-layers fa-fw">'
        . '<i class="' . $icon_name . '"></i>'
        . '<span class="fa-layers-counter" style="background:Tomato">' . $badge_number . '</span>'
        . '</span>';
}

function generate_menu_elements(&$menu_elements, $actual_folder, $level = 1, $previous_level = 1, $folder_reference = '')
{
    $result = recursive_menu_elements($menu_elements, $actual_folder, $level, $previous_level, $folder_reference);
    return $result;
}

/**
 *  Lee los elementos del menú y crea la estructura de cada uno de forma recursiva, entra en los elementos que son
 *  carpetas (arrays) y vuelve a repetir el proceso hasta tener la estructura completa
 *
 * @author  Daniel M. Hernández Vieira  <daniel.hernandez@x-netdigital.com>
 * @version 2022.0607
 *
 * @param $menu_elements
 * @param $actual_folder
 *
 * @return string
 */
function recursive_menu_elements($menu_elements, $actual_folder, $level = 1, $previous_level = 1, $folder_reference = '')
{
    $url_template = "index.php?page=";
    $cadena = "";

    // Generado de menú
    foreach ($menu_elements as $key_menu_element => $menu_element) {
        //Condición de se está en el nivel 1 para insertar el menú title

        if (is_array($menu_element) && !empty($menu_element)) {
            //$check_folder = check_empty_folder($menu_element);
            //$get_menu_data = get_menu_data($key_menu_element, $first_menu_element);
            $check_folder = false;
            if (!$check_folder) {
                if ($level == 1) {
                    $folder_reference = $key_menu_element;
                    $folder_data = get_page_info($key_menu_element);
                    $label = '<li class="menu-title">' . $folder_data['name'] . '</li>' .
                        recursive_menu_elements($menu_element, $actual_folder, $level + 1, $level, $folder_reference);
                    $cadena .= $label;
                } else {
                    $is_selected = false;
                    if (is_array($menu_element)) {
                        if (strpos($actual_folder, $key_menu_element) === 0) {
                            $is_selected = true;
                        }
                    }
                    $folder_classes = actual_page_classes($is_selected);

                    $folder_data = get_page_info($key_menu_element);
                    $label = '<li class="' . $folder_classes['class_open_folder'] . '">' .
                        '<li class="' . $folder_classes['class_open_folder'] . '">' .
                        '<a href="#" class="has-arrow d-flex' . $folder_classes['class_folder'] . '" aria-expanded="' . $folder_classes['expanded_folder'] . '" title="' . $folder_data['name'] . '">' .
                        '<i class="' . $folder_data['icon'] . '"></i>' .
                        '<span class="w-100">' . $folder_data['name'] . '</span>' .
                        '</a>' .
                        '<ul class="sub-menu" aria-expanded="true">' .
                        recursive_menu_elements($menu_element, $actual_folder, $level + 1, $level, $folder_reference) .
                        '</ul>' .
                        '</li>';
                    $cadena .= $label;
                }
            }
        } else {
            $class_link = '';
            if ($menu_element->showing()) {
                $class_link = 'fw-bold text-white active';
            }
            if ($menu_element->show_on_menu) {
                $label = "<li>" .
                    "<a href='" . $url_template . $menu_element->name . "' class='d-flex " . $class_link . "' title=''>" .
                    "<span class='w-100'>" . $menu_element->title . "</span>" .
                    "</a>" .
                    "</li>";
                $cadena .= $label;
            }
        }
    }

    return $cadena;
}

function get_html_folder_section($key_menu_element)
{
    $folder_data = get_page_info($key_menu_element);
    $label = "<li class='menu-title'>" . $folder_data['name'] . "</li>";
    return $label;
}

/**
 * Comprueba si la carpeta que seleccionó el usuario es la que se está leyendo la función
 * "recursive_menu_elements()" si es así devuelve unas clases para resaltarla, si no, devuelve sus valores
 *  por defecto
 *
 * @author  Daniel M. Hernández Vieira  <daniel.hernandez@x-netdigital.com>
 * @version 2022.0607
 *
 * @param $key_menu_element folder que actualmente se está leyendo en el array
 * @param $actual_folder    folder donde se encuentra la página seleccionada
 *
 * @return array
 */
function actual_page_classes($is_selected)
{

    $folder_classes = [
        'class_folder' => '',
        'class_open_folder' => '',
        'expanded_folder' => false,
    ];

    if ($is_selected) {
        $folder_classes['class_folder'] = 'fw-bold';
        $folder_classes['class_open_folder'] = 'mm-active';
        $folder_classes['expanded_folder'] = 'mm-true';
    }
    return $folder_classes;
}

/**
 * Devuelve una lista de iconos disponibles en la ruta de iconos gratuitos de fontawesome.
 *
 * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
 * @version 2022.0419
 *
 * @return array
 */
function get_free_fa_icons()
{
    $path = 'node_modules/@fortawesome/fontawesome-free/svgs';
    $directory = new RecursiveDirectoryIterator($path);
    $display = ['svg'];
    $items = [];
    foreach (new RecursiveIteratorIterator($directory) as $file) {
        $array = explode('.', $file);
        if (in_array(mb_strtolower(array_pop($array)), $display)) {
            $fileName = substr($file->getFilename(), 0, -4);
            $item = 'fa-' . basename($file->getPath()) . ' fa-' . $fileName . ' fa-fw';
            $items[$item] = $item;
        }
    }
    return $items;
}

function get_text_color()
{
    return [
        'text-primary' => 'Primario',
        'text-secondary' => 'Secundario',
        'text-success' => 'Satisfactorio',
        'text-danger' => 'Peligro',
        'text-warning' => 'Aviso',
        'text-light' => 'Claro',
        'text-dark' => 'Oscuro',
        'text-white' => 'Blanco',
    ];
}

if (!function_exists('debug_message')) {
    /**
     * Añade un mensaje de depuración, que será visible en la barra de depuración si está activada.
     *
     * @param mixed $msg
     */
    function debug_message($msg)
    {
        if (constant('FS_DEBUG')) {
            (new debugbar())->addMessage('messages', $msg, 'debug');
        }
    }
}

/**
 * Convierte caracteres especiales en entidades HTML. Convirtiendo NULL como texto vacío.
 *
 * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
 * @version 2022.0425
 *
 * @param $encoding
 * @param $double_encode
 *
 * @param $string
 * @param $flags
 *
 * @return string
 */
function xfs_htmlspecialchars($string, $flags = ENT_QUOTES | ENT_SUBSTITUTE, $encoding = null, $double_encode = true)
{
    return htmlspecialchars($string ?? '', $flags, $encoding, $double_encode);
}

/**
 * Devuelve el texto truncado.
 *
 * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
 * @version 2022.0511
 *
 * @param $text
 * @param $len
 *
 * @return mixed|string
 */
function truncate_text($text = '', $len = 50)
{
    if (mb_strlen($text) > $len) {
        return mb_substr($text, 0, $len - 3, 'UTF-8') . '...';
    }

    return $text;
}

/**
 * Send a cookie without urlencoding the cookie value.
 *
 * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
 * @version 2022.0511
 *
 * @param array  $options
 *
 * @param string $name
 * @param string $value
 *
 * @return bool
 */
function assign_cookie(string $name, $value = '', array $options = [])
{
    $expires = time() + constant('FS_COOKIES_EXPIRE') ?? 31536000;
    if (empty($value)) {
        $expires = time() - 3600;
    }

    $own_cookies = [
        'samesite' => 'Strict',
        'expires' => $expires,
        'path' => '',
        'domain' => '',
        'secure' => true,
        'httponly' => false,
    ];
    $options = !empty($options) ? $options : $own_cookies;
    if (version_compare(phpversion(), '7.3.0', '>=')) {
        $status = setcookie($name, $value, $options);
    } else {
        $status = setcookie($name, $value, $own_cookies['expires'], $own_cookies['path'], $own_cookies['domain'], $own_cookies['secure'], $own_cookies['httponly']);
    }
    return $status;
}

/**
 * Obiene los array de $data, para pintar los submenús de menú.
 *
 * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
 * @version 2022.0613
 *
 * @param $data
 *
 * @return array
 */
function get_arrays_from($data)
{
    $result = [];
    foreach ($data as $key => $datum) {
        if (is_array($datum)) {
            $result[$key] = $datum;
        }
    }
    return $result;
}

/**
 * Obtiene los items del array $data para pintar las opciones de menú.
 *
 * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
 * @version 2022.0613
 *
 * @param $data
 *
 * @return array
 */
function get_items_from($data)
{
    $result = [];
    foreach ($data as $key => $datum) {
        if (!is_array($datum)) {
            $result[$key] = $datum;
        }
    }
    return $result;
}

/**
 * Asigna temporalmente unos limites "ilimitados"
 *
 * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
 * @version 2022.0725
 *
 */
function set_temporaly_unlimited_limits()
{
    @set_time_limit(0);
    ini_set('max_execution_time', '0');
    ini_set('memory_limit', '-1');
}

/**
 * Genera un código UUID aleatorio
 *
 * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
 * @version 2022.0811
 *
 * @return string|void
 * @throws Exception
 */
function uuid()
{
    // Generate 16 bytes (128 bits) of random data or use the data passed into the function.
    $data = $data ?? random_bytes(16);
    assert(strlen($data) == 16);

    // Set version to 0100
    $data[6] = chr(ord($data[6]) & 0x0f | 0x40);
    // Set bits 6-7 to 10
    $data[8] = chr(ord($data[8]) & 0x3f | 0x80);

    // Output the 36 character UUID.
    return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
}

/**
 * Es como file_exists, pero sensible a mayúsculas incluso en sistemas como
 * Windows que no lo son.
 *
 * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
 * @version 2022.0810
 *
 * @param string $path
 *
 * @return bool
 */
function file_exists_case_sensitive(string $path): bool
{
    if (!file_exists($path)) {
        return false;
    }
    return strcmp(basename(realpath($path)), basename($path)) === 0;
}

/**
 * Devuelve una respuesta en formato JSON.
 *
 * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
 * @version 2022.0902
 *
 * @param array|stdClass $data
 * @param null|int       $flags
 *
 * @return void
 */
function json_response($data, $flags = null): void
{
    if ($flags == null) {
        $flags = constant('FS_DEBUG') ? JSON_PRETTY_PRINT : 0;
    }
    header('Content-Type: application/json; charset=utf-8');
    echo json_encode($data, $flags);
    die();
}

/**
 * Forma segura de confirmar la creación de una carpeta.
 *
 * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
 * @version 2022.0906
 *
 * @param string $directory
 * @param int    $permissions
 * @param bool   $recursive
 *
 * @return bool
 */
function createDir(string $directory, int $permissions = 0777, bool $recursive = true): bool
{
    $status = !(!\is_dir($directory) && !@\mkdir($directory, $permissions, $recursive) && !\is_dir($directory));
    if (!$status) {
        debug_message("ERROR al crear el directorio '$directory' con permisos '$permissions'.");
    }
    return $status;
}
