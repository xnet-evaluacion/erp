<?php

/*
 * This file is part of MiFactura.eu
 * Copyright (C) 2021 X-Net Software Solutions S.L. <xnetsoftware@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace Xnet\Core;

/**
 * Class XnetUpdateController
 *
 * Esta clase abstracta, ejecuta cada método que corresponda del descendiente correspondiente al plugin cuya base de
 * datos está siendo actualizada.
 *
 * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
 * @version 2022.0615
 *
 */
abstract class XnetUpdateController extends XnetAppController
{
    public function var2str($val)
    {
        if (is_null($val)) {
            return 'NULL';
        } elseif (is_bool($val)) {
            return $val ? 'TRUE' : 'FALSE';
        } elseif (preg_match('/^([0-9]{1,2})-([0-9]{1,2})-([0-9]{4})$/i', $val)) {
            /// es una fecha
            return "'" . date($this->db->date_style(), strtotime($val)) . "'";
        } elseif (preg_match('/^([0-9]{4})-([0-9]{1,2})-([0-9]{1,2}) ([0-9]{1,2}):([0-9]{1,2}):([0-9]{1,2})$/i', $val)) {
            /// es una fecha+hora
            return "'" . date($this->db->date_style() . ' H:i:s', strtotime($val)) . "'";
        }

        return $this->db->escape_string($val);
    }

    /**
     * Contiene la relación de métodos de la clase, de las que se tomarán los update_YYYYMMDDX
     *
     * @var string[]
     */
    public $updates;

    /**
     * Contiene el modelo version correspondiente al plugin actual.
     *
     * @var Version
     */
    private $plugin;

    private $method;

    public $db;

    public function __construct($plugin)
    {
        parent::__construct();

        $this->updates = get_class_methods($this);
        sort($this->updates);   // Es importante que estén ordenados, para que las actualizaciones se hagan en el orden correcto.
        $this->plugin = $plugin;
        $this->method = 'Pending';
        $this->db = new \fs_db2();

        require_all_models();
    }

    /**
     * Ejecuta las consultas correspondientes.
     * Si no pudieran ejecutarse, se hará un rollback.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2021.10
     *
     * @param string[][] $fixes [
     *                          'table_name_1' => ['query1','query2' ],
     *                          'table_name_2' => ['query1','query2' ],
     *                          ]
     *
     * @return array
     */
    public function exec_fix_queries(array $fixes = []): array
    {
        $result['status'] = true;

        $sqls = [];
        foreach ($fixes as $table => $queries) {
            if ($this->db->table_exists($table)) {
                $constraints = [];
                foreach ($this->db->get_constraints($table) as $constraint) {
                    if ($constraint['type'] == "FOREIGN KEY") {
                        $constraints[] = $constraint['name'];
                    }
                }

                if (isset($constraint) && is_numeric($constraint)) {
                    $sqls[] = $query;
                } else {
                    foreach ($queries as $constraint => $query) {
                        if (!in_array($constraint, $constraints)) {
                            $sqls[] = $query;
                        }
                    }
                }
            }
        }

        if (!empty($sqls)) {
            $this->db->begin_transaction();
            foreach ($sqls as $sql) {
                if (!$this->db->exec($sql)) {
                    $result['status'] = false;
                    $result['error'] = "Error al ejecutar la consulta '$sql'";
                }
            }
            if ($result['status']) {
                $this->db->commit();
            } else {
                $this->db->rollback();
            }
        }

        return $result;
    }

    public function exec($sql)
    {
        return $this->db->exec($sql);
    }

    public function select($sql)
    {
        return $this->db->select($sql);
    }

    /**
     * Ejecuta los métodos para versiones desde la última actualización hasta la versión actualmente instalada.
     *
     * @author     Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version    2022.0615
     *
     * @return bool
     *
     * @deprecated Esto se usaba en el archivo updater de la raiz, pero ya no se usa en las versiones más recientes.
     *             Se mantiene de momento por compatibilidad con las versiones más antiguas.
     */
    public function update()
    {
        if (!is_array($this->updates)) {
            return false;
        }

        $ok = true;
        foreach ($this->updates as $update) {
            // Se toman sólo los métodos que comiencen por 'update_'
            if (strpos($update, 'update_') !== 0) {
                continue;
            }

            $version = substr($update, 7);
            // La versión tiene que tener entre 8 y 9 caracteres YYYYMMDD y opcionalmente, un noveno carácter de revisión dentro del mismo día.
            if (!in_array(strlen($version), [8, 9])) {
                continue;
            }

            if (strlen($version) === 8) {
                $update_version = $version / 10000;
            } else {
                $update_version = $version / 100000;
            }

            // Si ya se actualizó esa versión... Se ignora
            if ($update_version <= $this->plugin->db_version) {
                continue;
            }

            // We need to remove all trigger_error because the function print the text and all
            // headers call after trigger_error will fails.
            $ok = $this->{$update}();
            if (!$ok) {
                debug_message("Error al intentar actualizar la versión del plugin {$this->plugin->plugin}.");
                return false;
            }
            $this->plugin->db_version = $update_version;
            if (!$this->plugin->save()) {
                debug_message("Error al intentar actualizar la versión del plugin {$this->plugin->plugin}.");
                return false;
            }
        }
        return $ok;
    }

    public function getMethods()
    {
        if (!is_array($this->updates)) {
            return false;
        }

        $return = [];
        foreach ($this->updates as $update) {
            // Se toman sólo los métodos que comiencen por 'update_'
            if (strpos($update, 'update_') !== 0) {
                continue;
            }

            $version = substr($update, 7);
            // La versión tiene que tener entre 8 y 9 caracteres YYYYMMDD y opcionalmente, un noveno carácter de revisión dentro del mismo día.
            if (!in_array(strlen($version), [8, 9])) {
                debug_message('Revise el método con nombre erróneo ' . $update . ' en ' . get_called_class() . '. Ha de tener el formato update_YYYYMMDDx');
                continue;
            }

            if (strlen($version) === 8) {
                $update_version = $version / 10000;
            } else {
                $update_version = $version / 100000;
            }

            // Si ya se actualizó esa versión... Se ignora
            if ($update_version <= $this->plugin->db_version) {
                continue;
            }

            $return[strval($update_version)] = $update;
        }
        return $return;
    }

    /**
     * Añade los campos que faltan en la tabla.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2022.0711
     *
     * @param $data [ 'tablename' => [ 'field' => 'details to add the field' ] ]
     *
     * @return array
     */
    public function addNewTableField(array $data): array
    {
        $result['status'] = true;

        foreach ($data as $tablename => $data2) {
            foreach ($data2 as $field => $type) {
                $sql = "SHOW COLUMNS FROM `$tablename` WHERE `Field` = '$field'";
                if (empty($this->db->select($sql))) {
                    $sql = "ALTER TABLE `$tablename` ADD `$field` $type";
                    if (!$this->db->exec($sql)) {
                        $result['status'] = false;
                        $result['error'][] = "Falla consulta '$sql'";
                    }
                }
            }
        }

        return $result;
    }

    /**
     * Elimina las restricciones de la tabla.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2022.0711
     *
     * @param $data [ 'tablename' => [ 'field' => 'details to add the field' ] ]
     *
     * @return array
     */
    public function removeOldConstraint(array $data): array
    {
        $result['status'] = true;
        foreach ($data as $tablename => $old_constraints) {
            foreach ($old_constraints as $old_constraint) {
                foreach ($this->db->get_constraints($tablename) as $pos => $constraint_details) {
                    if ($constraint_details['name'] == $old_constraint) {
                        if (!$this->db->delete_constraint($tablename, $old_constraint)) {
                            $result['status'] = false;
                            $result['error'][] = "Falla eliminación de constraint '$old_constraint'";
                        }
                    }
                }
            }
        }
        return $result;
    }

    /**
     * Elimina los índices de la tabla.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2022.0711
     *
     * @param $data [ 'tablename' => [ 'field' => 'details to add the field' ] ]
     *
     * @return array
     */
    public function removeOldIndex(array $data): array
    {
        $result['status'] = true;
        foreach ($data as $tablename => $old_indexes) {
            foreach ($old_indexes as $old_index) {
                foreach ($this->db->get_indexes($tablename) as $pos => $index_details) {
                    if ($index_details['name'] == $old_index) {
                        if (!$this->db->delete_index($tablename, $old_index)) {
                            $result['status'] = false;
                            $result['error'][] = "Falla eliminación del índice '$old_index'";
                        }
                    }
                }
            }
        }
        return $result;
    }

    /**
     * Elimina las constraints indicadas, y si hay éxito, también los índices.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0909
     *
     * @param array $data
     *
     * @return array
     */
    public function removeOldConstraintAndIndex(array $data): array
    {
        $result = $this->removeOldConstraint($data);
        if ($result['status'] === false) {
            return $result;
        }
        return $this->removeOldIndex($data);
    }
}
