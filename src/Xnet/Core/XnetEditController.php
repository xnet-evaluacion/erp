<?php

/*
 * This file is part of plugin xnetcommonhelpers for MiFactura.eu
 * Copyright (C) 2022 X-Net Software Solutions S.L. <xnetsoftware@gmail.com>
 */

namespace Xnet\Core;

use DateTimeZone;

/**
 * Class XnetAppController
 *
 * Controlador que permite la edición más o menos automatizada de un modelo.
 *
 * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
 * @version 2022.0724
 *
 * @package Xnet\Core
 */
abstract class XnetEditController extends XnetAuthController
{
    public const PHP_CACHE_FOLDER = 'controllers';
    public $mainModel;
    public $fields;
    public $title;
    public $code;
    public $javascriptCode;

    /**
     * Establecido a true, impide el cierre de un formulario que está siendo editado.
     *
     * @var bool
     */
    public $protectClose;

    public static function getTimezones()
    {
        $result = [];
        $data = DateTimeZone::listIdentifiers();
        foreach ($data as $datum) {
            $result[$datum] = $datum;
        }
        return $result;
    }

    public function get_timezone_list()
    {
        $zones_array = [];
        $timestamp = time();
        $timezone = date_default_timezone_get();
        foreach (timezone_identifiers_list() as $key => $zone) {
            date_default_timezone_set($zone);
            $zones_array[$key]['zone'] = $zone;
            $zones_array[$key]['diff_from_GMT'] = date('P', $timestamp);
        }

        date_default_timezone_set($timezone);
        return $zones_array;
    }

    public function __construct(array $config)
    {
        $this->javascriptCode = [];
        $this->mainModel = $this->getMainModel();

        parent::__construct($config);

        $this->fields = $this->getFields();
    }

    /**
     * Genera el código html para la edición de un campo.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0812
     *
     * @param $col_name
     * @param $col_config
     * @param $model
     *
     * @return string
     */
    public function show(string $col_name, array $col_config, XnetModel $model)
    {
        $label = $col_config['type'] == 'bool' ? '' : '<label>' . $col_config['label'] . '</label>';
        $required = $col_config['required'] ? ' required' : '';
        $readonly = $col_config['readonly'] ? ' readonly' : '';
        $onChange = ' onChange="modifiedField()"';
        $class = $col_config['class'];

        $html = '<div class="' . $class . '">';
        $html .= '<div class="form-group">' . $label;

        $name = $col_name;

        if (!isset($model->{$col_name}) && isset($col_config['default'])) {
            $model->{$col_name} = $col_config['default'];
        }

        $othersParams = '';
        foreach (['maxlength', 'placeholder', 'min', 'max', 'step', 'pattern'] as $param) {
            if (isset($col_config[$param])) {
                $othersParams .= (' ' . $param . '="' . $col_config[$param] . '"');
            }
        }

        $genericDataType = $col_config['generictype'] ?? null;
        if (isset($col_config['select'])) {
            $genericDataType = 'select';
        }

        if (isset($col_config['autocomplete'])) {
            $genericDataType = 'autocomplete';
        }

        switch ($genericDataType) {
            case 'select':
                $select = $col_config['select'];

                $html .= '<select id="' . $col_name . '" name="' . $col_name . '" class="form-control select2"' . $required . $readonly . '>';

                if (is_array($select)) {
                    $values = $select;
                } else {
                    $values = (new $select())->getAll();
                }

                foreach ($values as $key => $value) {
                    if ($model->{$col_name} == $key) {
                        $html .= '<option value="' . $key . '" selected="">' . $value . '</option>';
                    } else {
                        $html .= '<option value="' . $key . '">' . $value . '</option>';
                    }
                }

                $html .= '</select>';
                break;
            case 'autocomplete':
                $data = '';
                foreach ($col_config['autocomplete'] as $datum) {
                    $data .= '"' . $datum . '",';
                }
                $this->javascriptCode[] = '
                    var availableTags = [' . $data . '];
                    console.log(availableTags);
                    $("#' . $name . '").autocomplete({
                        source: availableTags
                    });
                ';
                $html .= '<input class="form-control" type = "text" name = "' . $name . '" value = "' . $model->{$col_name} . '"' . $onChange . $required . $readonly . $othersParams . $restrictions . '/>';
                break;
            case DBSchema::TYPE_TEXT:
                $html .= '<textarea class="form-control" name = "' . $name . '"' . $required . $readonly . $othersParams . $onChange . ' > ' . $model->{$col_name} . '</textarea > ';
                break;
            case DBSchema::TYPE_DATE:
                $html .= '<div class="input-group" > '
                    . '    <span class="input-group-text" > '
                    . '        <i class="fa-solid fa-calendar fa-fw" ></i > '
                    . '    </span > '
                    . '    <input class="form-control" type = "date" name = "' . $name . '" value = "' . $model->{$col_name} . '" autocomplete = "off"' . $required . $readonly . $othersParams . $onChange . ' />'
                    . '</div > ';
                break;
            case DBSchema::TYPE_BOOLEAN:
                // Se crea un campo chk_nombre para el checkbox, y por ajax, se cambia el valor de nombre al hacerle click.
                $checked = $model->{$col_name} ? ' checked = ""' : '';
                $click = "$('#{$name}').val($('#chk_{$name}').is(':checked'));";
                $html .=
                    '<div class="checkbox">' .
                    '<input type="hidden" id="' . $name . '" name="' . $name . '" value="1">' .
                    '<input type="checkbox" id="chk_' . $name . '" onClick="' . $click . '"' . $checked . $readonly . $onChange . '/> ' .
                    $col_config['label'] . '</div>';
                break;
            case DBSchema::TYPE_INTEGER:
            case DBSchema::TYPE_FLOAT:
            case DBSchema::TYPE_DECIMAL:
                $html .= '<input class="form-control" type="number" step="any" name="' . $name . '" value="' . $model->{$col_name} . '" autocomplete="off"' . $required . $readonly . $othersParams . $onChange . '/>';
                break;
            case DBSchema::TYPE_STRING:
            case DBSchema::TYPE_TIME:
            case DBSchema::TYPE_DATETIME:
            default:
                if (!in_array($col_config['type'], DBSchema::TYPES['string'])) {
                    dump('El tipo ' . $col_config['type'] . ' no es un input reconocido en ' . $name);
                }
                $restrictions = '';
                if (isset($col_config['extradata'])) {
                    foreach ($col_config['extradata'] as $key => $value) {
                        $restrictions .= ' ' . $key . '="' . $value . '"';
                    }
                }

                $html .= '<input class="form-control" type="text" name="' . $name . '" value="' . $model->{$col_name} . '" autocomplete="off"' . $onChange . $required . $readonly . $othersParams . $restrictions . ' />';
                break;
        }

        /*
        switch ($col_config['type']) {
            case "button":
                break;
            case "checkbox":
            case 'bool':    // pseudotipo
            case "color":
                break;
            case "date":
            case "datetime-local":
                $datetime_local = '';
                if ($model->{$col_name}) {
                    $datetime_local = substr(date(DATE_ATOM, strtotime($model->{$col_name})), 0, 16);
                }
                $html .= '<div class="input-group">'
                    . '    <span class="input-group-text">'
                    . '        <i class="fa-solid fa-calendar fa-fw"></i>'
                    . '    </span>'
                    . '    <input class="form-control" type="datetime-local" name="' . $name . '" value="' . $datetime_local . '" autocomplete="off"' . $required . $readonly . $inputid . $onChange . '/>'
                    . '</div>';
                break;
            case "email":
                break;
            case "file":
                break;
            case "hidden":
                $html .= '<input class="form-control" type="hidden" name="' . $name . '" value="' . $model->{$col_name} . '" autocomplete="off"  ' . $inputid . $onChange . '/>';
                break;
            case "image":
                break;
            case "month":
                break;
            case "number":
            case "money":       // pseudotipo
            case "password":
                break;
            case "radio":
                break;
            case 'range':
                //Por defecto max=100, min=0, step = 1
                if (isset($col_config['extradata'] ['min'])) {
                    $min = ' min="' . $col_config['extradata'] ['min'] . '"';
                } else {
                    $min = '0';
                }
                if (isset($col_config['extradata'] ['max'])) {
                    $max = ' max="' . $col_config['extradata'] ['max'] . '"';
                } else {
                    $max = '100';
                }
                if (isset($col_config['extradata'] ['step'])) {
                    $step = ' step="' . $col_config['extradata'] ['step'] . '"';
                } else {
                    $step = '1';
                }
                $html .= '<input class="form-range" type="range" step="' . $step . '" name="' . $name . '" value="' . $model->{$col_name} . '" autocomplete="off"' . $required . $readonly . $min . $max . $step . $inputid . $onChange . '/>';

                break;
            case "reset":
                break;
            case "search":
                break;
            case "submit":
                break;
            case "tel":
                break;
            case "time":
                break;
            case "url":
                break;
            case "week":
                break;

            //Se añaden otros pseudotipos (select, textarea y autocomplete)
            case 'select':
                $html .= $this->show_select($name, $col_config, $model);
                break;
            case 'textarea':
            case 'autocomplete':
                $html .= '<input class="form-control" type="hidden" name="' . $col_name . '" value="' . $model->{$col_name} . '" autocomplete="off"  id="' . $col_config['extradata'] ['inputid'] . '" />';
                if (isset($col_config['extradata'] ['ac_noSuggestion'])) { //el mensaje que se mostrará si no se encuentran coincidencias
                    $noSuggestion = "noSuggestionNotice: \"" . $col_config['extradata'] ['ac_noSuggestion'] . "\",";
                } else {
                    $noSuggestion = '';
                }
                $html .= '<script type="text/javascript">
                    $( document ).ready(function() {
                        $( "#ac_' . $col_name . '" ).autocomplete({
                            serviceUrl: "' . $col_config['extradata'] ['ac_url'] . '", //la url con el ajax de respuesta para los posibles valores
                            dataType: "json",
                            minLength: 2,
                            deferRequestBy: 200,
                            paramName: "query",
                            showNoSuggestionNotice: true,
                            onSearchStart: function(params) {
                                $("#' . $col_config['extradata'] ['inputid'] . '").val("");
                            },
                            ' . $noSuggestion . '
                            onSelect: function(suggestion) {
                               $("#' . $col_config['extradata'] ['inputid'] . '").val(suggestion.id);
                            }
                        })

                ';
                $html .= '
                    $.get( "' . $col_config['extradata'] ['ac_url'] . '",{ "get": "' . $model->{$col_name} . '" })  //pedimos el texto que deberia tener el campo con ese valor para ponerlo en el input correspondiente
                        .done(function (data) {
                            $( "#ac_' . $col_name . '" ).val(data);
                        });
                ';
                $html .= '
                    });
                    </script>
                ';
                $name = "ac_" . $col_name; //si es de tipo autocomplete, el input mostrado se le pone otro nombre
                $inputid = ' id="ac_' . $col_config['extradata'] ['inputid'] . '"'; //y algo similar hacemos con el id
                break;

            case "text":
            default:
        }
        */

        $html .= '</div>';
        $html .= '</div>';
        return $html;
    }

    /**
     * Retorna una instancia del controlador principal sobre el que se realiza
     * el mantenimiento de datos
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0812
     *
     * @return XnetModel
     */
    abstract public function getMainModel(): XnetModel;

    /**
     * Retorna un array asociativo en el que la clave es el nombre del campo
     * a mostrar, y el valor, el literal del título de la columna.
     *
     * Por ejemplo:
     *
     * return ['id' => 'Clave', 'name' => 'Nombre'];
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0812
     *
     * @return array
     */
    abstract public function getHeaders(): array;

    /**
     * Retorna un array con los campos de edición.
     * Cada campo será un array asociativo en el que la clave es el nombre del
     * campo, y el valor un array con cada uno de los parámetros necesarios para
     * la edición.
     *
     * Para facilitar la definición de dicho array, utilice el método setFields()
     *
     * Vea como ejemplo soporte_empresas/Controller/Countries.php
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0812
     *
     * @return array
     */
    public function getFields(): array
    {
        // Si la caché está desactivada, se retorna vacío para que se vuelva a regenerar.
        if ($this->disableYamlCache) {
            return [];
        }
        return XnetPhpFileCache::loadYamlFile(self::PHP_CACHE_FOLDER, $this->mainModel->tablename());
    }

    private function getFlatFields(): array
    {
        $fields = [];
        foreach ($this->fields as $field) {
            $fields = array_merge($fields, $field);
        }
        return $fields;
    }

    private static function getXml($tablename)
    {
        XnetModel::get_xml_table($tablename, $xml, $constraints);
        $result = [];
        foreach ($xml as $value) {
            $result[$value['nombre']] = $value;
        }
        return $result;
    }

    /**
     * Completa toda la información sobre los campos desde los datos que le pasan y los que puede
     * obtener de la tabla de la base de datos y los xml.
     * Una vez obtenidos los datos, los cachea en un archivo yaml.
     *
     * TODO: Parte de éste código, debe de ser procesado en el modelo.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0818
     *
     * @param $tablename
     * @param $array
     *
     * @return array
     */
    protected function setFields($tablename, $array): array
    {
        $data = DBSchema::getColumnsAssociativeArray($tablename);
        $xml = static::getXml($tablename);

        $result = [];
        foreach ($array as $row_key => $row) {
            foreach ($row as $key => $value) {
                $dataInfo = [];

                if (!isset($row['type'])) {
                    $row[$row_key]['type'] = $data[$key]['type'];
                }

                // El tipo boolean, es el único que se guarda con un nombre diferente tinyint(1)
                $xmlType = $xml[$key]['tipo'];
                if ($xmlType !== 'boolean') {
                    $xmlType = $data[$key]['type'];
                }
                $type = DBSchema::splitType($xmlType);
                $genericType = DBSchema::getTypeOf($type['type']);
                $dataInfo['generictype'] = $genericType;
                $dataInfo['type'] = $type['type'];

                if (isset($value['required']) && $value['required']) {
                    $dataInfo['required'] = true;
                }

                if (isset($xml[$key]['defecto'])) {
                    $dataInfo['default'] = trim($xml[$key]['defecto'], " \"'`");
                }

                $othersParameters = ['pattern', 'placeholder', 'select', 'autocomplete'];
                foreach ($othersParameters as $parameter) {
                    if (isset($value[$parameter])) {
                        $dataInfo[$parameter] = $value[$parameter];
                    }
                }

                switch ($genericType) {
                    case 'string':
                        if (!isset($value['maxlength']) || (int) $value['maxlength'] > (int) $type['length']) {
                            $value['maxlength'] = (int) $type['length'];
                        }
                        $dataInfo['maxlength'] = $type['length'];
                        break;
                    case 'integer':
                        /**
                         * Lo primero es ver la capacidad física máxima según el tipo de dato.
                         */
                        $bytes = 4;
                        switch ($type['type']) {
                            case 'tinyint':
                                $bytes = 1;
                                break;
                            case 'smallint':
                                $bytes = 2;
                                break;
                            case 'mediumint':
                                $bytes = 3;
                                break;
                            case 'int':
                                $bytes = 4;
                                break;
                            case 'bigint':
                                $bytes = 8;
                                break;
                        }
                        $bits = 8 * (int) $bytes;
                        $physicalMaxLength = 2 ** $bits;

                        /**
                         * $minDataLength y $maxDataLength contendrán el mínimo y máximo valor que puede contener el campo.
                         */
                        $unsigned = isset($type['unsigned']) && $type['unsigned'] === 'yes';
                        $minDataLength = $unsigned ? 0 : -$physicalMaxLength / 2;
                        $maxDataLength = ($unsigned ? $physicalMaxLength : $physicalMaxLength / 2) - 1;

                        /**
                         * De momento, se asignan los límites máximos por el tipo de dato.
                         * En $min y $max, iremos arrastrando los límites conforme se vayan comprobando.
                         * $min nunca podrá ser menor que $minDataLength.
                         * $max nunca podrá ser mayor que $maxDataLength.
                         */
                        $min = $minDataLength;
                        $max = $maxDataLength;

                        /**
                         * Se puede hacer una limitación física Se puede haber definido en el xml un min y un max.
                         * A todos los efectos, lo definido en el XML como min o max se toma como limitación
                         * física del campo.
                         * TODO: Habría que controlar en el modelo que esos límites no se sobrepasen.
                         */
                        if (isset($xml[$key]['min'])) {
                            $minXmlLength = $xml[$key]['min'];
                            if ($minXmlLength > $minDataLength) {
                                $min = $minXmlLength;
                            } else {
                                debug_message("{$tablename} ({$key}): Se ha especificado un min {$minXmlLength} en el XML, pero por el tipo de datos, el mínimo es {$minDataLength}.");
                            }
                        }
                        if (isset($xml[$key]['max'])) {
                            $maxXmlLength = $xml[$key]['max'];
                            if ($maxXmlLength < $maxDataLength) {
                                $max = $maxXmlLength;
                            } else {
                                debug_message("{$tablename} ({$key}): Se ha especificado un min {$maxXmlLength} en el XML, pero por el tipo de datos, el máximo es {$maxDataLength}.");
                            }
                        }

                        /**
                         * Se puede hacer una limitación desde el controlador, pero no debe de sobreapasar
                         * los límites anteriores en ningún caso.
                         */
                        if (isset($value['min'])) {
                            $minControllerLength = $value['min'];
                            if ($minControllerLength > $min) {
                                $min = $minControllerLength;
                            } else {
                                if ($minControllerLength < $minDataLength) {
                                    debug_message("{$tablename} ({$key}): El valor mínimo es de {$minDataLength}, pero en el controlador se limita a {$minControllerLength}.");
                                } elseif (isset($minXmlLength) && $minControllerLength < $minXmlLength) {
                                    debug_message("{$tablename} ({$key}): Ha sido limitado en el XML a {$minXmlLength}, pero en el controlador se limita a {$minControllerLength}.");
                                }
                            }
                        }
                        if (isset($value['max'])) {
                            $maxControllerLength = $value['max'];
                            if ($maxControllerLength < $max) {
                                $max = $maxControllerLength;
                            } else {
                                if ($maxControllerLength > $maxDataLength) {
                                    debug_message("{$tablename} ({$key}): El valor máximo es {$maxDataLength}, pero en el controlador se limita a {$maxControllerLength}.");
                                } elseif (isset($maxXmlLength) && $maxControllerLength > $maxXmlLength) {
                                    debug_message("{$tablename} ({$key}): Ha sido limitado en el XML a {$maxXmlLength}, pero en el controlador se limita a {$maxControllerLength}.");
                                }
                            }
                        }

                        /**
                         * Length es el número que está entre paréntesis al definir el tipo:
                         * por ejemplo 2 en tinyint(2).
                         * MariaDB asigna un valor si no lo haces tú en el xml.
                         * Sólo se tendrá en cuenta, si recorta el tipo máximo.
                         * Se avisa si length sobrepasa la capacidad física.
                         *
                         * El dato enter paréntesis al definir un campo, sólo influye en la visualización
                         * del campo, no en el tamaño de su contenido, que depende del tipo.
                         * https://stackoverflow.com/questions/12839927/mysql-tinyint-2-vs-tinyint1-what-is-the-difference
                         *
                         * |     | (1) | (2) | (3) |
                         * +-----+-----+-----+-----+
                         * | 1   | 1   |  1  |   1 |
                         * | 10  | 10  | 10  |  10 |
                         * | 100 | 100 | 100 | 100 |
                         */
                        if (isset($type['length'])) {
                            $maxViewLength = 10 ** $type['length'] - 1;
                            if ($maxViewLength < $max) {
                                debug_message("{$tablename} ({$key}): Se ha definido como {$xmlType}, y el valor máximo es {$maxXmlLength}.");
                            }
                        }

                        /**
                         * Es posible que en el controlado se haya especificado un min y un max.
                         * Si existe alguno de ellos, se toma ese como mínimo o máximo.
                         */
                        if (isset($type['min'])) {
                            $min = $type['min'];
                        }
                        if (isset($type['max'])) {
                            $max = $type['max'];
                        }

                        $dataInfo['min'] = $min;
                        $dataInfo['max'] = $max;
                        break;
                    default:
                        // ???
                }
                $result[$row_key][$key] = static::setField($key, $dataInfo);
            }
        }

        if (!XnetPhpFileCache::saveYamlFile(self::PHP_CACHE_FOLDER, $tablename, $result)) {
            debug_message('Se ha producido un error al guardar el archivo ' . $tablename);
        }

        /**
         * Para evitar que pueda haber un error al utilizar siempre el dato calculado,
         * se fuerza a cargar el yaml generado y utilizarlo en lugar de dicho dato calculado.
         */
        if ($this->disableYamlCache) {
            $result = XnetPhpFileCache::loadYamlFile(self::PHP_CACHE_FOLDER, $tablename);
        }

        return $result;
    }

    protected function setField(string $name, array $fieldData): array
    {
        $return = [];

        /**
         * Formato del array:
         *  - num_cols
         *  - label
         *  - type
         */

        $return['class'] = $fieldData['class'] ?? 'col-sm-6';
        unset($fieldData['class']);

        $return['label'] = $fieldData['label'] ?? $this->getHeaders()[$name] ?? $name;
        unset($fieldData['label']);

        $return['generictype'] = $fieldData['generictype'] ?? 'string';
        unset($fieldData['generictype']);

        $return['type'] = $fieldData['type'] ?? 'text';
        unset($fieldData['type']);

        $return['maxlength'] = $fieldData['maxlength'] ?? 'text';
        unset($fieldData['maxlength']);

        foreach (['min', 'max', 'step', 'pattern', 'placeholder', 'select', 'autocomplete', 'required', 'default'] as $field) {
            if (isset($fieldData[$field])) {
                $return[$field] = $fieldData[$field];
                unset($fieldData[$field]);
            }
        }

        if (constant('FS_DEBUG') && count($fieldData) > 0) {
            dump([
                'No se ha utilizado en ' . $this->getMainModel()->tablename() . ', ' . $name,
                $fieldData,
            ]);
        }

        return $return;
    }

    protected function prePrivateCore(): bool
    {
        $this->protectClose = true;
        $this->template = 'master/generic_list';

        $this->code = null;
        if (empty($this->action) || in_array($this->action, ['View'])) {
            $this->code = $this->filterRequest('code');
        }

        return parent::prePrivateCore();
    }

    /**
     * Código que se ejecutará en la parte privada
     */
    protected function privateCore(): bool
    {
        if ($this->code !== null) {
            // Si está vacío es que es una solicitud de alta
            if (!empty($this->code)) {
                $this->mainModel = $this->mainModel::getById($this->code);
            }
            $this->template = 'master/generic_edit';
        }

        return true;
    }

    protected function actionNew()
    {
        $this->new_message('Nuevo registro');
        $this->template = 'master/generic_edit';
        return true;
    }

    protected function doCancel()
    {
        $this->new_message('Se han descartado los cambios');
        $this->code = $_POST['record_code'];
        // Si $code está vacío, se ha descartado un alta
        if (!empty($this->code)) {
            $this->mainModel = $this->mainModel->get($this->code);
        }
        $this->template = 'master/generic_edit';
        return true;
    }

    protected function doBack()
    {
        return true;
    }

    protected function doRefresh()
    {
        return $this->doCancel();
    }

    protected function actionDelete()
    {
        $code = $_REQUEST['code'];
        $this->template = 'master/generic_list';
        $record = $this->mainModel->getById($code);
        if ($record !== null) {
            $ok = $record->delete();
            if ($ok) {
                $this->new_message('Registro "' . $record->get_name() . '" eliminado correctamente');
            } else {
                $this->new_error_msg('No se ha podido eliminar el registro ' . $record->get_name());
            }
            return $ok;
        }
        $this->new_error_msg('No se ha encontrado el registro con ćodigo ' . $code);
        return false;
    }

    protected function actionSave()
    {
        $this->fields = $this->getFields();

        $recordCode = self::filterRequest('record_code');

        $record = $this->mainModel;
        if (!empty($recordCode)) {
            $record = $this->mainModel::getById($recordCode);
            if (!isset($record)) {
                $this->new_error_msg('No se ha encontrado el registro ' . $recordCode);
                return false;
            }
        }

        // TODO (ahora está en $this->fields, pero lo lógico es que $this->fields no sea flat.
        $fields = $this->getFlatFields();

        foreach (get_object_vars($record) as $var => $value) {
            $type = $fields[$var]['generictype'] ?? 'string';
            if (isset($_REQUEST[$var])) {
                switch ($type) {
                    case DBSchema::TYPE_BOOLEAN:
                        $record->{$var} = ($_REQUEST[$var] === 'true');
                        break;
                    default:
                        $record->{$var} = $_REQUEST[$var];
                }
            }
        }

        if (!$record->save()) {
            $this->new_error_msg('Se ha producido un error al guardar el registro');
            $response = $record->getResponse();
            if (isset($response->errors)) {
                foreach ($response->errors as $error) {
                    $this->new_error_msg($error);
                }
            }
            return false;
        }

        $this->new_message('Datos actualizados correctamente.');
        return true;
    }

    /**
     * Devuelve un string aleatorio de longitud $length
     *
     * @param int $length la longitud del string
     *
     * @return string la cadena aleatoria
     */
    public function random_string($length = 30)
    {
        return mb_substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
    }

    public function anterior_url()
    {
        return '';
    }

    public function siguiente_url()
    {
        return '';
    }

}
