<?php

/*
 * This file is part of MiFactura.eu
 * Copyright (C) 2021 X-Net Software Solutions S.L. <xnetsoftware@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace Xnet\Core;

use fs_page;
use Symfony\Component\Yaml\Yaml;
use Xnet\Model\Action;
use Xnet\Model\ActionRole;
use Xnet\Model\Controller;
use Xnet\Model\Role;
use Xnet\Model\RoleUser;
use Xnet\Model\User;
use Xnet\Model\Version;

/**
 * Class XnetPolicy
 *
 * Gestiona las políticas de acceso y permisos de los usuarios según rol y página.
 *
 * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
 * @version 2022.0805
 *
 * @package Xnet\Core
 */
class XnetPolicy
{
    private static $policy;
    private static $menu;
    private static $arrayMenu;
    private static $accesses;

    public static function getPath(): string
    {
        return constant('BASE_PATH') . '/tmp/' . constant('FS_TMP_NAME');
    }

    public static function yamlCacheDisabled(): bool
    {
        return defined('DISABLE_YAML_CACHE') && constant('DISABLE_YAML_CACHE');
    }

    /**
     * Genera un array con los permisos del usuario.
     * Es un array en el que el primer elemento "default_if_not_defined" es un boolean,
     * y el resto es uno por cada controlador que tenga permisos definidos.
     * Cada elemento tiene un array clave valor boolean con la acción para el controlador.
     *
     * Para saber si un usuario tiene permisos, habría que ver primero si está definido
     * algún valor para el $result[controlador,acción].
     * Si está definida, si es true tendrá acceso, y si es false no.
     * Si no está definida, tendrá acceso si default_if_not_defined es true.
     *
     * El valor de default_if_not_defined es el valor por defecto del último rol cargado.
     *
     * NOTA: Se genera un fichero de trabajo con clave 0 en lugar de la clave de usuario,
     * que contiene todos los permisos sin tener en cuenta ni roles ni usuarios.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0822
     *
     * @param string $userid
     *
     * @return array
     */
    public static function createPolicyFile(string $userid)
    {
        // TODO: Ésto, igual ha de configurarse en otro sitio, aquí ya no hace falta
        $result = XnetPhpFileCache::loadYamlFile(XnetConfig::PHP_CACHE_FOLDER, 'actions');
        if (empty($result)) {
            $result = [];
            $actions = Action::getAll();
            foreach ($actions->data as $action) {
                $result[$action['controller_name']][$action['name']] = true;
            }
            if (XnetPhpFileCache::saveYamlFile(XnetConfig::PHP_CACHE_FOLDER, 'actions', $result)) {
                debug_message('Se ha producido un error al guardar los datos de caché de las políticas de admin');
            }
        }

        new RoleUser(); // Por si la tabla no existe

        $actions = [];
        $roles = RoleUser::getRolesByUser($userid);
        foreach ($roles as $role) {
            $actions['default_if_not_defined'] = $role->default;

            $accesses = $role->getAccesses();
            foreach ($accesses as $access) {
                $actions[$access['action']->controller_name][$access['action']->name] = $access['authorized'];
            }
        }
        return $actions;
    }

    public static function getFolders($folder)
    {
        $file = $folder . '/dataconfig/folders.php';
        if (!file_exists($file)) {
            return [];
        }
        return include($file);
    }

    public static function createFoldersFile()
    {
        $pluginsActivos = Version::getPlugins();
        $folders = self::getFolders(constant('FS_FOLDER'));
        foreach ($pluginsActivos as $plugin) {
            $other_folder = self::getFolders(constant('FS_FOLDER') . '/plugins/' . $plugin);
            $folders = array_merge($folders, $other_folder);
        }
        return $folders;
    }

    public static function createMenuFile(string $userId)
    {
        // Cargamos la estructura de carpetas del menú
        $folders = XnetPhpFileCache::loadYamlFile(XnetConfig::PHP_CACHE_FOLDER, 'folders');
        if (empty($folders) || self::yamlCacheDisabled()) {
            $folders = self::createFoldersFile();
            XnetPhpFileCache::saveYamlFile(XnetConfig::PHP_CACHE_FOLDER, 'folders', $folders);
        }

        $result = [];
        $user = User::getById($userId);
        $menu = (new \fs_page())->all();
        foreach ($menu as $option) {
            if (!$option->show_on_menu) {
                continue;
            }

            if (!XnetPolicy::accessTo($user, $option->name, 'View')) {
                continue;
            }

            $item = [];

            // Creo que ésto no sería necesario...
            $folder = $folders[$option->folder] ?? [];
            $item['icon'] = $folder['icon'];
            $item['icon_open'] = $folder['icon_open'];

            $item['folder'] = $option->folder;
            $item['name'] = $option->name;
            $item['title'] = $option->title;
            $item['alias'] = $option->alias;
            $item['description'] = $option->description;
            $item['order'] = $option->order;

            $index = $option->folder . '|' . str_pad($option->orden, '5', '0', STR_PAD_LEFT) . $option->name;

            $result[$index] = $item;
        }

        $menu = Controller::getAll();
        foreach ($menu->data as $option) {
            if (!$option['show_on_menu']) {
                continue;
            }

            if (!XnetPolicy::accessTo($user, $option['name'], 'View')) {
                continue;
            }

            $item = [];

            // Creo que ésto no sería necesario...
            $folder = $folders[$option['folder']] ?? [];
            $item['icon'] = $folder['icon'];
            $item['icon_open'] = $folder['icon_open'];

            $item['folder'] = $option['folder'];
            $item['name'] = $option['name'];
            $item['title'] = $option['title'];
            $item['alias'] = $option['alias'];
            $item['description'] = $option['description'];
            $item['order'] = $option['order'];

            $index = $option['folder'] . '|' . str_pad($option['order'], '5', '0', STR_PAD_LEFT) . $option['name'];
            $result[$index] = $item;
        }

        return $result;
    }

    public static function createArrayMenuFile(string $userId)
    {
        $menu = self::createMenuFile($userId);

        $result = [];
        foreach ($menu as $page) {
            $levels = explode('|', $page['folder']);
            if (count($levels) > 4) {
                debug_message('Número de niveles excedidos en ' . $page['name'] . ': ' . $page['folder']);
                continue;
            }
            if (isset($levels[2])) {
                $result[$levels[0]][$levels[0] . '|' . $levels[1]][$levels[0] . '|' . $levels[1] . '|' . $levels[2]][$page['name']] = $page;
            } elseif (isset($levels[1])) {
                $result[$levels[0]][$levels[0] . '|' . $levels[1]][$page['name']] = $page;
            } else {
                $result[$levels[0]][$page['name']] = $page;
            }
        }
        return $result;
    }

    private static function loadYamlFile(string $folder, string $userId, bool $yamlCacheDisabled = false): array
    {
        $filename = $folder . $userId;
        $result = XnetPhpFileCache::loadYamlFile($folder, $filename);
        if (empty($result) || $yamlCacheDisabled) {
            if ($yamlCacheDisabled) {
                $originalResult = $result;
            }
            $result = self::{'create' . ucfirst($folder) . 'File'}($userId);
            if (!XnetPhpFileCache::saveYamlFile($folder, $filename, $result)) {
                debug_message('Se ha producido un error al guardar los datos de caché ' . $filename);
            }

            /**
             * Si se deshabilita la caché para que el fichero se actualice cada vez,
             * también es importante cargarlo en lugar de utilizar lo obtenido, por si
             * hay algún error en la conversión a yaml y su restauración.
             */
            if ($yamlCacheDisabled) {
                $result = XnetPhpFileCache::loadYamlFile($folder, $filename);
            }
        }
        return $result;
    }

    /**
     * Carga la información sobre las políticas aplicadas al usuario.
     * En función de sus roles, tendrá unos determinados permisos.
     * Este método lee la información de los archivos yaml, y si no
     * existen, los genera.
     *
     * Recopila la siguiente información:
     * - XnetPolicy::$policy es un array con los permisos del usuario.
     * - XnetPolicy::$menu es un array con las opciones del menú del usuario.
     * - XnetPolicy::$arraymenu es un array con las opciones del menú del usuario.
     *
     * TODO: No estoy seguro de que ambos menús sean necesarios.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0819
     *
     * @param User $user
     *
     * @return bool
     */
    public static function loadUserData(User $user): bool
    {
        $yamlCacheDisabled = self::yamlCacheDisabled();

        self::$policy = self::loadYamlFile('policy', $user->get_id(), $yamlCacheDisabled);
        self::$menu = self::loadYamlFile('menu', $user->get_id(), $yamlCacheDisabled);
        self::$arrayMenu = self::loadYamlFile('arrayMenu', $user->get_id(), $yamlCacheDisabled);

        return true;
    }

    public static function getMenu()
    {
        return self::$menu;
    }

    public static function getArrayMenu()
    {
        return self::$arrayMenu;
    }

    /**
     * Retorna si $user tiene acceso a la $actionName del $controller.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0812
     *
     * @param User       $user
     * @param Controller $controller
     * @param string     $actionName
     *
     * @return bool
     */
    public static function accessTo(User $user, string $controllerName, string $actionName): bool
    {
        if ($user->admin) {
            return true;
        }

        new RoleUser();
        new ActionRole();

        $action = Action::getByName($controllerName, $actionName);

        $userRoles = RoleUser::getRolesByUser($user->id);
        $canAccess = null;
        foreach ($userRoles as $userRole) {
            $canAccess = $userRole->getAccessTo($action->get_id(), $canAccess);
        }

        return $canAccess ?? false;
    }
}
