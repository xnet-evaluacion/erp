<?php
/*
 * This file is part of plugin xnetcommonhelpers for MiFactura.eu
 * Copyright (C) 2022 X-Net Software Solutions S.L. <xnetsoftware@gmail.com>
 */

namespace Xnet\Core;

use Xnet\Model\Controller;

abstract class XnetAjaxController extends XnetAppController
{
    public $result;
    public $db;

    public function __construct()
    {
        parent::__construct();

        $this->template = null;
        $this->db = new \fs_db2();
        $this->db->connect();
        $this->result = [];
        $this->run();
    }

    protected function privateCore(): bool
    {
        return true;
    }

    public function run()
    {
        $this->privateCore();

        json_response($this->result);
    }
}
