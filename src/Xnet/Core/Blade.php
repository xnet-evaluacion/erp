<?php
namespace Xnet\Core;

use Illuminate\Container\Container;
use Illuminate\Contracts\Container\Container as ContainerInterface;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory as FactoryContract;
use Illuminate\Contracts\View\View;
use Illuminate\Events\Dispatcher;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\Facade;
use Illuminate\View\Compilers\BladeCompiler;
use Illuminate\View\Factory;
use Illuminate\View\ViewServiceProvider;

/**
 * Class Blade
 *
 * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
 * @version 2021.12
 *
 */
class Blade implements FactoryContract
{
    /**
     * Directorio principal de las plantillas
     */
    const TEMPLATES = 'Templates';

    /**
     * Cada una de los subdirectorios que hay que incluir por cada plugin activo y núcleo
     */
    const ROUTES = [
        '',
    ];

    /**
     * TODO: Missing documentation.
     *
     * @var Application
     */
    protected $container;

    /**
     * TODO: Missing documentation.
     *
     * @var Factory
     */
    private $factory;

    /**
     * TODO: Missing documentation.
     *
     * @var BladeCompiler
     */
    private $compiler;

    /**
     * TODO: Missing documentation.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2021.12
     *
     * @param                         $viewPaths
     * @param string                  $cachePath
     * @param ContainerInterface|null $container
     */
    public function __construct($viewPaths, string $cachePath, ContainerInterface $container = null)
    {
        $this->container = $container ?: new Container;

        $this->setupContainer((array) $viewPaths, $cachePath);
        (new ViewServiceProvider($this->container))->register();

        $this->factory = $this->container->get('view');
        $this->compiler = $this->container->get('blade.compiler');
    }

    /**
     * Obtiene un array con todas las rutas de búsqueda.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2021.12
     *
     * @return array
     */
    static function getRoutes()
    {
        $return = [];
            foreach (XnetConfig::getEnabledPlugins() as $plugin_dir) {
                $return = array_merge($return, self::getSubroutes('plugins/' . $plugin_dir . '/' . self::TEMPLATES));
            }
        return array_merge($return, self::getSubroutes(self::TEMPLATES));
    }

    /**
     * TODO: Missing documentation.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2021.12
     *
     * @param $mainRoute
     *
     * @return array
     */
    static private function getSubroutes($mainRoute)
    {
        $return = [];
        foreach (self::ROUTES as $route) {
            if (empty($route)) {
                $return[] = $mainRoute;
            } else {
                $return[] = $mainRoute . '/' . $route;
            }
        }
        return $return;
    }

    /**
     * TODO: Missing documentation.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2021.12
     *
     * @param string $view
     * @param array  $data
     * @param array  $mergeData
     *
     * @return string
     */
    public function render(string $view, array $data = [], array $mergeData = []): string
    {
        return $this->make($view, $data, $mergeData)
            ->render();
    }

    /**
     * TODO: Missing documentation.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2021.12
     *
     * @param $view
     * @param $data
     * @param $mergeData
     *
     * @return View
     */
    public function make($view, $data = [], $mergeData = []): View
    {
        return $this->factory->make($view, $data, $mergeData);
    }

    /**
     * TODO: Missing documentation.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2021.12
     *
     * @return BladeCompiler
     */
    public function compiler(): BladeCompiler
    {
        return $this->compiler;
    }

    /**
     * TODO: Missing documentation.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2021.12
     *
     * @param string   $name
     * @param callable $handler
     */
    public function directive(string $name, callable $handler)
    {
        $this->compiler->directive($name, $handler);
    }

    /**
     * TODO: Missing documentation.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2021.12
     *
     * @param          $name
     * @param callable $callback
     */
    public function if($name, callable $callback)
    {
        $this->compiler->if($name, $callback);
    }

    /**
     * TODO: Missing documentation.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2021.12
     *
     * @param $view
     *
     * @return bool
     */
    public function exists($view): bool
    {
        return $this->factory->exists($view);
    }

    /**
     * TODO: Missing documentation.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2021.12
     *
     * @param $path
     * @param $data
     * @param $mergeData
     *
     * @return View
     */
    public function file($path, $data = [], $mergeData = []): View
    {
        return $this->factory->file($path, $data, $mergeData);
    }

    /**
     * TODO: Missing documentation.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2021.12
     *
     * @param $key
     * @param $value
     *
     * @return mixed
     */
    public function share($key, $value = null)
    {
        return $this->factory->share($key, $value);
    }

    /**
     * TODO: Missing documentation.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2021.12
     *
     * @param $views
     * @param $callback
     *
     * @return array
     */
    public function composer($views, $callback): array
    {
        return $this->factory->composer($views, $callback);
    }

    /**
     * TODO: Missing documentation.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2021.12
     *
     * @param $views
     * @param $callback
     *
     * @return array
     */
    public function creator($views, $callback): array
    {
        return $this->factory->creator($views, $callback);
    }

    /**
     * TODO: Missing documentation.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2021.12
     *
     * @param $namespace
     * @param $hints
     *
     * @return $this
     */
    public function addNamespace($namespace, $hints): self
    {
        $this->factory->addNamespace($namespace, $hints);

        return $this;
    }

    /**
     * TODO: Missing documentation.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2021.12
     *
     * @param $namespace
     * @param $hints
     *
     * @return $this
     */
    public function replaceNamespace($namespace, $hints): self
    {
        $this->factory->replaceNamespace($namespace, $hints);

        return $this;
    }

    /**
     * TODO: Missing documentation.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2021.12
     *
     * @param string $method
     * @param array  $params
     *
     * @return false|mixed
     */
    public function __call(string $method, array $params)
    {
        return call_user_func_array([$this->factory, $method], $params);
    }

    /**
     * TODO: Missing documentation.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2021.12
     *
     * @param array  $viewPaths
     * @param string $cachePath
     */
    protected function setupContainer(array $viewPaths, string $cachePath)
    {
        $this->container->bindIf('files', function () {
            return new Filesystem;
        }, true);

        $this->container->bindIf('events', function () {
            return new Dispatcher;
        }, true);

        $this->container->bindIf('config', function () use ($viewPaths, $cachePath) {
            return [
                'view.paths' => $viewPaths,
                'view.compiled' => $cachePath,
            ];
        }, true);

        Facade::setFacadeApplication($this->container);
    }
}
