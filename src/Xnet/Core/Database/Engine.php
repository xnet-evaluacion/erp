<?php
/*
 * This file is part of MiFactura.eu
 * Copyright (C) 2021 X-Net Software Solutions S.L. <xnetsoftware@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace Xnet\Core\Database;

use DebugBar\DataCollector\PDO as PDODataCollector;
use DebugBar\DebugBarException;
use Exception;
use PDO;
use PDOException;
use Xnet\Core\DBSchema;
use Xnet\Core\XnetCache;
use Xnet\Core\XnetDebugBar;
use Xnet\Core\XnetLog;

/**
 * Es el tipo utilizado para los índices
 * TODO: Habría que probar si en Posgres funciona ésto, o hay que utilizar
 * define('DB_INDEX_TYPE', constant('FS_DB_INTEGER'));
 */
define('DB_INDEX_TYPE', 'bigint(20) unsigned');

/**
 * Description of fs_db_engine
 */
abstract class Engine
{
    /**
     * Array asociativo con la configuración de la base de datos.
     *
     * @var array
     */
    protected static $config;

    /**
     * Cadena DSN para conectar a PDO.
     *
     * @var string
     */
    protected static $dsn;

    /**
     * Permite conectar e interactuar con memcache.
     *
     * @var XnetCache
     */
    protected static $cache;

    /**
     * Almacena los índices para evitar re-consultar los valores.
     *
     * @var array
     */
    protected static $indexes;

    /**
     * Almacena las constraints para evitar re-consultar los valores.
     *
     * @var array
     */
    protected static $constraints;

    /**
     * Gestiona el log de todos los controladores, modelos y base de datos.
     *
     * @var XnetLog
     */
    protected static $coreLog;

    /**
     * El enlace con la base de datos.
     *
     * @var false|mysqli|resource
     */
    protected static $link;
    protected static $dbHandler;
    protected static $pdo;
    protected static $pdoCollector;
    protected static $statement;

    /**
     * Número de selects ejecutados.
     *
     * @var int
     */
    protected static $t_selects;

    /**
     * Número de transacciones ejecutadas.
     *
     * @var int
     */
    protected static $t_transactions;

    /**
     * Barra de depuración.
     *
     * @var XnetDebugBar
     */
    public static $debugBar;

    public static $errors = [];

    /**
     * fs_db_engine constructor.
     */
    public function __construct($config)
    {
        self::$config = $config;
        if (!isset(self::$link)) {
            self::$t_selects = 0;
            self::$t_transactions = 0;
            self::$coreLog = new XnetLog();
            self::$cache = new XnetCache();
            self::$debugBar = new XnetDebugBar();
        }
    }

    /**
     * Inicia una transacción SQL.
     *
     * @return bool
     */
    abstract public function begin_transaction();

    /**
     * Realiza comprobaciones extra a la tabla.
     *
     * @param string $table_name
     *
     * @return bool
     */
    abstract public function check_table_aux($table_name);

    /**
     * Desconecta de la base de datos.
     *
     * @return bool
     */
    abstract public function close();

    /**
     * Guarda los cambios de una transacción SQL.
     *
     * @return bool
     */
    abstract public function commit();

    /**
     * Compara dos arrays de columnas, devuelve una sentencia SQL en caso de encontrar diferencias.
     *
     * @param string $table_name
     * @param array  $xml_cols
     * @param array  $db_cols
     *
     * @return string
     */
    abstract public function compare_columns($table_name, $xml_cols, $db_cols);

    /**
     * Compara dos arrays de restricciones, devuelve una sentencia sql en caso de encontrar diferencias.
     *
     * @param string $table_name
     * @param array  $xml_cons
     * @param array  $db_cons
     * @param bool   $delete_only
     *
     * @return string
     */
    abstract public function compare_constraints($table_name, $xml_cons, $db_cons, $delete_only = false);

    /**
     * Conecta a la base de datos.
     *
     * @return bool
     */
    public static function connect(): bool
    {
        if (self::$dbHandler !== null) {
            self::$debugBar->addMessage('SQL', 'PDO: Already connected ' . self::$dsn);
            return true;
        }

        self::$debugBar->addMessage('SQL', 'PDO: ' . self::$dsn);

        try {
            // Logs SQL queries. You need to wrap your PDO object into a DebugBar\DataCollector\PDO\TraceablePDO object.
            // http://phpdebugbar.com/docs/base-collectors.html
            self::$pdo = new PDO(self::$dsn, self::$config['dbUser'], self::$config['dbPass'], self::$config['dbOptions']);
            //self::$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            self::$dbHandler = new PDODataCollector\TraceablePDO(self::$pdo);
            self::$pdoCollector = new PDODataCollector\PDOCollector(self::$dbHandler);
            self::$debugBar->getDebugTool()->addCollector(self::$pdoCollector);
        } catch (PDOException $e) {
            if (constant('FS_DEBUG')) {
                dump([
                    'Error' => 'PDOException',
                    'Code' => $e->getCode(),
                    'Message' => $e->getMessage(),
                ]);
            }
            die();
            return false;
        } catch (DebugBarException $e) {
            if (constant('FS_DEBUG')) {
                dump([
                    'Error' => 'DebugBarException',
                    'Code' => $e->getCode(),
                    'Message' => $e->getMessage(),
                ]);
            }
            die();
            return false;
        } catch (Exception $e) {
            if (constant('FS_DEBUG')) {
                dump([
                    'Error' => 'Exception',
                    'Code' => $e->getCode(),
                    'Message' => $e->getMessage(),
                ]);
            }
            die();
            return false;
        }
        return isset(self::$dbHandler);
    }

    public static function disconnect(): bool
    {
        if (self::$dbHandler !== null) {
            if (self::$dbHandler->exec('KILL CONNECTION_ID();')) {
                self::$debugBar->addMessage('SQL', 'PDO: disconnected ' . self::$dsn);
            }
            self::$dbHandler = null;
            return true;
        }
        return true;
    }

    /**
     * Devuelve el estilo de fecha del motor de base de datos.
     *
     * @return string
     */
    abstract public function date_style();

    /**
     * Escapa las comillas de la cadena de texto.
     *
     * @param string $str
     *
     * @return string
     */
    abstract public function escape_string($str);

    /**
     * Ejecuta sentencias SQL sobre la base de datos (inserts, updates o deletes).
     * Para hacer selects, mejor usar select() o selec_limit().
     * Por defecto se inicia una transacción, se ejecutan las consultas, y si
     * sale bien, se guarda, sinó se deshace.
     * Se puede evitar este modo de transacción si se pone false
     * en el parámetro transaction, o con la función set_auto_transactions(FALSE)
     *
     * @param string $sql
     * @param bool   $transaction
     *
     * @return bool
     */
    // abstract public function exec($sql, $transaction = true);

    /**
     * Devuelve la sentencia SQL necesaria para crear una tabla con la estructura proporcionada.
     *
     * @param string $table_name
     * @param array  $xml_cols
     * @param array  $xml_cons
     *
     * @return string
     */
    abstract public function generate_table($table_name, $xml_cols, $xml_cons);

    /**
     * Devuelve un array con las columnas de una tabla dada.
     *
     * @param string $table_name
     *
     * @return array
     */
    abstract public function get_columns($table_name);

    /**
     * Devuelve una array con las restricciones de una tabla dada:
     * clave primaria, claves ajenas, etc.
     *
     * @param string $table_name
     *
     * @return array
     */
    abstract public function get_constraints($table_name);

    /**
     * Devuelve una array con las restricciones de una tabla dada, pero aportando muchos más detalles.
     *
     * @param string $table_name
     *
     * @return array
     */
    abstract public function get_constraints_extended($table_name);

    /**
     * Devuelve una array con los indices de una tabla dada.
     *
     * @param string $table_name
     *
     * @return array
     */
    abstract public function get_indexes($table_name);

    /**
     * Devuelve un array con los datos de bloqueos en la base de datos.
     *
     * @return array
     */
    abstract public function get_locks();

    /**
     * Devuelve el último ID asignado al hacer un INSERT en la base de datos.
     *
     * @return int
     */
    abstract public function lastval();

    /**
     * Devuelve un array con los nombres de las tablas de la base de datos.
     *
     * @return array
     */
    abstract public function list_tables();

    /**
     * Deshace los cambios de una transacción SQL.
     *
     * @return bool
     */
    abstract public function rollback();

    /**
     * Ejecuta una sentencia SQL de tipo select, y devuelve un array con los resultados,
     * o false en caso de fallo.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0721
     *
     * @param string $query
     * @param array  $vars
     *
     * @return array|false
     */
    final public function select(string $query, array $vars = [])
    {
        // Remove extra blankspace to be more readable
        $query = preg_replace('/\s+/', ' ', $query);
        self::$statement = self::$dbHandler->prepare($query);
        if (self::$statement === false) {
            throw new PDOException('PDO ERROR in select.prepare: ' . $query);
        }
        if (self::$statement->execute($vars)) {
            $result = self::$statement->fetchAll(PDO::FETCH_ASSOC);
            if ($result === false) {
                throw new PDOException('PDO ERROR in select: ' . $query);
            }
            return $result;
        }
        throw new PDOException('PDO ERROR in select.execute: ' . $query);
    }

    final public static function exec(string $query, $vars = []): bool
    {
        // Remove extra blankspace to be more readable
        $query = preg_replace('/\s+/', ' ', $query);
        self::$statement = self::$dbHandler->prepare($query);
        if (self::$statement === false) {
            return false;
        }
        try {
            self::$statement->execute($vars);
            DBSchema::clearTableList(); // Para que se vuelva a revisar la integridad de las tablas
        } catch (PDOException $exception) {
            if (in_array($exception->errorInfo[0], [42000])) {
                debug_message($exception->errorInfo[2]);
                return true;
            }
            return false;
        }

        return true;
    }

    /**
     * Ejecuta una sentencia SQL de tipo select, pero con paginación,
     * y devuelve un array con los resultados o false en caso de fallo.
     * Limit es el número de elementos que quieres que devuelva.
     * Offset es el número de resultado desde el que quieres que empiece.
     *
     * @param string $sql
     * @param int    $limit
     * @param int    $offset
     *
     * @return false|array
     */
    abstract public function select_limit($sql, $limit = FS_ITEM_LIMIT, $offset = 0);

    /**
     * Devuelve el SQL necesario para convertir la columna a entero.
     *
     * @param string $col_name
     *
     * @return string
     */
    abstract public function sql_to_int($col_name);

    /**
     * Devuelve el motor de base de datos usado y la versión.
     *
     * @return string
     */
    abstract public function version();

    /**
     * Devuelve TRUE si se está conectado a la base de datos.
     *
     * @return bool
     */
    public function connected()
    {
        return isset(self::$dbHandler);
    }

    /**
     * Devuelve el historial SQL.
     *
     * @return array
     */
    public function get_history()
    {
        return self::$coreLog->get_sql_history();
    }

    /**
     * Devuelve el número de selects ejecutados
     *
     * @return int
     */
    public function get_selects()
    {
        return self::$t_selects;
    }

    /**
     * Devuele le número de transacciones realizadas
     *
     * @return int
     */
    public function get_transactions()
    {
        return self::$t_transactions;
    }

    /**
     * Look for a column with a value by his name in array.
     *
     * @param array  $items
     * @param string $index
     * @param string $value
     *
     * @return array
     */
    protected function search_in_array($items, $index, $value)
    {
        if (empty($items)) {
            return [];
        }

        foreach ($items as $column) {
            if ($column[$index] === $value) {
                return $column;
            }
        }

        return [];
    }

    /**
     * Devuelve el SQL necesario para realizar una búsqueda en columnas con acentos
     *
     * @param string $col_name
     * @param string $search
     * @param string $splitWord
     *
     * @return string
     */
    abstract public function search_diacritic_insensitive($col_name, $search, $splitWord = '');
}
