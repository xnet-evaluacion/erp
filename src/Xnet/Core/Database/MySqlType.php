<?php
/*
 * This file is part of MiFactura.eu
 * Copyright (C) 2021 X-Net Software Solutions S.L. <xnetsoftware@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace Xnet\Core\Database;

use Xnet\Core\DB;

/**
 * Clase para conectar a MySQL.
 * Atención: no es 100% identica a MariaDB.
 */
abstract class MySqlType extends Engine
{
    /**
     * True if the database engine supports SAVEPOINT in transactions
     *
     * @var bool
     */
    protected static $savePointsSupport = true;

    /**
     * Number of transactions in execution
     *
     * @var int
     */
    protected static $transactionDepth = 0;

    /**
     * Realiza comprobaciones extra a la tabla.
     *
     * @param string $table_name
     *
     * @return bool
     */
    public function check_table_aux($table_name)
    {
        /// ¿La tabla no usa InnoDB?
        $data = DB::select("SHOW TABLE STATUS FROM `" . constant('FS_DB_NAME') . "` LIKE '" . $table_name . "';");
        if ($data && $data[0]['Engine'] != 'InnoDB' && !$this->exec("ALTER TABLE " . $table_name . " ENGINE=InnoDB;")) {
            self::$coreLog->new_error('Imposible convertir la tabla ' . $table_name . ' a InnoDB. Imprescindible para MiFactura.eu.');
            return false;
        }

        return true;
    }

    /**
     * Ejecuta sentencias SQL sobre la base de datos (inserts, updates y deletes).
     * Para selects, mejor usar las funciones select() o select_limit().
     * Por defecto se inicia una transacción, se ejecutan las consultas, y si
     * sale bien, se guarda, sinó se deshace.
     * Se puede evitar este modo de transacción si se pone false
     * en el parámetro transaction.
     *
     * @param string $sql
     * @param bool   $transaction
     *
     * @return bool
     */
    public function _exec($sql, $transaction = false)
    {
        $result = false;

        if (self::$link) {
            /// añadimos la consulta sql al historial
            self::$coreLog->new_sql($sql);

            if ($transaction) {
                $this->beginTransaction();
            }

            $i = 0;
            if (self::$link->multi_query($sql)) {
                do {
                    if ($data = self::$link->store_result()) {
                        $data->free();
                    }
                    $i++;
                } while (self::$link->more_results() && self::$link->next_result());
                self::$coreLog->debug($sql, 'sql', false);
            }

            if (self::$link->errno) {
                $posicion = count(self::$coreLog->get_sql_history());
                switch (self::$link->errno) {
                    case '1451':
                        $error = '<b>No se puede eliminar el registro indicado. Está intentando eliminar un registro que ya tiene datos relacionados con él.</b>';
                        break;
                    default:
                        $error = 'Error al ejecutar la consulta ' . $i . ', la secuencia ocupa la posición ' . $posicion . '<br/>#' . self::$link->errno . ' - ' . self::$link->error . '.';
                        break;
                }

                $error_debug = '<pre>' . str_replace(';', ';' . PHP_EOL, self::$coreLog->get_sql_history()[$posicion - 1]) . '</pre>';
                $error_debug .= 'Error al ejecutar la consulta ' . $i . ', la secuencia ocupa la posición ' . $posicion . '<br/>#' . self::$link->errno . ' - ' . self::$link->error . '.';

                if (constant('FS_DB_HISTORY') || constant('FS_DEBUG')) {
                    $error .= '<br><br><b>DEBUG</b><pre>' . str_replace(';', ';' . PHP_EOL, self::$coreLog->get_sql_history()[$posicion - 1]) . '</pre>';
                }
                debug_message($error_debug);

                self::$coreLog->new_error($error);
                self::$coreLog->save($error);
            } else {
                $result = true;
            }

            if ($transaction) {
                if ($result) {
                    $this->commit();
                } else {
                    $this->rollback();
                }
            }
        }

        return $result;
    }

    /**
     * Inicia una transacción SQL.
     *
     * @return bool
     *
     * @deprecated Utilice en su lugar beginTransaction
     */
    public function begin_transaction()
    {
        /**
         * Ejecutamos START TRANSACTION en lugar de begin_transaction()
         * para mayor compatibilidad.
         */
        return self::$link ? self::$link->query("START TRANSACTION;") : false;
    }

    /**
     * Inicia una nueva transacción, que terminará correctamente con commit, o fallará con rollback.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0719
     *
     * @doc https://www.ibm.com/support/knowledgecenter/es/SSEPGG_9.1.0/com.ibm.db2.udb.apdv.php.doc/doc/t0023166.htm
     * Transactions support
     * @doc https://coderwall.com/p/rml5fa/nested-pdo-transactions
     *
     * @return bool
     */
    public function beginTransaction()
    {
        if (!self::$link) {
            return false;
        }

        $transactionDepth = self::$transactionDepth;
        if (self::$transactionDepth === 0 || !self::$savePointsSupport) {
            self::$coreLog->new_sql('BEGIN TRANSACTION;');
            $ret = self::$link->begin_transaction();
        } else {
            $sql = "SAVEPOINT LEVEL{$transactionDepth};";
            // añadimos la consulta sql al historial
            self::$coreLog->new_sql($sql);
            $ret = self::$link->query($sql);
        }

        self::$transactionDepth++;
        // $this->debugBar->addMessage('SQL', 'Transaction started, savepoint LEVEL' . self::$transactionDepth . ' saved');

        return $ret;
    }

    /**
     * Guarda los cambios de una transacción SQL.
     *
     * @return bool
     */
    public function old_commit()
    {
        if (self::$link) {
            /// aumentamos el contador de transacciones realizados
            self::$t_transactions++;
            return self::$link->commit();
        }

        return false;
    }

    public function commit()
    {
        if (!self::$link || self::$transactionDepth < 0) {
            return false;
        }

        // self::$debugTool->addMessage('SQL', 'Commit, savepoint LEVEL' . self::$transactionDepth);
        self::$transactionDepth--;

        if (self::$transactionDepth === 0 || !self::$savePointsSupport) {
            self::$t_transactions++;
            self::$coreLog->new_sql('COMMIT;');  // Se notifica finalización correcta de la transacción
            $ret = self::$link->commit();
        } else {
            $transactionDepth = self::$transactionDepth;
            $sql = "RELEASE SAVEPOINT LEVEL{$transactionDepth};";
            // añadimos la consulta sql al historial
            self::$coreLog->new_sql($sql);
            $ret = self::$link->query($sql);
        }

        return $ret;
    }

    public function rollback()
    {
        $ret = true;

        if (self::$transactionDepth === 0) {
            // throw new PDOException('Rollback error : There is no transaction started');
            return false;
        }

        // self::$debugTool->addMessage('SQL', 'Rollback, savepoint LEVEL' . self::$transactionDepth);
        self::$transactionDepth--;

        if (self::$transactionDepth === 0 || !self::$savePointsSupport) {
            self::$coreLog->new_sql('ROLLBACK');    // Se notifica aborto de transacción
            $ret = self::$link->rollback();
        } else {
            $transactionDepth = self::$transactionDepth;
            $sql = "ROLLBACK TO SAVEPOINT LEVEL{$transactionDepth};";
            // añadimos la consulta sql al historial
            self::$coreLog->new_sql($sql);
            self::exec($sql);
        }

        return $ret;
    }

    /**
     * Deshace los cambios de una transacción SQL.
     *
     * @return bool
     */
    public function old_rollback()
    {
        return self::$link ? self::$link->rollback() : false;
    }

    /**
     * Desconecta de la base de datos.
     *
     * @return bool
     */
    public function close()
    {
        if (self::$link) {
            $return = self::$link->close();
            self::$link = null;
            return $return;
        }

        return true;
    }

    /**
     * Compara dos arrays de columnas, devuelve una sentencia SQL en caso de encontrar diferencias.
     *
     * @param string $table_name
     * @param array  $xml_cols
     * @param array  $db_cols
     *
     * @return string
     */
    public function compare_columns($table_name, $xml_cols, $db_cols)
    {
        $sql = '';

        foreach ($xml_cols as $xml_col) {
            if (mb_strtolower($xml_col['tipo']) == 'integer') {
                /**
                 * Desde la pestaña avanzado el panel de control se puede cambiar
                 * el tipo de entero a usar en las columnas.
                 */
                $xml_col['tipo'] = FS_DB_INTEGER;
            }

            /**
             * Si el campo no está en la tabla, procedemos a su creación
             */
            $db_col = $this->search_in_array($db_cols, 'name', $xml_col['nombre']);
            if (empty($db_col)) {
                $sql .= 'ALTER TABLE `' . $table_name . '` ADD `' . $xml_col['nombre'] . '` ';
                if ($xml_col['tipo'] == 'serial') {
                    $sql .= '`' . $xml_col['nombre'] . '` ' . constant('FS_DB_INTEGER') . ' NOT NULL AUTO_INCREMENT;';
                    continue;
                }
                if ($xml_col['tipo'] == 'autoincrement') {
                    $sql .= '`' . $xml_col['nombre'] . '` ' . constant('DB_INDEX_TYPE') . ' NOT NULL AUTO_INCREMENT;';
                    continue;
                }
                if ($xml_col['tipo'] == 'relationship') {
                    $xml_col['tipo'] = constant('DB_INDEX_TYPE');
                }

                $sql .= $xml_col['tipo'];
                $sql .= ($xml_col['nulo'] == 'NO') ? " NOT NULL" : " NULL";

                if ($xml_col['defecto'] !== null) {
                    $sql .= " DEFAULT " . $xml_col['defecto'];
                } elseif ($xml_col['nulo'] == 'YES') {
                    $sql .= " DEFAULT NULL";
                }

                $sql .= ';';

                continue;
            }

            /**
             * Si el campo es un autoincremental o relacionado a uno, asignamos el tipo correcto para la constraint.
             * Si además es el índice, nos aseguramos de que no pueda ser nulo.
             */
            if (in_array($xml_col['tipo'], ['autoincrement', 'relationship'])) {
                if ($xml_col['tipo'] === 'autoincrement') {
                    $xml_col['nulo'] = 'NO';
                    $xml_col['defecto'] = 'nextval()';
                    $xml_col['autoincrement'] = true;
                }
                $xml_col['tipo'] = constant('DB_INDEX_TYPE');
            }

            /// columna ya presente en db_cols. La modificamos
            $compare_result = $this->compare_data_types($db_col['type'], $xml_col['tipo']);
            if (!$compare_result) {
                // Buscar todas las constraints relacionadas con este campo y eliminarlas
                foreach ($this->get_referenced_field_constraint($table_name, $xml_col['nombre']) as $pos => $constraint) {
                    $sql .= "ALTER TABLE `" . $constraint['TABLE_NAME'] . "` DROP FOREIGN KEY " . $constraint['CONSTRAINT_NAME'] . ";";
                }
                $sql .= 'ALTER TABLE `' . $table_name . '` MODIFY `' . $xml_col['nombre'] . '` ' . $xml_col['tipo'];
                if (isset($xml_col['autoincrement']) && $xml_col['autoincrement']) {
                    $sql .= ' NOT NULL AUTO_INCREMENT';
                }
                $sql .= ';';
            }

            if ($db_col['is_nullable'] == $xml_col['nulo']) {
                /// do nothing
            } elseif ($xml_col['nulo'] == 'YES') {
                $sql .= 'ALTER TABLE `' . $table_name . '` MODIFY `' . $xml_col['nombre'] . '` ' . $xml_col['tipo'] . ' NULL;';
            } else {
                $sql .= 'ALTER TABLE `' . $table_name . '` MODIFY `' . $xml_col['nombre'] . '` ' . $xml_col['tipo'] . ' NOT NULL;';
            }

            if (mb_strtolower(substr($xml_col['defecto'], 0, 8)) == "nextval(") { /// nextval es para postgresql
                if ($db_col['extra'] != 'auto_increment') {
                    $sql .= 'ALTER TABLE `' . $table_name . '` MODIFY `' . $xml_col['nombre'] . '` ' . $xml_col['tipo'];
                    $sql .= ($xml_col['nulo'] == 'YES') ? ' NULL AUTO_INCREMENT;' : ' NOT NULL AUTO_INCREMENT;';
                }
            } elseif ($this->compare_defaults($db_col['default'], $xml_col['defecto'])) {
                /// do nothing
            } elseif (is_null($xml_col['defecto']) || mb_strtolower($xml_col['defecto']) === 'null') {
                if ($this->exists_index($table_name, $db_col['name']) && !$this->unique_equals($table_name, $db_col, $xml_col)) {
                    $sql .= 'ALTER TABLE `' . $table_name . '` ALTER `' . $xml_col['nombre'] . '` DROP DEFAULT;';
                }
            } else {
                if ($db_col['default'] !== $xml_col['defecto']) {
                    $sql .= 'ALTER TABLE `' . $table_name . '` ALTER `' . $xml_col['nombre'] . '` SET DEFAULT ' . $xml_col['defecto'] . ";";
                }
            }
        }

        return $this->fix_postgresql($sql);
    }

    /**
     * Devuelve una array con los indices de una tabla dada.
     *
     * @param string $table_name
     *
     * @return array
     */
    public function get_indexes($table_name)
    {
        $indexes = [];
        $aux = DB::select("SHOW INDEXES FROM " . $table_name . ";");
        if ($aux) {
            foreach ($aux as $a) {
                $indexes[] = [
                    'name' => $a['Key_name'],
                    'table' => $table_name,
                    'column' => $a['Column_name'],
                ];
            }
        }

        return $indexes;
    }

    /**
     * Devuelve los detalles de la constraint de una tabla.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2021.11
     *
     * @param $table_name
     * @param $name
     *
     * @return array
     */
    public function get_extended_constraint($table_name, $name)
    {
        $data = [];

        if (!isset(self::$constraints[$table_name])) {
            self::$constraints[$table_name] = $this->get_constraints_extended($table_name);
        }

        foreach (self::$constraints[$table_name] as $item) {
            if ($item['type'] == 'UNIQUE') {
                if (!empty($data) && $item['name'] == $name) {
                    $data['column_name'] .= ',' . $item['column_name'];
                } elseif (empty($data) && $item['name'] == $name) {
                    $data = $item;
                }
            } elseif ($item['name'] == $name) {
                $data = $item;
                break;
            }
        }
        return $data;
    }

    /**
     * Devuelve una array con las restricciones de una tabla dada, pero aportando muchos más detalles.
     *
     * @param string $table_name
     *
     * @return array
     */
    public function get_constraints_extended($table_name)
    {
        $key_name = 'constraints_extended_' . $table_name;
        $constraints = self::$cache->get_array($key_name);
        if (!$constraints) {
            $constraints = [];
            $sql = "SELECT t1.constraint_name AS name,
            t1.constraint_type AS type,
            t2.column_name AS column_name,
            t2.referenced_table_name AS foreign_table_name,
            t2.referenced_column_name AS foreign_column_name,
            t3.update_rule AS on_update,
            t3.delete_rule AS on_delete
         FROM information_schema.table_constraints t1
         LEFT JOIN information_schema.key_column_usage t2
            ON t1.table_schema = t2.table_schema
            AND t1.table_name = t2.table_name
            AND t1.constraint_name = t2.constraint_name
         LEFT JOIN information_schema.referential_constraints t3
            ON t3.constraint_schema = t1.table_schema
            AND t3.constraint_name = t1.constraint_name
         WHERE t1.table_schema = SCHEMA() AND t1.table_name = '" . $table_name . "'
         ORDER BY type DESC, name ASC, column_name ASC;";

            $aux = DB::select($sql);
            if ($aux) {
                foreach ($aux as $a) {
                    $constraints[] = $a;
                }
            }
            self::$cache->set($key_name, $constraints);
        }

        return $constraints;
    }

    /**
     * Devuelve una array con las restricciones de una tabla y campo.
     *
     * @param string $table_name
     *
     * @return array
     */
    public function get_referenced_field_constraint($table_name, $field)
    {
        $sql = "SELECT TABLE_NAME, COLUMN_NAME, CONSTRAINT_NAME, REFERENCED_TABLE_NAME, REFERENCED_COLUMN_NAME
            FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE
            WHERE TABLE_SCHEMA = SCHEMA() AND REFERENCED_TABLE_NAME = '" . $table_name . "' AND REFERENCED_COLUMN_NAME = '" . $field . "';";

        $constraints = [];
        $aux = DB::select($sql);
        if ($aux) {
            foreach ($aux as $a) {
                $constraints[] = $a;
            }
        }
        return $constraints;
    }

    /**
     * Compara dos arrays de restricciones, devuelve una sentencia SQL en caso de encontrar diferencias.
     *
     * @param string $table_name
     * @param array  $xml_cons
     * @param array  $db_cons
     * @param bool   $delete_only
     *
     * @return string
     */
    public function compare_constraints($table_name, $xml_cons, $db_cons, $delete_only = false)
    {
        $sql = '';

        $xml_cons_aux = [];
        foreach ($xml_cons as $xml_con) {
            $xml_cons_aux[$xml_con['nombre']] = $xml_con;
        }

        $db_cons_aux = [];
        foreach ($db_cons as $db_con) {
            $db_cons_aux[$db_con['name']] = $db_con;
        }

        if (!empty($db_cons)) {
            /**
             * Comprobamos una a una las restricciones de la base de datos, si hay que eliminar una,
             * tendremos que eliminar todas para evitar problemas.
             */
            $delete = false;
            foreach ($db_cons as $db_con) {
                if (empty($xml_cons)) {
                    $delete = true;
                    break;
                }

                $found = false;
                foreach ($xml_cons as $xml_con) {
                    if ($db_con['name'] == 'PRIMARY' || $db_con['name'] == $xml_con['nombre']) {
                        $found = true;
                        break;
                    }
                }

                if (!$found) {
                    $delete = true;
                    break;
                }
            }

            /// eliminamos todas las restricciones
            if ($delete) {
                /// eliminamos antes las claves ajenas y luego los unique, evita problemas
                $sql_unique = '';
                foreach ($db_cons as $db_con) {
                    if ($db_con['type'] == 'FOREIGN KEY') {
                        if (isset($xml_cons_aux[$db_con['name']])) {
                            $xml_con = $xml_cons_aux[$db_con['name']];
                            if (isset($xml_con['nombre']) && $db_con['name'] == $xml_con['nombre'] && !$this->constrains_equals($table_name, $db_con, $xml_con)) {
                                // Solo haremos el DROP, si existe en la tabla o las constraints no son iguales
                                if ($this->exists_index($table_name, $db_con['name']) || !$this->constrains_equals($table_name, $db_con, $xml_con)) {
                                    $sql .= 'ALTER TABLE `' . $table_name . '` DROP FOREIGN KEY `' . $db_con['name'] . '`;';
                                    $this->delete_cached_constraints($table_name);
                                }
                            }
                        }
                    }
                    if ($db_con['type'] == 'UNIQUE' && isset($db_con['name'])) {
                        if (isset($xml_cons_aux[$db_con['name']])) {
                            $xml_con = $xml_cons_aux[$db_con['name']];
                            if (isset($xml_con['nombre']) && $db_con['name'] == $xml_con['nombre'] && !$this->unique_equals($table_name, $db_con, $xml_con)) {
                                // Solo haremos el DROP, si existe en la tabla o las constraints no son iguales
                                if ($this->exists_index($table_name, $db_con['name']) || !$this->unique_equals($table_name, $db_con, $xml_con)) {
                                    $sql .= 'ALTER TABLE `' . $table_name . '` DROP INDEX `' . $db_con['name'] . '`;';
                                    $this->delete_cached_constraints($table_name);
                                }
                            }
                        }
                    }
                }

                $sql .= $sql_unique;
                $db_cons = [];
            }
        }

        if (!empty($xml_cons) && !$delete_only && FS_FOREIGN_KEYS) {
            /// comprobamos una a una las nuevas
            foreach ($xml_cons as $xml_con) {
                $db_con = $this->search_in_array($db_cons, 'name', $xml_con['nombre']);
                if (substr($xml_con['consulta'], 0, 11) == 'FOREIGN KEY') {
                    if (isset($db_cons_aux[$xml_con['nombre']])) {
                        $db_con = $db_cons_aux[$xml_con['nombre']];
                        if (isset($db_con['name']) && $db_con['name'] == $xml_con['nombre'] && !$this->constrains_equals($table_name, $db_con, $xml_con)) {
                            $sql = $this->getSqlAlterConstraint($table_name, $db_con, $xml_con, $sql);
                        }
                    } else {
                        $sql = $this->getSqlAlterConstraint($table_name, $db_con, $xml_con, $sql);
                    }
                } elseif (substr($xml_con['consulta'], 0, 6) == 'UNIQUE') {
                    if (isset($db_cons_aux[$xml_con['nombre']])) {
                        $db_con = $db_cons_aux[$xml_con['nombre']];
                        if (isset($db_con['name']) && $db_con['name'] == $xml_con['nombre'] && !$this->unique_equals($table_name, $db_con, $xml_con)) {
                            $sql = $this->getSqlAlterConstraint($table_name, $db_con, $xml_con, $sql);
                        }
                    } else {
                        $sql = $this->getSqlAlterConstraint($table_name, $db_con, $xml_con, $sql);
                    }
                }
            }
        }

        return $this->fix_postgresql($sql);
    }

    /**
     * Devuelve el estilo de fecha del motor de base de datos.
     *
     * @return string
     */
    public function date_style()
    {
        return 'Y-m-d';
    }

    /**
     * Escapa las comillas de la cadena de texto.
     *
     * @param string $str
     *
     * @return string
     */
    public function escape_string($str)
    {
        return self::$link ? self::$link->escape_string($str) : $str;
    }

    /**
     * Devuelve la sentencia SQL necesaria para crear una tabla con la estructura proporcionada.
     *
     * @param string $table_name
     * @param array  $xml_cols
     * @param array  $xml_cons
     *
     * @return string
     */
    public function generate_table($table_name, $xml_cols, $xml_cons)
    {
        $sql = "CREATE TABLE `" . $table_name . "` ( ";

        $i = false;
        foreach ($xml_cols as $col) {
            /// añade la coma al final
            if ($i) {
                $sql .= ", ";
            } else {
                $i = true;
            }

            if (in_array($col['tipo'], ['serial', 'autoincrement'])) {
                $tipo = $col['tipo'] === 'serial' ? constant('FS_DB_INTEGER') : constant('DB_INDEX_TYPE');
                $sql .= '`' . $col['nombre'] . '` ' . $tipo . ' NOT NULL AUTO_INCREMENT';
            } else {
                if (mb_strtolower($col['tipo']) == 'integer') {
                    /**
                     * Desde la pestaña avanzado el panel de control se puede cambiar
                     * el tipo de entero a usar en las columnas.
                     */
                    $col['tipo'] = constant('FS_DB_INTEGER');
                }
                if (mb_strtolower($col['tipo']) == 'relationship') {
                    $col['tipo'] = constant('DB_INDEX_TYPE');
                }

                $sql .= '`' . $col['nombre'] . '` ' . $col['tipo'];

                if ($col['nulo'] == 'NO') {
                    $sql .= " NOT NULL";
                } else {
                    /// es muy importante especificar que la columna permite NULL
                    $sql .= " NULL";
                }

                if ($col['defecto'] !== null) {
                    $sql .= " DEFAULT " . $col['defecto'];
                }

                if (isset($col['comentario'])) {
                    $comment = $col['comentario'];
                    $sql .= " COMMENT '$comment'";
                }
            }
        }

        return $this->fix_postgresql($sql) . ' ' . $this->generate_table_constraints($xml_cons) . ' ) ' . 'ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;';
    }

    /**
     * Devuelve un array con las columnas de una tabla dada.
     *
     * @param string $table_name
     *
     * @return array
     */
    public function get_columns($table_name)
    {
        $columns = [];
        $aux = DB::select("SHOW COLUMNS FROM " . $table_name . ";");
        if ($aux) {
            foreach ($aux as $a) {
                $columns[] = [
                    'name' => $a['Field'],
                    'type' => $a['Type'],
                    'default' => $a['Default'],
                    'is_nullable' => $a['Null'],
                    'extra' => $a['Extra'],
                ];
            }
        }

        return $columns;
    }

    /**
     * Devuelve una array con las restricciones de una tabla dada:
     * clave primaria, claves ajenas, etc.
     *
     * @param string $table_name
     *
     * @return array
     */
    public function get_constraints($table_name)
    {
        $key_name = 'constraints_' . $table_name;
        $constraints = self::$cache->get_array($key_name);
        if (!$constraints) {
            $constraints = [];
            $sql = "SELECT CONSTRAINT_NAME as name, CONSTRAINT_TYPE as type"
                . " FROM information_schema.table_constraints"
                . " WHERE table_schema = schema() AND table_name = '" . $table_name . "';";

            $aux = DB::select($sql);
            if ($aux) {
                foreach ($aux as $a) {
                    $constraints[] = $a;
                }
            }
            self::$cache->set($key_name, $constraints);
        }

        return $constraints;
    }

    /**
     * Devuelve un array con los datos de bloqueos en la base de datos.
     *
     * @return array
     */
    public function get_locks()
    {
        return [];
    }

    /**
     * Devuelve el último ID asignado al hacer un INSERT en la base de datos.
     *
     * @return int|false
     */
    public function lastval()
    {
        $aux = DB::select('SELECT LAST_INSERT_ID() as num;');
        return $aux ? $aux[0]['num'] : false;
    }

    /**
     * Ejecuta una sentencia SQL de tipo select, pero con paginación,
     * y devuelve un array con los resultados,
     * o false en caso de fallo.
     * Limit es el número de elementos que quieres que devuelve.
     * Offset es el número de resultado desde el que quieres que empiece.
     *
     * @param string $sql
     * @param int    $limit
     * @param int    $offset
     *
     * @return false|array
     */
    public function select_limit($sql, $limit = FS_ITEM_LIMIT, $offset = 0)
    {
        /// añadimos limit y offset a la consulta sql
        $sql .= ' LIMIT ' . $limit . ' OFFSET ' . $offset . ';';
        return DB::select($sql);
    }

    /**
     * Devuelve el SQL necesario para convertir la columna a entero.
     *
     * @param string $col_name
     *
     * @return string
     */
    public function sql_to_int($col_name)
    {
        return 'CAST(' . $col_name . ' as UNSIGNED)';
    }

    abstract public function version();

    /**
     * Ejecuta las SQL para el cambio de charset y collation y devuelve su resultado.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2021.09
     *
     * @param string $charset
     * @param string $collation
     *
     * @return bool
     */
    public function update_collation($charset = 'utf8mb4', $collation = 'utf8mb4_bin')
    {
        $queries = [];
        $queries[] = 'SET foreign_key_checks = 0;';

        if (in_array('fs_logs', $this->list_tables())) {
            $queries[] = 'TRUNCATE `fs_logs`;';
        }

        if ($this->get_charset_db() != $charset || $this->get_collation_db() != $collation) {
            $queries[] = 'ALTER DATABASE ' . constant('FS_DB_NAME') . ' CHARACTER SET = "' . $charset . '"  COLLATE = "' . $collation . '";';
        }

        foreach ($this->list_tables() as $table) {
            if ($this->get_collation_from($table['name']) != $collation) {
                $queries[] = 'ALTER TABLE ' . $table['name'] . ' CONVERT TO CHARACTER SET ' . $charset . ' COLLATE ' . $collation . ';';
            }
        }

        $queries[] = 'SET foreign_key_checks = 1;';

        if (count($queries) === 2) {
            return true;
        }

        set_temporaly_unlimited_limits();

        $result = $this->exec(implode('', $queries));
        return $result;
    }

    /**
     * Devuelve un array con los nombres de las tablas de la base de datos.
     *
     * @return array
     */
    public function list_tables()
    {
        $tables = [];
        $aux = DB::select("SHOW TABLES;");
        if ($aux) {
            foreach ($aux as $a) {
                if (isset($a['Tables_in_' . constant('FS_DB_NAME')])) {
                    $tables[] = ['name' => $a['Tables_in_' . constant('FS_DB_NAME')]];
                }
            }
        }

        return $tables;
    }

    /**
     * Compara los tipos de datos de una columna. Devuelve TRUE si son iguales.
     *
     * @param string $db_type
     * @param string $xml_type
     *
     * @return bool
     */
    private function compare_data_types($db_type, $xml_type)
    {
        if (FS_CHECK_DB_TYPES != 1) {
            /// si está desactivada la comprobación de tipos, devolvemos que son iguales.
            return true;
        } elseif ($db_type == $xml_type) {
            return true;
        } elseif (in_array(mb_strtolower($xml_type), ['serial', 'autoincrement'])) {
            return true;
        } elseif ($db_type == 'tinyint(1)' && $xml_type == 'boolean') {
            return true;
        } elseif (substr($db_type, 0, 3) == 'int' && mb_strtolower($xml_type) == 'integer') {
            return true;
        } elseif (substr($db_type, 0, 6) == 'double' && $xml_type == 'double precision') {
            return true;
        } elseif (substr($db_type, 0, 4) == 'time' && substr($xml_type, 0, 4) == 'time') {
            return true;
        } elseif (substr($db_type, 0, 8) == 'varchar(' && substr($xml_type, 0, 18) == 'character varying(') {
            /// comprobamos las longitudes
            return (substr($db_type, 8, -1) == substr($xml_type, 18, -1));
        } elseif (substr($db_type, 0, 5) == 'char(' && substr($xml_type, 0, 18) == 'character varying(') {
            /// comprobamos las longitudes
            return (substr($db_type, 5, -1) == substr($xml_type, 18, -1));
        }

        return false;
    }

    /**
     * Compara los tipos por defecto. Devuelve TRUE si son equivalentes.
     *
     * @param string $db_default
     * @param string $xml_default
     *
     * @return bool
     */
    private function compare_defaults($db_default, $xml_default)
    {
        if ($db_default === $xml_default) {
            return true;
        }

        if (in_array($db_default, ['0', 'false', 'FALSE',])) {
            return in_array($xml_default, ['0', 'false', 'FALSE',]);
        }

        if (in_array($db_default, ['1', 'true', 'TRUE',])) {
            return in_array($xml_default, ['1', 'true', 'TRUE',]);
        }

        if ($db_default == '00:00:00' && $xml_default == 'now()') {
            return true;
        }

        if ($db_default == date('Y-m-d') . ' 00:00:00' && $xml_default == 'CURRENT_TIMESTAMP') {
            return true;
        }

        if ($db_default == 'CURRENT_DATE' && $xml_default == date("'Y-m-d'")) {
            return true;
        }

        if (substr($xml_default, 0, 8) == 'nextval(') {
            return true;
        }

        $db_default = str_replace(['::character varying', "'",], ['', '',], $db_default);
        $xml_default = str_replace(['::character varying', "'",], ['', '',], $xml_default);
        return ($db_default === $xml_default);
    }

    /**
     * Devuelve si el indice existe.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2021.11
     *
     * @param $table_name
     * @param $name
     *
     * @return bool
     */
    protected function exists_index($table_name, $name)
    {
        $exists = false;

        if (!isset(self::$indexes[$table_name])) {
            self::$indexes[$table_name] = $this->get_indexes($table_name);
        }

        foreach (self::$indexes[$table_name] as $index) {
            if ($index['name'] == $name) {
                $exists = true;
                break;
            }
        }
        $data = [
            'function' => __FUNCTION__,
            'table_name' => $table_name,
            'name' => $name,
            'exists' => $exists,
        ];
        $msg = '<pre>' . var_export($data, true) . '</pre>';
        self::$coreLog->debug($msg);
        return $exists;
    }

    /**
     * Devuelve si las constraints de la DB y del XML son iguales.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2021.11
     *
     * @param $table_name
     * @param $db_con
     * @param $xml_con
     *
     * @return bool
     */
    private function unique_equals($table_name, $db_con, $xml_con)
    {
        $extended = $this->get_extended_constraint($table_name, $db_con['name']);
        $sql_foreign = $extended['type'] . ' (' . $extended['column_name'] . ')';
        $sql_foreign2 = str_replace("\n", ' ', $xml_con['consulta']);
        $sql_foreign2 = trim(preg_replace('/\s\s+/', ' ', str_replace("\n", " ", $sql_foreign2)));

        if (constant('FS_DEBUG') && $sql_foreign != $sql_foreign2) {
            $data = [
                'function' => __FUNCTION__,
                'table_name' => $table_name,
                'name' => $extended['name'],
                'extended' => $extended,
                'db_con' => $db_con,
                'xml_con' => $xml_con,
                'from  db' => $sql_foreign,
                'from xml' => $sql_foreign2,
            ];
            debug_message($data);
            $msg = '<pre>' . var_export($data, true) . '</pre>';
            self::$coreLog->debug($msg);
        }

        return $sql_foreign == $sql_foreign2;
    }

    /**
     * Elimina código problemático de postgresql.
     *
     * @param string $sql
     *
     * @return string
     */
    private function fix_postgresql($sql)
    {
        return str_replace([
            '::character varying',
            'without time zone',
            'now()',
            'CURRENT_TIMESTAMP',
            'CURRENT_DATE',
        ], ['', '', "'00:00'", "'" . date('Y-m-d') . " 00:00:00'", date("'Y-m-d'"),], $sql);
    }

    /**
     * Devuelve si las constraints de la DB y del XML son iguales.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2021.11
     *
     * @param $table_name
     * @param $db_con
     * @param $xml_con
     *
     * @return bool
     */
    private function constrains_equals($table_name, $db_con, $xml_con)
    {
        $extended = $this->get_extended_constraint($table_name, $db_con['name']);
        $sql_foreign = $extended['type'] . ' (' . $extended['column_name'] . ') REFERENCES ' . $extended['foreign_table_name'] . ' (' . $extended['foreign_column_name'] . ')' . ' ON DELETE ' . $extended['on_delete'] . ' ON UPDATE ' . $extended['on_update'];
        $sql_foreign2 = str_replace("\n", ' ', $xml_con['consulta']);
        $sql_foreign2 = trim(preg_replace('/\s\s+/', ' ', str_replace("\n", " ", $sql_foreign2)));

        if (constant('FS_DEBUG') && $sql_foreign != $sql_foreign2) {
            $data = [
                'function' => __FUNCTION__,
                'table_name' => $table_name,
                'name' => $extended['name'],
                'extended' => $extended,
                'db_con' => $db_con,
                'xml_con' => $xml_con,
                'from  db' => $sql_foreign,
                'from xml' => $sql_foreign2,
            ];
            debug_message($data);
            $msg = '<pre>' . var_export($data, true) . '</pre>';
            self::$coreLog->debug($msg);
        }

        return $sql_foreign == $sql_foreign2;
    }

    /**
     * Elimina las constraints que pueden estar cacheadas de las tablas.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2021.11
     *
     * @param $table_name
     */
    protected function delete_cached_constraints($table_name)
    {
        self::$cache->delete('constraints_extended_' . $table_name);
        self::$cache->delete('constraints_' . $table_name);
    }

    /**
     * Genera el SQL para establecer las restricciones proporcionadas.
     *
     * @param array $xml_cons
     *
     * @return string
     */
    private function generate_table_constraints($xml_cons)
    {
        $sql = '';

        if (!empty($xml_cons)) {
            foreach ($xml_cons as $res) {
                if (strstr(mb_strtolower($res['consulta']), 'primary key')) {
                    $sql .= ', ' . $res['consulta'];
                } elseif (FS_FOREIGN_KEYS || substr($res['consulta'], 0, 11) != 'FOREIGN KEY') {
                    $sql .= ', CONSTRAINT ' . $res['nombre'] . ' ' . $res['consulta'];
                }
            }
        }

        return $this->fix_postgresql($sql);
    }

    /**
     * Devuelve el charset de la base de datos.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2021.09
     *
     * @return false|mixed
     */
    private function get_charset_db()
    {
        $data = DB::select("SELECT default_character_set_name AS charset_db FROM information_schema . SCHEMATA WHERE schema_name = '" . constant('FS_DB_NAME') . "';");
        if ($data) {
            return $data[0]['charset_db'];
        }

        return false;
    }

    /**
     * Devuelve el collation de la base de datos.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2021.09
     *
     * @return false|mixed
     */
    private function get_collation_db()
    {
        $data = DB::select("SELECT default_collation_name AS collation_db FROM information_schema . SCHEMATA WHERE schema_name = '" . constant('FS_DB_NAME') . "';");
        if ($data) {
            return $data[0]['collation_db'];
        }

        return false;
    }

    /**
     * Devuelve la collation de la tabla.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2021.09
     *
     * @param string $table_name
     *
     * @return false|string
     */
    private function get_collation_from($table_name)
    {
        $data = DB::select("SHOW TABLE STATUS FROM `" . constant('FS_DB_NAME') . "`"
            . " WHERE name LIKE '" . $table_name . "';");
        if ($data) {
            return $data[0]['Collation'];
        }

        return false;
    }

    /**
     * Solo haremos el ADD, si no existe en la base de datos o si existe en la tabla, y no es igual que en el XML
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2021.11
     *
     * @param string $table_name
     * @param array  $db_con
     * @param array  $xml_con
     * @param string $sql
     *
     * @return string
     */
    abstract protected function getSqlAlterConstraint(string $table_name, array $db_con, array $xml_con, string $sql): string;

    /**
     * Devuelve el SQL necesario para realizar una busqueda en columnas con acentos
     *
     * @param string $col_name
     * @param string $search
     * @param string $splitWord
     *
     * @return string
     */
    public function search_diacritic_insensitive($col_name, $search, $splitWord = '')
    {
        $search = $this->escape_string($search);
        $search = $splitWord ? explode($splitWord, $search) : [$search];
        $return = [];
        foreach ($search as $word) {
            $return[] = "CONVERT({$col_name} USING utf8) LIKE \"%{$word}%\"";
        }
        return "(" . implode(' AND ', $return) . ")";
    }

    /**
     * Devuelve el estado de eliminar la restricción de una tabla dada.
     *
     * @param string $table_name
     * @param string $constraint_name
     *
     * @return bool
     */
    public function delete_constraint($table_name, $constraint_name)
    {
        $sql = "ALTER TABLE `$table_name` DROP FOREIGN KEY `$constraint_name`;";
        $this->delete_cached_constraints($table_name);
        return $this->exec($sql);
    }

    /**
     * Devuelve el estado de eliminar el índice de una tabla dada.
     *
     * @param string $table_name
     * @param string $index_name
     *
     * @return bool
     */
    public function delete_index($table_name, $index_name)
    {
        $sql = "ALTER TABLE `$table_name` DROP INDEX `$index_name`;";
        $this->delete_cached_constraints($table_name);
        return $this->exec($sql);
    }
}
