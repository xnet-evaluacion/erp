<?php
/*
 * This file is part of plugin xnetcommonhelpers for MiFactura.eu
 * Copyright (C) 2022 X-Net Software Solutions S.L. <xnetsoftware@gmail.com>
 */

namespace Xnet\Core;

use Xnet\Model\Action;
use Xnet\Model\Controller;
use Xnet\Model\User;

/**
 * Class XnetAppController
 *
 * Añade una nueva capa al controlador para controlar el registro de la página y
 * la autenticación del usuario.
 *
 * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
 * @version 2022.0724
 *
 * @package Xnet\Core
 */
abstract class XnetAuthController extends XnetAppController
{
    /**
     * Contiene los datos de registro de la página en la tabla de controladores.
     *
     * @var Controller|null
     */
    public $controller;

    /**
     * @var XnetLogin
     */
    public $loginTools;

    /**
     * El usuario que ha hecho login
     *
     * @var User
     */
    public $user;

    /**
     * Contiene las acciones disponibles en el controlador.
     *
     * @var array
     */
    protected $actions;

    /**
     * Acción a ejecutar
     *
     * @var string|false
     */
    public $action;

    /**
     * Conexión a la base de datos por el método antiguo.
     * NOTA: Según la configuración del config.php, podría estar accediendo
     * a una base de datos diferente, si se ha definido FS_DB_NAME_SOURCE.
     *
     * define('FS_DB_NAME_SOURCE', 'old_db');
     *
     * Ésto permite realizar una importación a una base de datos diferente.
     *
     * @var \fs_db2
     *
     * @deprecated No se debe de usar en este controlador, utilice métodos estáticos de DB.
     */
    public $db;

    public $name;
    public $title;
    public $folder;
    public $show_on_menu;
    public $order;

    /**
     * Inicializa un controlador.
     *
     * Recibe un único parámetro que es un array asociativo con parámetros de definición y
     * configuración por defecto del controlador.
     *
     * @param array $config
     */
    public function __construct(array $config)
    {
        parent::__construct();

        $this->db = new \fs_db2();
        $this->db->connect();

        $this->name = $this->class_name;
        $this->title = $config['title'];
        $this->folder = $config['folder'];
        $this->description = $config['description'];
        $this->show_on_menu = $config['show_on_menu'];
        $this->order = $config['order'] ?? 100;

        // Obtenemos la lista de acciones posibles
        $this->actions = $this->getActions();

        // Obtenemos la acción que se desea ejecutar
        $this->action = self::filterRequest('action');
        if ($this->action === null) {
            $this->action = 'View';
        }

        $this->controller = Controller::getById($this->class_name);
        if ($this->controller === null) {
            $this->controller = new Controller();
        }

        Controller::regenerate();

        $fullname = get_called_class();
        $namearray = explode('\\', $fullname);
        $name = end($namearray);

        if (!$this->checkAuth()) {
            debug_message('Se ha producido un error al tratar de registrar la página');
        }

        $empresa_model = new \empresa();
        $this->empresa = $empresa_model->get(1);
        // Si no existe la empresa, la crea
        if ($this->empresa === false) {
            $empresa_model->nombre = 'Empresa de pruebas';
            $empresa_model->id = 1;
            $empresa_model->save();
            $this->empresa = $empresa_model->get(1);
        }

        /**
         * Esta llamada instancia la clase de autenticación de usuarios.
         * Se viene de un login, intenta hacer el login y dejaría el usuario
         * en $this->loginTools->user.
         */
        $this->loginTools = new XnetLogin($this->empresa->get_xid());

        // TODO: ¿Debería de haber un controlador específico para el logout?
        if (filter_input(INPUT_GET, 'logout')) {
            unset($_GET['logout']);
            $this->template = 'login/' . $this->empresa->plantilla_login;
            $this->loginTools->logout();
            return;
        }

        // TODO: ¿Cambiar esto a acciones y ponerlo dentro de un controlador AUTH o dentro de XnetLogin?
        if (filter_input(INPUT_POST, 'new_password') && filter_input(INPUT_POST, 'new_password2') && filter_input(INPUT_POST, 'user')) {
            $this->loginTools->changePassword();
            $this->template = 'login/' . $this->empresa->plantilla_login;
            return;
        }

        // Si está logueado, retorna el usuario en $this->user y continúa
        if (!$this->loginTools->logged()) {
            $this->template = 'login/' . $this->empresa->plantilla_login;
            return;
        }

        $this->loadExtensions();
        if ($name == __CLASS__) {
            $this->template = 'index';
        } else {
            $this->template = $name;
            if (file_exists(constant('MAINTENANCE_FILE'))) {
                $file_contents = json_decode(file_get_contents(constant('MAINTENANCE_FILE')), true);
                // Sólo permitiremos ejecutar los wizards pendientes mientras esté en mantenimiento
                if (isset($file_contents['wizards']) && !in_array($name, $file_contents['wizards'])) {
                    $this->template = 'maintenance_update';
                }
            }

            // Si está activándose el módulo, salimos
            if (isset($_GET['caca']) && get_called_class() !== $this->controller->name) {
                return;
            }

            // Si está activándose el módulo, salimos
            if ($name != 'admin_home' && isset($_GET['caca'])) {
                return;
            }

            // TODO: sobrepasado, bloqueado,...
            if (file_exists(FS_MYDOCS . 'images/sobrepasado.true') && in_array($name, $this->reseller_data->locked_controllers)) {
                $this->template = 'sobrepasado';
                return false;
            }

            if (!$this->prePrivateCore()) {
                $this->template = 'access_denied';
                return false;
            }

            if (isset($_POST['petition_id']) && $this->duplicated_petition($_POST['petition_id'])) {
                $this->new_error_msg('Petición duplicada');
            } else {
                $this->doAction(self::filterRequest('action'));
            }

            return $this->privateCore();
        }
    }

    /**
     * Se ejecuta si el usuario está identificado
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0823
     *
     * @return bool
     */
    abstract protected function privateCore(): bool;

    /**
     * Se ejecuta si el usuario no ha hecho login
     * Por defecto, invocaría a login.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0823
     *
     * @return bool
     */
    protected function publicCore(): bool
    {
        return false;
    }

    public function haveAccessTo($action)
    {
        // TODO: De acciones no definidas, se aceptan como permitidas
        if (!in_array($action, $this->actions)) {
            debug_message('Se ha solicitado permiso para ' . $action . ', pero no es una acción definida como configurable.');
            return true;
        }
        return XnetPolicy::accessTo($this->user, $this->controller->get_name(), $action);
    }

    /**
     * Código que se ejecuta antes de ejecutar privateCore
     * Retorna false si por algún motivo, se deniega el acceso.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0823
     *
     * @return bool
     */
    protected function prePrivateCore(): bool
    {
        $this->user = $this->loginTools->getLoggedUser();

        XnetPolicy::loadUserData($this->user);
        if (in_array($this->action, $this->actions) && !$this->haveAccessTo($this->action)) {
            $this->new_error_msg($this->action . ' denied!');
            return false;
        }

        $this->query = self::filterRequest('query');

        /// quitamos extensiones de páginas a las que el usuario no tenga acceso
        foreach ($this->extensions as $i => $value) {
            if ($value->type != 'config' && !$this->user->have_access_to($value->from)) {
                unset($this->extensions[$i]);
            }
        }

        return true;
    }

    private function menuHtmlLevel($menu, $actual_folder, $level = 1, $previous_level = 1, $folder_reference = '')
    {
        $url_template = 'index.php?page=';
        $cadena = '';
        // Generado de menú
        foreach ($menu as $key_menu_element => $menu_element) {
            // Condición de se está en el nivel 1 para insertar el menú title

            if (is_array($menu_element) && isset($menu_element['name'])) {
                $class_link = '';
                $label = "<li>" .
                    "<a href='" . $url_template . $menu_element['name'] . "' class='d-flex " . $class_link . "' title=''>" .
                    "<span class='w-100'>" . $menu_element['title'] . "</span>" .
                    "</a>" .
                    "</li>";
                $cadena .= $label;
                continue;
            }

            if ($level == 1) {
                $folder_reference = $key_menu_element;
                $folder_data = get_page_info($key_menu_element);
                $label = '<li class="menu-title">' . $folder_data['name'] . '</li>' .
                    $this->menuHtmlLevel($menu_element, $actual_folder, $level + 1, $level, $folder_reference);
                $cadena .= $label;
            } else {
                $is_selected = false;
                if (is_array($menu_element)) {
                    if (strpos($actual_folder, $key_menu_element) === 0) {
                        $is_selected = true;
                    }
                }

                $folder_classes = actual_page_classes($is_selected);
                $folder_data = get_page_info($key_menu_element);

                $label = '<li class="' . $folder_classes['class_open_folder'] . '">' .
                    '<li class="' . $folder_classes['class_open_folder'] . '">' .
                    '<a href="#" class="has-arrow d-flex' . $folder_classes['class_folder'] . '" aria-expanded="' . $folder_classes['expanded_folder'] . '" title="' . $folder_data['name'] . '">' .
                    '<i class="' . $folder_data['icon'] . '"></i>' .
                    '<span class="w-100">' . $folder_data['name'] . '</span>' .
                    '</a>' .
                    '<ul class="sub-menu" aria-expanded="true">' .
                    $this->menuHtmlLevel($menu_element, $actual_folder, $level + 1, $level, $folder_reference) .
                    '</ul>' .
                    '</li>';
                $cadena .= $label;
            }
        }

        return $cadena;
    }

    public function menuHtml(): string
    {
        $menu = XnetPolicy::getArrayMenu();
        return $this->menuHtmlLevel($menu, $this->class_name);
    }

    protected function checkAuth()
    {
        $this->controller = new Controller([
            'name' => $this->name,
            'title' => $this->title,
            'folder' => $this->folder,
            'version' => $this->version(),
            'show_on_menu' => $this->show_on_menu,
            'order' => $this->order,
        ]);

        $ok = false;

        // Ahora debemos comprobar si guardar o no
        if ($this->name !== 'fs_controller') {
            $controller = $this->controller->get($this->name);
            if ($controller === false) {
                $ok = $this->controller->save();
            } else {
                $ok = true;
                if ($controller->title != $this->title || $controller->folder != $this->folder || $controller->show_on_menu != $this->show_on_menu) {
                    $controller->name = $this->name;
                    $controller->title = $this->title;
                    $controller->folder = $this->folder;
                    $controller->show_on_menu = $this->show_on_menu;
                    $controller->order = $this->order;
                    $ok = $controller->save();
                }
                $this->controller = $controller;
            }
        }

        // Ahora revisamos los permisos que puede tener la página.
        $actionModel = new Action();
        foreach ($this->actions as $permName) {
            $action = $actionModel->getBy([
                'name' => $permName,
                'controller_name' => $this->name,
            ]);

            if (count($action) === 0) {
                Action::create([
                    'name' => $permName,
                    'controller_name' => $this->name,
                    'description' => $permName . ' para ' . $this->name,
                ]);
            }
        }

        return $ok;
    }

    /**
     * Devuelve información del sistema para el informe de errores
     *
     * @return string la información del sistema
     */
    public function system_info()
    {
        $txt = 'MiFactura: ' . $this->version() . "\n";

        if (DB::connected()) {
            if ($this->user->logged_on) {
                $txt .= 'os: ' . php_uname() . "\n";
                $txt .= 'php: ' . phpversion() . "\n";
                $txt .= 'database type: ' . constant('FS_DB_TYPE') . "\n";
                $txt .= 'database version: ' . DB::version() . "\n";

                if (constant('FS_FOREIGN_KEYS') == 0) {
                    $txt .= "foreign keys: NO\n";
                }

                if ($this->cache->connected()) {
                    $txt .= "memcache: YES\n";
                    $txt .= 'memcache version: ' . $this->cache->version() . "\n";
                } else {
                    $txt .= "memcache: NO\n";
                }

                if (function_exists('curl_init')) {
                    $txt .= "curl: YES\n";
                } else {
                    $txt .= "curl: NO\n";
                }

                $txt .= "max input vars: " . ini_get('max_input_vars') . "\n";

                $txt .= 'plugins: ' . join(',', XnetConfig::getEnabledPlugins()) . "\n";

                if ($this->check_for_updates()) {
                    $txt .= "updated: NO\n";
                }

                if (filter_input(INPUT_SERVER, 'REQUEST_URI')) {
                    $txt .= 'url: ' . filter_input(INPUT_SERVER, 'REQUEST_URI') . "\n------";
                }
            }
        } else {
            $txt .= 'os: ' . php_uname() . "\n";
            $txt .= 'php: ' . phpversion() . "\n";
            $txt .= 'database type: ' . constant('FS_DB_TYPE') . "\n";
        }

        foreach ($this->getErrors() as $e) {
            $txt .= "\n" . $e;
        }

        return str_replace('"', "'", $txt);
    }

    /**
     * Carga las extensiones para la clase en ejecución.
     */
    private function loadExtensions()
    {
        // Obtenemos las páginas a las que tiene acceso el usuario
        $Controllers = [];
        foreach ($this->menu as $Controller) {
            $Controllers[] = $Controller->name;
        }

        $fsext = new \fs_extension();
        foreach ($fsext->all() as $ext) {
            if ($ext->type == 'head') {
                $ext->delete();
            } else {
                /// Cargamos las extensiones para este controlador o para todos, y que además el usuario tenga acceso
                if (
                    in_array($ext->to, [null, $this->class_name,]) &&
                    in_array($ext->from, $Controllers)
                ) {
                    $this->extensions[] = $ext;
                }
            }
        }
    }

    /**
     * Intenta hacer un filter POST, y si no existe la variable, lo intenta con un GET.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0724
     *
     * @param string $var_name
     * @param int    $filter
     *
     * @return string|null
     */
    public static function filterRequest(string $var_name, int $filter = FILTER_DEFAULT): ?string
    {
        $result = filter_input(INPUT_POST, $var_name, $filter);
        if ($result === null) {
            $result = filter_input(INPUT_GET, $var_name, $filter);
        }
        return $result;
    }

    public function getMenu($reload = false)
    {
        $x = XnetPolicy::getMenu();
        return $x;
    }

    public function getArrayMenu($reload = false)
    {
        $x = XnetPolicy::getArrayMenu();
        return $x;
    }

    public function canExecute($action)
    {
        return true;
        return $this->user->haveAccessTo($action);
    }

    /**
     * Retorna todos los métodos que comiencen por $prefix, y la siguiente letra sea mayúscula.
     * Los retorna como un array sin el prefijo $prefix.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0809
     *
     * @return array
     */
    private static function getActions()
    {
        $prefix = self::ACTION_PREFIX;
        $length = mb_strlen($prefix);

        $result = [];
        $methods = get_class_methods(get_called_class());
        foreach ($methods as $method) {
            $nextLetter = mb_substr($method, $length, 1);
            if (mb_substr($method, 0, $length) === $prefix && $nextLetter === strtoupper($nextLetter)) {
                $result[] = mb_substr($method, $length);
            }
        }
        return $result;
    }

    public function doLogout()
    {
        $this->template = 'login/' . $this->empresa->plantilla_login;
        return $this->loginTools->logout();
    }

    public function getUserName()
    {
        return $this->user->code ?? 'Usuario';
    }

    public function getUserUrl()
    {
        return 'index.php?page=Users&id=' . $this->user->id;
    }

    public function getLogoutUrl()
    {
        return $this->url() . '&action=logout';
    }
}
