<?php
/*
 * This file is part of MiFactura.eu
 * Copyright (C) 2021 X-Net Software Solutions S.L. <xnetsoftware@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace Xnet\Core;

require_once constant('BASE_PATH') . '/model/fs_user.php';

use MiFactura\model\agente;
use MiFactura\model\fs_user;
use Xnet\Model\User;

/**
 * Class XnetLogin
 *
 * Contiene las herramientas de identificación de usuario
 *
 * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
 * @version 2022.0805
 *
 * @package Xnet\Core
 */
class XnetLogin
{
    /**
     * Mensaje de motivo del baneo.
     *
     * @var string
     */
    private $banMessage;

    /**
     * Permite conectar e interactuar con memcache.
     *
     * @var XnetCache
     */
    private $cache;

    /**
     * Gestiona el log de todos los controladores, modelos y base de datos.
     *
     * @var XnetLog
     */
    private $coreLog;

    /**
     * Acceso a la clase que gestiona el filtro por IP.
     *
     * @var XnetIpFilter
     */
    private $ip_filter;

    /**
     * Acceso a la clase de usuarios del entorno.
     *
     * @var User
     */
    private $userModel;

    /**
     * Identificador único de la empresa
     *
     * @var string
     */
    private $xid;

    /**
     * Usuario activo
     *
     * @var User
     */
    private $user;

    /**
     * Modelo de la tabla antigua de usuarios.
     *
     * @var fs_user
     *
     * @deprecated Se utiliza mientras coexistan fs_user y User.
     *             Una vez realizada la migración, debe de desaparecer.
     */
    private $deprecatedUserModel;

    /**
     * Usuario activo (versión antigua)
     *
     * @var fs_user
     *
     * @deprecated Se utiliza mientras coexistan fs_user y User.
     *             Una vez realizada la migración, debe de desaparecer.
     */
    private $deprecatedUser;

    /**
     * fs_login constructor.
     *
     * @param string $xid
     */
    public function __construct($xid)
    {
        $this->xid = $xid;
        $this->banMessage = 'Tendrás que esperar ' . XnetIpFilter::BAN_SECONDS . ' segundos antes de volver a intentar entrar.';
        $this->cache = new XnetCache();
        $this->coreLog = new XnetLog();
        $this->ip_filter = new XnetIpFilter();

        $this->deprecatedUserModel = new \fs_user();
        $this->deprecatedUser = $this->getUserFs();

        $this->userModel = new User();
        $this->user = $this->getUser();
    }

    private function getUser(): ?User
    {
        $nick = filter_input(INPUT_POST, 'user');
        $password = filter_input(INPUT_POST, 'password');

        // Si se le ha pasado usuario y contraseña, intentamos hacer login con ellos
        if (is_string($nick) && is_string($password)) {
            if ($nick === 'soporte') {
                return $this->loginSoporte($password);
            }
            return $this->login($nick, $password);
        }

        // Si no se le ha pasado usuario y contraseña, se trata de hacer login con las cookies
        return $this->loginCookies();
    }

    private function login(string $nick, string $password): ?User
    {
        $ip = fs_get_ip();

        if ($this->ip_filter->isBanned($ip)) {
            $this->coreLog->new_error('Tu IP ha sido baneada, ' . $nick . '. ' . $this->banMessage);
            $this->coreLog->save('Tu IP ha sido baneada, ' . $nick . '. ' . $this->banMessage, 'login', true);
            return null;
        }

        $this->ip_filter->setAttempt($ip);

        $userDeprecated = $this->deprecatedUserModel->get($nick);
        $user = $this->userModel->getByCode($nick);

        if (!isset($user->deleted_at) && $user->enabled && $user->password === sha1($password)) {
            $user->newLogkey();
            if ($user->save()) {
                $this->deprecatedUser->log_key = $user->log_key;
                $this->deprecatedUser->save();
            }
            $this->saveCookie($user);
            return $user;
        }

        return null;
    }

    private function saveCookie($user)
    {
        $time = time() + constant('FS_COOKIES_EXPIRE');

        assign_cookie('user_' . $this->xid, $user->code);
        assign_cookie('logkey_' . $this->xid, $user->log_key);

        // Cookie para el CDN de CloudFlare para DataTables
        assign_cookie('PHPSESSID', '.datatables.net', [
            'samesite' => 'None',
            'expires' => $time,
            'path' => '/',
            'domain' => '.datatables.net',
            'secure' => true,
            'httponly' => false,
        ]);
        assign_cookie('jsbin', '.live.datatables.net', [
            'samesite' => 'None',
            'expires' => $time,
            'path' => '/',
            'domain' => '.live.datatables.net',
            'secure' => true,
            'httponly' => false,
        ]);
    }

    private function loginSoporte(string $password): ?User
    {
        $nick = 'soporte';
        $user = $this->userModel->getByCode($nick);
        // Si no existe en la base de datos, lo creamos
        if (!$user) {
            $user = new User();
            $user->name = 'Usuario de soporte';
            $user->code = $nick;
            $user->email = 'soporte@x-netdigital.com';
            $user->admin = true;
            $user->enabled = true;
        }

        // Recuperamos la contraseña de soporte, y si no podemos, pues falla el login y se intenta como usuario normal con el password almacenado.
        $password_soporte = @file_get_contents('https://xnetsl.mifactura.eu/support.password.hash');
        if ($password_soporte === false) {
            return null;
        }

        // Si la contraseña es incorrecta, falla...
        if (md5($password) !== $password_soporte) {
            $this->coreLog->new_error('Identificación incorrecta como usuario de soporte');
            return null;
        }

        $this->coreLog->new_message('Identificado como usuario de soporte');

        // Si no activamos el usuario, nos cierra sessión
        if ($user->enabled != true) {
            $user->enabled = true;
        }

        if ($user->save()) {
            $this->saveCookie($user);

            /// añadimos el mensaje al log
            $this->coreLog->save('Login correcto.', 'login');

            /// limpiamos la lista de IPs
            $this->ip_filter->clear();

            return $user;
        }
        return null;
    }

    private function loginCookies(): ?User
    {
        $nick = filter_input(INPUT_COOKIE, 'user_' . $this->xid);

        $userDeprecated = $this->deprecatedUserModel->get($nick);
        $user = $this->userModel->getByCode($nick);

        if ($userDeprecated !== false && $user === null) {
            $employee_id = null;
            if (is_plugin_enabled('soporte_empresas') && isset($userDeprecated->codagente)) {
                $empleado = (new agente())->get($userDeprecated->codagente);
                /*
                if (isset($empleado->codagente)) {
                    new Employee();
                    $employee = Employee::getByCode($empleado->codagente);
                    if (isset($employee)) {
                        $employee_id = $employee->get_id();
                    }
                }
                */
            }

            $data = [
                'code' => $userDeprecated->nick,
                'name' => $userDeprecated->nick,
                'email' => $userDeprecated->email ?? 'user@xnet.com',
                'password' => $userDeprecated->password,
                'log_key' => $userDeprecated->log_key,
                'admin' => $userDeprecated->admin,
                'last_login' => $userDeprecated->last_login,
                'last_ip' => $userDeprecated->last_ip,
                'last_browser' => $userDeprecated->last_browser,
                'employee_id' => $employee_id,
            ];
            User::create($data);
        }

        if ($user === false || isset($user->deleted_at) || !$user->enabled) {
            $this->coreLog->new_error('¡El usuario ' . $nick . ' no existe o está desactivado!');
            $this->logout(true);
            $this->userModel->cleanCache(true);
            $this->cache->clean();
            return null;
        }

        $logkey = filter_input(INPUT_COOKIE, 'logkey_' . $this->xid);
        if ($user->log_key == $logkey) {
            $user->logged_on = true;
            $user->updateLogin();
            $this->saveCookie($user);
            return $user;
        }

        $this->coreLog->new_message('1¡Cookie no válida! Alguien ha accedido a esta cuenta desde otro PC con IP: ' . $user->last_ip . ". Si has sido tú, ignora este mensaje.");
        $this->logout();
        return $user;
    }

    /**
     * Obtiene el usuario de la tabla fs_user
     *
     * @author     Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version    2022.0808
     *
     * @return fs_user|null
     *
     * @deprecated Ésto sólo es válido mientras que se mantenga la tabla fs_user
     */
    private function getUserFs(): ?fs_user
    {
        $nick = fs_filter_input_post('user');
        $password = fs_filter_input_post('password');

        // Si se le ha pasado usuario y contraseña, intentamos hacer login con ellos
        if (is_string($nick) && is_string($password)) {
            if ($nick === 'soporte') {
                return $this->loginSoporteFs($password);
            }
            return $this->loginFs($nick, $password);
        }

        // Si no se le ha pasado usuario y contraseña, se trata de hacer login con las cookies
        return $this->loginCookiesFs();
    }

    /**
     *
     *
     * @author     Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version    2022.0808
     *
     * @param string $nick
     * @param string $password
     *
     * @return fs_user|null
     *
     * @deprecated Ésto sólo es válido mientras que se mantenga la tabla fs_user
     */
    private function loginFs(string $nick, string $password): ?fs_user
    {
        $ip = fs_get_ip();

        if ($this->ip_filter->isBanned($ip)) {
            $this->coreLog->new_error('Tu IP ha sido baneada, ' . $nick . '. ' . $this->banMessage);
            $this->coreLog->save('Tu IP ha sido baneada, ' . $nick . '. ' . $this->banMessage, 'login', true);
            return null;
        }

        $this->ip_filter->setAttempt($ip);

        $user = $this->deprecatedUserModel->get($nick);
        if ($user->enabled && $user->password === sha1($password)) {
            return $user;
        }

        return null;
    }

    /**
     *
     *
     * @author     Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version    2022.0808
     *
     * @param string $password
     *
     * @return fs_user|null
     *
     * @deprecated Ésto sólo es válido mientras que se mantenga la tabla fs_user
     */
    private function loginSoporteFs(string $password): ?fs_user
    {
        $nick = 'soporte';
        $user = $this->deprecatedUserModel->get($nick);
        // Si no existe en la base de datos, lo creamos
        if (!$user) {
            $user = new fs_user();
            $user->nick = $nick;
            $user->email = 'soporte@x-netdigital.com';
            $user->admin = true;
            $user->enabled = true;
        }

        // Recuperamos la contraseña de soporte, y si no podemos, pues falla el login y se intenta como usuario normal con el password almacenado.
        $password_soporte = @file_get_contents('https://xnetsl.mifactura.eu/support.password.hash');
        if ($password_soporte === false) {
            return null;
        }

        // Si la contraseña es incorrecta, falla...
        if (md5($password) !== $password_soporte) {
            $this->coreLog->new_error('Identificación incorrecta como usuario de soporte');
            return null;
        }

        $this->coreLog->new_message('Identificado como usuario de soporte');

        // Si no activamos el usuario, nos cierra sessión
        if ($user->enabled != true) {
            $user->enabled = true;
        }

        if ($user->save()) {
            $this->saveCookie($user);

            /// añadimos el mensaje al log
            $this->coreLog->save('Login correcto.', 'login');

            /// limpiamos la lista de IPs
            $this->ip_filter->clear();

            return $user;
        }
        return null;
    }

    /**
     *
     *
     * @author     Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version    2022.0808
     *
     * @return User|null
     *
     * @deprecated Ésto sólo es válido mientras que se mantenga la tabla fs_user
     */
    private function loginCookiesFs(): ?fs_user
    {
        $nick = filter_input(INPUT_COOKIE, 'user_' . $this->xid);

        $user = $this->deprecatedUserModel->get($nick);
        if ($user === false || !$user->enabled) {
            $this->coreLog->new_error('¡El usuario ' . $nick . ' no existe o está desactivado!');
            $this->logout(true);
            $this->deprecatedUserModel->clean_cache(true);
            $this->cache->clean();
            return null;
        }

        // TODO: Eliminar $logkey_old unas versiones más adelante cuando todos los clientes estén actualizados
        $logkey_old = filter_input(INPUT_COOKIE, 'logkey');
        $logkey = filter_input(INPUT_COOKIE, 'logkey_' . $this->xid);
        if ($user->log_key == $logkey) {
            $user->logged_on = true;
            $user->update_login();
            $this->save_cookie($user);
            return $user;
        }
        if ($user->log_key === $logkey_old) {
            // TODO: Eliminar esta condición unas versiones más adelante cuando todos los clientes estén actualizados
            $user->logged_on = true;
            $user->update_login();
            $this->save_cookie($user);
            // Caducamos la cookie vieja
            assign_cookie('logkey', '');
            return $user;
        }
        $this->coreLog->new_message('2¡Cookie no válida! Alguien ha accedido a esta cuenta desde otro PC con IP: ' . $user->last_ip . ". Si has sido tú, ignora este mensaje.");
        $this->logout();
        return $user;
    }

    /**
     * Gestiona el cierre de sesión
     *
     * @param bool $rmuser eliminar la cookie del usuario
     */
    public function logout($rmuser = false): bool
    {
        $path = '/';
        if (filter_input(INPUT_SERVER, 'REQUEST_URI')) {
            $aux = parse_url(str_replace('/index.php', '', filter_input(INPUT_SERVER, 'REQUEST_URI')));
            if (isset($aux['path'])) {
                $path = $aux['path'];
                if (substr($path, -1) != '/') {
                    $path .= '/';
                }
            }
        }

        /// borramos las cookies
        if (filter_input(INPUT_COOKIE, 'logkey_' . $this->xid)) {
            $ok = assign_cookie('logkey_' . $this->xid, '');
            if (constant('FS_DEBUG')) {
                debug_message('Borrar cookie de usuario (logkey): ' . $ok);
            }
        }

        /// ¿Eliminamos la cookie del usuario?
        $user = filter_input(INPUT_COOKIE, 'user_' . $this->xid);
        if ($rmuser && $user) {
            $ok = assign_cookie('user_' . $this->xid, '');
            if (constant('FS_DEBUG')) {
                debug_message('Borrar cookie de usuario (user): ' . $ok);
            }
        }

        $own_cookies = [
            'samesite' => 'Strict',
            'expires' => -1,
            'path' => '',
            //'domain' => $_SERVER['HTTP_HOST'],
            'domain' => '',
            'secure' => true,
            'httponly' => false,
        ];
        $own_cookies_incorrect = [
            'samesite' => 'Strict',
            'expires' => -1,
            'path' => '',
            'domain' => $_SERVER['HTTP_HOST'],
            'secure' => true,
            'httponly' => false,
        ];
        foreach ($_COOKIE as $name => $value) {
            assign_cookie($name, '', $own_cookies);
            assign_cookie($name, '', $own_cookies_incorrect);
        }

        /// guardamos el evento en el log
        $this->coreLog->set_user_nick($user);
        $this->coreLog->save('El usuario ha cerrado la sesión.', 'login');

        return true;
    }

    /**
     * Guarda las cookies del usuario.
     *
     * @param fs_user $user
     *
     * @deprecated Use saveCookie
     */
    private function save_cookie(fs_user $user)
    {
        $time = time() + constant('FS_COOKIES_EXPIRE');

        assign_cookie('user_' . $this->xid, $user->nick);
        assign_cookie('logkey_' . $this->xid, $user->log_key);

        // Cookie para el CDN de CloudFlare para DataTables
        assign_cookie('PHPSESSID', '.datatables.net', [
            'samesite' => 'None',
            'expires' => $time,
            'path' => '/',
            'domain' => '.datatables.net',
            'secure' => true,
            'httponly' => false,
        ]);
        assign_cookie('jsbin', '.live.datatables.net', [
            'samesite' => 'None',
            'expires' => $time,
            'path' => '/',
            'domain' => '.live.datatables.net',
            'secure' => true,
            'httponly' => false,
        ]);
    }

    /**
     * Cambia la contraseña del usuario si éste no ha sido baneado.
     *
     * @return bool
     */
    public function changePassword()
    {
        $ip = fs_get_ip();
        $nick = filter_input(INPUT_POST, 'user');
        if ($this->ip_filter->isBanned($ip)) {
            $this->ip_filter->setAttempt($ip);
            $this->coreLog->new_error('Tu IP ha sido baneada, ' . $nick . '. ' . $this->banMessage);
            $this->coreLog->save('Tu IP ha sido baneada, ' . $nick . '. ' . $this->banMessage);
            return false;
        }

        if (!$this->ip_filter->inWhiteList($ip)) {
            $this->coreLog->new_error('No puedes acceder desde esta IP, ' . $nick . '.');
            $this->coreLog->save('No puedes acceder desde esta IP, ' . $nick . '.', 'login', true);
            return false;
        }

        $new_password = filter_input(INPUT_POST, 'new_password');
        $new_password2 = filter_input(INPUT_POST, 'new_password2');

        if ($new_password != $new_password2) {
            $this->coreLog->new_error('Las contraseñas no coinciden, ' . $nick);
            return false;
        }

        if ($new_password == '') {
            $this->coreLog->new_error('Tienes que escribir una contraseña nueva, ' . $nick);
            return false;
        }

        $db_password = filter_input(INPUT_POST, 'db_password');
        if ($db_password != constant('FS_DB_PASS')) {
            $this->ip_filter->setAttempt($ip);
            $this->coreLog->new_error('La contraseña de la base de datos es incorrecta, ' . $nick);
            return false;
        }

        $ok = true;

        if ($this->deprecatedUser) {
            $this->deprecatedUser->set_password($new_password);
            if (!$this->deprecatedUser->save()) {
                $this->coreLog->new_error('Users: Imposible cambiar la contraseña del usuario ' . $nick);
                $ok = false;
            }
        }

        if ($this->user) {
            $this->user->setPassword($new_password);
            if (!$this->user->save()) {
                $this->coreLog->new_error('Users: Imposible cambiar la contraseña del usuario ' . $nick);
                $ok = false;
            }
        }

        if ($ok) {
            $this->coreLog->new_message('Contraseña cambiada correctamente para ' . $nick);
        }

        return $ok;
    }

    public function logged()
    {
        return isset($this->user) || isset($this->deprecatedUser);
    }

    public function getLoggedUser(): ?User
    {
        return $this->user;
    }
}
