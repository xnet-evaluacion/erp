<?php
/*
 * This file is part of MiFactura.eu
 * Copyright (C) 2021 X-Net Software Solutions S.L. <xnetsoftware@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace Xnet\Core;

use DebugBar\DataCollector\MessagesCollector;
use DebugBar\DebugBarException;
use DebugBar\StandardDebugBar;
use Xnet\Core\DebugBarCollectors\PhpCollector;

/**
 * Class debugBar
 */
class XnetDebugBar
{
    /**
     * Contiene la instancia común a la debugBar
     *
     * @var StandardDebugBar
     */
    private static $debugBar;

    /**
     * Clase que se encarga de la parte JS de la debugBar.
     *
     * @var \DebugBar\JavascriptRenderer
     */
    private static $debugBarRenderer;

    /**
     * debugBar constructor.
     *
     * @throws DebugBarException
     */
    public function __construct()
    {
        if (!isset(self::$debugBar)) {
            self::$debugBar = new StandardDebugBar();
            try {
                self::$debugBar->addCollector(new PhpCollector());
                self::$debugBar->addCollector(new MessagesCollector('SQL'));
                // TODO: Hay algún problema y se pierden las notificaciones
                //self::$debugBar->addCollector(new DatabaseCollector(new fs_db2()));
            } catch (DebugBarException $e) {
                echo $e->getCode() . ' ' . $e->getFile() . '[' . $e->getLine() . ']: ' . $e->getMessage();
            }
            $baseUrl = constant('FS_PATH') . 'vendor/maximebf/debugbar/src/DebugBar/Resources';
            self::$debugBarRenderer = self::$debugBar->getJavascriptRenderer($baseUrl, FS_FOLDER);
        }
    }

    /**
     * Añade una consulta SQL al colector de SQL.
     *
     * @param mixed $pos
     * @param mixed $message
     */
    public static function addQuery($pos, $message)
    {
        self::addMessage("SQL", "$pos: $message");
    }

    /**
     * Añade un mensaje al colector de mensajes.
     *
     * @param mixed $message
     */
    public static function addMessage($collector, $message, $label = 'info')
    {
        self::$debugBar[$collector]->addMessage($message, $label, is_string($message));
    }

    /**
     * Inicia el temporizador.
     *
     * @param string $name
     * @param mixed  $message
     *
     * @throws DebugBarException
     */
    public static function startTimer($name, $message)
    {
        self::$debugBar->getCollector('time')->startMeasure($name, $message);
    }

    /**
     * Detiene el temporizador.
     *
     * @param string $name
     *
     * @throws DebugBarException
     */
    public static function stopTimer($name)
    {
        self::$debugBar->getCollector('time')->/** @scrutinizer ignore-call */ stopMeasure($name);
    }

    /**
     * Devuelve la debugBar
     *
     * @return StandardDebugBar
     */
    public static function getDebugTool()
    {
        return self::$debugBar;
    }

    /**
     * Devuelve la cabecera de página para la debugBar.
     *
     * @return string
     */
    public static function getHeader()
    {
        if (self::$debugBarRenderer === null) {
            return '';
        }
        return self::$debugBarRenderer->renderHead();
    }

    /**
     * Devuelve el pié de página para la debugBar.
     *
     * @return string
     */
    public static function getFooter()
    {
        if (self::$debugBarRenderer === null) {
            return '';
        }
        return self::$debugBarRenderer->render();
    }
}
