<?php

namespace Xnet\Core;

use Xnet\Model\Version;

require_once constant('BASE_PATH') . '/base/fs_model.php';

class XnetPluginManager
{
    /**
     * TODO: Missing documentation
     *
     * @var Version
     */
    private $plugins;

    /**
     * PluginManager constructor.
     */
    public function __construct()
    {
        $this->plugins = new Version();
    }

    /**
     * TODO: Missing documentation
     */
    public function checkVersions()
    {
        $pluginsPath = constant('BASE_PATH') . '/plugins';

        // Se actualiza el núcleo
        $this->updateCore();

        $plugins = scandir($pluginsPath);
        foreach ($plugins as $plugin) {
            if (in_array($plugin, ['.', '..'])) {
                continue;
            }

            // Se actualiza cada uno de los plugins instalados y activos.
            // Si no existe el registro se crea.
            // Ver si es necesario añadir nuevos campos como instalado, actualizado,...
            // Es posible que se pueda controlar dependencias incumplidas o algo...
            $this->update($plugin);
        }
    }

    /**
     *
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0824
     *
     * @return false|void
     */
    public function updateCore()
    {
        $plugin_name = 'core';
        $updater = 'updates_' . $plugin_name;
        $class = constant('BASE_PATH') . 'dataconfig/' . $updater . '.php';

        // Se determina la versión del núcleo que puede ser de MiFactura o de XFS.
        $version = file_get_contents(constant('BASE_PATH') . '/MiVersion');
        if ($version === false) {
            $version = file_get_contents(constant('BASE_PATH') . '/XfsVersion');
        }

        $plugin = $this->plugins->getByName($plugin_name);
        if (!$plugin) {
            $plugin = new version();
            $plugin->plugin = $plugin_name;
            $plugin->id = 1;
        }
        $plugin->version = $version;

        if (!$plugin->save()) {
            return false;
        }

        if (!file_exists($class)) {
            // echo "<p>No existe '$class'.</p>";
            return true;
        }

        require_once $class;
        $updater = new $updater($plugin);
        $updater->update();
    }

    /**
     * TODO: Missing documentation
     */
    public function update($plugin_name)
    {
        $updater = 'updates_' . $plugin_name;
        $class = constant('BASE_PATH') . '/plugins/' . $plugin_name . '/dataconfig/' . $updater . '.php';

        $plugin = $this->plugins->getByName($plugin_name);
        if (!$plugin) {
            $plugin = new version();
            $plugin->plugin = $plugin_name;
        }

        $config_filename = constant('BASE_PATH') . '/plugins/' . $plugin_name . '/mifactura.ini';
        if (!file_exists($config_filename)) {
            $config_filename = constant('BASE_PATH') . '/plugins/' . $plugin_name . '/xfs.ini';
        }

        $parameters = parse_ini_file($config_filename);
        $plugin->version = $parameters['version'];

        if (!$plugin->save()) {
            return false;
        }

        if (!file_exists($class)) {
            // echo "<p>No existe '$class'.</p>";
            return true;
        }

        require_once $class;
        $updater = new $updater($plugin);
        $updater->update();
    }

}
