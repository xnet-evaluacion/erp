<?php
/*
 * This file is part of facturacion_base for MiFactura.eu
 * Copyright (C) 2021 X-Net Software Solutions S.L. <xnetsoftware@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace Xnet\Controllers;

use Xnet\Core\XnetAuthController;

/**
 * Description of dashboard
 */
class Dashboard extends XnetAuthController
{
    /**
     * dashboard constructor.
     */
    public function __construct()
    {
        parent::__construct([
            'show_on_menu' => true,
            'folder' => 'admin|dashboard',
            'title' => 'Dashboard',
            'alias' => 'Dashboard',
            'description' => 'Dashboard',
            'order' => 100,
        ]);
        $this->show_background = true;
    }

    /**
     * Código que se ejecutará en la parte privada
     */
    protected function privateCore(): bool
    {
        return true;
    }
}
