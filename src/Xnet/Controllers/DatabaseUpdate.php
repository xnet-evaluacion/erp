<?php

/*
 * This file is part of MiFactura.eu
 * Copyright (C) 2021 X-Net Software Solutions S.L. <xnetsoftware@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace Xnet\Controllers;

use Xnet\Core\XnetAjaxController;
use Xnet\Model\Version;

/**
 * Class database_update
 *
 * Verifica el estado de los plugins y actualiza la tabla versiones y la base de datos.
 *
 * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
 * @version 2022.0624
 *
 */
class DatabaseUpdate extends XnetAjaxController
{
    function process($name, $sequence, $plugin_version)
    {
        $versiones = new Version();
        $version = $versiones->getByName($name);
        if ($version === false) {
            $version = clone $versiones;
            $version->plugin = $name;
        }
        $version->sequence = $sequence;

        $class = 'updates_' . $name;
        $ruta = constant('FS_FOLDER');
        if ($name !== 'core') {
            $ruta .= '/plugins/' . $name;
        }
        $ruta .= '/dataconfig/' . $class . '.php';

        if (file_exists($ruta)) {
            require_once($ruta);
            $update = new $class($version);
            $update->update();
        }
    }

    protected function privateCore(): bool
    {
        parent::privateCore();

        $versiones = new Version();
        // $versiones->checkPlugins();

        $core = $versiones->getByName('core');
        if (empty($core)) {
            Version::checkPlugins();
            $core = $versiones->getByName('core');
            if (empty($core)) {
                $this->result['core'][] = [
                    'status' => false,
                    'plugin' => 'core',
                    'message' => 'El núcleo no existe en la tabla de versiones.',
                ];
                return false;
            }
        }

        if ($core->version > 9999) {
            $versiones->check_table('versions');
            $core = $versiones->getByName('core');
            $core->version = 0;
            $core->save();
        }

        $activates = $versiones->getActivatedPlugins();

        //        dump(['activates' => $activates]);
        //        die();

        // Data va a contener los métodos a ejecutar en orden [version][activacion][plugin] => método a ejecutar.
        $data = [];
        foreach ($activates as $sequence => $item) {
            $functions = $item->getFunctions($item);
            foreach ($functions as $version => $function) {
                $data[$version][$sequence][$item->plugin] = $function;
            }
        }

        foreach ($data as $version => $data2) {
            foreach ($data2 as $sequence => $data3) {
                foreach ($data3 as $class => $method) {
                    $plugin = Version::getByName($class);
                    if (!isset($plugin)) {
                        continue;
                    }
                    $ruta = Version::getRoute($class);
                    if ($ruta === false) {
                        continue;
                    }

                    $class = 'updates_' . $class;
                    $pluginName = $plugin->plugin;

                    require_once($ruta);
                    $update = new $class($plugin);

                    $result = $update->{$method}();
                    if ($result['status'] === false) {
                        $this->result[$pluginName][] = [
                            'status' => false,
                            'plugin' => $plugin->plugin . ' v' . $plugin->version,
                            'route' => $ruta,
                            'method' => $method,
                            'message' => 'Actualización fallida para ' . $plugin->plugin,
                            'errors' => $result['error'] ?? [],
                            'advices' => $result['advice'] ?? [],
                        ];
                        return false;
                    }

                    $plugin->db_version = $version;
                    if (!$plugin->save()) {
                        $this->result[$pluginName][] = [
                            'status' => false,
                            'plugin' => $plugin->plugin . ' v' . $plugin->version,
                            'route' => $ruta,
                            'method' => $method,
                            'message' => 'No se han podido guardar los cambios para la actualización ' . $plugin->plugin,
                        ];
                        return false;
                    }

                    $this->result[$pluginName][] = [
                        'status' => true,
                        'plugin' => $plugin->plugin . ' v' . $plugin->version,
                        'route' => $ruta,
                        'method' => $method,
                        'message' => 'Actualización correcta para ' . $plugin->plugin,
                    ];
                }
            }
        }

        return true;
    }
}
