<?php

namespace Xnet\Model;

use Xnet\Core\XnetModel;

class Role extends XnetModel
{
    public function get_name()
    {
        return $this->name;
    }

    static public function primaryKey()
    {
        return 'id';
    }

    static public function tablename()
    {
        return 'roles';
    }

    public static function getByCode($code): ?XnetModel
    {
        $data = Role::getBy(['code' => $code]);
        if (count($data) === 0) {
            return null;
        }
        return Role::getById(reset($data)['id']);
    }

    public function getAccesses()
    {
        new ActionRole();
        new Action();

        $actions = ActionRole::getBy(['role_id' => $this->get_id()]);

        $result = [];
        foreach ($actions as $action) {
            $result[] = [
                'action' => Action::getById($action['action_id']),
                'authorized' => $action['authorized'],
            ];
        }

        return $result;
    }

    /**
     * Retorna true si la acción está autorizada por el rol.
     * Si la acción no ha sido definida en el rol pueden pasar dos cosas:
     * - Si se le ha pasado null en $default, tomará el valor por defecto del rol.
     * - Si se le ha pasado un bool en $default, tomará ese valor por defecto.
     *
     * La forma normal de obtener el acceso es la siguiente.
     * Se recorren todos los roles tomando null en $default del primer rol disponible.
     * Luego, se va pasando el $default el resultado para el rol anterior, de manera que
     * si se tome el último definido, o el valor por defecto del primero si no hay nada
     * definido para esa acción.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0812
     *
     * @param int       $actionId
     * @param bool|null $default
     *
     * @return bool
     */
    public function getAccessTo(int $actionId, ?bool $default = null): bool
    {
        $info = ['action_id' => $actionId, 'role_id' => $this->get_id()];
        $data = ActionRole::getBy($info);

        // Si no se ha definido la acción en el rol, se retorna lo arrastrado del anterior,
        // o por defecto si no se ha arrastrado nada.
        if (count($data) === 0) {
            return $default ?? $this->default;
        }
        return reset($data)['authorized'];
    }

    public static function import()
    {
        new Role(); // Si no existe la tabla, que la cree

        $roles = new \fs_rol();
        foreach ($roles->all() as $rolData) {
            $rol = Role::getByCode($rolData->codrol);

            if ($rol === null) {
                $rol = new Role();
                $rol->code = $rolData->codrol;
            }

            $rol->name = $rolData->descripcion;
            if (!$rol->save()) {
                die('Error al guardar el rol ' . $rolData->codrol);
            }
        }
    }
}
