<?php

namespace Xnet\Model;

use Xnet\Core\XnetModel;

class Action extends XnetModel
{
    public function get_name()
    {
        return $this->name;
    }

    static public function primaryKey()
    {
        return 'id';
    }

    static public function tablename()
    {
        return 'actions';
    }

    public function get_folder()
    {
        return '';
    }

    public function is_default()
    {
        return '';
    }

    public static function getByName($controllerName, $actionName): ?XnetModel
    {
        $info = ['name' => $actionName, 'controller_name' => $controllerName];
        $data = static::getBy($info);
        if (count($data) === 0) {
            $controller = Controller::getById($controllerName);
            if (!$controller) {
                $oldController = (new \fs_page())->get($controllerName);
                if (!$oldController) {
                    debug_message('No se ha encontrado el controlador ' . $controllerName . ' en fs_pages');
                    return null;
                }

                $data = [
                    'name' => $oldController->name,
                    'title' => $oldController->title,
                    'alias' => $oldController->alias,
                    'description' => $oldController->description,
                    'folder' => $oldController->folder,
                    'show_on_menu' => $oldController->show_on_menu,
                    'order' => $oldController->orden,
                ];
                $controller = Controller::create($data);
                if (!$controller->save()) {
                    debug_message('No se ha podido guardar el controlador ' . $controllerName . ' en controllers');
                    return null;
                }
            }

            $info['description'] = $actionName . ' para ' . $controllerName;
            return static::create($info);
        }
        return static::getById(reset($data)['id']);
    }

}