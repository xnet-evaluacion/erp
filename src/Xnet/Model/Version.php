<?php

namespace Xnet\Model;

use phpDocumentor\Reflection\PseudoTypes\PositiveInteger;
use Xnet\Core\DB;
use Xnet\Core\DBSchema;
use Xnet\Core\XnetModel;

class Version extends XnetModel
{
    const CONFIG_FILENAME = 'mifactura.ini';

    /**
     * Índice del registro
     *
     * @var PositiveInteger
     */
    var $id;

    /**
     * Nombre del plugin
     *
     * @var string
     */
    var $plugin;

    /**
     * Versión del plugin con formato YYYY.MMDDX
     *
     * Siendo YYYY el año, MM el mes, DD el día y X el orden dentro del día si ha habido más de una actualización.
     *
     * @var float
     */
    var $version;

    /**
     * Versión mínima de núcleo necesaria para la activación del plugin YYYY.MMDDX
     *
     * Siendo YYYY el año, MM el mes, DD el día y X el orden dentro del día si ha habido más de una actualización.
     *
     * @var float
     */
    var $core_version;

    /**
     * Última versión que ha actualizado la base de datos, con formato YYYY.MMDDX
     *
     * Siendo YYYY el año, MM el mes, DD el día y X el orden dentro del día si ha habido más de una actualización.
     *
     * @var float
     */
    var $db_version;

    /**
     * Secuencia de activación del plugin.
     *
     * Si el plugin no ha sido activado, contendrá 0 o null (cambiar null por 0).
     *
     * @var int
     */
    var $sequence;

    /**
     * Crea la tabla de versiones si no existe.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0802
     *
     * @return bool
     */
    public static function _createTable()
    {
        $xmlFile = 'src/Xnet/Model/table/versions.xml';
        if (!file_exists($xmlFile)) {
            debug_message('No se encuentra el archivo ' . $xmlFile);
            return false;
        }
        $xml = simplexml_load_file($xmlFile);

        $ok = false;
        if ($xml->columna) {
            $i = 0;
            foreach ($xml->columna as $col) {
                $columns[$i]['nombre'] = (string) $col->nombre;
                $columns[$i]['tipo'] = (string) $col->tipo;

                $columns[$i]['nulo'] = 'YES';
                if ($col->nulo && mb_strtolower($col->nulo) == 'no') {
                    $columns[$i]['nulo'] = 'NO';
                }

                if ($col->defecto == '') {
                    $columns[$i]['defecto'] = null;
                } else {
                    $columns[$i]['defecto'] = (string) $col->defecto;
                }

                if (isset($col->comentario)) {
                    $columns[$i]['comentario'] = $col->comentario;
                } elseif (isset($col->comment)) {
                    $columns[$i]['comentario'] = $col->comment;
                }

                $i++;
            }

            /// debe de haber columnas, si no es un fallo
            $ok = true;
        }

        if ($xml->restriccion) {
            $i = 0;
            foreach ($xml->restriccion as $col) {
                $constraints[$i]['nombre'] = (string) $col->nombre;
                $constraints[$i]['consulta'] = (string) $col->consulta;
                $i++;
            }
        }

        $sql = DBSchema::generateTable(self::tablename(), $columns, $constraints);

        return $ok && DB::exec($sql);
    }

    public function get_name()
    {
        return $this->plugin;
    }

    static public function primaryKey()
    {
        return 'id';
    }

    static public function tablename()
    {
        return 'versions';
    }

    /**
     * Obtiene un array con todos los plugins instalados.
     * Si no se especifica false como parámetro, sólo se obtendrán los activados y por orden de activación.
     * Si se especifica true, o no se especifica parámetro, se obtendrán todos.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0825
     *
     * @param bool $activated
     *
     * @return array
     */
    public static function getPlugins(bool $activated = true): array
    {
        if (!DBSchema::tableExists('versions')) {
            if (!DBSchema::generateTable('versions')) {
                die('No se ha podido generar la tabla de versiones');
            }
        }

        $where = '';
        if ($activated) {
            $where = ' WHERE sequence > 0 AND sequence IS NOT NULL ORDER BY sequence ASC';
        }
        $sql = 'SELECT * FROM `' . self::tablename() . '`' . $where . ';';
        $data = DB::select($sql);
        if (!$data || empty($data)) {
            return [];
        }
        return $data;
    }

    /**
     * Retorna el registro con el nombre especificado, o null si no existe.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0825
     *
     * @param string $name
     *
     * @return XnetModel|null
     */
    public static function getByName(string $name): ?XnetModel
    {
        $sql = "SELECT * "
            . " FROM `" . static::tablename() . "`"
            . " WHERE plugin = '{$name}'"
            . ";";
        $data = DB::select($sql);
        if (!$data || empty($data)) {
            return null;
        }
        return new static($data[0]);
    }

    /**
     * Retorna un array con los nombres de los plugins ordenados por activación.
     * Se puede devolver en orden inverso si se le pasa true.
     * Incluye el núcleo como primer plugin.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0825
     *
     * @param $reverse
     *
     * @return array
     */
    public static function getEnabledPluginsArray($reverse = false): array
    {
        if (!DBSchema::tableExists('versions')) {
            return [];
        }

        /**
         * TODO: Será necesario mientras haya versiones con raintpl que contengan un archivo versions.
         * Esto es necesario debido a la migración de versiones muy antiguas con una tabla
         * versions que usaba integers en lugar de decimal.
         */
        $tablename = 'versions';
        foreach (['version', 'db_version', 'core_version'] as $field) {
            $sql = "UPDATE `{$tablename}` SET {$field} = 0 WHERE {$field} >= 9999;";
            DB::exec($sql);
        }

        $sql = "SELECT * FROM `$tablename` WHERE sequence > 0 AND sequence IS NOT NULL ORDER BY sequence ASC;";
        $data = DB::select($sql);

        $result = [];
        foreach ($data as $datum) {
            $result[] = $datum['plugin'];
        }
        return $reverse ? array_reverse($result) : $result;
    }

    /**
     * Comprueba los datos del modelo, devuelve TRUE si son correctos.
     *
     * @return bool
     *
     */
    public function test()
    {
        $ok = parent::test();

        if (empty($this->version)) {
            $this->version = 0;
        }

        return $ok;
    }

    /**
     * Guarda el plugin actual y renumera el orden de activación.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0825
     *
     * @return bool
     */
    public function save(): bool
    {
        return parent::save() && $this->reSequence();
    }

    /**
     * Renumera la secuencia de activación de los plugins, por si al activar o desactivar algunos, quedan descuadrados.
     * Se le puede pasar un array con el nombre de los plugins si queremos establecer un orden determinado.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0824
     *
     */
    public static function reSequence(array $order = null): bool
    {
        $plugins = $order;
        if (!isset($plugins)) {
            $plugins = [];
            foreach (self::getPlugins() as $plugin) {
                $plugins[] = $plugin['plugin'];
            }
        }

        // Se eliminan todas las secuencias
        DB::exec('UPDATE ' . static::tablename() . ' SET sequence=null');
        DB::exec('UPDATE ' . static::tablename() . ' SET sequence=0 WHERE plugin="core";');
        $sequence = 1;
        foreach ($plugins as $plugin) {
            DB::exec('UPDATE ' . static::tablename() . ' SET sequence=' . $sequence . ' WHERE plugin="' . $plugin . '";');
            $sequence++;
        }

        return true;
    }

    /**
     * Activa el plugin actual.
     *
     * @return bool
     */
    public function enable()
    {
        if (!empty($this->sequence)) {
            return true;
        }
        $this->sequence = 1 + $this->maxSequence();
        return $this->save();
    }

    /**
     * Desactiva el plugin actual
     *
     * @return bool
     */
    public function disable()
    {
        if ($this->sequence === null) {
            return true;
        }
        $this->sequence = null;
        return $this->save();
    }

    private static function savePlugin($pluginName, array $pluginData)
    {
        $version = static::getByName($pluginName);
        if ($version === false) {
            $version = new Version();
            $version->plugin = $pluginName;
        }
        foreach ($pluginData as $key => $value) {
            $version->{$key} = $value;
        }
        return $version->save();
    }

    public static function checkPlugins(array $enable = []): bool
    {
        $ok = static::savePlugin('core', ['version' => file_get_contents(constant('BASE_PATH') . '/MiVersion')]);

        $plugins = scandir(constant('BASE_PATH') . '/plugins');
        foreach ($plugins as $pluginName) {
            // Nos aseguramos de que sea una carpeta real, y no haga referencia a sí mismo, o a la anterior
            if (!is_dir(constant('BASE_PATH') . '/plugins/' . $pluginName) || in_array($pluginName, ['.', '..'])) {
                continue;
            }

            // Leemos la versión del código.
            $filename = constant('BASE_PATH') . '/plugins/' . $pluginName . '/' . self::CONFIG_FILENAME;
            if (!file_exists($filename)) {
                continue;
            }

            $parameters = parse_ini_file($filename);

            $data = [];
            $data['plugin'] = $pluginName;
            $data['version'] = $parameters['version'];
            $data['core_version'] = $parameters['min_version'] ?? 0;
            $data['sequence'] = null;

            $ok = $ok && static::savePlugin($pluginName, $data);
        }

        if ($ok) {
            // Se eliminan todas las secuencias
            DB::exec('UPDATE ' . static::tablename() . ' SET sequence=null');
            DB::exec('UPDATE ' . static::tablename() . ' SET sequence=0 WHERE plugin="core";');
            $sequence = 1;
            foreach ($enable as $plugin) {
                DB::exec('UPDATE ' . static::tablename() . ' SET sequence=' . $sequence . ' WHERE plugin="' . $plugin . '";');
                $sequence++;
            }
        }

        return $ok && static::reSequence();
    }

    private function _setSequence($pluginName, $sequence)
    {
        $version = $this->getByName($pluginName);
        if ($version === false) {
            $version = new version();
        }
        $version->sequence = $sequence;
        return $version->save();
    }

    /**
     * Renumera los plugins y activa los que están definidos en "enabled_plugins.list".
     *
     * @author     Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version    2022.0824
     *
     * @deprecated Utilice en su lugar reSequence(), que ya no toca para nada el archivo "enabled_plugins.list".
     */
    private function _setSequences()
    {
        $pluginsList = constant('FS_FOLDER') . '/tmp/' . constant('FS_TMP_NAME') . 'enabled_plugins.list';
        if (!file_exists($pluginsList)) {
            self::errorMessage('No existe el archivo enabled_plugins.list');
            return false;
        }
        $enabledPlugins = array_reverse(explode(',', file_get_contents($pluginsList)));

        // Se eliminan todas las secuencias
        DB::exec('UPDATE ' . $this->tablename() . ' SET sequence=0');

        $plugins = [];
        $sequence = 1;
        $plugins[$sequence] = 'core';
        $previousPlugin = 'core';
        foreach ($enabledPlugins as $plugin_name) {
            // Por error, hay veces que se duplican.
            if ($plugin_name === $previousPlugin) {
                continue;
            }
            if (!in_array($plugin_name, $plugins)) {
                $sequence++;                            // Se incrementa la secuencia
                $plugins[$sequence] = $plugin_name;    // Se guarda para activarlo
            }
        }

        foreach ($plugins as $sequence => $name) {
            $this->setSequence($name, $sequence);
        }
    }

    public static function _enableAllPlugins()
    {
        static::savePlugin('core', ['version' => file_get_contents(constant('BASE_PATH') . '/MiVersion')]);

        $plugins = scandir(constant('BASE_PATH') . '/plugins');

        // Activamos los plugins que sabemos que van antes...
        static::enablePlugin('core');
        static::enablePlugin('xnetcommonhelpers');
        static::enablePlugin('ayuda_soporte');
        static::enablePlugin('soporte_empresas');
        static::enablePlugin('facturacion');

        foreach ($plugins as $pluginName) {
            // Nos aseguramos de que sea una carpeta real, y no haga referencia a sí mismo, o a la anterior
            if (!is_dir(constant('BASE_PATH') . '/plugins/' . $pluginName) || in_array($pluginName, ['.', '..'])) {
                continue;
            }
            static::enablePlugin($pluginName);
        }

        static::reSequence();
    }

    /**
     * Hace un chequeo de los plugins instalados para actualizar la tabla de versiones.
     * También traspasa la información de enabled_plugins.list a la tabla de versiones.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0624
     *
     */
    public function _check_plugins()
    {
        $this->savePlugin('core', ['version' => file_get_contents(constant('BASE_PATH') . '/MiVersion')]);

        $plugins = scandir(constant('BASE_PATH') . '/plugins');
        foreach ($plugins as $pluginName) {
            // Nos aseguramos de que sea una carpeta real, y no haga referencia a sí mismo, o a la anterior
            if (!is_dir(constant('BASE_PATH') . '/plugins/' . $pluginName) || in_array($pluginName, ['.', '..'])) {
                continue;
            }

            // Leemos la versión del código.
            $filename = constant('BASE_PATH') . '/plugins/' . $pluginName . '/' . self::CONFIG_FILENAME;
            if (!file_exists($filename)) {
                continue;
            }

            $parameters = parse_ini_file($filename);
            $version = $parameters['version'];

            $this->savePlugin($pluginName, $version);
        }

        $this->reSequence();
    }

    /**
     * Obtiene un array con todos los plugins instalados.
     * El índice es el nombre del array (incluyendo core), y el valor es el contenido
     * del archivo de configuración del plugin (mifactura.ini).
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0825
     *
     * @return array
     */
    public static function _getInstalledPlugins(): array
    {
        $plugins = [];
        $plugins['core']['version'] = file_get_contents(constant('BASE_PATH') . '/MiVersion');

        foreach ($plugins as $pluginName) {
            // Nos aseguramos de que sea una carpeta real, y no haga referencia a sí mismo, o a la anterior
            if (!is_dir(constant('BASE_PATH') . '/plugins/' . $pluginName) || in_array($pluginName, ['.', '..'])) {
                continue;
            }

            // Leemos la versión del código.
            $filename = constant('BASE_PATH') . '/plugins/' . $pluginName . '/' . self::CONFIG_FILENAME;
            if (!file_exists($filename)) {
                continue;
            }

            $parameters = parse_ini_file($filename);
            $plugins[$filename] = $parameters;
        }
        return $plugins;
    }

    /**
     * Obtiene un array con los plugins que están activados, por orden de activación.
     * NOTA: Este método incluye a core como un plugin más.
     *
     * @return static[]
     */
    public static function getActivatedPlugins()
    {
        $sql = "SELECT * "
            . " FROM `" . self::tablename() . "`"
            . " WHERE sequence IS NOT null ORDER BY sequence ASC;";
        $data = DB::select($sql);
        if (!$data || empty($data)) {
            return [];
        }
        $result = [];
        foreach ($data as $record) {
            $result[] = new static($record);
        }
        return $result;
    }

    public static function getRoute($pluginName)
    {
        $ruta = constant('FS_FOLDER');
        if ($pluginName !== 'core') {
            $ruta .= '/plugins/' . $pluginName;
        }
        $class = 'updates_' . $pluginName;
        $ruta .= '/dataconfig/' . $class . '.php';

        if (file_exists($ruta)) {
            return $ruta;
        }
        return false;
    }

    public static function getFunctions($plugin)
    {
        $pluginName = $plugin->plugin;

        $class = 'updates_' . $pluginName;
        $ruta = static::getRoute($pluginName);

        if (file_exists($ruta)) {
            require_once($ruta);
            $update = new $class($plugin);
            return $update->getMethods();
        }
        return [];
    }

    public static function _enablePlugin($pluginName)
    {
        $plugin = Version::getByName($pluginName);
        if ($plugin === null) {
            return false;
        }

        // Core no tiene carpeta en plugins... Así que directamente se activa
        if ($pluginName === 'core') {
            return $plugin->enable();
        }

        // Comprobamos que sea un plugin (tenga el archivo de configuración)
        $filename = constant('BASE_PATH') . '/plugins/' . $pluginName . '/' . self::CONFIG_FILENAME;
        if (!file_exists($filename)) {
            return false;
        }
        $parameters = parse_ini_file($filename);
        if (isset($parameters['require'])) {
            $requires = explode(',', $parameters['require']);
            foreach ($requires as $require) {
                static::enablePlugin($require);
            }
        }

        return $plugin->enable();
    }

    /**
     * Retorna el código SQL a ejecutar tras la creación de la tabla (por si hay datos iniciales)
     *
     * @return string
     */
    public function _install()
    {
        return '';
    }

    /**
     * Obtiene valor mayor de orden de activación.
     *
     * @return int
     */
    private function maxSequence()
    {
        $max_sequence = 0;
        $sql = "SELECT max(sequence) as sequence FROM " . $this->tablename() . ";";
        $data = DB::select($sql);
        if ($data && count($data) > 0) {
            $max_sequence = $this->intval($data[0]['sequence']);
        }
        return $max_sequence;
    }

    /**
     * Desactiva todos los plugins.
     *
     * @return bool
     */
    private function _disable_all_plugins()
    {
        $sql = "UPDATE `" . $this->tablename() . "` SET "
            . "sequence = " . $this->var2str(0)
            . ";";
        return DB::exec($sql);
    }
}