<?php

namespace Xnet\Model;

use Xnet\Core\XnetModel;

class Controller extends XnetModel
{
    public function get_name()
    {
        return $this->name;
    }

    static public function primaryKey()
    {
        return 'name';
    }

    static public function tablename()
    {
        return 'controllers';
    }

    public function is_default()
    {
        return '';
    }

    public function getFolder()
    {
        return implode(' » ', explode('|', $this->folder));
    }

    public static function regenerate()
    {
        //        dump(XnetConfig::getBaseFolders());
    }

    public function import()
    {
        $pages = new \fs_page();
        foreach ($pages->all() as $page) {
            Controller::create([
                'name' => $page->name,
                'title' => $page->title,
                'alias' => $page->alias,
                'description' => $page->description,
                'folder' => $page->folder,
                'show_on_menu' => $page->show_on_menu,
                'order' => $page->orden,
            ]);
        }
    }

}