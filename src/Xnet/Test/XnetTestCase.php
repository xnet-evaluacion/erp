<?php

namespace Xnet\Test;

use PHPUnit\Framework\TestCase;
use Xnet\Core\DB;
use Xnet\Core\XnetCache;
use Xnet\Core\XnetModel;
use Xnet\Model\Version;

class XnetTestCase extends TestCase
{
    protected $dateOfCreation;

    public function __construct($name = null, array $data = [], $dataName = '')
    {
        if (!defined('FS_PATH')) {
            define('FS_PATH', realpath(__DIR__ . '/../../..'));
            define('FS_FOLDER', constant('FS_PATH'));

            require_once constant('FS_PATH') . '/vendor/autoload.php';

            if (file_exists('config_test.php')) {
                require_once 'config_test.php';
                new DB();
                // DB::connect();
                // $sql=
                //      'DROP DATABASE IF EXISTS `' . constant('FS_DB_NAME') . '`;'.
                //      'CREATE DATABASE `' . constant('FS_DB_NAME') . '`;';
                // DB::exec($sql);
                // DB::disconnect();
                DB::connect();
            } elseif (file_exists('config.php')) {
                require_once 'config.php';
                new DB();
                DB::connect();
            } else {
                echo "No se encuentra el archivo: " . realpath('config_test.php');
                die('El test necesita el archivo de configuración config_test.php, o en su defecto config.php.');
            }
        }

        chdir(FS_PATH);

        XnetModel::clear_cache();
        XnetCache::clean();

        $version = new Version();
        $version->check_table('versions');
        //$version->enableAllPlugins();

        parent::__construct($name, $data, $dataName);
    }

    public function setUp(): void
    {
        $this->dateOfCreation = gmdate('Y-m-d H:i:s');
    }

}
