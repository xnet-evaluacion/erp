# Introducción

La estructura del entorno es un pequeño MVC. No es necesario conocerlo ni dominarlo, pero si se quiere consultar
información hay en docs/dev/src. Lo que hace falta complementar es:

- Desde plugins/fsdk/lib/generar_datos_prueba.php, añadir método de testeo para varios de los modelos anteriores.
- Desde el controlador plugins/fsdk/controller/fsdk_home.php, añadir soporte para testear los generadores que se hayan
  implementado, que deberán generar datos correctos e incorrectos.
- Desde la vista plugins/fsdk/Templates/fsdk_home.blade.php, añadir un nuevo botón para ejecutar la generación de datos
  con un click, como en el Generador datos de prueba. En este caso será "Probar modelos".
- Desde la ruta model/table/*.xml tenemos el diseño de cada una de estas tablas que va relacionada con el modelo.

# Tareas a realizar

La labor de las funciones que se implementen para testear los modelos, deben poner en entredicho que estos modelos se
estén validando debidamente intentando provocar fallos que no estén correctamente comprobados. Tales como datos
inválidos, longitudes incorrectas, datos relacionados con otras tablas correctos...

Se quiere tener una función que genere datos de prueba para distintos modelos desde el controlador fsdk_home. Reiteramos
la importancia de que la generación de datos, no se limite a generar datos válidos, sino también datos no válidos que
sean debidamente filtrados por los tests.

Su finalidad es probar que los modelos básicos incluidos en el núcleo están bien probados para no permitir guardar datos
que no cumplan las condiciones a nivel SQL y por tanto que sus correspondientes test deben evitar que se inserten antes
de mostrar un error no controlado.

- Generar datos de pruebas para algunos (no hace falta que sean todos) de los siguientes modelos del núcleo (carpeta
  model y model/core):
    - almacen
    - contacto
    - cuenta_banco
    - divisa
    - ejercicio
    - forma_pago
    - impuesto
    - notificacion_tipo
    - pais
    - serie

# Valoración

- Que el testeo sea lo más exhaustivo posible.
- Que se generen la cantidad de registros solicitada, a pesar de que ello implique desechar mucha cantidad por ser datos
  inválidos correctamente comprobados. Es prioritario el buen funcionamiento de los tests, para lo cual, la generación
  debe de fabricar datos erróneos.
- Hay código de muestra que puede ayudar a agilizar ciertos aspectos, pero en otros dicho código no cumple estos
  requisitos.
- Su generación debe ser parecida a nivel de usuario del botón "Generar datos de prueba", con la diferencia principal
  que ese genera hasta una cantidad máxima y si falla deja de generar más datos. Mientras que este debe asegurar que
  genera dicha cantidad o bien encontrar un fallo no controlado.

# Necesidades del entorno

Apache + MySQL/MaridaDB + PHP >=7.2 y <8.0

## Preparación del entorno

```
    git clone git@gitlab.com:xnet-evaluacion/erp.git
    cd erp
    composer install
    npm install
```

## Introducción esencial

La explicación va a ser desde la ruta relativa **erp**, compartiendo nombre con la carpeta donde se clonó anteriormente.

- Cargar desde el navegador erp/install.php
    - Asignar un nombre de usuario y contraseña válido para logear en MySQL/MariaDB, la DB debe crearse automáticamente.
- Identificarse con usuario y contraseña **admin**
- Activar todas las páginas y pulsar Guardar
- Ir a la pestaña **Plugins** y para el plugin **fsdk** pulsar en **Activar**
- Desde el menú ir a **Panel de control** -> **Configuración** -> **FSDK**
    - En esta página es donde se probarán los cambios solicitados.

La solución deberá publicarse en una solicitud de Merge Request, en una rama con el DNI/NIE/NIF del aspirante.
