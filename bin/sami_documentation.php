<?php

/*
 * This file is part of MiFactura.eu
 * Copyright (C) 2021 X-Net Software Solutions S.L. <xnetsoftware@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

use Sami\Parser\Filter\TrueFilter;
use Sami\RemoteRepository\GitLabRemoteRepository;
use Sami\Sami;
use Symfony\Component\Finder\Finder;

$iterator = Finder::create()
    ->files()
    ->name('*.php')
    ->exclude('bin')
    ->exclude('cache')
    ->exclude('components')
    ->exclude('docker')
    ->exclude('extras')
    ->exclude('node_modules')
    ->exclude('plugins/buscador_centralizado/vendor')
    ->exclude('plugins/documentos_detallados/qrcode')
    ->exclude('plugins/domaincontroller/vendor')
    ->exclude('plugins/factura_detallada/qrcode')
    ->exclude('plugins/factura_plantilla/extra')
    ->exclude('plugins/facturacion_base/extras/ezpdf')
    ->exclude('plugins/facturae/vendor')
    ->exclude('plugins/factusol/vendor')
    ->exclude('plugins/fsdk/template')
    ->exclude('plugins/haiti/vendor')
    ->exclude('plugins/hitos_y_tareas_con_diagrama_gantt/extras')
    ->exclude('plugins/import_export_csv/vendor')
    ->exclude('plugins/import_export_spreadsheet/Classes')
    ->exclude('plugins/jasper/jasperstarter')
    ->exclude('plugins/jocar_estado_vehiculos/extras')
    ->exclude('plugins/nomina/vendor')
    ->exclude('plugins/planmex/vendor')
    ->exclude('plugins/plantillas_html2pdf/extras')
    ->exclude('plugins/prestashop_micro/vendor')
    ->exclude('plugins/regularizacion_masiva_stock/vendor')
    ->exclude('plugins/reportico/vendor')
    ->exclude('plugins/republica_dominicana/extras/rospdf')
    ->exclude('plugins/reservas/extras')
    ->exclude('plugins/responsive_file_manager/view/filemanager')
    ->exclude('plugins/simple_jquery_im/class')
    ->exclude('plugins/tcpdf')
    ->exclude('plugins/tesoreria/lib/sephpa')
    ->exclude('plugins/woocommerce/vendor')
    ->exclude('plugins/xapi/vendor')
    ->exclude('plugins/xnetcommonhelpers/extras')
    ->exclude('resources')
    ->exclude('vendor')
    ->in('.')
    ->notName('index.php');

$sam = new Sami($iterator, [
    'theme' => 'default',
    'title' => 'MiFactura',
    'build_dir' => __DIR__ . '/../docs/dev/classes',
    'cache_dir' => __DIR__ . '/../cache/docs/',
    'remote_repository' => new GitLabRemoteRepository('mifactura/mifactura.eu', '.', 'https://gitlab.com/xnetdigital-private/mifactura/mifactura.eu'),
    'default_opened_level' => 2,
]);
// document all methods and properties
$sam['filter'] = function () {
    return new TrueFilter();
};

return $sam;
