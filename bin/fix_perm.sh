#!/bin/bash
WHOAMI=$(whoami)
sudo find . -not -path "./docker/*" -type d -exec chmod 0775 {} \;
sudo find . -not -path "./docker/*" -type f -exec chmod 0664 {} \;
sudo find . -not -path "./docker/*" -exec chown ${WHOAMI}:http {} \;

