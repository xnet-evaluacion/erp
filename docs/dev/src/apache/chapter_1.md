# Apache

[Apache](https://httpd.apache.org/) es uno de los servidores web más utilizados.

Recomiendo definir VirtualHost diferentes para poder utilizar versiones diferentes de PHP cómodamente, en función del
nombre concreto de localhost.

Por ejemplo:

- localhost, la versión por defecto configurada en el equipo.
- localhost72, Apache con PHP 7.2
- localhost73, Apache con PHP 7.3
- localhost74, Apache con PHP 7.4
- localhost80, Apache con PHP 8.0
- localhost81, Apache con PHP 8.1

Esta recomendación no es ninguna obligación, pero permite probar entre múltiples versiones de PHP con un esfuerzo
mínimo.

## Ejemplo de VirtualHost para PHP 7.2

Adicionalmente en este VirtualHost se está cambiando en las rutas donde se almacenan los logs y desde que carpeta se van
a servir los archivos, de forma que no se requieren permisos de root para utilizar la ruta por defecto.

```
<VirtualHost localhost72:80>
    ServerAdmin francesc.pineda@x-netdigital.com
    DocumentRoot "/home/francescps/Público"
    ServerName localhost72
    ErrorLog "/home/francescps/Público/logs/localhost72-error_log"
    CustomLog "/home/francescps/Público/logs/localhost72-access_log" common
    <FilesMatch \.php$>
        SetHandler "proxy:unix:/var/run/php72-fpm/php-fpm.sock|fcgi://localhost72/"
    </FilesMatch>
    <Directory "/home/francescps/Público">
       DirectoryIndex index.php index.html
       Options Indexes MultiViews FollowSymLinks
       Require all granted
       AllowOverride All
    </Directory>
</VirtualHost>
```

También puede utilizarse [Nginx](https://www.nginx.com/) como alternativa, como por ejemplo hacemos mediante Docker,
pero sin embargo es más tedioso de lidiar con determinados problemas que sin embargo con Apache simplemente funcionan.