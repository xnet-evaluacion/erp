# Gulp

[Gulp](https://gulpjs.com/) es un conjunto de herramientas que ayudan a automatizar tareas dolorosas o que consumen
mucho tiempo en su flujo de trabajo de desarrollo.

*Recuerda:* En el caso que utilices docker, los comandos debes ejecutarlos tras este:

```
docker exec -ti xnet_php bash
```

Como es posible extender su funcionalidad, es posible ver las tareas disponibles con:

```
gulp -T
```

Para la parte más esencial, tenemos integración con NPM para realizar las tareas más básicas y esenciales a través del
propio *npm install*:

- Descargar las dependencias de todos los plugins
- Compilar SCSS
- Minificar los archivos CSS y JS

Adicionalmente pueden ejecutarse scripts de forma separada como:

- *gulp build*: Tarea de construcción, prepara todos los archivos necesarios.
- *gulp default*: Tarea de construcción, prepara todos los archivos necesarios y además se mantiene monitorizando
  cambios para agilizar su desarrollo.
- *gulp coreDependencies*: Equivalente a realizar un "composer install" y "npm install".
- *gulp pluginsDependencies*: Equivalente a realizar un "composer install" y "npm install" para cada uno de los plugins.
- *gulp clean:packageLock*: Elimina el archivo package-lock.json del núcleo.
- *gulp clean:dist*: Eliminar el contenido de "Templates/dist" en el núcleo.
- *gulp copy:all*: Copia los archivos finales de Templates/src a la carpeta Templates/dist.
- *gulp fileinclude*: Missing documentation
- *gulp scss*: Compila y minifica los archivos SCSS.
- *gulp js*: Minimifica los archivos JS.
- *gulp jsPages*: Minimifica los archivos JS en pages.
- *gulp watch*: Monitoriza los cambios en ciertos archivos para ejecutar comandos de forma automática.
- *gulp browsersync*: Se encarga de mantener sincronizado y reiniciar el servidor local.
- *gulp browsersyncReload*: Se encarga de mantener sincronizado y reiniciar el servidor local.
- *gulp devDoc*: Genera la documentación de desarrollo.
- *gulp devPhpDoc*: Genera la documentación de clases de PHP.
- *gulp userDoc*: Genera la documentación de desarrollo.
