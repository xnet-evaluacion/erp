# PHP

PHP es el lenguaje base de buena parte del desarrollo de nuestro entorno.

Como se puede ver en la sección de Apache para los VirtualHost, se utiliza PHP-FPM para poder gestionar múltiples
versiones de PHP.

Esta explicación varía en función del sistema operativo y los correspondientes gestores de paquetes.

Ubuntu y Manjaro por ejemplo si soportan esta característica.

- [Cómo ejecutar varias versiones de PHP en un servidor usando Apache y PHP-FPM en Ubuntu 18.04](https://www.digitalocean.com/community/tutorials/how-to-run-multiple-php-versions-on-one-server-using-apache-and-php-fpm-on-ubuntu-18-04-es)
- [Install PHP 8.0 | PHP 7.4 on Garuda|Arch|Manjaro](https://techviewleo.com/how-to-install-php-on-garuda-arch-manjaro/)

## Ajustes sobre los php.ini

A través de las siguientes variables, podemos forzar en un entorno de desarrollo para que nos muestre más avisos y con
mayor claridad.

El caso ideal es que nunca veamos ni un warning.

```
display_errors = On
error_reporting = E_ALL & ~E_DEPRECATED & ~E_STRICT
log_errors = On
html_errors = On
error_log = php_errors.log
```

## Documentación

Nuestra documentación de clases PHP puede generarse con:

```
./bin/generate-documentation.sh
```

o

```
gulp devPhpDoc
```

Y estará disponible en [Documentación de PHP](../../classes)