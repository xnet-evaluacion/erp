# Docker

[Docker](https://www.docker.com/)  es un proyecto de código abierto que automatiza el despliegue de aplicaciones dentro
de contenedores de software, proporcionando una capa adicional de abstracción y automatización de virtualización de
aplicaciones en múltiples sistemas operativos.

Disponemos de integración con Docker, pero la documentación es más bien nula y debe solventarse este problema aquí.

Está compuesto por 3 contenedores:

- Contenedor web con Nginx
- Contenedor para PHP
- Contenedor con MySQL

## Instalación

### Windows

- Instalar [Docker](https://docs.docker.com/desktop/windows/install/)
- Desde terminal:
  - cd docker
  - docker-compose-v1.exe up

Ya aparecerán las máquinas de docker en el listado de contenedores.

### Linux

- Instalar el paquete de Docker para la correspondiente distribución 
- Desde terminal:
    - cd docker
    - docker-compose up

## Configuración

- Abrir [el instalador](http://localhost:8080/install.php)
- Rellenar con los siguientes datos:
    - Tipo de servidor SQL: MySQL
    - Servidor: host.docker.internal
    - Puerto: 8306
    - Nombre de base de datos: xnet
    - Usuario: dbuser
    - Contraseña: dbuser

## Para conectar en terminal a los contenedores

### En el contenedor para nGinx

```docker exec -ti xnet_nginx bash```

### En el contenedor para PHP

```docker exec -ti xnet_php bash```

### En el contenedor para MySQL

```docker exec -ti xnet_db bash```

### En el contenedor para phpMyAdmin

```docker exec -ti xnet_phpmyadmin bash```

### Listar todos los contenedores

```docker ps```

### Borrar todos los contenedores creados

```docker container rm $(docker container ls -aq)```

### Detener todos los contenedores creados

```docker container stop $(docker container ls -aq)```

### Informacion de todos los contenedores

```docker image ls```
