# Diseño MVC

En el entorno de X-Net ERP utilizamos el diseño conocido como [MVC o Modelo-Vista-Controlador](https://es.wikipedia.org/wiki/Modelo%E2%80%93vista%E2%80%93controlador), con
algunos matices para conseguir complementar y/o modificar el entorno, pero en esencia se basa en él.

Si desconoces como está orientado este diseño, puedes ver
el [Ejemplo PHP + POO + MVC de Victor Robles](https://victorroblesweb.es/2014/07/15/ejemplo-php-poo-mvc/), que es
totalmente agnóstico a frameworks (aunque haga uso de ciertas herramientas/librerías de terceros para abstraer de
ciertas partes como bootstrap o fluent=. Para aprender con un ejemplo mucho más reducido en que principios de diseño nos
basamos, se obtiene una idea más que suficiente, la cual se sigue evolucionando día a día.