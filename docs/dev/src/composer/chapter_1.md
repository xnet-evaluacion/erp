# Composer

[Composer](https://getcomposer.org/) es el gestor de paquetes de PHP, mediante el cual gestionamos las dependencias
externas.

Pueden verse todas las dependencias de terceros en composer.json en la raíz del proyecto.

Sus dependencias pueden instalarse mediante:

```
composer install
```

*Recuerda:* En el caso que utilices docker, los comandos debes ejecutarlos tras este:

```
docker exec -ti xnet_php bash
```
