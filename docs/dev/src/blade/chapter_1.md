# Blade

Blade es el motor de plantilla utilizadas en X-Net ERP, y su documentación al completo está disponible en el siguiente
enlace [https://laravel.com/docs/8.x/blade](https://laravel.com/docs/8.x/blade).

Actualmente se está utilizando [BootStrap v5.1.3](https://getbootstrap.com/docs/5.1/getting-started/introduction/) con
la
plantilla [Skote](https://themesbrand.com/skote/layouts/index.html). Pueden verse más detalles mediante el archivo
package.json.
