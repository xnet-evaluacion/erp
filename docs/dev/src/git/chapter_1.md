# Integración con GIT

GIT dispone de su propia documentación y además en
español [https://git-scm.com/book/es/v2](https://git-scm.com/book/es/v2), cualquier duda o aclaración dirígete a ella
para resolver las dudas.

Se asumirá que las acciones básicas de GIT y las indicadas en esta página se entienden y utilizan.

## Integración de repositorios como submódulos.

Todos los plugins de X-Net ERP han sido integrados como submódulos del proyecto principal, de modo que no sea necesario
clonar proyecto a proyecto de forma separada.

Si necesitas más detalles puedes leer la sección
correspondiente: [7.11 Herramientas de Git - Submódulos](https://git-scm.com/book/es/v2/Herramientas-de-Git-Subm%C3%B3dulos)
.

## Preparación en entorno de desarrollo

Para clonar el repositorio con todos sus submódulos:

```
git clone -b blade --recurse-submodules git@gitlab.com:xnetdigital-private/mifactura/mifactura.eu.git 
```

Para clonar el repositorio sin los submódulos:

```
git clone -b blade git@gitlab.com:xnetdigital-private/mifactura/mifactura.eu.git
```

Para añadir todos los submódulos, si ya había sido clonado con anterioridad:

```
git submodule init
git submodule update
```

## Actualizando proyecto y plugins

Si queremos actualizar el proyecto o repositorio (plugin) de manera independiente, debemos situarnos en el directorio
del plugin o proyecto específico y ejecutar el siguiente comando:

```
git pull 
```

Si lo que queremos es actualizar el proyecto y además todos los repositorios, desde la raíz del proyecto principal:

```
git pull --recurse-submodules
```

## Añadir submódulo
**IMPORTANTE:** Nunca hay que tocar a mano el archivo .gitmodules, hay que gestionarlo con los comandos de esta
explicación.

Al crear un nuevo plugin, antes de nada va a ser necesario crear el repositorio vacío en gitlab, pero con las ramas en
las que vamos a trabajar.
Los desarrollos relacionados con este entorno cuelgan de https://gitlab.com/xnetdigital-private/mifactura, vamos a este
enlace y pulsamos **New project** y seguimos estos pasos:

- Create blank project
- Nombre del plugin, tal y como estará en la carpeta de plugins del ERP
- Sin cambiar nada más, pulsamos **Create project**
- Acto seguido pulsamos en "Add README.md", si no tenemos permiso, deberos solicitarlo a un compañero con más permisos
  en gitlab.
- Esto creará un repositorio con su rama **main**.

```
git submodule add -f <git@gitlab.com:xnetdigital-private/mifactura/NUEVO_PLUGIN.git> <plugins/NUEVO_PLUGIN>
```

De esta manera, ya integraremos el plugin nuevo en el núcleo sin necesidad de avisar al resto de miembros.

El parámetro -f únicamente es necesario en el caso de que ya exista un directorio con ese nombre y queramos forzar el
agregado del módulo. En cualquier otro caso no es necesario.

```
git checkout -b blade
```

- Actualmente trabajamos en la rama **blade**, por ir avanzando cambios en una rama que se ha alargado más de lo que
  preveíamos en el tiempo.
    - Una vez estabilicemos esta rama, la que crearemos en su sitio será **dev** donde se seguirá el desarrollo del día
      a día.

## Cambio a rama en todos el proyecto

```
git submodule foreach 'git checkout main'
```

```
git submodule foreach 'git checkout blade'
```
