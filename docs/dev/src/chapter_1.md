# Introducción

X-Net ERP es un software de facturación y contabilidad para pymes, fácil, libre y con actualizaciones constantes.

## Instalación

Para instalarlo necesitas:

- PHP >=7.2 y <8.0
- MySQL >= 5.6, MariaDB >=10.2 o PostgreSQL >= 9.0.
- Apache o NGINX
- Como gestores de dependencias de terceros
    - Composer
    - NPM
