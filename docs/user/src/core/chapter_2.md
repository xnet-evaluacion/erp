# Usuarios, roles y permisos

Uno de los pilares fundamentales de una aplicación es la definición de los usuarios y qué pueden hacer, o no hacer. La versión actual de MiFactura, presenta unas limitaciones bastante serias a ese respecto. 

La gestión de permisos se gestiona desde la ficha de usuarios, dónde existe una pestaña que permite autorizar o denegar para cada controlador dos únicas acciones

- Ver/Modificar, que si está activada permite al usuario acceder al controlador. **No existe la opción de ver y no modificar**.
- Eliminar, que si está activada permite al usuario eliminar un registro en dicho controlador.

Si el usuario es administrador, esta definición no se tiene en cuenta, y el usuario no tiene restricciones.

## Los roles

En MiFactura, un rol permite definir una configuración que luego se puede **copiar** a un usuario. Si posteriormente se realiza algún cambio en el rol, este cambio no afectará a los usuarios salvo que el rol vuelva a aplicarse.

## Funcionamiento del nuevo controlador

Los nuevos controladores, que estarán disponibles en cuanto se vayan migrando los actuales, funciona de forma diferente, pero manteniendo en la medida de lo posible la compatibilidad con los anteriores.

### Los roles

Un rol, permite establecer una opción por defecto (se permite todo, o por defecto, todo restringido). Posteriormente, se podrá acción por acción, ir cambiando lo que los usuarios que tengan dicho rol podrán hacer o no.

Los roles son acumulativos, esto es, un usuario puede tener más de un rol, en cuyo caso, se irán aplicando uno tras otro en el orden establecido.

A diferencia de lo que ocurre con los controladores antiguos, aquí no existen permisos por usuarios. Un usuario tendrá una serie de roles aplicados, y sus permisos serán los especificados por dichos roles.

Si un rol permite algo, y posteriormente se cambia para que no lo permita, eso afectará inmediatamente a todos los usuarios que tengan ese rol.

### Las acciones

A diferencia de MiFactura, los permisos que se pueden definir en cada página, dependen de la propia página.

Una página define una serie de acciones... Ver, guardar, imprimir, eliminar, añadir,... Cualquiera de esas acciones, podría permitirse o denegarse en los roles.

Evidentemente, si no puedes ver, no podrás acceder y poco importa cómo se hayan definido el resto de acciones; de la misma forma que si no puedes guardar, tampoco podrás añadir, aunque así lo definas.