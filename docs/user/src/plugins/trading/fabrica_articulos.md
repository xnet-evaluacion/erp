# Artículos

En la opción del menú **Artículos**, se puede consultar los artículos asociados a cada fábrica.

Lo primero que hay es, que seleccionar una fábrica y una dirección de esa fábrica. Una vez seleccionada la dirección, se
accederá al catálogo de productos de esa fábrica.

> Se puede cambiar la referencia, el nombre y el tipo de IVA de un producto, e incluso se podría marcar como eliminado,
> pero no hay garantías de que eso último se tenga en cuenta. Considere que un producto aparecerá en la búsqueda del
> pedido, si existe en la tarifa que se haya asignado a dicho pedido.
