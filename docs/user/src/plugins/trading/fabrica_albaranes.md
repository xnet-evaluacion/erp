# Albaranes de fábrica

Una vez que desde un pedido se ha generado un albarán, podemos acceder a él desde el propio pedido, o bien desde la
opción **Albaranes**. En el listado, por defecto sólo se ven los albaranes pendientes de facturar, pero usando los
filtros de fábrica, cliente y estado, se puede afinar la selección.

El funcionamiento general es similar al de pedidos. Pinchando en una línea, nos lleva a la edición del albarán.

## Edición de un pedido

La edición de un albarán, como ocurre con los pedidos, tiene varias zonas definidas.

- Botones de acción:
  - Pulsando en **<- Albaranes** se retorna al listado de albaranes, descartando los cambios.
  - El botón **Guardar** almacena los cambios que han sido realizados en el albarán.
- Cabecera del documento:
  - Existen una serie de campos no editables como son los datos de fábrica y cliente, el número de documento y la fecha 
  y hora de creación.
  - La fecha de la factura de la fábrica, desde la que se calculará la de vencimiento una vez se guarde el documento.
  - El número de factura de la fábrica.
  - Un empleado asociado (que puede ser diferente al de creación).
- Líneas del documento:
    - Lo único que puede modificarse en las líneas de documento, es la cantidad servida. Todo lo demás, tendría que 
      haber sido corregido en el pedido.
    - Se pueden añadir nuevos pedidos al albarán, tan sólo marcando el pedido en el check del pedido. Al marcar un 
      pedido y guardar el albarán, las líneas de dicho pedido con el stock pendiente de servir, se añadirán al albarán.
    - Si un pedido tiene todas las líneas a cero, podría desmarcarse para eliminar sus líneas.

> Hay que tener en cuenta que antes de pulsar cualquier botón de acción (Imprimir, Albaranar, Recalcular, etc), es
necesario pulsar en **Guardar** ya que de otro modo, la acción se hará sobre los últimos datos que fueron guardados.

Regresando al listado de albaranes, todos los que pertenezcan a la misma fábrica y fecha de vencimiento, podrán ser
pasados a factura.

Para facturar, se puede pulsar el botón de facturar del listado, o bien seleccionarlo en el menú.
