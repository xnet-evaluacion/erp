# Formas de pago

La opción del menú **Formas de pago**, nos lleva a un mantenimiento de formas de pago, que no son más que literales que
aparecen en el pedido de fábrica a título orientativo. Explica la forma en la que el cliente final pagará a la fábrica
el pedido, pero no tiene ninguna relación con el ERP, ya que el pago que recibiremos, será el de la factura que se 
emitirá a la fábrica con las comisiones por los pedidos gestionados.

En la ficha de la fábrica, podemos seleccionar una forma de pago y un número de cuenta.

> De momento, sólo se permite seleccionar una forma de pago en la fábrica sin tener en cuenta para nada el número de 
> cuenta bancaria, pues ese dato, ya está en conocimiento del cliente final y no es necesario plasmarlo en el pedido.
> 
> Por otro lado, es posible que al leer ésto, ya se haya implementado la opción de definir parámetros en la relación
> entre fábrica y cliente, con lo cual, se pueda establecer una forma de pago diferente para una relación fábrica 
> cliente, a la establecida en la fábrica. Ésto es, que por defecto todos los pedidos a una fábrica tengan una forma de
> pago (establecida en la ficha de la fábrica), pero que para un determinado cliente, se pueda seleccionar una forma
> de pago diferente.
