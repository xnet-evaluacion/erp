# Relación entre fábricas y clientes

En los mantenimientos de **fábricas** y **clientes finales**, existe un botón que nos permite relacionar unos con otros.
En el caso de fábricas, el botón es clientes, y en el caso de clientes, el botón es fábricas. Dichos botones nos
muestran un listado con las entidades relacionadas con la que estamos editando; esto es, si pulsamos en clientes desde
la ficha de una fábrica, veremos un listado de clientes, dónde podremos marcar o desmarcar los clientes que están
relacionados con ella, aunque lo más habitual será hacerlo desde el cliente, seleccionando con qué fabricas va a
trabajar.

Otra forma de relacionar una fábrica con un cliente, es directamente al hacer un pedido. En ese punto, que veremos en el
siguiente capítulo, podremos marcar un check al seleccionar una fábrica y un cliente para un pedido, y **realizar
directamente la relación entre ellos**.
