# Fábricas

Al acceder a la opción de *fábricas*, tendremos un listado con todas las fábricas existentes, dónde podremos ver de un
vistazo la información más relevante como son los datos de contacto, el cliente asociado y la comisión que se aplica
**por defecto**.

Además de la pestaña *Fábricas*, dónde se puede ver dicho listado, existe otra pestaña llamada *Grupos*, que nos permite
llevar un mantenimiento de **Grupos de fábrica**. Los grupos de fábrica, en un principio son sólo un criterio de 
clasificación, para poder agrupar fábricas que pertenezcan al mismo grupo.

Para crear una nueva fábrica, basta con pulsar al botón **Nuevo**.

## Alta de una nueva fábrica

Desde el apartado de **Fábricas**, pulsando el botón **Nuevo**, aparece un formulario dónde se pueden introducir los
datos principales de la fábrica y una dirección que se considerará la dirección principal.

Si se necesitan cumplimentar datos adicionales, bastará con editarla como se explica en el siguiente apartado.

## Edición de una fábrica existente

Al seleccionar una fábrica, se pueden modificar la mayoría de sus datos, además quedan disponibles los siguientes 
botones de acción:
- Pulsar en el botón **<- Fábricas** para retornar al listado de fábricas. Se se ha realizado algún cambio, será
necesario pulsar antes en guardar para no perder dichos cambios.
- Existe un botón **Dar de baja**, que permite marcar la fábrica como *de baja*; o bien, **Editar baja** si ya está de 
baja, y queremos reestablecerla.

Al dar de baja a una fábrica, desaparecerá de las búsquedas pero no del listado, para poderla recuperar si fuese 
necesario. La baja no es inmediata, sino que aparece una pantalla emergente donde hay que marcar el *check*
para darla de baja, o desmarcarlo para recuperarla.

### Pestaña *Datos*

En la pestaña de datos, tenemos la información general de la fábricaa, como es el nombre, el CIF, los datos de contacto
(teléfonos, fax y email); el cliente del ERP asociado a la fábrica (para la emisión de la factura de comisiones), la
forma y la cuenta de pago, y lo más importante, la **comisión y los descuentos aplicados por defecto**.

Hay que tener en cuenta, que los datos de pago, son orientativos, ya que son los que se emiten en los pedidos de 
fábrica, pero nada tiene que ver con la forma de pago de la fábrica, que estará definida en la ficha del cliente
del ERP asociado.

Las comisiones y los descuentos son datos tomados por defecto, esto es, pueden definirse datos diferentes en las tarifas
de fábrica y cliente.

### Pestaña *Bancos*

La pestaña de bancos, permite definir números de cuentas bancarias asociadas a la fábrica.

El dato es meramente informativo y pueden aparecer en el pedido si fuese necesario.

### Pestaña *Direcciones*

Por defecto, al dar de alta la fábrica, se crea una dirección **Principal**. En esta pestaña, podemos asociar nuevas 
direcciones a la fábrica, desde las que servir mercancía.

De haber más de una dirección, las tarifas pueden ser diferentes para cada una de ella, y además, al hacer un pedido, 
habrá que indicar a qué dirección se hace.

### Pestaña *Impresión*

La pestaña impresión, permite seleccionar un logotipo personalizado para los documentos de la fábrica.

### Pestaña *Vencimientos*

La pestaña de vencimientos, indica cómo se calcula el vencimiento de las facturas emitidas a fábrica.

- El **periodo** indica cuántos meses componen cada periodo de facturación. Si la facturación es mensual habrá que poner
un 1, si es trimestral un 3, y así sucesivamente.
- El **día de vencimiento**, indica el día del mes en el que vence la factura.
- Los **meses de carencia**  indican cuántos meses hay que añadir a la fecha de facturación, ésto es, si facturamos un 
albarán de fábrica en marzo, un valor de 1, hará que la factura se emita en abril, un 2 en mayo, y así sucesivamente.

### Pestaña *Tarifas*

La pestaña de tarifas, nos permite añadir distintas tarifas de la fábrica por cada dirección y periodo.

Cada dirección tendrá sus tarifas independientes, por lo que hay que seleccionar la dirección en primer lugar, si no es
la que aparece marcada por defecto, y pulsar en botón **Seleccionar**.

Una vez seleccionada la dirección correcta, se podrá:

- Seleccionar uno de los periodos disponibles que irá de fecha a fecha (o de fecha a indefinida, si no se ha 
especificado una fecha de finalización de la tarifa). Una vez marcado el periodo deseado, habrá que pulsar en el botón
**Seleccionar**.
- Sobre la tarifa seleccionada, se podrá pulsar en el botón **Importar** para cargar el catálogo de productos del 
cliente, que deberá de estar en formato CSV, separado por punto y coma (;).
- Se podrá crear en cualquier momento una nueva tarifa, pulsando en el botón **Nueva**. Al crear una nueva tarifa, se
deberá de seleccionar la fecha de comienzo, y opcionalmente la fecha de finalización. Si no se especifica una fecha de
finalización, se asumirá que es hasta nueva tarifa (indefinida).

Hay que tener las siguientes consideraciones:

- Una tarifa no puede solaparse con otra. Si se crea una tarifa cuya fecha coincida con las de otras, la nueva 
sustituirá a las existentes hasta el día antes de la anterior y el día siguiente del de finalización.
- Si la última tarifa no tiene fecha de finalización, al abrir una posterior, la última será cerrada el día antes a la 
fecha de inicio de la nueva.
- El archivo CSV tendrá varias columnas con los siguientes títulos:
  - referencia, que detalla las referencias de los productos, según la fábrica (ha de ser única en la tarifa).
  - nombre, es el nombre del producto.
  - precio de venta.
  - comisión (opcional), indica la comisión que aporta la venta del producto. De dejarse en blanco, se toma la comisión 
  por defecto de la fábrica.
  - descuento1 a descuento3 (opcionales), son los posibles descuentos en línea del producto. Si se dejan en blanco, se
  asumen los descuentos indicados en la ficha de la fábrica.

### Pestaña *Ofertas*

La pestaña de ofertas, nos permite añadir distintas ofertas de la fábrica.

El funcionamiento de las ofertas es similar al de las tarifas con las siguientes diferencias:

- Una oferta es un descuento puntual que se añade al precio de venta habitual, y que se aplica durante un peridodo de 
tiempo determinado (una feria, los días de oro, promoción de verano, especial aniversario,...).
- El archivo CSV tiene una estructura diferente, ya que los datos ya deben de existir de la tarifa:
  - referencia, que identifique el producto de fábrica de forma inquívoca.
  - nombre.
  - descuento, que se aplicará como descuento 4.

Por lo demás, el funcionamiento de las ofertas, es idéntico al de las tarifas.
