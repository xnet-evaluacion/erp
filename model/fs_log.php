<?php

/*
 * This file is part of MiFactura.eu
 * Copyright (C) 2021 X-Net Software Solutions S.L. <xnetsoftware@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Clase para almacenar el historial de acciones de los usuarios y eventos del sistema.
 * Accesible desde admin -> información del sistema.
 */
class fs_log extends fs_model
{
    /**
     * TRUE -> resaltar en el listado.
     *
     * @var bool
     */
    public $alerta;

    /**
     * Nombre del controlador.
     *
     * @var string
     */
    public $controlador;

    /**
     * Texto del log. Sin longitud máxima.
     *
     * @var string
     */
    public $detalle;

    /**
     * Fecha.
     *
     * @var string
     */
    public $fecha;

    /**
     * Clave primaria.
     *
     * @var null|int
     */
    public $id;

    /**
     * IP.
     *
     * @var string
     */
    public $ip;

    /**
     * Tipo.
     *
     * @var string
     */
    public $tipo;

    /**
     * Nick del usuario.
     *
     * @var string
     */
    public $usuario;

    /**
     * fs_log constructor.
     *
     * @param false|array $data
     */
    public function __construct($data = false)
    {
        parent::__construct('fs_logs');
        if ($data) {
            $this->alerta = $this->str2bool($data['alerta']);
            $this->controlador = $data['controlador'];
            $this->detalle = $data['detalle'];
            $this->fecha = date('Y-m-d H:i:s', strtotime($data['fecha']));
            $this->id = intval($data['id']);
            $this->ip = $data['ip'];
            $this->tipo = $data['tipo'];
            $this->usuario = $data['usuario'];
        } else {
            $this->alerta = false;
            $this->controlador = null;
            $this->detalle = null;
            $this->fecha = date('Y-m-d H:i:s');
            $this->id = null;
            $this->ip = null;
            $this->tipo = null;
            $this->usuario = null;
        }
    }

    /**
     * Devuelve el registro por ID o false si no se encuentra.
     *
     * @param string $id
     *
     * @return false|static
     */
    public function get($id)
    {
        $sql = "SELECT *"
            . " FROM `" . $this->table_name() . "`"
            . " WHERE id = " . $this->var2str($id)
            . ";";
        $data = $this->db->select($sql);
        if ($data) {
            return new static($data[0]);
        }

        return false;
    }

    /**
     * Guarda o actualiza los datos en la tabla, devuelve TRUE o FALSE
     *
     * @return bool
     */
    public function save()
    {
        if (!$this->test()) {
            return false;
        }

        if ($this->exists()) {
            $sql = "UPDATE `" . $this->table_name() . "`"
                . " SET fecha = " . $this->var2str($this->fecha)
                . ", tipo = " . $this->var2str($this->tipo)
                . ", detalle = " . $this->var2str($this->detalle)
                . ", usuario = " . $this->var2str($this->usuario)
                . ", ip = " . $this->var2str($this->ip)
                . ", alerta = " . $this->var2str($this->alerta)
                . ", controlador = " . $this->var2str($this->controlador)
                . " WHERE id=" . $this->var2str($this->id)
                . ";";

            return $this->db->exec($sql);
        }

        $sql = "INSERT INTO `" . $this->table_name() . "` (fecha,tipo,detalle,usuario,ip,alerta,controlador) "
            . "VALUES ("
            . $this->var2str($this->fecha)
            . ", " . $this->var2str($this->tipo)
            . ", " . $this->var2str($this->detalle)
            . ", " . $this->var2str($this->usuario)
            . ", " . $this->var2str($this->ip)
            . ", " . $this->var2str($this->alerta)
            . ", " . $this->var2str($this->controlador)
            . ");";

        if ($this->db->exec($sql)) {
            $this->id = $this->db->lastval();
            return true;
        }

        return false;
    }

    /**
     * Comprueba los datos del modelo, devuelve TRUE si son correctos
     *
     * @return bool
     */
    public function test()
    {
        $this->controlador = $this->no_html($this->controlador);
        $this->detalle = $this->no_html($this->detalle);
        return true;
    }

    /**
     * Esta función devuelve TRUE si los datos del objeto se encuentran
     * en la base de datos.
     *
     * @return bool
     */
    public function exists()
    {
        if (is_null($this->id)) {
            return false;
        }

        $sql = "SELECT *"
            . " FROM `" . $this->table_name() . "`"
            . " WHERE id = " . $this->var2str($this->id)
            . ";";
        return (bool) $this->db->select($sql);
    }

    /**
     * Elimina el registro de la tabla, devuelve el resultado de la consulta.
     *
     * @return bool
     */
    public function delete()
    {
        $sql = "DELETE FROM `" . $this->table_name() . "`"
            . " WHERE id = " . $this->var2str($this->id)
            . ";";
        return $this->db->exec($sql);
    }

    /**
     * Devuelve un array con todos las registros.
     *
     * @param int $offset
     * @param int $limit
     *
     * @return static[]
     */
    public function all($offset = 0, $limit = FS_ITEM_LIMIT)
    {
        $sql = "SELECT *"
            . " FROM `" . $this->table_name() . "`"
            . " ORDER BY fecha DESC";
        return $this->all_from($sql, $offset, $limit);
    }

    /**
     * Devuelve todos los registros filtrado por el usuario.
     *
     * @param string $usuario
     *
     * @return static[]
     */
    public function all_from_usuario($usuario)
    {
        $sql = "SELECT *"
            . " FROM `" . $this->table_name() . "`"
            . " WHERE usuario = " . $this->var2str($usuario)
            . " ORDER BY fecha DESC";
        return $this->all_from($sql, 0, 0);
    }

    /**
     * Devuelve todos los registros filtrado por el tipo.
     *
     * @param string $tipo
     *
     * @return static[]
     */
    public function all_by($tipo)
    {
        $sql = "SELECT *"
            . " FROM `" . $this->table_name() . "`"
            . " WHERE tipo = " . $this->var2str($tipo)
            . " ORDER BY fecha DESC";
        return $this->all_from($sql, 0, 0);
    }

    /**
     * Si un modelo depende de otro, en este método deben hacerse las correspondientes instanciaciones
     * A su vez, debe invocar al fix_db de dicho modelo dependiente, para poder corregir al vuelo posibles errores que
     * se hubieran detectado con el tiempo.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2021.09
     */
    protected function check_model_dependencies()
    {
        (new fs_user())->fix_db();

        parent::check_model_dependencies();
    }
}
