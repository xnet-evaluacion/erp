<?php

/*
 * This file is part of MiFactura.eu
 * Copyright (C) 2021 X-Net Software Solutions S.L. <xnetsoftware@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

require_once constant('BASE_PATH') . '/model/core/agente_alta.php';

/**
 * Esta tabla contiene la relación de altas y bajas del agente.
 * Por compatibilidad, el último dato se mantendrá en la tabla de agentes, y aquí estará el histórico.
 * Al dar de alta se elmiminaría la posible baja del fichero de agentes.
 * Tiene que ofrecerse la opción de editar la última alta/baja.
 */
class agente_alta extends \MiFactura\model\agente_alta
{
}
