<?php

/*
 * This file is part of MiFactura.eu
 * Copyright (C) 2021 X-Net Software Solutions S.L. <xnetsoftware@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Elemento del menú de MiFactura.eu, cada uno se corresponde con un controlador.
 */
class fs_page extends fs_model
{
    /**
     * Nombre de la clave para almacenar/acceder a la consulta en caché
     */
    const CACHE_KEY_ALL = 'm_fs_page_all';

    /**
     * Clave primaria.
     *
     * Es el nombre del controlador.
     *
     * @var string
     */
    public $name;

    /**
     * Es el título de la página, y no es editable, se detalla en el constructor.
     *
     * @var string
     */
    public $title;

    /**
     * Es el nombre que aparece en el menú, y es editable por el usuario.
     * Por defecto, es el title.
     *
     * @var string
     */
    public $alias;

    /**
     * Es una descripción corta de para qué sirve la página.
     * Por defecto, es el title.
     *
     * @var string
     */
    public $description;

    /**
     * Es la posición en el menú.
     * Admite hasta 3 niveles de menú y se completa con el Alias.
     * En la base de datos se almacena como un string separado por |
     *
     * @var string
     */
    public $folder;

    /**
     * Versión.
     *
     * @var null|string
     */
    public $version;

    /**
     * FALSE -> ocultar en el menú.
     *
     * @var bool
     */
    public $show_on_menu;
/**
     * TRUE -> Se añade a la sección de páginas favoritas
     *
     * @var bool
     */
    public $favorite;

    /**
     * La página existe?
     *
     * @var false
     */
    public $exists;

    /**
     * La página está activada?
     *
     * @var false
     */
    public $enabled;

    /**
     * Cadena adicional para la URL.
     *
     * @var string
     */
    public $extra_url;

    /**
     * Cuando un usuario no tiene asignada una página por defecto, se selecciona
     * la primera página importante a la que tiene acceso.
     */
    public $important;

    /**
     * Orden para el menú dentro de su carpeta.
     *
     * @var null|int
     */
    public $orden;

    /**
     * fs_page constructor.
     *
     * @param false|array $data
     */
    public function __construct($data = false)
    {
        parent::__construct('fs_pages');
        if ($data) {
            $this->name = $data['name'];
            $this->title = $data['title'];
            $this->folder = $data['folder'];
            $this->alias = $data['alias'] ?? $this->title;
            $this->description = $data['description'] ?? $this->title;

            if (empty($this->alias)) {
                $this->alias = $this->title;
            }

            if (empty($this->description)) {
                $this->alias = $this->description;
            }

            $this->version = null;
            if (isset($data['version'])) {
                $this->version = $data['version'];
            }

            $this->show_on_menu = $this->str2bool($data['show_on_menu']);
            $this->favorite = $this->str2bool($data['favorite']);
            $this->important = $this->str2bool($data['important']);

            $this->orden = 100;
            if (isset($data['orden'])) {
                $this->orden = $this->intval($data['orden']);
            }
        } else {
            $this->name = null;
            $this->title = null;
            $this->alias = null;
            $this->description = null;
            $this->folder = null;
            $this->version = null;
            $this->show_on_menu = true;
            $this->favorite = false;
            $this->important = false;
            $this->orden = 100;
        }

        $this->exists = false;
        $this->enabled = false;
        $this->extra_url = '';
    }

    /**
     * Clona el objeto.
     */
    public function __clone()
    {
        $page = new static();
        $page->name = $this->name;
        $page->title = $this->title;
        $page->alias = $this->alias;
        $page->description = $this->description;
        $page->folder = $this->folder;
        $page->version = $this->version;
        $page->show_on_menu = $this->show_on_menu;
        $page->favorite = $this->favorite;
        $page->important = $this->important;
        $page->orden = $this->orden;
    }

    /**
     * Devuelve la url donde se pueden ver/modificar estos datos
     *
     * @return string
     */
    public function url()
    {
        if (is_null($this->name)) {
            return 'index.php?page=admin_home';
        }

        return 'index.php?page=' . $this->name . $this->extra_url;
    }

    /**
     * Devuelve TRUE si este es la opción predeterminado de la empresa.
     *
     * @return bool
     */
    public function is_default()
    {
        return ($this->name == $this->default_items->default_page());
    }

    /**
     * Devuelve true si es la página que se está mostrando.
     *
     * @return bool
     */
    public function showing()
    {
        return ($this->name == $this->default_items->showing_page());
    }

    /**
     * Devuelve una página por su nombre.
     *
     * @param string $name
     *
     * @return false|static
     */
    public function get($name)
    {
        $sql = "SELECT *"
            . " FROM `" . $this->table_name() . "`"
            . " WHERE name = " . $this->var2str($name)
            . ";";
        $data = $this->db->select($sql);
        if ($data) {
            return new static($data[0]);
        }

        return false;
    }

    public function get_by_folder($folder)
    {
        $sql = "SELECT *"
            . " FROM `" . $this->table_name() . "`"
            . " WHERE folder = " . $this->var2str($folder)
            . ";";
        $data = $this->db->select($sql);
        if ($data) {
            return new static($data[0]);
        }
        return false;
    }

    /**
     * Devuelve true si ha añadido o actualizado el registro, sinó false
     *
     * @return bool
     */
    public function save()
    {
        $this->clean_cache();

        if (!$this->test()) {
            return false;
        }

        if ($this->exists()) {
            $sql = "UPDATE `" . $this->table_name() . "`"
                . " SET title = " . $this->var2str($this->title)
                . ", folder = " . $this->var2str($this->folder)
                . ", alias = " . $this->var2str($this->alias)
                . ", description = " . $this->var2str($this->description)
                . ", version = " . $this->var2str($this->version)
                . ", show_on_menu = " . $this->var2str($this->show_on_menu)
                . ", favorite = " . $this->var2str($this->favorite)
                . ", important = " . $this->var2str($this->important)
                . ", orden = " . $this->var2str($this->orden)
                . " WHERE name = " . $this->var2str($this->name)
                . ";";
        } else {
            $sql = "INSERT INTO `" . $this->table_name() . "` (name,title,alias,description,folder,version,show_on_menu,favorite,important,orden) VALUES ("
                . $this->var2str($this->name)
                . ", " . $this->var2str($this->title)
                . ", " . $this->var2str($this->alias)
                . ", " . $this->var2str($this->description)
                . ", " . $this->var2str($this->folder)
                . ", " . $this->var2str($this->version)
                . ", " . $this->var2str($this->show_on_menu)
                . ", " . $this->var2str($this->favorite)
                . ", " . $this->var2str($this->important)
                . ", " . $this->var2str($this->orden)
                . ");";
        }

        return $this->db->exec($sql);
    }

    /**
     * Limpia la caché
     */
    public function clean_cache()
    {
        $this->cache->delete(self::CACHE_KEY_ALL);
    }

    /**
     * Comprueba los datos del empleado/agente, devuelve TRUE si son correctos
     *
     * @return bool
     */
    public function test()
    {
        $status = true;

        if (strlen($this->name) < 1 || strlen($this->name) > 90) {
            $this->new_error_msg("Nombre de página no válido (" . $this->name . "), debe tener entre 1 y 90 caracteres.");
            $status = false;
        }

        if (strlen($this->title) < 1 || strlen($this->title) > 50) {
            $this->new_error_msg("Título de página no válido (" . $this->title . "), debe tener entre 1 y 50 caracteres.");
            $status = false;
        }

        if (strlen($this->folder) < 1 || strlen($this->folder) > 100) {
            $this->new_error_msg("Nombre de carpeta no válido (" . $this->folder . "), debe tener entre 1 y 100 caracteres.");
            $status = false;
        }

        return $status;
    }

    /**
     * Esta función devuelve TRUE si los datos del objeto se encuentran
     * en la base de datos.
     *
     * @return bool
     */
    public function exists()
    {
        if (is_null($this->name)) {
            return false;
        }

        $sql = "SELECT *"
            . " FROM `" . $this->table_name() . "`"
            . " WHERE name = " . $this->var2str($this->name)
            . ";";
        return $this->db->select($sql);
    }

    /**
     * Elimina el registro de la tabla, devuelve el resultado de la consulta.
     *
     * @return bool
     */
    public function delete()
    {
        $this->clean_cache();
        $sql = "DELETE FROM `" . $this->table_name() . "`"
            . " WHERE name = " . $this->var2str($this->name)
            . ";";
        return $this->db->exec($sql);
    }

    /**
     * Retorna un literal con la ruta de menú de la forma que usa admin_home.
     *
     * @return string
     */
    public function get_folder()
    {
        return implode(' » ', explode('|', $this->folder));
    }

    /**
     * Devuelve todas las páginas o entradas del menú
     *
     * @return static[]
     */
    public function all()
    {
        $sql = "SELECT *"
            . " FROM `" . $this->table_name() . "`"
            . " ORDER BY LOWER(folder) ASC, orden ASC, LOWER(title) ASC;";
        $result = $this->all_from_cached(self::CACHE_KEY_ALL, $sql);

        // Si no se ha definido alias y description, se toman las del título
        foreach ($result as $key => $value) {
            if (empty($value->alias)) {
                $result[$key]->alias = $value->title;
            }
            if (empty($value->description)) {
                $result[$key]->description = $value->title;
            }
        }
        return $result;
    }

    /**
     * Esta función es llamada al crear una tabla.
     * Permite insertar valores en la tabla.
     *
     * @return string
     */
    public function install()
    {
        $this->clean_cache();
        return "INSERT INTO `" . $this->table_name() . "` (name,title,folder,version,show_on_menu,favorite)
        VALUES ('admin_home','panel de control','admin|dashboard',NULL,TRUE,FALSE),
        ('dashboard','dashboard','admin|company',NULL,TRUE,FALSE),
        ('admin_notificacion','Notificaciones','admin|company',NULL,TRUE,FALSE);";
    }

    /**
     * Realizamos algunas correcciones a la base de datos y devuelve su estado.
     * Principalmente debe corregir fallos en relaciones entre tablas.
     * NOTA: Algunos controladores llaman a este método, así que las consultas deben ser lo más eficientes posibles
     * hasta que se reemplace por otra forma.
     *
     * @return bool
     * @version 2021.09
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     */
    public function fix_db()
    {
        $fixes = [
            'fs_access' => [
                // 'fs_access_page2' => 'UPDATE `fs_access` SET fs_page = NULL WHERE fs_page NOT IN (SELECT name FROM `fs_pages`);',
                'fs_access_page2' => 'DELETE FROM `fs_access` WHERE fs_page NOT IN (SELECT name FROM `fs_pages`);',
            ],
            'fs_roles_access' => [
                // 'fs_roles_access_fs_page' => 'UPDATE `fs_roles_access` SET fs_page = NULL WHERE fs_page NOT IN (SELECT name FROM `fs_pages`);',
                'fs_roles_access_fs_page' => 'DELETE FROM `fs_roles_access` WHERE fs_page NOT IN (SELECT name FROM `fs_pages`);',
            ],
            'fs_users' => [
                // 'ca_fs_users_fs_pages' => 'UPDATE `fs_users` SET fs_page = NULL WHERE fs_page NOT IN (SELECT name FROM `fs_pages`);',
                'ca_fs_users_fs_pages' => 'DELETE FROM  `fs_users` WHERE fs_page NOT IN (SELECT name FROM `fs_pages`);',
            ],
        ];

        return $this->exec_fix_queries($fixes);
    }

    /**
     * Si una tabla de un modelo tiene problemas a nivel SQL que corregir, deben ejecutarse aquí.
     *
     * @return bool
     * @version 2021.09
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     */
    protected function fix_model_table_before()
    {
        /**
         * Se eliminan las posibles integridades referenciales que puedan implicar a name de fs_pages.
         * El motivo de esta acción, es porque inicialmente la longitud era 50 y se pasó a 90.
         * Sólo se hará si la longitud no es 90.
         */
        $fields = $this->db->get_columns($this->table_name());
        foreach ($fields as $field) {
            if ($field['name'] === 'name' && $field['type'] !== 'varchar(90)') {
                $bulk_table = new fs_access();
                $this->db->delete_constraint($bulk_table->table_name(), 'fs_access_page2');
                $this->db->exec('ALTER TABLE `' . $bulk_table->table_name() . '` DROP PRIMARY KEY;');
                $this->db->exec('ALTER TABLE `' . $bulk_table->table_name() . '` CHANGE `fs_page` `fs_page` VARCHAR(90) NOT NULL;');

                $bulk_table = new fs_user();
                $this->db->delete_constraint($bulk_table->table_name(), 'ca_fs_users_fs_pages');
                $this->db->exec('ALTER TABLE `' . $bulk_table->table_name() . '` DROP PRIMARY KEY;');
                $this->db->exec('ALTER TABLE `' . $bulk_table->table_name() . '` CHANGE `fs_page` `fs_page` VARCHAR(90) NOT NULL;');

                $bulk_table = new fs_rol_access();
                $this->db->delete_constraint($bulk_table->table_name(), 'fs_roles_access_fs_page');
                $this->db->exec('ALTER TABLE `' . $bulk_table->table_name() . '` DROP PRIMARY KEY;');
                $this->db->exec('ALTER TABLE `' . $bulk_table->table_name() . '` CHANGE `fs_page` `fs_page` VARCHAR(90) NOT NULL;');

                $bulk_table = new fs_extension();
                $this->db->delete_constraint($bulk_table->table_name(), 'ca_fs_extensions2_fs_pages');
                $this->db->delete_constraint($bulk_table->table_name(), 'ca_fs_extensions2_fs_pages2');
                $this->db->exec('ALTER TABLE `' . $bulk_table->table_name() . '` DROP PRIMARY KEY;');
                $this->db->exec('ALTER TABLE `' . $bulk_table->table_name() . '` CHANGE `page_from` `page_from` VARCHAR(90) NOT NULL;');
                $this->db->exec('ALTER TABLE `' . $bulk_table->table_name() . '` CHANGE `page_to` `page_to` VARCHAR(90) NOT NULL;');

                $this->db->exec('ALTER TABLE `' . $this->table_name() . '` DROP PRIMARY KEY;');
                $this->db->exec('ALTER TABLE `' . $this->table_name() . '` CHANGE `name` `name` VARCHAR(90) NOT NULL;');
                $this->db->exec('ALTER TABLE `' . $this->table_name() . '` ADD PRIMARY KEY (`name`);');
            }
        }

        $fixes = [
            $this->table_name() => [
                /**
                 * Estas 2 no deben eliminarse, ya que una vez registradas, no pasa nada aunque dejen de estar
                 * disponibles, sino se pierden sus extensiones si la página de integración deja de estar disponible
                 * temporalmente.
                 *
                 * Se dejan comentadas para evitar añadirlas de nuevo por error.
                 */
                // 'DELETE FROM `fs_extensions2` WHERE page_from NOT IN (SELECT DISTINCT(name) FROM `fs_pages`);',
                // 'DELETE FROM `fs_extensions2` WHERE page_to NOT IN (SELECT DISTINCT(name) FROM `fs_pages`);',
            ],
        ];

        return $this->exec_fix_queries($fixes);
    }

    /**
     * Cambiamos el estado favorite de la página actual
     *
     * @param $actual_page
     *
     * @return mixed
     * @author  Daniel M. Hernández Vieira  <daniel.hernandez@x-netdigital.com>
     * @version 2022.0621
     *
     */
    public function set_favorite_page($actual_folder)
    {
        $sql = "SELECT favorite"
            . " FROM `" . $this->table_name() . "`"
            . " WHERE folder = " . $this->var2str($actual_folder) . " AND"
            . " name = " . $this->var2str($this->default_items->showing_page())
            . ";";

        $actual_favorite_page_state = $this->db->select($sql)[0][favorite];
        $actual_favorite_page_state = $actual_favorite_page_state == "0" ? true : false;
        debug_message($actual_folder);

        $sql = "UPDATE `" . $this->table_name() . "`"
            . " SET favorite = " . $actual_favorite_page_state
            . " WHERE folder = " . $this->var2str($actual_folder) . " AND"
            . " name = " . $this->var2str($this->default_items->showing_page())
            . ";";
        return $this->db->exec($sql);

    }
}
