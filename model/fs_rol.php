<?php

/*
 * This file is part of MiFactura.eu
 * Copyright (C) 2021 X-Net Software Solutions S.L. <xnetsoftware@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Define un paquete de permisos para asignar rápidamente a usuarios.
 */
class fs_rol extends fs_model
{
    /**
     * Código del rol.
     *
     * @var null|string
     */
    public $codrol;

    /**
     * Descripción.
     *
     * @var null|string
     */
    public $descripcion;

    /**
     * fs_rol constructor.
     *
     * @param false|array $data
     */
    public function __construct($data = false)
    {
        parent::__construct('fs_roles');
        if ($data) {
            $this->codrol = $data['codrol'];
            $this->descripcion = $data['descripcion'];
        } else {
            $this->codrol = null;
            $this->descripcion = null;
        }
    }

    /**
     * Devuelve la url donde se pueden ver/modificar estos datos
     *
     * @return string
     */
    public function url()
    {
        if (is_null($this->codrol)) {
            return 'index.php?page=admin_rol';
        }

        return 'index.php?page=admin_rol&codrol=' . urlencode($this->codrol);
    }

    /**
     * Retorna los datos del modelo según el parámetro
     *
     * @param string $codrol
     *
     * @return false|static
     */
    public function get($codrol)
    {
        $sql = "SELECT *"
            . " FROM `" . $this->table_name() . "`"
            . " WHERE codrol = " . $this->var2str($codrol)
            . ";";
        $data = $this->db->select($sql);
        if ($data) {
            return new static($data[0]);
        }

        return false;
    }

    /**
     * Devuelve la lista de accesos permitidos del rol.
     *
     * @return static[]
     */
    public function get_accesses()
    {
        $access = new fs_rol_access();
        return $access->all_from_rol($this->codrol);
    }

    /**
     * Devuelve la lista de usuarios con este rol.
     *
     * @return array
     */
    public function get_users()
    {
        $ru = new fs_rol_user();
        return $ru->all_from_rol($this->codrol);
    }

    /**
     * Devuelve true si ha añadido o actualizado el registro, sinó false
     *
     * @return bool
     */
    public function save()
    {
        $this->descripcion = $this->no_html($this->descripcion);

        if ($this->exists()) {
            $sql = "UPDATE `" . $this->table_name() . "`"
                . " SET descripcion = " . $this->var2str($this->descripcion)
                . " WHERE codrol = " . $this->var2str($this->codrol)
                . ";";
        } else {
            $sql = "INSERT INTO `" . $this->table_name() . "` (codrol,descripcion) VALUES ("
                . $this->var2str($this->codrol)
                . ", " . $this->var2str($this->descripcion)
                . ");";
        }

        return $this->db->exec($sql);
    }

    /**
     * Esta función devuelve TRUE si los datos del objeto se encuentran
     * en la base de datos.
     *
     * @return bool
     */
    public function exists()
    {
        if (is_null($this->codrol)) {
            return false;
        }

        $sql = "SELECT *"
            . " FROM `" . $this->table_name() . "`"
            . " WHERE codrol = " . $this->var2str($this->codrol)
            . ";";
        return $this->db->select($sql);
    }

    /**
     * Elimina el registro de la tabla, devuelve el resultado de la consulta.
     *
     * @return bool
     */
    public function delete()
    {
        $sql = "DELETE FROM `" . $this->table_name() . "`"
            . " WHERE codrol = " . $this->var2str($this->codrol)
            . ";";
        return $this->db->exec($sql);
    }

    /**
     * Devuelve todos los registros de la tabla
     *
     * @return static[]
     */
    public function all()
    {
        $sql = "SELECT *"
            . " FROM `" . $this->table_name() . "`"
            . " ORDER BY descripcion ASC;";
        return $this->all_from($sql, 0, 0);
    }

    /**
     * Retorna todos los datos del modelo según el parámetro
     *
     * @param string $nick
     *
     * @return static[]
     */
    public function all_for_user($nick)
    {
        $sql = "SELECT *"
            . " FROM `" . $this->table_name() . "`"
            . " WHERE codrol IN"
            . " ("
            . "  SELECT codrol"
            . "  FROM `fs_roles_users`"
            . "  WHERE fs_user = " . $this->var2str($nick)
            . " );";
        return $this->all_from($sql, 0, 0);
    }

    /**
     * Esta función es llamada al crear una tabla.
     * Permite insertar valores en la tabla.
     *
     * @return string
     */
    public function install()
    {
        return '';
    }

    /**
     * Realizamos algunas correcciones a la base de datos y devuelve su estado.
     * Principalmente debe corregir fallos en relaciones entre tablas.
     * NOTA: Algunos controladores llaman a este método, así que las consultas deben ser lo más eficientes posibles
     * hasta que se reemplace por otra forma.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2021.09
     *
     * @return bool
     */
    public function fix_db()
    {
        $fixes = [
            'fs_roles_access' => [
                'fs_roles_access_fs_roles' => 'UPDATE `fs_roles_access` SET codrol = NULL WHERE codrol NOT IN (SELECT codrol FROM `fs_roles`);',
            ],
            'fs_roles_users' => [
                'fs_roles_users_fs_roles' => 'UPDATE `fs_roles_users` SET codrol = NULL WHERE codrol NOT IN (SELECT codrol FROM `fs_roles`);',
            ],
        ];

        return $this->exec_fix_queries($fixes);
    }
}
