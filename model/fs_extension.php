<?php

/*
 * This file is part of MiFactura.eu
 * Copyright (C) 2021 X-Net Software Solutions S.L. <xnetsoftware@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Permite que un plugin añada elementos a una vista existente,
 * sin necesidad de un reemplazo completo.
 * Los tipos soportados depende de cada página, pero suelen ser:
 * head, css, button y tab.
 */
class fs_extension extends fs_model
{
    /**
     * Identificador de la extensión para poder buscarlo fácilemnte.
     * No es la clave primaria. La clave primaria es name+from.
     *
     * @var string
     */
    public $name;

    /**
     * Nombre de la página (controlador) que ofrece la extensión.
     *
     * @var string
     */
    public $from;

    /**
     * Nombre de la página (controlador) que recibe la extensión.
     *
     * @var string
     */
    public $to;

    /**
     * Tipo de extensión: head, css, button, tab...
     *
     * @var string
     */
    public $type;

    /**
     * Texto del botón, del tab...
     *
     * @var string
     */
    public $text;

    /**
     * Parámetros extra para la URL. Debes añadir el &
     *
     * @var string
     */
    public $params;

    /**
     * fs_extension constructor.
     *
     * @param false|array $data
     */
    public function __construct($data = false)
    {
        parent::__construct('fs_extensions2');
        if ($data) {
            $this->name = $data['name'];
            $this->from = $data['page_from'];
            $this->to = $data['page_to'];
            $this->type = $data['type'];
            $this->text = $data['text'];
            $this->params = $data['params'];
        } else {
            $this->name = null;
            $this->from = null;
            $this->to = null;
            $this->type = null;
            $this->text = null;
            $this->params = null;
        }
    }

    /**
     * Obtiene el registro para name y page_from.
     *
     * @param string $name
     * @param string $from
     *
     * @return false|static
     */
    public function get($name, $from)
    {
        $sql = "SELECT *"
            . " FROM `" . $this->table_name() . "`"
            . " WHERE name = " . $this->var2str($name)
            . " AND page_from = " . $this->var2str($from)
            . ";";

        $data = $this->db->select($sql);
        if ($data) {
            return new static($data[0]);
        }

        return false;
    }

    /**
     * Devuelve true si ha añadido o actualizado el registro, sinó false
     *
     * @return bool
     */
    public function save()
    {
        if ($this->exists()) {
            $sql = "UPDATE `" . $this->table_name() . "`"
                . " SET page_to = " . $this->var2str($this->to)
                . ", type = " . $this->var2str($this->type)
                . ", text = " . $this->var2str($this->text)
                . ", params = " . $this->var2str($this->params)
                . " WHERE name = " . $this->var2str($this->name)
                . " AND page_from = " . $this->var2str($this->from)
                . ";";
        } else {
            $sql = "INSERT INTO `" . $this->table_name() . "` (name,page_from,page_to,type,text,params) VALUES ("
                . $this->var2str($this->name)
                . ", " . $this->var2str($this->from)
                . ", " . $this->var2str($this->to)
                . ", " . $this->var2str($this->type)
                . ", " . $this->var2str($this->text)
                . ", " . $this->var2str($this->params)
                . ");";
        }

        return $this->db->exec($sql);
    }

    /**
     * Esta función devuelve TRUE si los datos del objeto se encuentran
     * en la base de datos.
     *
     * @return bool
     */
    public function exists()
    {
        if (is_null($this->name)) {
            return false;
        }

        $sql = "SELECT *"
            . " FROM `" . $this->table_name() . "`"
            . " WHERE name = " . $this->var2str($this->name)
            . " AND page_from = " . $this->var2str($this->from)
            . ";";
        return $this->db->select($sql);
    }

    /**
     * Elimina el registro de la tabla, devuelve el resultado de la consulta.
     *
     * @return bool
     */
    public function delete()
    {
        $sql = "DELETE FROM `" . $this->table_name() . "`"
            . " WHERE name = " . $this->var2str($this->name)
            . " AND page_from = " . $this->var2str($this->from)
            . ";";
        return $this->db->exec($sql);
    }

    /**
     * Devuelve todos los registros donde $from coincida con el valor dado.
     *
     * @param string $from
     *
     * @return static[]
     */
    public function all_from_page($from)
    {
        $sql = "SELECT *"
            . " FROM `" . $this->table_name() . "`"
            . " WHERE page_from = " . $this->var2str($from)
            . " ORDER BY name ASC;";
        return $this->all_from($sql, 0, 0);
    }

    /**
     * Devuelve todos los registros donde $to coincida con el valor dado.
     *
     * @param string $to
     *
     * @return static[]
     */
    public function all_to($to)
    {
        $sql = "SELECT *"
            . " FROM `" . $this->table_name() . "`"
            . " WHERE page_to = " . $this->var2str($to)
            . " ORDER BY name ASC;";
        return $this->all_from($sql, 0, 0);
    }

    /**
     * Devuelve todos los registros para el tipo indicado.
     *
     * @param string $tipo
     *
     * @return static[]
     */
    public function all_4_type($tipo)
    {
        $sql = "SELECT *"
            . " FROM `" . $this->table_name() . "`"
            . " WHERE type = " . $this->var2str($tipo)
            . " ORDER BY name ASC;";
        return $this->all_from($sql, 0, 0);
    }

    /**
     * Devuelve todos los registros de la tabla.
     *
     * @return static[]
     */
    public function all()
    {
        $sql = "SELECT *"
            . " FROM `" . $this->table_name() . "`"
            . " ORDER BY name ASC;";
        return $this->all_from($sql, 0, 0);
    }

    /**
     * Si un modelo depende de otro, en este método deben hacerse las correspondientes instanciaciones
     * A su vez, debe invocar al fix_db de dicho modelo dependiente, para poder corregir al vuelo posibles errores que
     * se hubieran detectado con el tiempo.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2021.09
     */
    protected function check_model_dependencies()
    {
        (new fs_page())->fix_db();

        parent::check_model_dependencies();
    }

    /**
     * Si una tabla de un modelo tiene problemas a nivel SQL que corregir, deben ejecutarse aquí.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2021.09
     *
     * @return bool
     */
    protected function fix_model_table_before()
    {
        $fixes = [
            $this->table_name() => [
                'ca_fs_extensions2_fs_pages' => 'UPDATE `' . $this->table_name() . '` SET page_from = NULL WHERE page_from = "";',
                'ca_fs_extensions2_fs_pages2' => 'UPDATE `' . $this->table_name() . '` SET page_to = NULL WHERE page_to = "";',
            ],
        ];

        return $this->exec_fix_queries($fixes);
    }
}
