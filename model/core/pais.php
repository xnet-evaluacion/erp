<?php

/*
 * This file is part of MiFactura.eu
 * Copyright (C) 2021 X-Net Software Solutions S.L. <xnetsoftware@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace MiFactura\model;

use fs_model;

/**
 * Un país, por ejemplo España.
 */
class pais extends fs_model
{
    /**
     * Nombre de la clave para almacenar/acceder a la consulta en caché
     */
    const CACHE_KEY_ALL = 'm_pais_all';

    /**
     * Clave primaria. Varchar(3).
     *
     * @var string Código alfa-3 del país.
     * http://es.wikipedia.org/wiki/ISO_3166-1
     */
    public $codpais;

    /**
     * Código alfa-2 del país.
     * http://es.wikipedia.org/wiki/ISO_3166-1
     *
     * @var string
     */
    public $codiso;

    /**
     * Nombre del pais.
     *
     * @var string
     */
    public $nombre;

    /**
     * pais constructor.
     *
     * @param false|array $data
     */
    public function __construct($data = false)
    {
        parent::__construct('paises');
        if ($data) {
            $this->codpais = $data['codpais'];
            $this->codiso = $data['codiso'];
            $this->nombre = $data['nombre'];
        } else {
            $this->codpais = '';
            $this->codiso = null;
            $this->nombre = '';
        }
    }

    /**
     * Devuelve la url donde se pueden ver/modificar estos datos
     *
     * @return string
     */
    public function url()
    {
        if (is_null($this->codpais)) {
            return 'index.php?page=admin_paises';
        }

        return 'index.php?page=admin_paises#' . $this->codpais;
    }

    /**
     * Devuelve TRUE si el pais es el predeterminado de la empresa
     *
     * @return bool
     */
    public function is_default()
    {
        return ($this->codpais == $this->default_items->codpais());
    }

    /**
     * Devuelve el pais con codpais = $cod
     *
     * @param string $cod
     *
     * @return false|static
     */
    public function get($cod)
    {
        $sql = "SELECT *"
            . " FROM `" . $this->table_name() . "`"
            . " WHERE codpais = " . $this->var2str($cod)
            . ";";
        $data = $this->db->select($sql);
        if ($data) {
            return new static($data[0]);
        }

        return false;
    }

    /**
     * Devuelve el pais con codido = $cod
     *
     * @param string $cod
     *
     * @return false|static
     */
    public function get_by_iso($cod)
    {
        $sql = "SELECT *"
            . " FROM `" . $this->table_name() . "`"
            . " WHERE codiso = " . $this->var2str($cod)
            . ";";
        $data = $this->db->select($sql);
        if ($data) {
            return new static($data[0]);
        }

        return false;
    }

    /**
     * Devuelve true si ha añadido o actualizado el registro, sinó false
     *
     * @return bool
     */
    public function save()
    {
        if ($this->test()) {
            $this->clean_cache();

            if ($this->exists()) {
                $sql = "UPDATE `" . $this->table_name() . "` SET "
                    . "codiso = " . $this->var2str($this->codiso)
                    . ", nombre = " . $this->var2str($this->nombre)
                    . " WHERE codpais = " . $this->var2str($this->codpais)
                    . ";";
            } else {
                $sql = "INSERT INTO `" . $this->table_name() . "` (codpais,codiso,nombre) VALUES ("
                    . $this->var2str($this->codpais)
                    . ", " . $this->var2str($this->codiso)
                    . ", " . $this->var2str($this->nombre)
                    . ");";
            }

            return $this->db->exec($sql);
        }

        return false;
    }

    /**
     * Comprueba los datos del pais, devuelve TRUE si son correctos
     *
     * @return bool
     */
    public function test()
    {
        $status = true;

        $this->codpais = trim($this->codpais);
        $this->nombre = $this->no_html($this->nombre);
        $this->codiso = trim($this->codiso);

        if (!preg_match("/^[A-Z0-9]{1,3}$/i", $this->codpais)) {
            $this->new_error_msg("Código Alfa-3 del país no válido (debe contener los caracteres A-Z o 0-9 y tener entre 1 y 3 de longitud): " . $this->codpais);
            $status = false;
        }
        if (!preg_match("/^[A-Z0-9]{1,2}$/i", $this->codiso)) {
            $this->new_error_msg("Código Alfa-2 del país no válido (debe contener los caracteres A-Z o 0-9 y tener entre 1 y 2 de longitud): " . $this->codiso);
            $status = false;
        }
        if (strlen($this->nombre) < 1 || strlen($this->nombre) > 100) {
            $this->new_error_msg("Nombre del país no válido (Debe tener una longitud entre 1 y 100).");
            $status = false;
        }

        return $status;
    }

    /**
     * Limpia la caché
     */
    public function clean_cache()
    {
        $this->cache->delete(self::CACHE_KEY_ALL);
    }

    /**
     * Esta función devuelve TRUE si los datos del objeto se encuentran
     * en la base de datos.
     *
     * @return bool
     */
    public function exists()
    {
        if (is_null($this->codpais)) {
            return false;
        }

        $sql = "SELECT *"
            . " FROM `" . $this->table_name() . "`"
            . " WHERE codpais = " . $this->var2str($this->codpais)
            . ";";
        return $this->db->select($sql);
    }

    /**
     * Elimina el registro de la tabla, devuelve el resultado de la consulta.
     *
     * @return bool
     */
    public function delete()
    {
        $this->clean_cache();
        $sql = "DELETE FROM `" . $this->table_name() . "`"
            . " WHERE codpais = " . $this->var2str($this->codpais)
            . ";";
        return $this->db->exec($sql);
    }

    /**
     * Devuelve un array con todos los paises
     *
     * @return static[]
     */
    public function all()
    {
        $sql = "SELECT *"
            . " FROM `" . $this->table_name() . "`"
            . " ORDER BY nombre ASC;";
        return $this->all_from_cached(self::CACHE_KEY_ALL, $sql);
    }

    /**
     * Esta función es llamada al crear una tabla.
     * Permite insertar valores por defecto en la tabla.
     *
     * @return string
     */
    public function install()
    {
        $this->clean_cache();
        return "INSERT INTO `" . $this->table_name() . "` (codpais,codiso,nombre)"
            . " VALUES ('ESP','ES','España'),"
            . " ('AFG','AF','Afganistán'),"
            . " ('ALB','AL','Albania'),"
            . " ('DEU','DE','Alemania'),"
            . " ('AND','AD','Andorra'),"
            . " ('AGO','AO','Angola'),"
            . " ('AIA','AI','Anguila'),"
            . " ('ATA','AQ','Antártida'),"
            . " ('ATG','AG','Antigua y Barbuda'),"
            . " ('ANT','AN','Antillas Holandesas'),"
            . " ('SAU','SA','Arabia Saudí'),"
            . " ('DZA','DZ','Argelia'),"
            . " ('ARG','AR','Argentina'),"
            . " ('ARM','AM','Armenia'),"
            . " ('ABW','AW','Aruba'),"
            . " ('AUS','AU','Australia'),"
            . " ('AUT','AT','Austria'),"
            . " ('AZE','AZ','Azerbaiyán'),"
            . " ('BHS','BS','Bahamas'),"
            . " ('BHR','BH','Bahréin'),"
            . " ('BGD','BD','Bangladesh'),"
            . " ('BRB','BB','Barbados'),"
            . " ('BEL','BE','Bélgica'),"
            . " ('BLZ','BZ','Belice'),"
            . " ('BEN','BJ','Benín'),"
            . " ('BMU','BM','Bermudas'),"
            . " ('BTN','BT','Bhután'),"
            . " ('BLR','BY','Bielorrusia'),"
            . " ('BOL','BO','Bolivia'),"
            . " ('BIH','BA','Bosnia y Herzegovina'),"
            . " ('BWA','BW','Botsuana'),"
            . " ('BRA','BR','Brasil'),"
            . " ('BRN','BN','Brunéi'),"
            . " ('BGR','BG','Bulgaria'),"
            . " ('BFA','BF','Burkina Faso'),"
            . " ('BDI','BI','Burundi'),"
            . " ('CPV','CV','Cabo Verde'),"
            . " ('KHM','KH','Camboya'),"
            . " ('CMR','CM','Camerún'),"
            . " ('CAN','CA','Canadá'),"
            . " ('TCD','TD','Chad'),"
            . " ('CHL','CL','Chile'),"
            . " ('CHN','CN','China'),"
            . " ('CYP','CY','Chipre'),"
            . " ('VAT','VA','Ciudad del Vaticano'),"
            . " ('COL','CO','Colombia'),"
            . " ('COM','KM','Comoras'),"
            . " ('COG','CG','Congo'),"
            . " ('PRK','KP','Corea del Norte'),"
            . " ('KOR','KR','Corea del Sur'),"
            . " ('CIV','CI','Costa de Marfil'),"
            . " ('CRI','CR','Costa Rica'),"
            . " ('HRV','HR','Croacia'),"
            . " ('CUB','CU','Cuba'),"
            . " ('DNK','DK','Dinamarca'),"
            . " ('DMA','DM','Dominica'),"
            . " ('ECU','EC','Ecuador'),"
            . " ('EGY','EG','Egipto'),"
            . " ('SLV','SV','El Salvador'),"
            . " ('ARE','AE','Emiratos Árabes Unidos'),"
            . " ('ERI','ER','Eritrea'),"
            . " ('SVK','SK','Eslovaquia'),"
            . " ('SVN','SI','Eslovenia'),"
            . " ('USA','US','Estados Unidos'),"
            . " ('EST','EE','Estonia'),"
            . " ('ETH','ET','Etiopía'),"
            . " ('PHL','PH','Filipinas'),"
            . " ('FIN','FI','Finlandia'),"
            . " ('FJI','FJ','Fiyi'),"
            . " ('FRA','FR','Francia'),"
            . " ('GAB','GA','Gabón'),"
            . " ('GMB','GM','Gambia'),"
            . " ('GEO','GE','Georgia'),"
            . " ('GHA','GH','Ghana'),"
            . " ('GIB','GI','Gibraltar'),"
            . " ('GRD','GD','Granada'),"
            . " ('GRC','GR','Grecia'),"
            . " ('GRL','GL','Groenlandia'),"
            . " ('GLP','GP','Guadalupe'),"
            . " ('GUM','GU','Guam'),"
            . " ('GTM','GT','Guatemala'),"
            . " ('GUF','GF','Guayana Francesa'),"
            . " ('GIN','GN','Guinea'),"
            . " ('GNQ','GQ','Guinea Ecuatorial'),"
            . " ('GNB','GW','Guinea-Bissau'),"
            . " ('GUY','GY','Guyana'),"
            . " ('HTI','HT','Haití'),"
            . " ('HND','HN','Honduras'),"
            . " ('HKG','HK','Hong Kong'),"
            . " ('HUN','HU','Hungría'),"
            . " ('IND','IN','India'),"
            . " ('IDN','ID','Indonesia'),"
            . " ('IRN','IR','Irán'),"
            . " ('IRQ','IQ','Iraq'),"
            . " ('IRL','IE','Irlanda'),"
            . " ('BVT','BV','Isla Bouvet'),"
            . " ('CXR','CX','Isla de Navidad'),"
            . " ('NFK','NF','Isla Norfolk'),"
            . " ('ISL','IS','Islandia'),"
            . " ('CYM','KY','Islas Caimán'),"
            . " ('CCK','CC','Islas Cocos'),"
            . " ('COK','CK','Islas Cook'),"
            . " ('FRO','FO','Islas Feroe'),"
            . " ('SGS','GS','Islas Georgias del Sur y Sandwich del Sur'),"
            . " ('ALA','AX','Islas Gland'),"
            . " ('HMD','HM','Islas Heard y McDonald'),"
            . " ('FLK','FK','Islas Malvinas'),"
            . " ('MNP','MP','Islas Marianas del Norte'),"
            . " ('MHL','MH','Islas Marshall'),"
            . " ('PCN','PN','Islas Pitcairn'),"
            . " ('SLB','SB','Islas Salomón'),"
            . " ('TCA','TC','Islas Turcas y Caicos'),"
            . " ('UMI','UM','Islas Ultramarinas de Estados Unidos'),"
            . " ('VGB','VG','Islas Vírgenes Británicas'),"
            . " ('VIR','VI','Islas Vírgenes de los Estados Unidos'),"
            . " ('ISR','IL','Israel'),"
            . " ('ITA','IT','Italia'),"
            . " ('JAM','JM','Jamaica'),"
            . " ('JPN','JP','Japón'),"
            . " ('JOR','JO','Jordania'),"
            . " ('KAZ','KZ','Kazajstán'),"
            . " ('KEN','KE','Kenia'),"
            . " ('KGZ','KG','Kirguistán'),"
            . " ('KIR','KI','Kiribati'),"
            . " ('KWT','KW','Kuwait'),"
            . " ('LAO','LA','Laos'),"
            . " ('LSO','LS','Lesotho'),"
            . " ('LVA','LV','Letonia'),"
            . " ('LBN','LB','Líbano'),"
            . " ('LBR','LR','Liberia'),"
            . " ('LBY','LY','Libia'),"
            . " ('LIE','LI','Liechtenstein'),"
            . " ('LTU','LT','Lituania'),"
            . " ('LUX','LU','Luxemburgo'),"
            . " ('MAC','MO','Macao'),"
            . " ('MKD','MK','Macedonia'),"
            . " ('MDG','MG','Madagascar'),"
            . " ('MYS','MY','Malasia'),"
            . " ('MWI','MW','Malaui'),"
            . " ('MDV','MV','Maldivas'),"
            . " ('MLI','ML','Malí'),"
            . " ('MLT','MT','Malta'),"
            . " ('MAR','MA','Marruecos'),"
            . " ('MTQ','MQ','Martinica'),"
            . " ('MUS','MU','Mauricio'),"
            . " ('MRT','MR','Mauritania'),"
            . " ('MYT','YT','Mayotte'),"
            . " ('MEX','MX','México'),"
            . " ('FSM','FM','Micronesia'),"
            . " ('MDA','MD','Moldavia'),"
            . " ('MCO','MC','Mónaco'),"
            . " ('MNG','MN','Mongolia'),"
            . " ('MNE','ME','Montenegro'),"
            . " ('MSR','MS','Montserrat'),"
            . " ('MOZ','MZ','Mozambique'),"
            . " ('MMR','MM','Myanmar'),"
            . " ('NAM','NA','Namibia'),"
            . " ('NRU','NR','Nauru'),"
            . " ('NPL','NP','Nepal'),"
            . " ('NIC','NI','Nicaragua'),"
            . " ('NER','NE','Níger'),"
            . " ('NGA','NG','Nigeria'),"
            . " ('NIU','NU','Niue'),"
            . " ('NOR','NO','Noruega'),"
            . " ('NCL','NC','Nueva Caledonia'),"
            . " ('NZL','NZ','Nueva Zelanda'),"
            . " ('OMN','OM','Omán'),"
            . " ('NLD','NL','Países Bajos'),"
            . " ('PAK','PK','Pakistán'),"
            . " ('PLW','PW','Palaos'),"
            . " ('PSE','PS','Palestina'),"
            . " ('PAN','PA','Panamá'),"
            . " ('PNG','PG','Papúa Nueva Guinea'),"
            . " ('PRY','PY','Paraguay'),"
            . " ('PER','PE','Perú'),"
            . " ('PYF','PF','Polinesia Francesa'),"
            . " ('POL','PL','Polonia'),"
            . " ('PRT','PT','Portugal'),"
            . " ('PRI','PR','Puerto Rico'),"
            . " ('QAT','QA','Qatar'),"
            . " ('GBR','GB','Reino Unido'),"
            . " ('CAF','CF','República Centroafricana'),"
            . " ('CZE','CZ','República Checa'),"
            . " ('COD','CD','República Democrática del Congo'),"
            . " ('DOM','DO','República Dominicana'),"
            . " ('REU','RE','Reunión'),"
            . " ('RWA','RW','Ruanda'),"
            . " ('ROU','RO','Rumania'),"
            . " ('RUS','RU','Rusia'),"
            . " ('ESH','EH','Sahara Occidental'),"
            . " ('WSM','WS','Samoa'),"
            . " ('ASM','AS','Samoa Americana'),"
            . " ('KNA','KN','San Cristóbal y Nieves'),"
            . " ('SMR','SM','San Marino'),"
            . " ('SPM','PM','San Pedro y Miquelón'),"
            . " ('VCT','VC','San Vicente y las Granadinas'),"
            . " ('SHN','SH','Santa Helena'),"
            . " ('LCA','LC','Santa Lucía'),"
            . " ('STP','ST','Santo Tomé y Príncipe'),"
            . " ('SEN','SN','Senegal'),"
            . " ('SRB','RS','Serbia'),"
            . " ('SYC','SC','Seychelles'),"
            . " ('SLE','SL','Sierra Leona'),"
            . " ('SGP','SG','Singapur'),"
            . " ('SYR','SY','Siria'),"
            . " ('SOM','SO','Somalia'),"
            . " ('LKA','LK','Sri Lanka'),"
            . " ('SWZ','SZ','Suazilandia'),"
            . " ('ZAF','ZA','Sudáfrica'),"
            . " ('SDN','SD','Sudán'),"
            . " ('SWE','SE','Suecia'),"
            . " ('CHE','CH','Suiza'),"
            . " ('SUR','SR','Surinam'),"
            . " ('SJM','SJ','Svalbard y Jan Mayen'),"
            . " ('THA','TH','Tailandia'),"
            . " ('TWN','TW','Taiwán'),"
            . " ('TZA','TZ','Tanzania'),"
            . " ('TJK','TJ','Tayikistán'),"
            . " ('IOT','IO','Territorio Británico del Océano Índico'),"
            . " ('ATF','TF','Territorios Australes Franceses'),"
            . " ('TLS','TL','Timor Oriental'),"
            . " ('TGO','TG','Togo'),"
            . " ('TKL','TK','Tokelau'),"
            . " ('TON','TO','Tonga'),"
            . " ('TTO','TT','Trinidad y Tobago'),"
            . " ('TUN','TN','Túnez'),"
            . " ('TKM','TM','Turkmenistán'),"
            . " ('TUR','TR','Turquía'),"
            . " ('TUV','TV','Tuvalu'),"
            . " ('UKR','UA','Ucrania'),"
            . " ('UGA','UG','Uganda'),"
            . " ('URY','UY','Uruguay'),"
            . " ('UZB','UZ','Uzbekistán'),"
            . " ('VUT','VU','Vanuatu'),"
            . " ('VEN','VE','Venezuela'),"
            . " ('VNM','VN','Vietnam'),"
            . " ('WLF','WF','Wallis y Futuna'),"
            . " ('YEM','YE','Yemen'),"
            . " ('DJI','DJ','Yibuti'),"
            . " ('ZMB','ZM','Zambia'),"
            . " ('ZWE','ZW','Zimbabue');";
    }

    /**
     * Realizamos algunas correcciones a la base de datos y devuelve su estado.
     * Principalmente debe corregir fallos en relaciones entre tablas.
     * NOTA: Algunos controladores llaman a este método, así que las consultas deben ser lo más eficientes posibles
     * hasta que se reemplace por otra forma.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2021.09
     *
     * @return bool
     */
    public function fix_db()
    {
        $fixes = [
            'agentes' => [
                'ca_empresa_paises' => 'UPDATE `agentes` SET codpais = NULL WHERE codpais NOT IN (SELECT codpais FROM `paises`);',
            ],
            'albaranescli' => [
                'ca_albaranescli_paises' => 'UPDATE `albaranescli` SET codpais = NULL WHERE codpais NOT IN (SELECT codpais FROM `paises`);',
                'ca_albaranescli_paises2' => 'UPDATE `albaranescli` SET codpaisenv = NULL WHERE codpaisenv NOT IN (SELECT codpais FROM `paises`);',
            ],
            'almacenes' => [
                'ca_almacenes_paises' => 'UPDATE `almacenes` SET codpais = NULL WHERE codpais NOT IN (SELECT codpais FROM `paises`);',
            ],
            'candidatos' => [
                'ca_candidatos_paises' => 'UPDATE `candidatos` SET codpais = NULL WHERE codpais NOT IN (SELECT codpais FROM `paises`);',
            ],
            'crm_contactos' => [
                'ca_crm_contactos_paises' => 'UPDATE `crm_contactos` SET codpais = NULL WHERE codpais NOT IN (SELECT codpais FROM `paises`);',
            ],
            'dirclientes' => [
                'ca_dirclientes_paises' => 'UPDATE `dirclientes` SET codpais = NULL WHERE codpais NOT IN (SELECT codpais FROM `paises`);',
            ],
            'dirproveedores' => [
                'ca_dirproveedores_paises' => 'UPDATE `dirproveedores` SET codpais = NULL WHERE codpais NOT IN (SELECT codpais FROM `paises`);',
            ],
            'empresa' => [
                'ca_empresa_paises' => 'UPDATE `empresa` SET codpais = NULL WHERE codpais NOT IN (SELECT codpais FROM `paises`);',
            ],
            'facturascli' => [
                'ca_facturascli_paises' => 'UPDATE `facturascli` SET codpais = NULL WHERE codpais NOT IN (SELECT codpais FROM `paises`);',
                'ca_facturascli_paises2' => 'UPDATE `facturascli` SET codpaisenv = NULL WHERE codpaisenv NOT IN (SELECT codpais FROM `paises`);',
            ],
            'pedidoscli' => [
                'ca_pedidoscli_paises' => 'UPDATE `pedidoscli` SET codpais = NULL WHERE codpais NOT IN (SELECT codpais FROM `paises`);',
                'ca_pedidoscli_paises2' => 'UPDATE `pedidoscli` SET codpaisenv = NULL WHERE codpaisenv NOT IN (SELECT codpais FROM `paises`);',
                'ca_pedidoscli_paises3' => 'UPDATE `pedidoscli` SET ejecucion_codpais = NULL WHERE ejecucion_codpais NOT IN (SELECT codpais FROM `paises`);',
            ],
            'presupuestoscli' => [
                'ca_presupuestoscli_paises' => 'UPDATE `presupuestoscli` SET codpais = NULL WHERE codpais NOT IN (SELECT codpais FROM `paises`);',
                'ca_presupuestoscli_paises2' => 'UPDATE `presupuestoscli` SET codpaisenv = NULL WHERE codpaisenv NOT IN (SELECT codpais FROM `paises`);',
                'ca_presupuestoscli_paises3' => 'UPDATE `presupuestoscli` SET ejecucion_codpais = NULL WHERE ejecucion_codpais NOT IN (SELECT codpais FROM `paises`);',
            ],
            'servicioscli' => [
                'ca_servicioscli_paises' => 'UPDATE `servicioscli` SET codpais = NULL WHERE codpais NOT IN (SELECT codpais FROM `paises`);',
                'ca_servicioscli_paises2' => 'UPDATE `servicioscli` SET ejecucion_codpais = NULL WHERE ejecucion_codpais NOT IN (SELECT codpais FROM `paises`);',
            ],
        ];

        return $this->exec_fix_queries($fixes);
    }

    /**
     * Si una tabla de un modelo tiene problemas a nivel SQL que corregir, deben ejecutarse aquí.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2021.09
     *
     * @return bool
     */
    protected function fix_model_table_before()
    {
        $fixes = [
            'agentes' => [
                'ca_agentes_paises' => 'UPDATE `agentes` SET codpais = NULL WHERE codpais = "";',
            ],
            'albaranescli' => [
                'ca_albaranescli_paises' => 'UPDATE `albaranescli` SET codpais = NULL WHERE codpais = "";',
                'ca_albaranescli_paises2' => 'UPDATE `albaranescli` SET codpaisenv = NULL WHERE codpaisenv = "";',
            ],
            'almacenes' => [
                'ca_almacenes_paises' => 'UPDATE `almacenes` SET codpais = NULL WHERE codpais = "";',
            ],
            'candidatos' => [
                'ca_candidatos_paises' => 'UPDATE `candidatos` SET codpais = NULL WHERE codpais = "";',
            ],
            'crm_contactos' => [
                'ca_crm_contactos_paises' => 'UPDATE `crm_contactos` SET codpais = NULL WHERE codpais = "";',
            ],
            'dirclientes' => [
                'ca_dirclientes_paises' => 'UPDATE `dirclientes` SET codpais = NULL WHERE codpais = "";',
            ],
            'dirproveedores' => [
                'ca_dirproveedores_paises' => 'UPDATE `dirproveedores` SET codpais = NULL WHERE codpais = "";',
            ],
            'empresa' => [
                'ca_empresa_paises' => 'UPDATE `empresa` SET codpais = NULL WHERE codpais = "";',
            ],
            'facturascli' => [
                'ca_facturascli_paises' => 'UPDATE `facturascli` SET codpais = NULL WHERE codpais = "";',
                'ca_facturascli_paises2' => 'UPDATE `facturascli` SET codpaisenv = NULL WHERE codpaisenv = "";',
            ],
            // 'mf_asig_plugin_pais' => [
            //     '' => 'UPDATE `mf_asig_plugin_pais` SET codpais = NULL WHERE codpais = "";',
            // ],
            'pedidoscli' => [
                'ca_pedidoscli_paises' => 'UPDATE `pedidoscli` SET codpais = NULL WHERE codpais = "";',
                'ca_pedidoscli_paises2' => 'UPDATE `pedidoscli` SET codpaisenv = NULL WHERE codpaisenv = "";',
                'ca_pedidoscli_paises3' => 'UPDATE `pedidoscli` SET ejecucion_codpais = NULL WHERE ejecucion_codpais = "";',
            ],
            'presupuestoscli' => [
                'ca_presupuestoscli_paises' => 'UPDATE `presupuestoscli` SET codpais = NULL WHERE codpais = "";',
                'ca_presupuestoscli_paises2' => 'UPDATE `presupuestoscli` SET codpaisenv = NULL WHERE codpaisenv = "";',
                'ca_presupuestoscli_paises3' => 'UPDATE `presupuestoscli` SET ejecucion_codpais = NULL WHERE ejecucion_codpais = "";',
            ],
            'reciboscli' => [
                'ca_reciboscli_divisas' => 'UPDATE `reciboscli` SET codpais = NULL WHERE codpais = "";',
            ],
            'servicioscli' => [
                'ca_servicios_paises' => 'UPDATE `servicioscli` SET codpais = NULL WHERE codpais = "";',
            ],
            // 'tpv_comandas' => [
            //     '' => 'UPDATE `tpv_comandas` SET codpais = NULL WHERE codpais = "";',
            // ],
        ];

        return $this->exec_fix_queries($fixes);
    }
}
