<?php

/*
 * This file is part of MiFactura.eu
 * Copyright (C) 2021 X-Net Software Solutions S.L. <xnetsoftware@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace MiFactura\model;

use fs_model;

/**
 * Un impuesto (IVA) que puede estar asociado a artículos, líneas de albaranes,
 * facturas, etc.
 */
class impuesto extends fs_model
{
    /**
     * Nombre de la clave para almacenar/acceder a la consulta en caché
     */
    const CACHE_KEY_ALL = 'm_impuesto_all';

    /**
     * Clave primaria. varchar(10).
     *
     * @var string
     */
    public $codimpuesto;

    /**
     * Código de la subcuenta para ventas.
     *
     * @var string
     */
    public $codsubcuentarep;

    /**
     * Código de la subcuenta para compras.
     *
     * @var string
     */
    public $codsubcuentasop;

    /**
     * Descripción.
     *
     * @var string
     */
    public $descripcion;

    /**
     * Valor del % de impuesto.
     *
     * @var float|int
     */
    public $iva;

    /**
     * Valor del % del recargo de equivalencia.
     *
     * @var float|int
     */
    public $recargo;

    /**
     * impuesto constructor.
     *
     * @param false|array $data
     */
    public function __construct($data = false)
    {
        parent::__construct('impuestos');
        if ($data) {
            $this->codimpuesto = $data['codimpuesto'];
            $this->codsubcuentarep = $data['codsubcuentarep'];
            $this->codsubcuentasop = $data['codsubcuentasop'];
            $this->descripcion = $data['descripcion'];
            $this->iva = floatval($data['iva']);
            $this->recargo = floatval($data['recargo']);
        } else {
            $this->codimpuesto = null;
            $this->codsubcuentarep = null;
            $this->codsubcuentasop = null;
            $this->descripcion = null;
            $this->iva = 0.0;
            $this->recargo = 0.0;
        }
    }

    /**
     * Devuelve la url donde se pueden ver/modificar estos datos
     *
     * @return string
     */
    public function url()
    {
        if (is_null($this->codimpuesto)) {
            return 'index.php?page=contabilidad_impuestos';
        }

        return 'index.php?page=contabilidad_impuestos#' . $this->codimpuesto;
    }

    /**
     * Devuelve TRUE si el impuesto es el predeterminado del usuario
     *
     * @return bool
     */
    public function is_default()
    {
        return ($this->codimpuesto == $this->default_items->codimpuesto());
    }

    /**
     * Devuelve el impuesto con código $cod
     *
     * @param string $cod
     *
     * @return false|static
     */
    public function get($cod)
    {
        $sql = "SELECT *"
            . " FROM `" . $this->table_name() . "`"
            . " WHERE codimpuesto = " . $this->var2str($cod)
            . ";";
        $data = $this->db->select($sql);
        if ($data) {
            return new static($data[0]);
        }

        return false;
    }

    /**
     * Devuelve el primer objeto para ese valor de impuesto.
     * OJO: Esta no es una forma correcta de obtener un impuesto concreto, y puede implicar otros problemas no contemplados.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2022.01
     *
     * @param $iva
     *
     * @return false|static
     */
    public function get_by_iva($iva)
    {
        $sql = "SELECT *"
            . " FROM `" . $this->table_name() . "`"
            . " WHERE iva = " . $this->var2str(floatval($iva))
            . " ORDER BY codimpuesto ASC"
            . ";";
        $data = $this->db->select($sql);
        if ($data) {
            return new static($data[0]);
        }

        return false;
    }

    /**
     * Elimina el registro de la tabla, devuelve el resultado de la consulta.
     *
     * @return bool
     */
    public function delete()
    {
        $this->clean_cache();
        $sql = "DELETE FROM `" . $this->table_name() . "`"
            . " WHERE codimpuesto = " . $this->var2str($this->codimpuesto)
            . ";";
        return $this->db->exec($sql);
    }

    /**
     * Limpia la caché
     */
    public function clean_cache()
    {
        $this->cache->delete(self::CACHE_KEY_ALL);
    }

    /**
     * Devuelve un array con todos los impuestos
     *
     * @return static[]
     */
    public function all()
    {
        $sql = "SELECT *"
            . " FROM `" . $this->table_name() . "`"
            . " ORDER BY iva DESC;";
        return $this->all_from_cached(self::CACHE_KEY_ALL, $sql);
    }

    /**
     * Esta función es llamada al crear una tabla.
     * Permite insertar valores en la tabla.
     *
     * @return string
     */
    public function install()
    {
        $this->clean_cache();
        return "INSERT INTO `" . $this->table_name() . "` (codimpuesto,descripcion,iva,recargo) VALUES ('IVA0','IVA 0%','0','0'),('IVA21','IVA 21%','21','5.2'),"
            . "('IVA10','IVA 10%','10','1.4'),('IVA4','IVA 4%','4','0.5');";
    }

    /**
     * Realizamos algunas correcciones a la base de datos y devuelve su estado.
     * Principalmente debe corregir fallos en relaciones entre tablas.
     * NOTA: Algunos controladores llaman a este método, así que las consultas deben ser lo más eficientes posibles
     * hasta que se reemplace por otra forma.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2021.09
     *
     * @return bool
     */
    public function fix_db()
    {
        $status = true;
        // Creamos `impuestos` que existen en otras tablas pero no en agentes
        $sqls = [];

        if ($this->db->table_exists('lineasfacturascli')) {
            $sqls[] = "SELECT DISTINCT(codimpuesto), iva FROM `lineasfacturascli` WHERE codimpuesto NOT IN (SELECT codimpuesto FROM `impuestos`)";
        }
        if ($this->db->table_exists('lineasfacturasprov')) {
            $sqls[] = "SELECT DISTINCT(codimpuesto), iva FROM `lineasfacturasprov` WHERE codimpuesto NOT IN (SELECT codimpuesto FROM `impuestos`)";
        }
        if ($this->db->table_exists('lineasivafactcli')) {
            $sqls[] = "SELECT DISTINCT(codimpuesto), iva FROM `lineasivafactcli` WHERE codimpuesto NOT IN (SELECT codimpuesto FROM `impuestos`)";
        }
        if ($this->db->table_exists('lineasivafactprov')) {
            $sqls[] = "SELECT DISTINCT(codimpuesto), iva FROM `lineasivafactprov` WHERE codimpuesto NOT IN (SELECT codimpuesto FROM `impuestos`)";
        }
        if ($this->db->table_exists('lineasalbaranescli')) {
            $sqls[] = "SELECT DISTINCT(codimpuesto), iva FROM `lineasalbaranescli` WHERE codimpuesto NOT IN (SELECT codimpuesto FROM `impuestos`)";
        }
        if ($this->db->table_exists('lineasalbaranesprov')) {
            $sqls[] = "SELECT DISTINCT(codimpuesto), iva FROM `lineasalbaranesprov` WHERE codimpuesto NOT IN (SELECT codimpuesto FROM `impuestos`)";
        }
        if ($this->db->table_exists('lineaspedidoscli')) {
            $sqls[] = "SELECT DISTINCT(codimpuesto), iva FROM `lineaspedidoscli` WHERE codimpuesto NOT IN (SELECT codimpuesto FROM `impuestos`)";
        }
        if ($this->db->table_exists('lineaspedidosprov')) {
            $sqls[] = "SELECT DISTINCT(codimpuesto), iva FROM `lineaspedidosprov` WHERE codimpuesto NOT IN (SELECT codimpuesto FROM `impuestos`)";
        }
        if ($this->db->table_exists('lineaspresupuestoscli	')) {
            $sqls[] = "SELECT DISTINCT(codimpuesto), iva FROM `lineaspresupuestoscli`	 WHERE codimpuesto NOT IN (SELECT codimpuesto FROM `impuestos`)";
        }
        if ($this->db->table_exists('lineasservicioscli')) {
            $sqls[] = "SELECT DISTINCT(codimpuesto), iva FROM `lineasservicioscli` WHERE codimpuesto NOT IN (SELECT codimpuesto FROM `impuestos`)";
        }

        if (!empty($sqls)) {
            $sqls = [
                implode(' UNION ', $sqls) . ';',
            ];
        }

        foreach ($sqls as $sql) {
            $data = $this->db->select($sql);
            if ($data) {
                foreach ($data as $item) {
                    if (!empty($item['impuestos'])) {
                        if (!(new agente())->get($item['impuestos'])) {
                            $impuesto = new impuesto();
                            $impuesto->codimpuesto = $item['codimpuesto'];
                            $impuesto->descripcion = FS_IVA . $item['codimpuesto'] . '%';
                            $impuesto->iva = $item['iva'];
                            $impuesto->recargo = 0;
                            $status &= $impuesto->save();
                        }
                    }
                }
            }
        }

        $fixes = [
            'albaranescli' => [
                'ca_albaranescli_impuestos' => 'UPDATE `albaranescli` SET isp_codimpuesto = NULL WHERE isp_codimpuesto NOT IN (SELECT codimpuesto FROM `impuestos`);',
            ],
            'albaranesprov' => [
                'ca_albaranesprov_impuestos' => 'UPDATE `albaranesprov` SET isp_codimpuesto = NULL WHERE isp_codimpuesto NOT IN (SELECT codimpuesto FROM `impuestos`);',
            ],
            'articulos' => [
                'ca_articulos_impuestos' => 'UPDATE `articulos` SET codimpuesto = NULL WHERE codimpuesto NOT IN (SELECT codimpuesto FROM `impuestos`);',
            ],
            'articulosprov' => [
                'ca_articulosprov_impuestos' => 'UPDATE `articulosprov` SET codimpuesto = NULL WHERE codimpuesto NOT IN (SELECT codimpuesto FROM `impuestos`);',
            ],
            'co_subcuentas' => [
                'ca_co_subcuentas_impuestos' => 'UPDATE `co_subcuentas` SET codimpuesto = NULL WHERE codimpuesto NOT IN (SELECT codimpuesto FROM `impuestos`);',
            ],
            'empresa' => [
                'ca_empresa_impuestos' => 'UPDATE `empresa` SET codimpuesto = NULL WHERE codimpuesto NOT IN (SELECT codimpuesto FROM `impuestos`);',
                'ca_empresa_impuestos2' => 'UPDATE `empresa` SET codimpuesto_sinimpuestos = NULL WHERE codimpuesto_sinimpuestos NOT IN (SELECT codimpuesto FROM `impuestos`);',
            ],
            'facturascli' => [
                'ca_facturascli_impuestos' => 'UPDATE `facturascli` SET isp_codimpuesto = NULL WHERE isp_codimpuesto NOT IN (SELECT codimpuesto FROM `impuestos`);',
            ],
            'facturasprov' => [
                'ca_facturasprov_impuestos' => 'UPDATE `facturasprov` SET isp_codimpuesto = NULL WHERE isp_codimpuesto NOT IN (SELECT codimpuesto FROM `impuestos`);',
            ],
            'fuentes_csv' => [
                'ca_fuentres_csv_impuestos' => 'UPDATE `fuentes_csv` SET codimpuesto = NULL WHERE codimpuesto NOT IN (SELECT codimpuesto FROM `impuestos`);',
            ],
            'lineasalbaranescli' => [
                'ca_lineasalbaranescli_impuestos' => 'UPDATE `lineasalbaranescli` SET codimpuesto = NULL WHERE codimpuesto NOT IN (SELECT codimpuesto FROM `impuestos`);',
            ],
            'lineasalbaranesprov' => [
                'ca_lineasalbaranesprov_impuestos' => 'UPDATE `lineasalbaranesprov` SET codimpuesto = NULL WHERE codimpuesto NOT IN (SELECT codimpuesto FROM `impuestos`);',
            ],
            'lineasfacturascli' => [
                'ca_lineasfacturascli_impuestos' => 'UPDATE `lineasfacturascli` SET codimpuesto = NULL WHERE codimpuesto NOT IN (SELECT codimpuesto FROM `impuestos`);',
            ],
            'lineasfacturasprov' => [
                'ca_lineasfacturasprov_impuestos' => 'UPDATE `lineasfacturasprov` SET codimpuesto = NULL WHERE codimpuesto NOT IN (SELECT codimpuesto FROM `impuestos`);',
            ],
            'lineasgastos' => [
                'ca_lineasgastos_impuestos' => 'UPDATE `lineasgastos` SET codimpuesto = NULL WHERE codimpuesto NOT IN (SELECT codimpuesto FROM `impuestos`);',
            ],
            'lineasingresos' => [
                'ca_lineasingresos_impuestos' => 'UPDATE `lineasingresos` SET codimpuesto = NULL WHERE codimpuesto NOT IN (SELECT codimpuesto FROM `impuestos`);',
            ],
            'residentes_fact_prog_conceptos' => [
                'ca_residentes_fact_proc_conceptos_impuestos' => 'UPDATE `residentes_fact_prog_conceptos` SET codimpuesto = NULL WHERE codimpuesto NOT IN (SELECT codimpuesto FROM `impuestos`);',
            ],
            'lineasivafactcli' => [
                'ca_lineasivafactcli_impuestos' => 'UPDATE `lineasivafactcli` SET codimpuesto = NULL WHERE codimpuesto NOT IN (SELECT codimpuesto FROM `impuestos`);',
            ],
            'lineasivafactprov' => [
                'ca_lineasivafactprov_impuestos' => 'UPDATE `lineasivafactprov` SET codimpuesto = NULL WHERE codimpuesto NOT IN (SELECT codimpuesto FROM `impuestos`);',
            ],
            'lineaspedidoscli' => [
                'ca_lineaspedidoscli_impuestos' => 'UPDATE `lineaspedidoscli` SET codimpuesto = NULL WHERE codimpuesto NOT IN (SELECT codimpuesto FROM `impuestos`);',
            ],
            'lineaspedidosprov' => [
                'ca_lineaspedidosprov_impuestos' => 'UPDATE `lineaspedidosprov` SET codimpuesto = NULL WHERE codimpuesto NOT IN (SELECT codimpuesto FROM `impuestos`);',
            ],
            'lineaspresupuestoscli' => [
                'ca_lineaspresupuestoscli_impuestos' => 'UPDATE `lineaspresupuestoscli` SET codimpuesto = NULL WHERE codimpuesto NOT IN (SELECT codimpuesto FROM `impuestos`);',
            ],
            'lineasservicioscli' => [
                'ca_lineasservicioscli_impuestos' => 'UPDATE `lineasservicioscli` SET codimpuesto = NULL WHERE codimpuesto NOT IN (SELECT codimpuesto FROM `impuestos`);',
            ],
            'tpv_lineascomanda' => [
                'ca_tpv_lineascomanda_impuestos' => 'UPDATE `tpv_lineascomanda` SET codimpuesto = NULL WHERE codimpuesto NOT IN (SELECT codimpuesto FROM `impuestos`);',
            ],
        ];

        return $status && $this->exec_fix_queries($fixes);
    }

    /**
     * Devuelve true si ha añadido o actualizado el registro, sinó false
     *
     * @return bool
     */
    public function save()
    {
        if ($this->test()) {
            $this->clean_cache();

            if ($this->exists()) {
                $sql = "UPDATE `" . $this->table_name() . "` SET "
                    . "codsubcuentarep = " . $this->var2str($this->codsubcuentarep)
                    . ", codsubcuentasop = " . $this->var2str($this->codsubcuentasop)
                    . ", descripcion = " . $this->var2str($this->descripcion)
                    . ", iva = " . $this->var2str($this->iva)
                    . ", recargo = " . $this->var2str($this->recargo)
                    . " WHERE codimpuesto = " . $this->var2str($this->codimpuesto)
                    . ";";
            } else {
                $sql = "INSERT INTO `" . $this->table_name() . "` (codimpuesto,codsubcuentarep,codsubcuentasop,
                     descripcion,iva,recargo) VALUES ("
                    . $this->var2str($this->codimpuesto)
                    . ", " . $this->var2str($this->codsubcuentarep)
                    . ", " . $this->var2str($this->codsubcuentasop)
                    . ", " . $this->var2str($this->descripcion)
                    . ", " . $this->var2str($this->iva)
                    . ", " . $this->var2str($this->recargo)
                    . ");";
            }

            return $this->db->exec($sql);
        }

        return false;
    }

    /**
     * Comprueba los datos del empleado/agente, devuelve TRUE si son correctos
     *
     * @return bool
     */
    public function test()
    {
        $status = false;

        $this->codimpuesto = trim($this->codimpuesto);
        $this->descripcion = $this->no_html($this->descripcion);

        if (strlen($this->codimpuesto) < 1 || strlen($this->codimpuesto) > 10) {
            $this->new_error_msg("Código del impuesto no válido. Debe tener entre 1 y 10 caracteres.");
        } elseif (strlen($this->descripcion) < 1 || strlen($this->descripcion) > 50) {
            $this->new_error_msg("Descripción del impuesto no válida.");
        } else {
            $status = true;
        }

        return $status;
    }

    /**
     * Esta función devuelve TRUE si los datos del objeto se encuentran
     * en la base de datos.
     *
     * @return bool
     */
    public function exists()
    {
        if (is_null($this->codimpuesto)) {
            return false;
        }

        $sql = "SELECT *"
            . " FROM `" . $this->table_name() . "`"
            . " WHERE codimpuesto = " . $this->var2str($this->codimpuesto)
            . ";";
        return $this->db->select($sql);
    }

    /**
     * Si un modelo depende de otro, en este método deben hacerse las correspondientes instanciaciones
     * A su vez, debe invocar al fix_db de dicho modelo dependiente, para poder corregir al vuelo posibles errores que
     * se hubieran detectado con el tiempo.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2021.09
     */
    protected function check_model_dependencies()
    {
        // (new subcuenta())->fix_db();

        parent::check_model_dependencies();
    }

    /**
     * Devuelve todos los elementos como array ['key' => 'clave', 'value' => 'valor']
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2022.0815
     *
     * @return array
     */
    public function all_key_value($key = 'key', $value = 'value')
    {
        $list = [];
        foreach ($this->all() as $impuesto) {
            $list[] = [
                $key => $impuesto->codimpuesto,
                $value => $impuesto->descripcion,
                'iva' => $impuesto->iva,
                'recargo' => $impuesto->recargo,
            ];
        }
        return $list;
    }
}
