<?php

/*
 * This file is part of MiFactura.eu
 * Copyright (C) 2021 X-Net Software Solutions S.L. <xnetsoftware@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace MiFactura\model;

use fs_model;

/**
 * Una notificación.
 */
class notificacion extends \fs_extended_model
{
    /**
     * Nombre de la clave para almacenar/acceder a la consulta en caché
     */
    const CACHE_KEY_ALL = 'm_notificaciones_all';

    /**
     * Clave primaria. Integer.
     *
     * @var integer id de la notificación.
     *
     */
    public $id;

    /**
     * Código del empleado.
     *
     * @var string
     */
    public $codagente;

    /**
     * Código del empleado de destino de la notificación.
     *
     * @var string
     */
    public $codagente_destino;

    /**
     * Tipo de notificación.
     *
     * @var integer
     */
    public $idtipo_notificacion;

    /**
     * Mensaje de notificación.
     *
     * @var string
     */
    public $mensaje;

    /**
     * Enlace de notificación (si es necesario).
     *
     * @var string|null
     */
    public $enlace;

    /**
     * Fecha emisión de notificación.
     *
     * @var date
     */
    public $f_emision;

    /**
     * Fecha aviso de notificación.
     *
     * @var date
     */
    public $f_aviso;

    /**
     * Fecha lectura de notificación.
     *
     * @var date
     */
    public $f_lectura;

    /**
     * notificaciones constructor.
     *
     * @param false|array $data
     */
    public function __construct($data = false)
    {
        parent::__construct('notificaciones');
        if ($data) {
            $this->id = $data['id'];
            $this->codagente = $data['codagente'];
            $this->codagente_destino = $data['codagente_destino'];
            $this->idtipo_notificacion = $data['idtipo_notificacion'];
            $this->mensaje = $data['mensaje'];
            $this->enlace = $data['enlace'];
            $this->f_emision = $data['f_emision'];
            $this->f_aviso = $data['f_aviso'];
            $this->f_lectura = $data['f_lectura'];
        } else {
            $this->id = '';
            $this->codagente = '';
            $this->codagente_destino = '';
            $this->idtipo_notificacion = '';
            $this->mensaje = '';
            $this->enlace = '';
            $this->f_emision = '';
            $this->f_aviso = '';
            $this->f_lectura = null;
        }
    }

    /**
     * Devuelve la notificacion con idusuario = $idusuario
     *
     * @param string $codagente
     * @param string $codagente_destino
     * @param string $idtipo_notificacion
     * @param string $f_emision
     * @param string $f_aviso
     * @param string $orden
     * @param string $limite
     *
     * @return false|static[]
     */
    public function get_by_id_sel($codagente, $codagente_destino = null, $idtipo_notificacion = null, $f_emision = null, $f_aviso = null, $orden = ' f_aviso ASC', $limite = 10)
    {
        $sql = "SELECT *"
            . " FROM `" . $this->table_name() . "`"
            . " WHERE f_lectura IS NULL ";
        if (!empty($codagente)) {
            $sql .= " AND codagente = " . $this->var2str($codagente);
        }
        if (!empty($codagente_destino)) {
            $sql .= " AND codagente_destino = " . $this->var2str($codagente_destino);
        }
        if (!empty($idtipo_notificacion)) {
            $sql .= " AND idtipo_notificacion = " . $this->var2str($idtipo_notificacion);
        }
        if ($f_emision != null) {
            $sql .= " AND f_emision <= " . $this->var2str($f_emision) . " 23:59:59";
        }
        if ($f_aviso != null) {
            $sql .= " AND f_aviso <= " . $this->var2str(form_date());
        }

        if (!empty($orden)) {
            $sql .= " ORDER BY " . $orden;
        }
        return $this->all_from($sql, 0, $limite);
    }

    /**
     * Devuelve el número total de mensajes sin leer.
     *
     * @param $codagente
     * @param $con_fechaaviso  true solo reporta las que tengan fecha de aviso
     *
     * @return integer
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2022.0406
     *
     */
    public function get_total_unreaded($codagente, $con_fechaaviso = false)
    {
        $sql = 'SELECT COUNT(id) as total'
            . ' FROM ' . $this->table_name()
            . ' WHERE f_lectura IS NULL';
        if ($con_fechaaviso) {
            $sql .= ' AND f_aviso IS NOT NULL';
        }
        $sql .= ' AND codagente_destino = ' . $this->var2str($codagente);

        $sql .= " AND f_aviso <= " . $this->var2str(form_date());
        $query = $this->db->select($sql);
        return $query[0]['total'] ?: 0;
    }

    /**
     * Devuelve true si ha añadido o actualizado el registro, sinó false
     *
     * @return bool
     */
    public function save()
    {
        if ($this->test()) {
            $this->clean_cache();

            if ($this->exists()) {
                $sql = "UPDATE `" . $this->table_name() . "` SET "
                    . " codagente = " . $this->var2str($this->codagente)
                    . ", codagente_destino = " . $this->var2str($this->codagente_destino)
                    . ", idtipo_notificacion = " . $this->var2str($this->idtipo_notificacion)
                    . ", mensaje = " . $this->var2str($this->mensaje)
                    . ", enlace = " . $this->var2str($this->enlace)
                    . ", f_emision = " . $this->var2str($this->f_emision)
                    . ", f_aviso = " . $this->var2str($this->f_aviso)
                    . ", f_lectura = " . $this->var2str($this->f_lectura)
                    . " WHERE id = " . $this->var2str($this->id)
                    . ";";
            } else {
                $sql = "INSERT INTO `" . $this->table_name() . "` (codagente,codagente_destino,idtipo_notificacion,mensaje,enlace,f_emision,f_aviso,f_lectura) VALUES ("
                    . $this->var2str($this->codagente)
                    . ", " . $this->var2str($this->codagente_destino)
                    . ", " . $this->var2str($this->idtipo_notificacion)
                    . ", " . $this->var2str($this->mensaje)
                    . ", " . $this->var2str($this->enlace)
                    . ", " . $this->var2str($this->f_emision)
                    . ", " . $this->var2str($this->f_aviso)
                    . ", " . $this->var2str($this->f_lectura)
                    . ");";
            }
            return $this->db->exec($sql);
        }

        return false;
    }

    /**
     * Atualiza fecha de lectura notificacion
     *
     * @return bool
     */
    public function actualizarLectura($id)
    {
        $fecha_actual = date('Y-m-d H:s:m');
        $sql = "UPDATE `" . $this->table_name() . "` SET "
            . " f_lectura = " . "'$fecha_actual'"
            . " WHERE id = " . $this->var2str($id)
            . ";";
        return $this->db->exec($sql);
    }

    /**
     * Atualiza fecha de lectura notificacion
     *
     * @return bool
     */
    public function actualizarEstatusLeido($id, $sw)
    {
        $fecha_actual = date('Y-m-d H:s:m');

        $sql = "UPDATE `" . $this->table_name() . "` SET ";
        if ($sw != 'No') {
            $sql .= " f_lectura = " . "'$fecha_actual'";
        } else {
            $sql .= " f_lectura = null";
        }
        $sql .= " WHERE id = " . $this->var2str($id)
            . ";";
        return $this->db->exec($sql);
    }

    /**
     * Comprueba los datos de la notificación, devuelve TRUE si son correctos
     *
     * @return bool
     */
    public function test()
    {
        $status = true;

        if (empty($this->f_emision)) {
            $this->f_emision = date('Y-m-d H:i:s');
        }

        if (empty($this->f_aviso)) {
            $this->f_aviso = date('Y-m-d H:i:s');
        } else {
            $tempFechaAviso = strtotime($this->f_aviso);
            $this->f_aviso = $tempFechaAviso ? date('Y-m-d H:i:s', $tempFechaAviso) : date('Y-m-d H:i:s');
        }

        // TODO: Posiblemente este se quiera cambiar y permitir null para los automatizados por el sistema
        if (empty($this->codagente) || strlen($this->codagente) < 1 || strlen($this->codagente) > 10) {
            $status = false;
            $this->new_error_msg("Código de empleado no válido. Debe tener entre 1 y 10 caracteres.");
        }
        if (empty($this->codagente_destino) || strlen($this->codagente_destino) < 1 || strlen($this->codagente_destino) > 10) {
            $status = false;
            $this->new_error_msg("Código de empleado de destino no válido. Debe tener entre 1 y 10 caracteres.");
        }
        if (!empty($this->enlace) && strlen($this->enlace) > 150) {
            $status = false;
            $this->new_error_msg("El mensaje contiene " . strlen($this->enlace) . " caracteres, el máximo son 150.");
        }
        if (empty($this->idtipo_notificacion)) {
            $status = false;
            $this->new_error_msg("El tipo de notificación no puede ser nulo.");
        }
        // if (empty($this->f_emision)) {
        //     $status = false;
        //     $this->new_error_msg("La fecha de emisión del mensaje no puede estar vacía.");
        // }

        return $status;
    }

    /**
     * Limpia la caché
     */
    public function clean_cache()
    {
        $this->cache->delete(self::CACHE_KEY_ALL);
    }

    /**
     * Esta función devuelve TRUE si los datos del objeto se encuentran
     * en la base de datos.
     */
    public function exists()
    {
        if (empty($this->id)) {
            return false;
        }
        $sql = "SELECT *"
            . " FROM `" . $this->table_name() . "`"
            . " WHERE id = " . $this->var2str($this->id);
        return (bool)$this->db->select($sql);
    }

    /**
     * Elimina el registro de la tabla, devuelve el resultado de la consulta.
     *
     * @return bool
     */
    public function delete()
    {
        $this->clean_cache();
        $sql = "DELETE FROM `" . $this->table_name() . "`"
            . " WHERE id = " . $this->var2str($this->id)
            . ";";
        return $this->db->exec($sql);
    }

    /**
     * Devuelve un array con todos los notificaciones
     *
     * @return static[]
     */
    public function all()
    {
        $sql = "SELECT *"
            . " FROM `" . $this->table_name() . "`"
            . " ORDER BY f_aviso ASC;";
        return $this->all_from($sql, 0, 0);
    }

    /**
     * TODO: Missing documentation
     *
     * @param $length
     *
     * @return string
     * @author  Benis (rellena tus datos en File -> Settings -> Editor -> File and Code Templates)
     * @version 2022.0406
     *
     */
    public function get_mensaje_resume($length = 30)
    {
        $resultado = $this->mensaje;

        if (strlen($resultado) < $length) {
            return $resultado;
        } else {
            return substr($resultado, 0, $length - 3) . '...';
        }

        return $resultado;
    }
    /**
     * TODO: Missing documentation
     *
     * @param $length
     *
     * @return string
     * @author  Hugo Rivas 
     * @version 2022.0613
     *
     */
    public function get_tipo_notificacion()
    {
        static $tipo_noticiones;

        if (empty($tipo_noticiones)) {
            foreach ((new notificacion_tipo())->all() as $value) {
                $tipo_noticiones[$value->id] = $value;
            }
        }

        return  $tipo_noticiones[$this->idtipo_notificacion] ?: false;
    }
    /**
     * Devuelve el agente asociado al expediente.
     *
     * @return agente|false
     */
    public function get_agente()
    {
        static $agente;
        if (empty($agente[$this->codagente]) && $this->codagente) {
            $agente[$this->codagente] = (new agente())->get($this->codagente);
        }
        return $agente[$this->codagente] ?: false;
    }
    /**
     * Devuelve el agente asociado al expediente.
     *
     * @return agente|false
     */
    public function get_agente_destino()
    {
        static $agente;
        if (empty($agente[$this->codagente_destino]) && $this->codagente_destino) {
            $agente[$this->codagente_destino] = (new agente())->get($this->codagente_destino);
        }
        return $agente[$this->codagente_destino] ?: false;
    }
    /**
     * Devuelve el tiempo transcurrido de forma legible para el registro cargado.
     *
     * @return string
     * @version 2022.0421
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     */
    public function get_tiempo_transcurrido()
    {
        $timestamp = strtotime($this->f_emision);
        if (!$this->f_emision) {
            return "Sin fecha de emisión";
        }

        $strSingular = ["segundo", "minuto", "hora", "dia", "mes", "año"];
        $strPlural = ["segundos", "minutos", "horas", "dias", "meses", "años"];
        $length = ["60", "60", "24", "30", "12", "10"];

        $currentTime = time();
        $diff = 0;
        if ($currentTime >= $timestamp) {
            $diff = time() - $timestamp;
            for ($i = 0; $diff >= $length[$i] && $i < count($length) - 1; $i++) {
                $diff = $diff / $length[$i];
            }

            $diff = round($diff);
        }
        return "Hace " . $diff . " " . ($diff == 1 ? $strSingular[$i] : $strPlural[$i]);
    }

    public function primary_column()
    {
        return 'id';
    }

    /**
     * Devuelve las url para la inserción, modificación y listado de los datos.
     *
     * @param string $type
     *
     * @return string
     */
    public function url($type = 'auto')
    {
        $edit_url = 'index.php?page=edit_notificacion&code=' . $this->id;
        $list_url = 'index.php?page=admin_notificaciones';

        switch ($type) {
            case 'edit':
                return $edit_url;

            case 'list':
                return $list_url;

            default:
                return is_null($this->id) ? $list_url : $edit_url;
        }
    }

    /**
     * Si un modelo depende de otro, en este método deben hacerse las correspondientes instanciaciones
     * A su vez, debe invocar al fix_db de dicho modelo dependiente, para poder corregir al vuelo posibles errores que
     * se hubieran detectado con el tiempo.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2021.09
     */
    protected function check_model_dependencies()
    {
        (new notificacion_tipo())->fix_db();

        parent::check_model_dependencies();
    }
}
