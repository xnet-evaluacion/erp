<?php

/*
 * This file is part of MiFactura.eu
 * Copyright (C) 2021 X-Net Software Solutions S.L. <xnetsoftware@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace MiFactura\model;

use fs_model;

/**
 * Una divisa (moneda) con su símbolo y su tasa de conversión respecto al euro.
 */
class divisa extends fs_model
{
    /**
     * Nombre de la clave para almacenar/acceder a la consulta en caché
     */
    const CACHE_KEY_ALL = 'm_divisa_all';

    /**
     * Clave primaria. Varchar (3).
     *
     * @var string
     */
    public $coddivisa;

    /**
     * Descripción.
     *
     * @var null|string
     */
    public $descripcion;

    /**
     * Tasa de conversión respecto al euro.
     *
     * @var float|int
     */
    public $tasaconv;

    /**
     * Tasa de conversión respecto al euro (para compras).
     *
     * @var float|int
     */
    public $tasaconv_compra;

    /**
     * código ISO 4217 en número: http://en.wikipedia.org/wiki/ISO_4217
     *
     * @var string
     */
    public $codiso;

    /**
     * Símbolo.
     *
     * @var string
     */
    public $simbolo;

    /**
     * divisa constructor.
     *
     * @param false|array $data
     */
    public function __construct($data = false)
    {
        parent::__construct('divisas');
        if ($data) {
            $this->coddivisa = $data['coddivisa'];
            $this->descripcion = $data['descripcion'];
            $this->tasaconv = floatval($data['tasaconv']);
            $this->codiso = $data['codiso'];
            $this->simbolo = $data['simbolo'];

            if ($this->simbolo == '' && $this->coddivisa == 'EUR') {
                $this->simbolo = '€';
                $this->save();
            }

            if (is_null($data['tasaconv_compra'])) {
                $this->tasaconv_compra = floatval($data['tasaconv']);

                /// forzamos guardar para asegurarnos que siempre hay una tasa para compras
                $this->save();
            } else {
                $this->tasaconv_compra = floatval($data['tasaconv_compra']);
            }
        } else {
            $this->coddivisa = null;
            $this->descripcion = '';
            $this->tasaconv = 1.00;
            $this->tasaconv_compra = 1.00;
            $this->codiso = null;
            $this->simbolo = '?';
        }
    }

    /**
     * Devuelve true si ha añadido o actualizado el registro, sinó false
     *
     * @return bool
     */
    public function save()
    {
        if ($this->test()) {
            $this->clean_cache();

            if ($this->exists()) {
                $sql = "UPDATE `" . $this->table_name() . "` SET "
                    . "descripcion = " . $this->var2str($this->descripcion)
                    . ", tasaconv = " . $this->var2str($this->tasaconv)
                    . ", tasaconv_compra = " . $this->var2str($this->tasaconv_compra)
                    . ", codiso = " . $this->var2str($this->codiso)
                    . ", simbolo = " . $this->var2str($this->simbolo)
                    . " WHERE coddivisa = " . $this->var2str($this->coddivisa)
                    . ";";
            } else {
                $sql = "INSERT INTO `" . $this->table_name() . "` (coddivisa,descripcion,tasaconv,tasaconv_compra,codiso,simbolo)"
                    . " VALUES ("
                    . $this->var2str($this->coddivisa)
                    . ", " . $this->var2str($this->descripcion)
                    . ", " . $this->var2str($this->tasaconv)
                    . ", " . $this->var2str($this->tasaconv_compra)
                    . ", " . $this->var2str($this->codiso)
                    . ", " . $this->var2str($this->simbolo)
                    . ");";
            }

            return $this->db->exec($sql);
        }

        return false;
    }

    /**
     * Comprueba los datos de la divisa, devuelve TRUE si son correctos
     *
     * @return bool
     */
    public function test()
    {
        $status = false;
        $this->descripcion = $this->no_html($this->descripcion);
        $this->simbolo = $this->no_html($this->simbolo);

        if (!preg_match("/^[A-Z0-9]{1,3}$/i", $this->coddivisa)) {
            $this->new_error_msg("Código de divisa no válido.");
        } elseif (isset($this->codiso) && !preg_match("/^[A-Z0-9]{1,5}$/i", $this->codiso)) {
            $this->new_error_msg("Código ISO no válido.");
        } elseif ($this->tasaconv == 0) {
            $this->new_error_msg('La tasa de conversión no puede ser 0.');
        } elseif ($this->tasaconv_compra == 0) {
            $this->new_error_msg('La tasa de conversión para compras no puede ser 0.');
        } else {
            $status = true;
        }

        return $status;
    }

    /**
     * Limpia la caché
     */
    public function clean_cache()
    {
        $this->cache->delete(self::CACHE_KEY_ALL);
    }

    /**
     * Esta función devuelve TRUE si los datos del objeto se encuentran
     * en la base de datos.
     *
     * @return bool
     */
    public function exists()
    {
        if (is_null($this->coddivisa)) {
            return false;
        }

        $sql = "SELECT *"
            . " FROM `" . $this->table_name() . "`"
            . " WHERE coddivisa = " . $this->var2str($this->coddivisa)
            . ";";
        return $this->db->select($sql);
    }

    /**
     * Devuelve la url donde se pueden ver/modificar estos datos
     *
     * @return string
     */
    public function url()
    {
        return 'index.php?page=admin_divisas';
    }

    /**
     * Devuelve TRUE si esta es la divisa predeterminada de la empresa
     *
     * @return bool
     */
    public function is_default()
    {
        return ($this->coddivisa == $this->default_items->coddivisa());
    }

    /**
     * Devuelve la divisa con coddivsa = $cod
     *
     * @param string $cod
     *
     * @return false|static
     */
    public function get($cod)
    {
        $sql = "SELECT *"
            . " FROM `" . $this->table_name() . "`"
            . " WHERE coddivisa = " . $this->var2str($cod)
            . ";";
        $data = $this->db->select($sql);
        if ($data) {
            return new static($data[0]);
        }

        return false;
    }

    /**
     * Elimina el registro de la tabla, devuelve el resultado de la consulta.
     *
     * @return bool
     */
    public function delete()
    {
        $this->clean_cache();
        $sql = "DELETE FROM `" . $this->table_name() . "`"
            . " WHERE coddivisa = " . $this->var2str($this->coddivisa)
            . ";";
        return $this->db->exec($sql);
    }

    /**
     * Devuelve un array con todas las divisas.
     *
     * @return static[]
     */
    public function all()
    {
        $sql = "SELECT *"
            . " FROM `" . $this->table_name() . "` ORDER BY coddivisa ASC;";
        return $this->all_from_cached(self::CACHE_KEY_ALL, $sql);
    }

    /**
     * Esta función es llamada al crear una tabla.
     * Permite insertar valores por defecto en la tabla.
     *
     * @return string
     */
    public function install()
    {
        $this->clean_cache();
        return "INSERT INTO `" . $this->table_name() . "` (coddivisa,descripcion,tasaconv,tasaconv_compra,codiso,simbolo)"
            . " VALUES ('EUR','EUROS','1','1','978','€')"
            . ", ('ARS','PESOS (ARG)','16.684','16.684','32','AR$')"
            . ", ('CLP','PESOS (CLP)','704.0227','704.0227','152','CLP$')"
            . ", ('COP','PESOS (COP)','3140.6803','3140.6803','170','CO$')"
            . ", ('DOP','PESOS DOMINICANOS','49.7618','49.7618','214','RD$')"
            . ", ('GBP','LIBRAS ESTERLINAS','0.865','0.865','826','£')"
            . ", ('HTG','GOURDES','72.0869','72.0869','322','G')"
            . ", ('MXN','PESOS (MXN)','23.3678','23.3678','484','MX$')"
            . ", ('PAB','BALBOAS','1.128','1.128','590','B')"
            . ", ('PEN','SOLES','3.736','3.736','604','S/')"
            . ", ('PYG','GUARANÍ','6750','6750','4217','Gs')"
            . ", ('USD','DÓLARES EE.UU.','1.129','1.129','840','$')"
            . ", ('VEF','BOLÍVARES','10.6492','10.6492','937','Bs')";
    }

    /**
     * Realizamos algunas correcciones a la base de datos y devuelve su estado.
     * Principalmente debe corregir fallos en relaciones entre tablas.
     * NOTA: Algunos controladores llaman a este método, así que las consultas deben ser lo más eficientes posibles
     * hasta que se reemplace por otra forma.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2021.09
     *
     * @return bool
     */
    public function fix_db()
    {
        $fixes = [
            'albaranescli' => [
                'ca_albaranescli_divisas' => 'UPDATE `albaranescli` SET coddivisa = NULL WHERE coddivisa NOT IN (SELECT coddivisa FROM `divisas`);',
            ],
            'albaranesprov' => [
                'ca_albaranesprov_divisas' => 'UPDATE `albaranesprov` SET coddivisa = NULL WHERE coddivisa NOT IN (SELECT coddivisa FROM `divisas`);',
            ],
            'amortizaciones' => [
                'ca_amortizaciones_divisas' => 'UPDATE `amortizaciones` SET coddivisa = NULL WHERE coddivisa NOT IN (SELECT coddivisa FROM `divisas`);',
            ],
            'articulosprov' => [
                'ca_articulosprov_divisas' => 'UPDATE `articulosprov` SET coddivisa = NULL WHERE coddivisa NOT IN (SELECT coddivisa FROM `divisas`);',
            ],
            'clientes' => [
                'ca_clientes_divisas' => 'UPDATE `clientes` SET coddivisa = NULL WHERE coddivisa NOT IN (SELECT coddivisa FROM `divisas`);',
            ],
            'co_partidas' => [
                'ca_co_partidas_divisas' => 'UPDATE `co_partidas` SET coddivisa = NULL WHERE coddivisa NOT IN (SELECT coddivisa FROM `divisas`);',
            ],
            'co_subcuentas' => [
                'ca_co_subcuentas_divisas' => 'UPDATE `co_subcuentas` SET coddivisa = NULL WHERE coddivisa NOT IN (SELECT coddivisa FROM `divisas`);',
            ],
            'crm_oportunidades' => [
                'ca_crm_oportunidades_divisas' => 'UPDATE `crm_oportunidades` SET coddivisa = NULL WHERE coddivisa NOT IN (SELECT coddivisa FROM `divisas`);',
            ],
            'distribucion_faltantes' => [
                'ca_distribucion_faltantes_divisas' => 'UPDATE `distribucion_faltantes` SET coddivisa = NULL WHERE coddivisa NOT IN (SELECT coddivisa FROM `divisas`);',
            ],
            'facturascli' => [
                'ca_facturascli_divisas' => 'UPDATE `facturascli` SET coddivisa = NULL WHERE coddivisa NOT IN (SELECT coddivisa FROM `divisas`);',
            ],
            'facturasprov' => [
                'ca_facturasprov_divisas' => 'UPDATE `facturasprov` SET coddivisa = NULL WHERE coddivisa NOT IN (SELECT coddivisa FROM `divisas`);',
            ],
            // El plugins gastos_ingresos está obsoleto, no hace falta
            // 'gastos' => [
            //     'ca_gastos_divisas' => 'UPDATE `gastos` SET coddivisa = NULL WHERE coddivisa NOT IN (SELECT coddivisa FROM `divisas`);',
            // ],
            // El plugins gastos_ingresos está obsoleto, no hace falta
            // 'ingresos' => [
            //     'ca_ingresos_divisas' => 'UPDATE `ingresos` SET coddivisa = NULL WHERE coddivisa NOT IN (SELECT coddivisa FROM `divisas`);',
            // ],
            'pedidoscli' => [
                'ca_pedidoscli_divisas' => 'UPDATE `pedidoscli` SET coddivisa = NULL WHERE coddivisa NOT IN (SELECT coddivisa FROM `divisas`);',
            ],
            'pedidosprov' => [
                'ca_pedidosprov_divisas' => 'UPDATE `pedidosprov` SET coddivisa = NULL WHERE coddivisa NOT IN (SELECT coddivisa FROM `divisas`);',
            ],
            'presupuestoscli' => [
                'ca_presupuestoscli_divisas' => 'UPDATE `presupuestoscli` SET coddivisa = NULL WHERE coddivisa NOT IN (SELECT coddivisa FROM `divisas`);',
            ],
            'proveedores' => [
                'ca_proveedores_divisas' => 'UPDATE `proveedores` SET coddivisa = NULL WHERE coddivisa NOT IN (SELECT coddivisa FROM `divisas`);',
            ],
            'reciboscli' => [
                'ca_reciboscli_divisas' => 'UPDATE `reciboscli` SET coddivisa = NULL WHERE coddivisa NOT IN (SELECT coddivisa FROM `divisas`);',
            ],
            'recibosprov' => [
                'ca_recibosprov_divisas' => 'UPDATE `recibosprov` SET coddivisa = NULL WHERE coddivisa NOT IN (SELECT coddivisa FROM `divisas`);',
            ],
            'remesas_sepa' => [
                'ca_remesas_sepa_divisas' => 'UPDATE `remesas_sepa` SET coddivisa = NULL WHERE coddivisa NOT IN (SELECT coddivisa FROM `divisas`);',
            ],
            'servicioscli' => [
                'ca_servicioscli_divisas' => 'UPDATE `servicioscli` SET coddivisa = NULL WHERE coddivisa NOT IN (SELECT coddivisa FROM `divisas`);',
            ],
        ];
        return $this->exec_fix_queries($fixes);
    }
}
