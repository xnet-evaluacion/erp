<?php

/*
 * This file is part of MiFactura.eu
 * Copyright (C) 2021 X-Net Software Solutions S.L. <xnetsoftware@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace MiFactura\model;

use fs_model;

/**
 * Una serie de facturación o contabilidad, para tener distinta numeración
 * en cada serie.
 */
class serie extends fs_model
{
    /**
     * Nombre de la clave para almacenar/acceder a la consulta en caché
     */
    const CACHE_KEY_ALL = 'm_serie_all';

    /**
     * Clave primaria. Varchar (2).
     *
     * @var string
     */
    public $codserie;

    /**
     * Descripción.
     *
     * @var null|string
     */
    public $descripcion;

    /**
     * TRUE -> las facturas asociadas no encluyen IVA.
     *
     * @var bool
     */
    public $siniva;

    /**
     * % de retención IRPF de las facturas asociadas.
     *
     * @var float|int
     */
    public $irpf;

    /**
     * ejercicio para el que asignamos la numeración inicial de la serie.
     *
     * @var string
     */
    public $codejercicio;

    /**
     * numeración inicial para las facturas de esta serie.
     *
     * @var int
     */
    public $numfactura;

    /**
     * serie constructor.
     *
     * @param false|array $data
     */
    public function __construct($data = false)
    {
        parent::__construct('series');
        if ($data) {
            $this->codserie = $data['codserie'];
            $this->descripcion = $data['descripcion'];
            $this->siniva = $this->str2bool($data['siniva']);
            $this->irpf = floatval($data['irpf']);
            $this->codejercicio = $data['codejercicio'];
            $this->numfactura = max([1, intval($data['numfactura']),]);
        } else {
            $this->codserie = '';
            $this->descripcion = '';
            $this->siniva = false;
            $this->irpf = 0.00;
            $this->codejercicio = null;
            $this->numfactura = 1;
        }
    }

    /**
     * Devuelve la url donde se pueden ver/modificar estos datos
     *
     * @return string
     */
    public function url()
    {
        if (is_null($this->codserie)) {
            return 'index.php?page=contabilidad_series';
        }

        return 'index.php?page=contabilidad_series#' . $this->codserie;
    }

    /**
     * Devuelve TRUE si la serie es la predeterminada de la empresa
     *
     * @return bool
     */
    public function is_default()
    {
        return ($this->codserie == $this->default_items->codserie());
    }

    /**
     * Devuelve la serie solicitada o false si no la encuentra.
     *
     * @param string $cod
     *
     * @return false|static
     */
    public function get($cod)
    {
        $sql = "SELECT *"
            . " FROM `" . $this->table_name() . "`"
            . " WHERE codserie = " . $this->var2str($cod)
            . ";";
        $data = $this->db->select($sql);
        if ($data) {
            return new static($data[0]);
        }

        return false;
    }

    /**
     * Devuelve true si ha añadido o actualizado el registro, sinó false
     *
     * @return bool
     */
    public function save()
    {
        if ($this->test()) {
            $this->clean_cache();

            if ($this->exists()) {
                $sql = "UPDATE `" . $this->table_name() . "` SET "
                    . "descripcion = " . $this->var2str($this->descripcion)
                    . ", siniva = " . $this->var2str($this->siniva)
                    . ", irpf = " . $this->var2str($this->irpf)
                    . ", codejercicio = " . $this->var2str($this->codejercicio)
                    . ", numfactura = " . $this->var2str($this->numfactura)
                    . " WHERE codserie = " . $this->var2str($this->codserie)
                    . ";";
            } else {
                $sql = "INSERT INTO `" . $this->table_name() . "` (codserie,descripcion,siniva,irpf,codejercicio,numfactura) VALUES ("
                    . $this->var2str($this->codserie)
                    . ", " . $this->var2str($this->descripcion)
                    . ", " . $this->var2str($this->siniva)
                    . ", " . $this->var2str($this->irpf)
                    . ", " . $this->var2str($this->codejercicio)
                    . ", " . $this->var2str($this->numfactura)
                    . ");";
            }

            return $this->db->exec($sql);
        }

        return false;
    }

    /**
     * Comprueba los datos de la serie, devuelve TRUE si son correctos
     *
     * @return bool
     */
    public function test()
    {
        $status = false;

        $this->codserie = trim($this->codserie);
        $this->descripcion = $this->no_html($this->descripcion);

        if ($this->numfactura < 1) {
            $this->numfactura = 1;
        }

        if (!preg_match("/^[A-Z0-9]{1,2}$/i", $this->codserie)) {
            $this->new_error_msg("Código de serie no válido.");
        } elseif (strlen($this->descripcion) < 1 || strlen($this->descripcion) > 100) {
            $this->new_error_msg("Descripción de serie no válida.");
        } else {
            $status = true;
        }

        return $status;
    }

    /**
     * Limpia la caché
     */
    public function clean_cache()
    {
        $this->cache->delete(self::CACHE_KEY_ALL);
    }

    /**
     * Esta función devuelve TRUE si los datos del objeto se encuentran
     * en la base de datos.
     *
     * @return bool
     */
    public function exists()
    {
        if (is_null($this->codserie)) {
            return false;
        }

        $sql = "SELECT *"
            . " FROM `" . $this->table_name() . "`"
            . " WHERE codserie = " . $this->var2str($this->codserie)
            . ";";
        return $this->db->select($sql);
    }

    /**
     * Elimina el registro de la tabla, devuelve el resultado de la consulta.
     *
     * @return bool
     */
    public function delete()
    {
        $this->clean_cache();
        $sql = "DELETE FROM `" . $this->table_name() . "`"
            . " WHERE codserie = " . $this->var2str($this->codserie)
            . ";";
        return $this->db->exec($sql);
    }

    /**
     * Devuelve un array con todas las series
     *
     * @return static[]
     */
    public function all()
    {
        $sql = "SELECT *"
            . " FROM `" . $this->table_name() . "`"
            . " ORDER BY codserie ASC;";
        return $this->all_from_cached(self::CACHE_KEY_ALL, $sql);
    }

    /**
     * Esta función es llamada al crear una tabla.
     * Permite insertar valores por defecto en la tabla.
     *
     * @return string
     */
    public function install()
    {
        $this->clean_cache();
        return "INSERT INTO `" . $this->table_name() . "` (codserie,descripcion,siniva,irpf) VALUES ('A','SERIE A',FALSE,'0'),('R','RECTIFICATIVAS',FALSE,'0');";
    }

    /**
     * Realizamos algunas correcciones a la base de datos y devuelve su estado.
     * Principalmente debe corregir fallos en relaciones entre tablas.
     * NOTA: Algunos controladores llaman a este método, así que las consultas deben ser lo más eficientes posibles
     * hasta que se reemplace por otra forma.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2021.09
     *
     * @return bool
     */
    public function fix_db()
    {
        $fixes = [
            'albaranescli' => [
                'ca_albaranescli_series2' => 'UPDATE `albaranescli` SET codserie = NULL WHERE codserie NOT IN (SELECT codserie FROM `series`);',
            ],
            'albaranesprov' => [
                'ca_albaranesprov_series2' => 'UPDATE `albaranesprov` SET codserie = NULL WHERE codserie NOT IN (SELECT codserie FROM `series`);',
            ],
            'amortizaciones' => [
                'ca_amortizaciones_series' => 'UPDATE `amortizaciones` SET codserie = NULL WHERE codserie NOT IN (SELECT codserie FROM `series`);',
            ],
            'cajas_terminales' => [
                'ca_cajas_terminales_series' => 'UPDATE `cajas_terminales` SET codserie = NULL WHERE codserie NOT IN (SELECT codserie FROM `series`);',
            ],
            'clientes' => [
                'ca_clientes_series' => 'UPDATE `clientes` SET codserie = NULL WHERE codserie NOT IN (SELECT codserie FROM `series`);',
            ],
            'co_partidas' => [
                'ca_co_partidas_series' => 'UPDATE `co_partidas` SET codserie = NULL WHERE codserie NOT IN (SELECT codserie FROM `series`);',
            ],
            'empresa' => [
                'ca_empresa_series' => 'UPDATE `empresa` SET codserie = NULL WHERE codserie NOT IN (SELECT codserie FROM `series`);',
            ],
            'expedientes' => [
                'ca_expedientes_series' => 'UPDATE `expedientes` SET codserie = NULL WHERE codserie NOT IN (SELECT codserie FROM `series`);',
            ],
            'facturascli' => [
                'ca_facturascli_series2' => 'UPDATE `facturascli` SET codserie = NULL WHERE codserie NOT IN (SELECT codserie FROM `series`);',
            ],
            'facturasprov' => [
                'ca_facturasprov_series2' => 'UPDATE `facturasprov` SET codserie = NULL WHERE codserie NOT IN (SELECT codserie FROM `series`);',
            ],
            // El plugins gastos_ingresos está obsoleto, no hace falta
            // 'gastos' => [
            //     'ca_gastos_series' => 'UPDATE `gastos` SET codserie = NULL WHERE codserie NOT IN (SELECT codserie FROM `series`);',
            // ],
            // El plugins gastos_ingresos está obsoleto, no hace falta
            // 'ingresos' => [
            //     'ca_ingresos_series' => 'UPDATE `ingresos` SET codserie = NULL WHERE codserie NOT IN (SELECT codserie FROM `series`);',
            // ],
            'lineaslibroiva' => [
                'ca_lineaslibroiva_series' => 'UPDATE `lineaslibroiva` SET codserie = NULL WHERE codserie NOT IN (SELECT codserie FROM `series`);',
            ],
            'pedidoscli' => [
                'ca_pedidoscli_series' => 'UPDATE `pedidoscli` SET codserie = NULL WHERE codserie NOT IN (SELECT codserie FROM `series`);',
            ],
            'pedidosprov' => [
                'ca_pedidosprov_series' => 'UPDATE `pedidosprov` SET codserie = NULL WHERE codserie NOT IN (SELECT codserie FROM `series`);',
            ],
            'presupuestoscli' => [
                'ca_presupuestoscli_series' => 'UPDATE `presupuestoscli` SET codserie = NULL WHERE codserie NOT IN (SELECT codserie FROM `series`);',
            ],
            'proveedores' => [
                'ca_proveedores_series' => 'UPDATE `proveedores` SET codserie = NULL WHERE codserie NOT IN (SELECT codserie FROM `series`);',
            ],
            'reciboscli' => [
                'ca_reciboscli_series' => 'UPDATE `reciboscli` SET codserie = NULL WHERE codserie NOT IN (SELECT codserie FROM `series`);',
            ],
            'recibosprov' => [
                'ca_recibosprov_series' => 'UPDATE `recibosprov` SET codserie = NULL WHERE codserie NOT IN (SELECT codserie FROM `series`);',
            ],
            'secuenciasejercicios' => [
                'ca_secuenciasejercicios_series' => 'UPDATE `secuenciasejercicios` SET codserie = NULL WHERE codserie NOT IN (SELECT codserie FROM `series`);',
            ],
            'servicioscli' => [
                'ca_servicioscli_series' => 'UPDATE `servicioscli` SET codserie = NULL WHERE codserie NOT IN (SELECT codserie FROM `series`);',
            ],
        ];

        return $this->exec_fix_queries($fixes);
    }

    /**
     * Si un modelo depende de otro, en este método deben hacerse las correspondientes instanciaciones
     * A su vez, debe invocar al fix_db de dicho modelo dependiente, para poder corregir al vuelo posibles errores que
     * se hubieran detectado con el tiempo.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2021.09
     */
    protected function check_model_dependencies()
    {
        (new ejercicio())->fix_db();
        // (new cuenta())->fix_db();

        parent::check_model_dependencies();
    }
}
