<?php

/*
 * This file is part of MiFactura.eu
 * Copyright (C) 2021 X-Net Software Solutions S.L. <xnetsoftware@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace MiFactura\model;

use fs_model;

/**
 * Ejercicio contable. Es el periodo en el que se agrupan asientos, facturas, albaranes...
 */
class ejercicio extends fs_model
{
    /**
     * Nombre de la clave para almacenar/acceder a la consulta en caché
     */
    const CACHE_KEY_ALL = 'm_ejercicio_all';
    const CACHE_KEY_ALL_ABIERTOS = 'm_ejercicio_all_abiertos';

    /**
     * Clave primaria. Varchar(4).
     *
     * @var string
     */
    public $codejercicio;

    /**
     * Nombre.
     *
     * @var null|string
     */
    public $nombre;

    /**
     * Fecha de inicio.
     *
     * @var false|string
     */
    public $fechainicio;

    /**
     * Fecha de fin.
     *
     * @var false|string
     */
    public $fechafin;

    /**
     * Estado del ejercicio: ABIERTO|CERRADO
     *
     * @var string
     */
    public $estado;

    /**
     * ID del asiento de cierre del ejercicio.
     *
     * @var null|int
     */
    public $idasientocierre;

    /**
     * ID del asiento de pérdidas y ganancias.
     *
     * @var null|int
     */
    public $idasientopyg;

    /**
     * ID del asiento de apertura.
     *
     * @var null|int
     */
    public $idasientoapertura;

    /**
     * Identifica el plan contable utilizado. Esto solamente es necesario
     * para dar compatibilidad con Eneboo. En MiFactura.eu no se utiliza.
     *
     * @var string
     */
    public $plancontable;

    /**
     * Longitud de caracteres de las subcuentas asignadas. Esto solamente es necesario
     * para dar compatibilidad con Eneboo. En MiFactura.eu no se utiliza.
     *
     * @var int
     */
    public $longsubcuenta;

    /**
     * ejercicio constructor.
     *
     * @param false|array $data
     */
    public function __construct($data = false)
    {
        parent::__construct('ejercicios');
        if ($data) {
            $this->codejercicio = $data['codejercicio'];
            $this->nombre = $data['nombre'];
            $this->fechainicio = date('Y-m-d', strtotime($data['fechainicio']));
            $this->fechafin = date('Y-m-d', strtotime($data['fechafin']));
            $this->estado = $data['estado'];
            $this->idasientocierre = $this->intval($data['idasientocierre']);
            $this->idasientopyg = $this->intval($data['idasientopyg']);
            $this->idasientoapertura = $this->intval($data['idasientoapertura']);
            $this->plancontable = $data['plancontable'];
            $this->longsubcuenta = $this->intval($data['longsubcuenta']);
        } else {
            $this->codejercicio = null;
            $this->nombre = '';
            $this->fechainicio = date('01-01-Y');
            $this->fechafin = date('31-12-Y');
            $this->estado = 'ABIERTO';
            $this->idasientocierre = null;
            $this->idasientopyg = null;
            $this->idasientoapertura = null;
            $this->plancontable = '08';
            $this->longsubcuenta = 10;
        }
    }

    /**
     * Devuelve el año asociado al ejercicio.
     *
     * @return false|string
     */
    public function year()
    {
        return date('Y', strtotime($this->fechainicio));
    }

    /**
     * Devuelve la url donde se pueden ver/modificar estos datos
     *
     * @return string
     */
    public function url()
    {
        if (is_null($this->codejercicio)) {
            return 'index.php?page=contabilidad_ejercicios';
        }

        return 'index.php?page=contabilidad_ejercicio&cod=' . $this->codejercicio;
    }

    /**
     * Devuelve TRUE si este es el ejercicio predeterminado de la empresa
     *
     * @return bool
     */
    public function is_default()
    {
        return ($this->codejercicio == $this->default_items->codejercicio());
    }

    /**
     * Devuelve la fecha más próxima a $fecha que esté dentro del intervalo de este ejercicio
     *
     * @param staring $fecha
     * @param bool    $show_error
     *
     * @return string
     */
    public function get_best_fecha($fecha, $show_error = false)
    {
        $fecha2 = strtotime($fecha);

        if ($fecha2 >= strtotime($this->fechainicio) and $fecha2 <= strtotime($this->fechafin)) {
            return $fecha;
        }

        if ($fecha2 > strtotime($this->fechainicio)) {
            if ($show_error) {
                $this->new_error_msg('La fecha seleccionada está fuera del rango del ejercicio. Se ha seleccionado una mejor.');
            }
            return $this->fechafin;
        }

        if ($show_error) {
            $this->new_error_msg('La fecha seleccionada está fuera del rango del ejercicio. Se ha seleccionado una mejor.');
        }
        return $this->fechainicio;
    }

    /**
     * Devuelve el ejercicio con codejercicio = $cod
     *
     * @param string $cod
     *
     * @return false|static
     */
    public function get($cod)
    {
        $sql = "SELECT *"
            . " FROM `" . $this->table_name() . "`"
            . " WHERE codejercicio = " . $this->var2str($cod)
            . ";";
        $data = $this->db->select($sql);
        if ($data) {
            return new static($data[0]);
        }

        return false;
    }

    /**
     * Devuelve el ejercicio para la fecha indicada.
     * Si no existe, lo crea.
     *
     * @param string $fecha
     * @param bool   $solo_abierto
     * @param bool   $crear
     *
     * @return false|static
     */
    public function get_by_fecha($fecha, $solo_abierto = true, $crear = true)
    {
        $sql = "SELECT *"
            . " FROM `" . $this->table_name() . "`"
            . " WHERE fechainicio <= " . $this->var2str(date('Y-m-d', strtotime($fecha)))
            . " AND fechafin >= " . $this->var2str(date('Y-m-d', strtotime($fecha)))
            . ";";

        $data = $this->db->select($sql);
        if ($data) {
            $eje = new static($data[0]);
            if ($eje->abierto() || !$solo_abierto) {
                return $eje;
            }

            return false;
        } elseif ($crear) {
            $eje = new static();
            $eje->codejercicio = $eje->get_new_codigo(Date('Y', strtotime($fecha)));
            $eje->nombre = date('Y', strtotime($fecha));
            $eje->fechainicio = date('1-1-Y', strtotime($fecha));
            $eje->fechafin = date('31-12-Y', strtotime($fecha));

            if (strtotime($fecha) < 1) {
                $this->new_error_msg("Fecha no válida: " . $fecha);
            } elseif ($eje->save()) {
                return $eje;
            }
        }

        return false;
    }

    /**
     * Devuelve cierto o falso en función de si el ejercicio está o no en estado ABIERTO.
     *
     * @return bool
     */
    public function abierto()
    {
        return ($this->estado == 'ABIERTO');
    }

    /**
     * Devuelve un nuevo código para un ejercicio
     *
     * @param string $cod
     *
     * @return string
     */
    public function get_new_codigo($cod = '0001')
    {
        $sql = "SELECT *"
            . " FROM `" . $this->table_name() . "`"
            . " WHERE codejercicio = " . $this->var2str($cod)
            . ";";
        if (!$this->db->select($sql)) {
            return $cod;
        }

        $sql = "SELECT MAX(" . $this->db->sql_to_int('codejercicio') . ") as cod"
            . " FROM `" . $this->table_name() . "`"
            . ";";
        $data = $this->db->select($sql);
        if (!empty($data)) {
            return sprintf('%04s', (1 + intval($data[0]['cod'])));
        }

        return '0001';
    }

    /**
     * Devuelve true si ha añadido o actualizado el registro, sinó false
     *
     * @return bool
     */
    public function save()
    {
        if ($this->test()) {
            $this->clean_cache();

            if ($this->exists()) {
                $sql = "UPDATE `" . $this->table_name() . "` SET "
                    . "nombre = " . $this->var2str($this->nombre)
                    . ", fechainicio = " . $this->var2str($this->fechainicio)
                    . ", fechafin = " . $this->var2str($this->fechafin)
                    . ", estado = " . $this->var2str($this->estado)
                    . ", longsubcuenta = " . $this->var2str($this->longsubcuenta)
                    . ", plancontable = " . $this->var2str($this->plancontable)
                    . ", idasientoapertura = " . $this->var2str($this->idasientoapertura)
                    . ", idasientopyg = " . $this->var2str($this->idasientopyg)
                    . ", idasientocierre = " . $this->var2str($this->idasientocierre)
                    . " WHERE codejercicio = " . $this->var2str($this->codejercicio)
                    . ";";
            } else {
                $sql = "INSERT INTO `" . $this->table_name() . "` (codejercicio,nombre,fechainicio,fechafin,
               estado,longsubcuenta,plancontable,idasientoapertura,idasientopyg,idasientocierre)
               VALUES ("
                    . $this->var2str($this->codejercicio)
                    . ", " . $this->var2str($this->nombre)
                    . ", " . $this->var2str($this->fechainicio)
                    . ", " . $this->var2str($this->fechafin)
                    . ", " . $this->var2str($this->estado)
                    . ", " . $this->var2str($this->longsubcuenta)
                    . ", " . $this->var2str($this->plancontable)
                    . ", " . $this->var2str($this->idasientoapertura)
                    . ", " . $this->var2str($this->idasientopyg)
                    . ", " . $this->var2str($this->idasientocierre)
                    . ");";
            }

            return $this->db->exec($sql);
        }

        return false;
    }

    /**
     * Comprueba los datos del ejercicio, devuelve TRUE si son correctos
     *
     * @return bool
     */
    public function test()
    {
        $status = false;

        $this->codejercicio = trim($this->codejercicio);
        $this->nombre = $this->no_html($this->nombre);

        if (!preg_match("/^[A-Z0-9_]{1,4}$/i", $this->codejercicio)) {
            $this->new_error_msg("Código de ejercicio no válido.");
        } elseif (strlen($this->nombre) < 1 || strlen($this->nombre) > 100) {
            $this->new_error_msg("Nombre del ejercicio no válido.");
        } elseif (strtotime($this->fechainicio) > strtotime($this->fechafin)) {
            $this->new_error_msg("La fecha de inicio (" . $this->fechainicio . ") es posterior a la fecha fin (" . $this->fechafin . ").");
        } elseif (strtotime($this->fechainicio) < 1) {
            $this->new_error_msg("Fecha no válida.");
        } else {
            $status = true;
        }

        return $status;
    }

    /**
     * Limpia la caché
     */
    public function clean_cache()
    {
        $this->cache->delete(self::CACHE_KEY_ALL);
        $this->cache->delete(self::CACHE_KEY_ALL_ABIERTOS);
    }

    /**
     * Esta función devuelve TRUE si los datos del objeto se encuentran
     * en la base de datos.
     *
     * @return bool
     */
    public function exists()
    {
        if (is_null($this->codejercicio)) {
            return false;
        }

        $sql = "SELECT *"
            . " FROM `" . $this->table_name() . "`"
            . " WHERE codejercicio = " . $this->var2str($this->codejercicio)
            . ";";
        return $this->db->select($sql);
    }

    /**
     * Comprueba más datos del ejercicio, devuelve TRUE si es correcto
     *
     * @return bool
     */
    public function full_test()
    {
        $status = true;

        /// comprobamos la suma de las subcuentas
        if ($this->db->table_exists('co_subcuentas')) {
            $sql = "SELECT SUM(ROUND(debe,2)) as debe, SUM(ROUND(haber,2)) as haber FROM `co_subcuentas` "
                . " WHERE codejercicio = " . $this->var2str($this->codejercicio)
                . ";";

            $data = $this->db->select($sql);
            if ($data) {
                $debe = floatval($data[0]['debe']);
                $haber = floatval($data[0]['haber']);

                if (!$this->floatcmp($debe, $haber, FS_NF0, true)) {
                    $this->new_error_msg('El ejercicio ' . $this->nombre . ' está descuadrado a nivel de subcuentas.' . ' Debe: ' . $debe . ' | Haber: ' . $haber);
                    $status = false;
                }
            }
        }

        /// comprobamos la suma de las partidas de los asientos
        if ($this->db->table_exists('co_partidas')) {
            $sql = "SELECT SUM(ROUND(debe,2)) as debe, SUM(ROUND(haber,2)) as haber FROM `co_partidas` "
                . " WHERE idasiento IN (SELECT idasiento FROM `co_asientos`"
                . " WHERE codejercicio = " . $this->var2str($this->codejercicio) . ");";

            $data = $this->db->select($sql);
            if ($data) {
                $debe = floatval($data[0]['debe']);
                $haber = floatval($data[0]['haber']);

                if (!$this->floatcmp($debe, $haber, FS_NF0, true)) {
                    $this->new_error_msg('El ejercicio ' . $this->nombre . ' está descuadrado a nivel de asientos.' . ' Debe: ' . $debe . ' | Haber: ' . $haber);
                    $status = false;
                } elseif (!$status) {
                    $this->new_error_msg('Pero <b>NO</b> está descuadrado a nivel de asientos.' . ' Debe: ' . $debe . ' | Haber: ' . $haber);
                }
            }
        }

        return $status;
    }

    /**
     * Elimina el registro de la tabla, devuelve el resultado de la consulta.
     *
     * @return bool
     */
    public function delete()
    {
        $this->clean_cache();
        $sql = "DELETE FROM `" . $this->table_name() . "`"
            . " WHERE codejercicio = " . $this->var2str($this->codejercicio)
            . ";";
        return $this->db->exec($sql);
    }

    /**
     * Devuelve un array con todos los ejercicios
     *
     * @return static[]
     */
    public function all()
    {
        $sql = "SELECT *"
            . " FROM `" . $this->table_name() . "`"
            . " ORDER BY fechainicio DESC;";
        return $this->all_from_cached(self::CACHE_KEY_ALL, $sql);
    }

    /**
     * Devuelve un array con todos los ejercicio abiertos
     *
     * @return static[]
     */
    public function all_abiertos()
    {
        $sql = "SELECT *"
            . " FROM `" . $this->table_name() . "`"
            . " WHERE estado = 'ABIERTO' ORDER BY codejercicio DESC;";
        return $this->all_from_cached(self::CACHE_KEY_ALL_ABIERTOS, $sql);
    }

    /**
     * Esta función es llamada al crear una tabla.
     * Permite insertar valores en la tabla.
     *
     * @return string
     */
    public function install()
    {
        $this->clean_cache();

        return "INSERT INTO `" . $this->table_name() . "` (codejercicio,nombre,fechainicio,fechafin,
         estado,longsubcuenta,plancontable,idasientoapertura,idasientopyg,idasientocierre)
         VALUES ('" . date('Y') . "','" . date('Y') . "'," . $this->var2str(Date('01-01-Y')) . "," . $this->var2str(Date('31-12-Y')) . ",'ABIERTO',10,'08',NULL,NULL,NULL);";
    }

    /**
     * Realizamos algunas correcciones a la base de datos y devuelve su estado.
     * Principalmente debe corregir fallos en relaciones entre tablas.
     * NOTA: Algunos controladores llaman a este método, así que las consultas deben ser lo más eficientes posibles
     * hasta que se reemplace por otra forma.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2021.09
     *
     * @return bool
     */
    public function fix_db()
    {
        $fixes = [
            'albaranescli' => [
                'ca_albaranescli_ejercicios2' => "UPDATE `albaranescli` SET codejercicio = NULL WHERE codejercicio NOT IN (SELECT codejercicio FROM `ejercicios`);",
            ],
            'albaranesprov' => [
                'ca_albaranesprov_series2' => "UPDATE `albaranesprov` SET codejercicio = NULL WHERE codejercicio NOT IN (SELECT codejercicio FROM `ejercicios`);",
            ],
            'co_asientos' => [
                'ca_co_asientos_ejercicios2' => "UPDATE `co_asientos` SET codejercicio = NULL WHERE codejercicio NOT IN (SELECT codejercicio FROM `ejercicios`);",
            ],
            'co_cuentas' => [
                'ca_co_cuentas_ejercicios' => "UPDATE `co_cuentas` SET codejercicio = NULL WHERE codejercicio NOT IN (SELECT codejercicio FROM `ejercicios`);",
            ],
            'co_epigrafes' => [
                'ca_co_epigrafes_ejercicios' => "UPDATE `co_epigrafes` SET codejercicio = NULL WHERE codejercicio NOT IN (SELECT codejercicio FROM `ejercicios`);",
            ],
            'co_gruposepigrafes' => [
                'ca_co_gruposepigrafes_ejercicios' => "UPDATE `co_gruposepigrafes` SET codejercicio = NULL WHERE codejercicio NOT IN (SELECT codejercicio FROM `ejercicios`);",
            ],
            'co_regiva' => [
                'ca_co_regiva_ejercicios' => "UPDATE `co_regiva` SET codejercicio = NULL WHERE codejercicio NOT IN (SELECT codejercicio FROM `ejercicios`);",
            ],
            'co_secuencias' => [
                'ca_co_secuencias_ejercicios' => "UPDATE `co_secuencias` SET codejercicio = NULL WHERE codejercicio NOT IN (SELECT codejercicio FROM `ejercicios`);",
            ],
            'co_subcuentas' => [
                'ca_co_subcuentas_ejercicios' => "UPDATE `co_subcuentas` SET codejercicio = NULL WHERE codejercicio NOT IN (SELECT codejercicio FROM `ejercicios`);",
            ],
            'co_subcuentascli' => [
                'ca_co_subcuentascli_ejercicios' => "UPDATE `co_subcuentascli` SET codejercicio = NULL WHERE codejercicio NOT IN (SELECT codejercicio FROM `ejercicios`);",
            ],
            'co_subcuentasprov' => [
                'ca_co_subcuentasprov_ejercicios' => "UPDATE `co_subcuentasprov` SET codejercicio = NULL WHERE codejercicio NOT IN (SELECT codejercicio FROM `ejercicios`);",
            ],
            'empresa' => [
                'ca_empresa_ejercicios' => "UPDATE `empresa` SET codejercicio = NULL WHERE codejercicio NOT IN (SELECT codejercicio FROM `ejercicios`);",
            ],
            'expedientes' => [
                'ca_expedientes_ejercicios' => "UPDATE `expedientes` SET codejercicio = NULL WHERE codejercicio NOT IN (SELECT codejercicio FROM `ejercicios`);",
            ],
            'facturascli' => [
                'ca_facturascli_ejercicios2' => "UPDATE `facturascli` SET codejercicio = NULL WHERE codejercicio NOT IN (SELECT codejercicio FROM `ejercicios`);",
            ],
            'facturasprov' => [
                'ca_facturasprov_ejercicios2' => "UPDATE `facturasprov` SET codejercicio = NULL WHERE codejercicio NOT IN (SELECT codejercicio FROM `ejercicios`);",
            ],
            // El plugins gastos_ingresos está obsoleto, no hace falta
            // 'gastos' => [
            //     "UPDATE `gastos` SET codejercicio = NULL WHERE codejercicio NOT IN (SELECT codejercicio FROM `ejercicios`);",
            // ],
            // El plugins gastos_ingresos está obsoleto, no hace falta
            // 'ingresos' => [
            //     'ca_ingresos_ejercicios' => "UPDATE `ingresos` SET codejercicio = NULL WHERE codejercicio NOT IN (SELECT codejercicio FROM `ejercicios`);",
            // ],
            'lineaslibroiva' => [
                'ca_lineaslibroiva_ejercicios' => "UPDATE `lineaslibroiva` SET codejercicio = NULL WHERE codejercicio NOT IN (SELECT codejercicio FROM `ejercicios`);",
            ],
            'pedidoscli' => [
                'ca_pedidoscli_ejercicios' => "UPDATE `pedidoscli` SET codejercicio = NULL WHERE codejercicio NOT IN (SELECT codejercicio FROM `ejercicios`);",
            ],
            'pedidosprov' => [
                'ca_pedidosprov_ejercicios' => "UPDATE `pedidosprov` SET codejercicio = NULL WHERE codejercicio NOT IN (SELECT codejercicio FROM `ejercicios`);",
            ],
            'presupuestoscli' => [
                'ca_presupuestoscli_ejercicios' => "UPDATE `presupuestoscli` SET codejercicio = NULL WHERE codejercicio NOT IN (SELECT codejercicio FROM `ejercicios`);",
            ],
            'secuenciasejercicios' => [
                'ca_secuenciasejercicios_ejercicios' => "UPDATE `secuenciasejercicios` SET codejercicio = NULL WHERE codejercicio NOT IN (SELECT codejercicio FROM `ejercicios`);",
            ],
            'series' => [
                'ca_series_ejercicios' => "UPDATE `series` SET codejercicio = NULL WHERE codejercicio NOT IN (SELECT codejercicio FROM `ejercicios`);",
            ],
            'servicioscli' => [
                'ca_servicioscli_ejercicios' => "UPDATE `servicioscli` SET codejercicio = NULL WHERE codejercicio NOT IN (SELECT codejercicio FROM `ejercicios`);",
            ],
        ];

        return $this->exec_fix_queries($fixes);
    }

    /**
     * Si un modelo depende de otro, en este método deben hacerse las correspondientes instanciaciones
     * A su vez, debe invocar al fix_db de dicho modelo dependiente, para poder corregir al vuelo posibles errores que
     * se hubieran detectado con el tiempo.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2021.09
     */
    protected function check_model_dependencies()
    {
        // (new asiento())->fix_db();

        parent::check_model_dependencies();
    }
}
