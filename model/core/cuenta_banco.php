<?php

/*
 * This file is part of MiFactura.eu
 * Copyright (C) 2021 X-Net Software Solutions S.L. <xnetsoftware@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace MiFactura\model;

use fs_model;

/**
 * Una cuenta bancaria de la propia empresa.
 */
class cuenta_banco extends fs_model
{
    /**
     * Clave primaria. Varchar (6).
     *
     * @var string
     */
    public $codcuenta;

    /**
     * Descripción.
     *
     * @var null|string
     */
    public $descripcion;

    /**
     *Número IBAN.
     *
     * @var null|string
     */
    public $iban;

    /**
     * Número SWIFT.
     *
     * @var null|string
     */
    public $swift;

    /**
     * Código de la subcuenta de contabilidad
     *
     * @var string
     */
    public $codsubcuenta;

    /**
     * cuenta_banco constructor.
     *
     * @param false|array $data
     */
    public function __construct($data = false)
    {
        parent::__construct('cuentasbanco');
        if ($data) {
            $this->codcuenta = $data['codcuenta'];
            $this->descripcion = $data['descripcion'];
            $this->iban = $data['iban'];
            $this->swift = $data['swift'];
            $this->codsubcuenta = $data['codsubcuenta'];
        } else {
            $this->codcuenta = null;
            $this->descripcion = null;
            $this->iban = null;
            $this->swift = null;
            $this->codsubcuenta = null;
        }
    }

    /**
     * Devuelve el IBAN con o sin espacios.
     *
     * @param bool $espacios
     *
     * @return string
     */
    public function iban($espacios = false)
    {
        if ($espacios) {
            $txt = '';
            $iban = str_replace(' ', '', $this->iban);
            for ($i = 0; $i < strlen($iban); $i += 4) {
                $txt .= substr($iban, $i, 4) . ' ';
            }
            return $txt;
        }

        return str_replace(' ', '', $this->iban);
    }

    /**
     * Devuelve la url donde se pueden ver/modificar estos datos
     *
     * @return string
     */
    public function url()
    {
        return 'index.php?page=admin_empresa#cuentasb';
    }

    /**
     * Devuelve la cuenta bancaria con codcuenta = $cod
     *
     * @param string $cod
     *
     * @return false|static
     */
    public function get($cod)
    {
        $sql = "SELECT *"
            . " FROM `" . $this->table_name() . "`"
            . " WHERE codcuenta = " . $this->var2str($cod)
            . ";";
        $data = $this->db->select($sql);
        if ($data) {
            return new static($data[0]);
        }

        return false;
    }

    /**
     * Guarda los datos en la base de datos.
     *
     * @return bool
     */
    public function save()
    {
        $this->descripcion = $this->no_html($this->descripcion);

        if ($this->exists()) {
            $sql = "UPDATE `" . $this->table_name() . "` SET "
                . "descripcion = " . $this->var2str($this->descripcion)
                . ", iban = " . $this->var2str($this->iban)
                . ", swift = " . $this->var2str($this->swift)
                . ", codsubcuenta = " . $this->var2str($this->codsubcuenta)
                . " WHERE codcuenta = " . $this->var2str($this->codcuenta)
                . ";";
        } else {
            $this->codcuenta = $this->get_new_codigo();
            $sql = "INSERT INTO `" . $this->table_name() . "` (codcuenta,descripcion,iban,swift,codsubcuenta) VALUES ("
                . $this->var2str($this->codcuenta)
                . ", " . $this->var2str($this->descripcion)
                . ", " . $this->var2str($this->iban)
                . ", " . $this->var2str($this->swift)
                . ", " . $this->var2str($this->codsubcuenta)
                . ");";
        }

        return $this->db->exec($sql);
    }

    /**
     * Esta función devuelve TRUE si los datos del objeto se encuentran
     * en la base de datos.
     *
     * @return bool
     */
    public function exists()
    {
        if (is_null($this->codcuenta)) {
            return false;
        }

        $sql = "SELECT *"
            . " FROM `" . $this->table_name() . "`"
            . " WHERE codcuenta = " . $this->var2str($this->codcuenta)
            . ";";
        return $this->db->select($sql);
    }

    /**
     * Elimina el registro de la tabla, devuelve el resultado de la consulta.
     *
     * @return bool
     */
    public function delete()
    {
        $sql = "DELETE FROM `" . $this->table_name() . "`"
            . " WHERE codcuenta = " . $this->var2str($this->codcuenta)
            . ";";
        return $this->db->exec($sql);
    }

    /**
     * Devuelve todos los registros de la tabla
     *
     * @return static[]
     */
    public function all()
    {
        return $this->all_from_empresa();
    }

    /**
     * Devuelve un array con todas las cuentas bancarias de la empresa
     *
     * @return static[]
     */
    public function all_from_empresa()
    {
        $sql = "SELECT *"
            . " FROM `" . $this->table_name() . "`"
            . " ORDER BY descripcion ASC;";
        return $this->all_from($sql, 0, 0);
    }

    /**
     * Calcula el IBAN a partir de la cuenta bancaria del cliente CCC
     *
     * @param string $ccc
     *
     * @return string
     */
    public function calcular_iban($ccc)
    {
        $codpais = substr($this->default_items->codpais(), 0, 2);

        $pesos = [
            'A' => '10',
            'B' => '11',
            'C' => '12',
            'D' => '13',
            'E' => '14',
            'F' => '15',
            'G' => '16',
            'H' => '17',
            'I' => '18',
            'J' => '19',
            'K' => '20',
            'L' => '21',
            'M' => '22',
            'N' => '23',
            'O' => '24',
            'P' => '25',
            'Q' => '26',
            'R' => '27',
            'S' => '28',
            'T' => '29',
            'U' => '30',
            'V' => '31',
            'W' => '32',
            'X' => '33',
            'Y' => '34',
            'Z' => '35',
        ];

        $dividendo = $ccc . $pesos[substr($codpais, 0, 1)] . $pesos[substr($codpais, 1, 1)] . '00';
        $digitoControl = 98 - bcmod($dividendo, '97');

        if (strlen($digitoControl) == 1) {
            $digitoControl = '0' . $digitoControl;
        }

        return $codpais . $digitoControl . $ccc;
    }

    /**
     * Esta función es llamada al crear una tabla.
     * Permite insertar valores en la tabla.
     *
     * @return string
     */
    public function install()
    {
        return '';
    }

    /**
     * Realizamos algunas correcciones a la base de datos y devuelve su estado.
     * Principalmente debe corregir fallos en relaciones entre tablas.
     * NOTA: Algunos controladores llaman a este método, así que las consultas deben ser lo más eficientes posibles
     * hasta que se reemplace por otra forma.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2021.09
     *
     * @return bool
     */
    public function fix_db()
    {
        $fixes = [
            'formaspago' => [
                'ca_formaspago_cuentasbanco' => 'UPDATE `formaspago` SET codcuenta = NULL WHERE codcuenta NOT IN (SELECT codcuenta FROM `cuentasbanco`);',
            ],
        ];

        return $this->exec_fix_queries($fixes);
    }

    /**
     * Si un modelo depende de otro, en este método deben hacerse las correspondientes instanciaciones
     * A su vez, debe invocar al fix_db de dicho modelo dependiente, para poder corregir al vuelo posibles errores que
     * se hubieran detectado con el tiempo.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2021.09
     */
    protected function check_model_dependencies()
    {
        // (new subcuenta())->fix_db();

        parent::check_model_dependencies();
    }

    /**
     * Devuelve un nuevo código para una cuenta bancaria
     *
     * @return int
     */
    private function get_new_codigo()
    {
        $sql = "SELECT MAX(" . $this->db->sql_to_int('codcuenta') . ") as cod FROM `" . $this->table_name() . "`;";
        $data = $this->db->select($sql);
        if ($data) {
            return (string) (1 + (int) $data[0]['cod']);
        }

        return '1';
    }
}
