<?php

/*
 * This file is part of MiFactura.eu
 * Copyright (C) 2021 X-Net Software Solutions S.L. <xnetsoftware@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace MiFactura\model;

use fs_model;

/**
 * El almacén donde están físicamente los artículos.
 */
class almacen extends fs_model
{
    /**
     * Nombre de la clave para almacenar/acceder a la consulta en caché
     */
    const CACHE_KEY_ALL = 'm_almacen_all';

    /**
     * Clave primaria. Varchar (4).
     *
     * @var string
     */
    public $codalmacen;

    /**
     * Nombre.
     *
     * @var null|string
     */
    public $nombre;

    /**
     * Código de país.
     *
     * @var null|string
     */
    public $codpais;

    /**
     * Provincia.
     *
     * @var null|string
     */
    public $provincia;

    /**
     * Población.
     *
     * @var null|string
     */
    public $poblacion;

    /**
     * Código postal.
     *
     * @var null|string
     */
    public $codpostal;

    /**
     * Dirección.
     *
     * @var null|string
     */
    public $direccion;

    /**
     * Contacto.
     *
     * @var null|string
     */
    public $contacto;

    /**
     * Número de fax.
     *
     * @var null|string
     */
    public $fax;

    /**
     * Teléfono.
     *
     * @var null|string
     */
    public $telefono;

    /**
     * Todavía sin uso.
     *
     * @var string
     */
    public $observaciones;

    /**
     * almacen constructor.
     *
     * @param false|array $data
     */
    public function __construct($data = false)
    {
        parent::__construct('almacenes');
        if ($data) {
            $this->codalmacen = $data['codalmacen'];
            $this->nombre = $data['nombre'];
            $this->codpais = $data['codpais'];
            $this->provincia = $data['provincia'];
            $this->poblacion = $data['poblacion'];
            $this->codpostal = $data['codpostal'];
            $this->direccion = $data['direccion'];
            $this->contacto = $data['contacto'];
            $this->fax = $data['fax'];
            $this->telefono = $data['telefono'];
            $this->observaciones = $data['observaciones'];
        } else {
            $this->codalmacen = null;
            $this->nombre = '';
            $this->codpais = null;
            $this->provincia = null;
            $this->poblacion = null;
            $this->codpostal = '';
            $this->direccion = '';
            $this->contacto = '';
            $this->fax = '';
            $this->telefono = '';
            $this->observaciones = '';
        }
    }

    /**
     * Devuelve la url donde se pueden ver/modificar estos datos
     *
     * @return string
     */
    public function url()
    {
        if (is_null($this->codalmacen)) {
            return 'index.php?page=admin_almacenes';
        }

        return 'index.php?page=admin_almacenes#' . $this->codalmacen;
    }

    /**
     * Devuelve TRUE si este es almacén predeterminado de la empresa.
     *
     * @return bool
     */
    public function is_default()
    {
        return ($this->codalmacen == $this->default_items->codalmacen());
    }

    /**
     * Devuelve el almacén con codalmacen = $cod
     *
     * @param string $cod
     *
     * @return false|static
     */
    public function get($cod)
    {
        $sql = "SELECT *"
            . " FROM `" . $this->table_name() . "`"
            . " WHERE codalmacen = " . $this->var2str($cod)
            . ";";
        $data = $this->db->select($sql);
        if ($data) {
            return new static($data[0]);
        }

        return false;
    }

    /**
     * Devuelve true si ha añadido o actualizado el registro, sinó false
     *
     * @return bool
     */
    public function save()
    {
        if ($this->test()) {
            $this->clean_cache();
            if ($this->exists()) {
                $sql = "UPDATE `" . $this->table_name() . "` SET "
                    . "nombre = " . $this->var2str($this->nombre)
                    . ", codpais = " . $this->var2str($this->codpais)
                    . ", provincia = " . $this->var2str($this->provincia)
                    . ", poblacion = " . $this->var2str($this->poblacion)
                    . ", direccion = " . $this->var2str($this->direccion)
                    . ", codpostal = " . $this->var2str($this->codpostal)
                    . ", telefono = " . $this->var2str($this->telefono)
                    . ", fax = " . $this->var2str($this->fax)
                    . ", contacto = " . $this->var2str($this->contacto)
                    . " WHERE codalmacen = " . $this->var2str($this->codalmacen)
                    . ";";
            } else {
                $sql = "INSERT INTO `" . $this->table_name() . "` (codalmacen,nombre,codpais,provincia,
               poblacion,direccion,codpostal,telefono,fax,contacto) VALUES ("
                    . $this->var2str($this->codalmacen)
                    . ", " . $this->var2str($this->nombre)
                    . ", " . $this->var2str($this->codpais)
                    . ", " . $this->var2str($this->provincia)
                    . ", " . $this->var2str($this->poblacion)
                    . ", " . $this->var2str($this->direccion)
                    . ", " . $this->var2str($this->codpostal)
                    . ", " . $this->var2str($this->telefono)
                    . ", " . $this->var2str($this->fax)
                    . ", " . $this->var2str($this->contacto)
                    . ");";
            }
            return $this->db->exec($sql);
        }

        return false;
    }

    /**
     * Comprueba los datos del almacén, devuelve TRUE si son correctos
     *
     * @return bool
     */
    public function test()
    {
        $status = false;

        $this->codalmacen = trim($this->codalmacen);
        $this->nombre = $this->no_html($this->nombre);
        $this->provincia = $this->no_html($this->provincia);
        $this->poblacion = $this->no_html($this->poblacion);
        $this->direccion = $this->no_html($this->direccion);
        $this->codpostal = $this->no_html($this->codpostal);
        $this->telefono = $this->no_html($this->telefono);
        $this->fax = $this->no_html($this->fax);
        $this->contacto = $this->no_html($this->contacto);

        if (!preg_match("/^[A-Z0-9]{1,4}$/i", $this->codalmacen)) {
            $this->new_error_msg("Código de almacén no válido.");
        } elseif (strlen($this->nombre) < 1 || strlen($this->nombre) > 100) {
            $this->new_error_msg("Nombre de almacén no válido.");
        } else {
            $status = true;
        }

        return $status;
    }

    /**
     * Limpia la caché
     */
    public function clean_cache()
    {
        $this->cache->delete(self::CACHE_KEY_ALL);
    }

    /**
     * Esta función devuelve TRUE si los datos del objeto se encuentran
     * en la base de datos.
     *
     * @return bool
     */
    public function exists()
    {
        if (is_null($this->codalmacen)) {
            return false;
        }

        $sql = "SELECT *"
            . " FROM `" . $this->table_name() . "`"
            . " WHERE codalmacen = " . $this->var2str($this->codalmacen)
            . ";";
        return $this->db->select($sql);
    }

    /**
     * Elimina el registro de la tabla, devuelve el resultado de la consulta.
     *
     * @return bool
     */
    public function delete()
    {
        $this->clean_cache();
        $sql = "DELETE FROM `" . $this->table_name() . "`"
            . " WHERE codalmacen = " . $this->var2str($this->codalmacen)
            . ";";
        return $this->db->exec($sql);
    }

    /**
     * Devuelve un array con todos los almacenes
     *
     * @return static[]
     */
    public function all()
    {
        $sql = "SELECT *"
            . " FROM `" . $this->table_name() . "`"
            . " ORDER BY codalmacen ASC;";
        return $this->all_from_cached(self::CACHE_KEY_ALL, $sql);
    }

    /**
     * Esta función es llamada al crear una tabla.
     * Permite insertar valores por defecto en la tabla.
     *
     * @return string
     */
    public function install()
    {
        $this->clean_cache();
        return "INSERT INTO `" . $this->table_name() . "` (codalmacen,nombre,poblacion,"
            . "direccion,codpostal,telefono,fax,contacto) VALUES ('ALG','ALMACEN GENERAL','','','','','','');";
    }

    /**
     * Realizamos algunas correcciones a la base de datos y devuelve su estado.
     * Principalmente debe corregir fallos en relaciones entre tablas.
     * NOTA: Algunos controladores llaman a este método, así que las consultas deben ser lo más eficientes posibles
     * hasta que se reemplace por otra forma.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2021.09
     *
     * @return bool
     */
    public function fix_db()
    {
        $fixes = [
            'albaranescli' => [
                'ca_albaranescli_almacenes' => 'UPDATE `albaranescli` SET codalmacen = NULL WHERE codalmacen NOT IN (SELECT codalmacen FROM `almacenes`);',
            ],
            'albaranesprov' => [
                'ca_albaranesprov_almacenes' => 'UPDATE `albaranesprov` SET codalmacen = NULL WHERE codalmacen NOT IN (SELECT codalmacen FROM `almacenes`);',
            ],
            'cajas_general' => [
                'ca_cajas_general_almacenes' => 'UPDATE `cajas_general` SET codalmacen = NULL WHERE codalmacen NOT IN (SELECT codalmacen FROM `almacenes`);',
            ],
            'empresa' => [
                'ca_empresa_almacenes' => 'UPDATE `empresa` SET codalmacen = NULL WHERE codalmacen NOT IN (SELECT codalmacen FROM `almacenes`);',
            ],
            'facturascli' => [
                'ca_facturascli_almacenes' => 'UPDATE `facturascli` SET codalmacen = NULL WHERE codalmacen NOT IN (SELECT codalmacen FROM `almacenes`);',
            ],
            'facturasprov' => [
                'ca_facturasprov_almacenes' => 'UPDATE `facturasprov` SET codalmacen = NULL WHERE codalmacen NOT IN (SELECT codalmacen FROM `almacenes`);',
            ],
            'lineasregstocks' => [
                'ca_lineasregstocks_almacenes' => 'UPDATE `lineasregstocks` SET codalmacendest = NULL WHERE codalmacendest NOT IN (SELECT codalmacen FROM `almacenes`);',
            ],
            'pedidoscli' => [
                'ca_pedidoscli_almacenes' => 'UPDATE `pedidoscli` SET codalmacen = NULL WHERE codalmacen NOT IN (SELECT codalmacen FROM `almacenes`);',
            ],
            'pedidosprov' => [
                'ca_pedidosprov_almacenes' => 'UPDATE `pedidosprov` SET codalmacen = NULL WHERE codalmacen NOT IN (SELECT codalmacen FROM `almacenes`);',
            ],
            'presupuestoscli' => [
                'ca_presupuestoscli_almacenes' => 'UPDATE `presupuestoscli` SET codalmacen = NULL WHERE codalmacen NOT IN (SELECT codalmacen FROM `almacenes`);',
            ],
            'servicioscli' => [
                'ca_servicioscli_almacenes' => 'UPDATE `servicioscli` SET codalmacen = NULL WHERE codalmacen NOT IN (SELECT codalmacen FROM `almacenes`);',
            ],
            'stocks' => [
                'ca_stocks_almacenes3' => 'UPDATE `stocks` SET codalmacen = NULL WHERE codalmacen NOT IN (SELECT codalmacen FROM `almacenes`);',
            ],
            'transstock' => [
                'ca_transstock_almacenes' => 'UPDATE `transstock` SET codalmadestino = NULL WHERE codalmadestino NOT IN (SELECT codalmacen FROM `almacenes`);',
                'ca_transstock_almacenes2' => 'UPDATE `transstock` SET codalmaorigen = NULL WHERE codalmaorigen NOT IN (SELECT codalmacen FROM `almacenes`);',
            ],
        ];

        return $this->exec_fix_queries($fixes);
    }

    /**
     * Si un modelo depende de otro, en este método deben hacerse las correspondientes instanciaciones
     * A su vez, debe invocar al fix_db de dicho modelo dependiente, para poder corregir al vuelo posibles errores que
     * se hubieran detectado con el tiempo.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2021.09
     */
    protected function check_model_dependencies()
    {
        (new pais())->fix_db();

        parent::check_model_dependencies();
    }
}
