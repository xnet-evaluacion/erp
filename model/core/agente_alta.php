<?php

/*
 * This file is part of MiFactura.eu
 * Copyright (C) 2021 X-Net Software Solutions S.L. <xnetsoftware@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace MiFactura\model;

use fs_model;

/**
 * Class agente_alta
 */
class agente_alta extends fs_model
{
    /**
     * Clave primaria
     *
     * @var null|int
     */
    public $id;

    /**
     * Código del agente
     *
     * @var int
     */
    public $codagente;

    /**
     * Fecha de alta del empleado
     *
     * @var string
     */
    public $f_alta;

    /**
     * Fecha de baja del empleado
     *
     * @var null|string
     */
    public $f_baja;

    /**
     * agente_alta constructor.
     *
     * @param false|array $data
     */
    public function __construct($data = false)
    {
        parent::__construct('agentes_altas');
        if ($data) {
            $this->id = $data['id'];
            $this->codagente = $data['codagente'];
            $this->f_alta = $data['f_alta'];
            $this->f_baja = $data['f_baja'];
        } else {
            $this->id = null;
            $this->codagente = null;
            $this->f_alta = null;
            $this->f_baja = null;
        }
    }

    /**
     * Devuelve true si ha añadido o actualizado el registro, sinó false
     *
     * @return bool
     */
    public function save()
    {
        if ($this->test()) {
            if ($this->exists()) {
                $sql = "UPDATE `" . $this->table_name() . "` SET "
                    . "codagente = " . $this->var2str($this->codagente)
                    . ", f_alta = " . $this->var2str($this->f_alta)
                    . ", f_baja = " . $this->var2str($this->f_baja)
                    . " WHERE id = " . $this->var2str($this->id)
                    . ";";
            } else {
                $sql = "INSERT INTO `" . $this->table_name() . "` (codagente,f_alta,f_baja) VALUES ("
                    . $this->var2str($this->codagente)
                    . ", " . $this->var2str($this->f_alta)
                    . ", " . $this->var2str($this->f_baja)
                    . ");";
            }
            if ($this->db->exec($sql)) {
                if (null === $this->id) {
                    $this->id = $this->db->lastval();
                }
                return true;
            }
        }

        return false;
    }

    /**
     * Comprueba los datos del modelo, devuelve TRUE si son correctos
     *
     * @return bool
     */
    public function test()
    {
        if ($this->f_baja != null && strtotime($this->f_alta) > strtotime($this->f_baja)) {
            $this->new_message('La fecha de alta debe de ser anterior a la fecha de baja');
            return false;
        }
        return true;
    }

    /**
     * Esta función devuelve TRUE si los datos del objeto se encuentran
     * en la base de datos.
     *
     * @return bool
     */
    public function exists()
    {
        if ($this->id == null) {
            return false;
        }
        $sql = "SELECT *"
            . " FROM `" . $this->table_name() . "`"
            . " WHERE id = " . $this->var2str($this->id);
        return (bool) $this->db->select($sql);
    }

    /**
     * Devuelve la relación de altas asociadas al agente indicado.
     *
     * @param string $codagente
     *
     * @return static[]
     */
    public function get_alta_agente($agente)
    {
        $sql = "SELECT *"
            . " FROM `" . $this->table_name() . "`"
            . " WHERE codagente = '$codagente' ORDER BY f_alta DESC;";
        return $this->all_from($sql, 0, 0);
    }

    /**
     * Retorna los datos del contrato según por el id
     *
     * @param int $id
     *
     * @return false|static
     */
    public function get($id)
    {
        $sql = "SELECT *"
            . " FROM `" . $this->table_name() . "`"
            . " WHERE id='$id';";
        $data = $this->db->select($sql);
        if (isset($data[0])) {
            return new static($data[0]);
        }
        return false;
    }

    /**
     * Devuelve todos los registros asociados al código de agente indicado.
     *
     * @param string $codagente
     *
     * @return static[]
     */
    public function all($codagente)
    {
        $sql = "SELECT *"
            . " FROM `" . $this->table_name() . "`"
            . " WHERE codagente='$codagente' ORDER BY f_alta DESC;";
        return $this->all_from($sql, 0, 0);
    }

    /**
     * Elimina el registro de la tabla, devuelve el resultado de la consulta.
     *
     * @return bool
     */
    public function delete()
    {
        $sql = "DELETE FROM `" . $this->table_name() . "`"
            . " WHERE id = " . $this->var2str($this->id);
        return $this->db->exec($sql);
    }

    /**
     * Si un modelo depende de otro, en este método deben hacerse las correspondientes instanciaciones
     * A su vez, debe invocar al fix_db de dicho modelo dependiente, para poder corregir al vuelo posibles errores que
     * se hubieran detectado con el tiempo.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2021.09
     */
    protected function check_model_dependencies()
    {
        (new agente())->fix_db();

        parent::check_model_dependencies();
    }

    /**
     * Si una tabla de un modelo tiene problemas a nivel SQL que corregir, deben ejecutarse aquí.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2021.09
     *
     * @return bool
     */
    protected function fix_model_table_before()
    {
        $fixes = [
            $this->table_name() => [
                'fs_agente_alta' => 'UPDATE `' . $this->table_name() . '` SET codagente = NULL WHERE codagente = "";',
            ],
        ];

        return $this->exec_fix_queries($fixes);
    }

}
