<?php

/*
 * This file is part of MiFactura.eu
 * Copyright (C) 2021 X-Net Software Solutions S.L. <xnetsoftware@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace MiFactura\model;

use fs_model;

/**
 * Define un tipo de notificación
 */
class notificacion_tipo extends \fs_extended_model
{
    /**
     * Nombre de la clave para almacenar/acceder a la consulta en caché
     */
    const CACHE_KEY_ALL = 'm_notificaciones_tipo_all';

    /**
     * Clave primaria. Integer.
     *
     * @var integer id del tipo de notificación.
     *
     */
    public $id;

    /**
     * TODO: Missing documentation
     *
     * @var string
     */
    public $nombre;

    /**
     * TODO: Missing documentation
     *
     * @var string
     */
    public $estilo;

    /**
     * TODO: Missing documentation
     *
     * @var string
     */
    public $icono;

    /**
     * notificaciones constructor.
     *
     * @param false|array $data
     */
    public function __construct($data = false)
    {
        parent::__construct('notificaciones_tipo');
        if ($data) {
            $this->id = $data['id'];
            $this->nombre = $data['nombre'];
            $this->estilo = $data['estilo'];
            $this->icono = $data['icono'];
        } else {
            $this->id = null;
            $this->nombre = null;
            $this->estilo = null;
            $this->icono = null;
        }
    }

    /**
     * Devuelve true si ha añadido o actualizado el registro, sinó false
     *
     * @return bool
     */
    public function save()
    {
        if ($this->test()) {
            $this->clean_cache();
            if ($this->exists()) {
                $sql = "UPDATE `" . $this->table_name() . "` SET "
                    . " nombre = " . $this->var2str($this->nombre)
                    . ", estilo = " . $this->var2str($this->estilo)
                    . ", icono = " . $this->var2str($this->icono)
                    . " WHERE id = " . $this->var2str($this->id)
                    . ";";
            } else {
                $sql = "INSERT INTO `" . $this->table_name() . "` (nombre,estilo,icono) VALUES ("
                    . $this->var2str($this->nombre)
                    . ", " . $this->var2str($this->estilo)
                    . ", " . $this->var2str($this->icono)
                    . ");";
            }

            return $this->db->exec($sql);
        }

        return false;
    }

    /**
     * Comprueba los datos de la notificación, devuelve TRUE si son correctos
     *
     * @return bool
     */
    public function test()
    {
        $status = true;

        if (empty($this->nombre) || strlen($this->nombre) < 1 || strlen($this->nombre) > 150) {
            $status = false;
            $this->new_error_msg("Nombre no válido. Debe tener entre 1 y 10 caracteres.");
        }

        return $status;
    }

    /**
     * Limpia la caché
     */
    public function clean_cache()
    {
        $this->cache->delete(self::CACHE_KEY_ALL);
    }

    /**
     * Esta función es llamada al crear una tabla.
     * Permite insertar valores en la tabla.
     *
     * @return string
     */
    public function install()
    {
        $tipos = [
            [
                'nombre' => 'Aviso',
                'estilo' => 'text-warning',
                'icono' => 'fa-solid fa-triangle-exclamation fa-fw',
            ],
            [
                'nombre' => 'Informativo',
                'estilo' => 'text-success',
                'icono' => 'fa-solid fa-circle-info fa-fw',
            ],
            [
                'nombre' => 'Alerta',
                'estilo' => 'text-danger',
                'icono' => 'fa-solid fa-circle-exclamation fa-fw',
            ],
        ];
        $this->clean_cache();
        $query = "INSERT INTO `" . $this->table_name() . "`(nombre,estilo,icono) VALUES ";
        $sep = '';
        foreach ($tipos as $tipo) {
            $query .= $sep . "('" . $tipo['nombre'] . "','" . $tipo['estilo'] . "','" . $tipo['icono'] . "')";
            $sep = ',';
        }
        $query .= ";";

        return $query;
    }

    /**
     * Esta función devuelve TRUE si los datos del objeto se encuentran
     * en la base de datos.
     */
    public function exists()
    {
        if ($this->id == null) {
            return false;
        }
        $sql = "SELECT *"
            . " FROM `" . $this->table_name() . "`"
            . " WHERE id = " . $this->var2str($this->id);
        return (bool) $this->db->select($sql);
    }

    /**
     * Elimina el registro de la tabla, devuelve el resultado de la consulta.
     *
     * @return bool
     */
    public function delete()
    {
        $this->clean_cache();
        $sql = "DELETE FROM `" . $this->table_name() . "`"
            . " WHERE id = " . $this->var2str($this->id)
            . ";";
        return $this->db->exec($sql);
    }

    /**
     * Devuelve un array con todos los paises
     *
     * @return static[]
     */
    public function all()
    {
        $sql = "SELECT *"
            . " FROM `" . $this->table_name() . "`"
            . " ORDER BY id DESC;";
        return $this->all_from($sql, 0, 0);
    }

    public function fix_db()
    {
        return true;
    }

    public function primary_column()
    {
        return 'id';
    }
}