<?php

/*
 * This file is part of MiFactura.eu
 * Copyright (C) 2021 X-Net Software Solutions S.L. <xnetsoftware@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace MiFactura\model;

use fs_model;

/**
 * El agente/empleado es el que se asocia a un albarán, factura o caja.
 * Cada usuario puede estar asociado a un agente, y un agente puede
 * estar asociado a varios usuarios o a ninguno.
 */
class agente extends fs_model
{
    /**
     * Nombre de la clave para almacenar/acceder a la consulta en caché
     */
    const CACHE_KEY_ALL = 'm_agente_all';

    /**
     * Nombre de la clave para almacenar/acceder a la consulta en caché
     */
    const CACHE_KEY_ALL_FULL = 'm_agente_all_full';

    /**
     * Nombre de la clave para almacenar/acceder a la consulta individual en caché
     */
    const CACHE_KEY_SINGLE = 'm_agente_{id}';

    /**
     * Clave primaria. Varchar (10).
     *
     * @var string
     */
    public $codagente;

    /**
     * Identificador fiscal (CIF/NIF).
     *
     * @var string
     */
    public $dnicif;

    /**
     * Nombre.
     *
     * @var null|string
     */
    public $nombre;

    /**
     * Apellidos.
     *
     * @var null|string
     */
    public $apellidos;

    /**
     * Email.
     *
     * @var null|string
     */
    public $email;

    /**
     * Teléfono.
     *
     * @var null|string
     */
    public $telefono;

    /**
     * Teléfono.
     *
     * @var null|string
     */
    public $telefono2;

    /**
     * Código postal.
     *
     * @var null|string
     */
    public $codpostal;

    /**
     * Provincia.
     *
     * @var null|string
     */
    public $provincia;

    /**
     * Ciudad.
     *
     * @var null|string
     */
    public $ciudad;

    /**
     * Dirección.
     *
     * @var null|string
     */
    public $direccion;

    /**
     * Pais
     *
     * @var null|string
     */
    public $codpais;

    /**
     * Número de la seguridad social.
     *
     * @var string
     */
    public $seg_social;

    /**
     * cargo en la empresa.
     *
     * @var string
     */
    public $cargo;

    /**
     * Cuenta bancaria
     *
     * @var string
     */
    public $banco;

    /**
     * Fecha de nacimiento.
     *
     * @var string
     */
    public $f_nacimiento;

    /**
     * Fecha de alta en la empresa.
     *
     * @var string
     */
    public $f_alta;

    /**
     * Fecha de baja en la empresa.
     *
     * @var string
     */
    public $f_baja;

    /**
     * Porcentaje de comisión del agente. Se utiliza en presupuestos, pedidos, albaranes y facturas.
     *
     * @var float|int
     */
    public $porcomision;

    /**
     * Observaciones.
     *
     * @var null|string
     */
    public $observaciones;

    /**
     * Coste para la empresa por cada hora del empleado, utilizado en expedientes para calcular el gasto para la
     * empresa que supone el empleado
     *
     * @var float|int
     */
    public $coste_hora;

    /**
     * PIN del agente.
     *
     * @var null|string
     */
    public $pin;

    /**
     * RFID del agente.
     *
     * @var null|string
     */
    public $rfid;

    /**
     * Número de hijos
     *
     * @var null|int
     */
    public $hijos;

    /**
     * Texto con la nacionalidad del agente
     * TODO: Mejor sería seleccionar un país de la tabla de países
     *
     * @var null|string
     */
    public $nacionalidad;

    /**
     * Indicar si tiene o no carné de conducir
     * TODO: Mejor seleccionar un carné de una tabla de tipos, o mejor aún poder seleccionar varios.
     *
     * @var bool
     */
    public $carnet_conducir;

    /**
     * Indica si las horas de trabajo de este empleado deben computarse como gasto.
     * Esta función está implementada a través de servicios y expedientes.
     *
     * @var bool
     */
    public $imputar_gasto;

    /**
     * Color hexadecimal usado para el calendario de servicios del agente.
     *
     * @var string
     */
    public $color_calendario;

    /**
     * Imagen del agente.
     *
     * @var string
     */
    public $imagen;

    /**
     * agente constructor.
     *
     * @param false|array $data
     */
    public function __construct($data = false)
    {
        parent::__construct('agentes');
        if ($data) {
            $this->codagente = $data['codagente'];
            $this->nombre = $data['nombre'];
            $this->apellidos = $data['apellidos'];
            $this->dnicif = $data['dnicif'];
            $this->email = $data['email'];
            $this->telefono = $data['telefono'];
            $this->telefono2 = $data['telefono2'];
            $this->codpostal = $data['codpostal'];
            $this->provincia = $data['provincia'];
            $this->ciudad = $data['ciudad'];
            $this->direccion = $data['direccion'];
            $this->codpais = $data['codpais'];
            $this->porcomision = floatval($data['porcomision']);
            $this->seg_social = $data['seg_social'];
            $this->banco = $data['banco'];
            $this->cargo = $data['cargo'];
            $this->numdocs = $data['numdocs'];
            $this->hijos = $data['hijos'];
            $this->nacionalidad = $data['nacionalidad'];
            $this->carnet_conducir = $data['carnet_conducir'];
            $this->color_calendario = $data['color_calendario'] ?? '#000000';
            $this->color_calendario = empty($this->color_calendario) ? '#000000' : $this->color_calendario;
            if (isset($data['coste_hora'])) {
                $this->coste_hora = (float) $data['coste_hora'];
            } else {
                $this->coste_hora = 0.00;
            }
            $this->observaciones = $data['observaciones'];

            $this->f_alta = null;
            if ($data['f_alta'] != '') {
                $this->f_alta = date('Y-m-d', strtotime($data['f_alta']));
            }

            $this->f_baja = null;
            if ($data['f_baja'] != '') {
                $this->f_baja = date('Y-m-d', strtotime($data['f_baja']));
            }

            $this->f_nacimiento = null;
            if ($data['f_nacimiento'] != '') {
                $this->f_nacimiento = date('Y-m-d', strtotime($data['f_nacimiento']));
            }
            $this->pin = $data['pin'];
            $this->rfid = $data['rfid'];
            $this->imputar_gasto = $data['imputar_gasto'] ?? false;
            $this->imagen = null;
        } else {
            $this->codagente = null;
            $this->nombre = '';
            $this->apellidos = '';
            $this->dnicif = '';
            $this->email = null;
            $this->telefono = null;
            $this->telefono2 = null;
            $this->codpostal = null;
            $this->provincia = null;
            $this->ciudad = null;
            $this->direccion = null;
            $this->codpais = null;
            $this->porcomision = 0.00;
            $this->seg_social = null;
            $this->banco = null;
            $this->cargo = null;
            $this->f_alta = date('Y-m-d');
            $this->f_baja = null;
            $this->f_nacimiento = date('Y-m-d');
            $this->coste_hora = 0.00;
            $this->observaciones = null;
            $this->numdocs = 0;
            $this->pin = null;
            $this->rfid = null;
            $this->hijos = 0;
            $this->nacionalidad = null;
            $this->carnet_conducir = false;
            $this->color_calendario = '#000000';
            $this->imputar_gasto = false;
            $this->imagen = null;
        }
    }

    /**
     * Retorna el nombre del agente.
     *
     * @return string
     */
    public function get_description()
    {
        return $this->get_fullname();
    }

    /**
     * Devuelve nombre + apellidos del agente.
     *
     * @return string
     */
    public function get_fullname()
    {
        return $this->nombre . " " . $this->apellidos;
    }

    /**
     * Devuelve la url donde se pueden ver/modificar estos datos
     *
     * @return string
     */
    public function url()
    {
        if (is_null($this->codagente)) {
            return "index.php?page=admin_agentes";
        }

        return "index.php?page=admin_agente&cod=" . $this->codagente;
    }

    /**
     * Elimina el registro de la tabla, devuelve el resultado de la consulta.
     *
     * @return bool
     */
    public function delete()
    {
        $this->clean_cache();
        $sql = "DELETE FROM `" . $this->table_name() . "`"
            . " WHERE codagente = " . $this->var2str($this->codagente)
            . ";";
        return $this->db->exec($sql);
    }

    /**
     * Limpia la caché
     */
    public function clean_cache()
    {
        $this->cache->delete(self::CACHE_KEY_ALL);
        $this->cache->delete(self::CACHE_KEY_ALL_FULL);

        foreach ($this->all_full() as $key => $agente) {
            $name = str_replace('{id}', $agente->codagente, self::CACHE_KEY_SINGLE);
            $this->cache->delete($name);
        }
    }

    /**
     * Retorna true si estaba de baja el día $today (string con formato Y-m-d)
     * Si no se pasa $today, o se null, se asume el día de hoy.
     *
     * @param string|null $today
     *
     * @return bool
     *
     * TODO: Para determinar la fecha de baja, tiene en cuenta sólo el último contrato
     */
    public function de_baja($today = null)
    {
        // Tenemos último contrato?
        $ultimo_contrato = false;
        $id_ultimo_contrato = $this->get_ultimo_contrato();
        if ($id_ultimo_contrato !== null) {
            $ultimo_contrato = (new agente_alta())->get($id_ultimo_contrato);
        }

        if ($ultimo_contrato) {
            $f_alta = $ultimo_contrato->f_alta;
            $f_baja = $ultimo_contrato->f_baja;
        } else {
            $f_alta = $this->f_alta;
            $f_baja = $this->f_baja;
        }

        // Si no tiene fecha de alta, es que está de baja
        if ($f_alta === null) {
            return true;
        }

        if ($today === null) {
            $today = date('Y-m-d');
        }

        // Aún no ha alcanzado la fecha de alta del contrato
        if (strtotime($f_alta) > strtotime($today)) {
            return true;
        }

        // Si no tiene fecha de baja, o si aún no la ha alcanzado
        if ($f_baja === null || strtotime($this->f_baja) >= strtotime($today)) {
            return false;
        }

        // Sólo queda que tenga fecha de baja, y que sea anterior a hoy
        return true;
    }

    /**
     * Return the last contact id of the current agent
     *
     * @author  Cayetano H. Osma <cayetano.hernandez@x-netdigital.com>
     * @version 2021.04
     * @return int|null
     *
     */
    public function get_ultimo_contrato()
    {
        $ultimo_contrato = null;

        $sql = "SHOW TABLES LIKE 'agentes_altas';";
        if (!empty($this->db->select($sql))) {
            $sql = "SELECT id FROM `agentes_altas`"
                . " WHERE codagente = " . $this->var2str($this->codagente)
                . " ORDER BY f_alta DESC LIMIT 1";
            $query = $this->db->select($sql);
            if (!empty($query) && isset($query[0]['id'])) {
                $ultimo_contrato = $query[0]['id'];
            }
        }

        return $ultimo_contrato;
    }

    /**
     * Devuelve un array con todos los agentes/empleados.
     *
     * @return static[]
     */
    public function all($incluir_debaja = false)
    {
        if ($incluir_debaja) {
            $sql = "SELECT *"
                . " FROM `" . $this->table_name() . "` ORDER BY nombre ASC, apellidos ASC;";
            $list = $this->all_from($sql, 0, 0);
        } else {
            $sql = "SELECT *"
                . " FROM `" . $this->table_name() . "`"
                . " WHERE f_baja IS NULL ORDER BY nombre ASC, apellidos ASC;";
            $list = $this->all_from_cached(self::CACHE_KEY_ALL, $sql);
        }

        return $list;
    }

    /**
     * Devuelve un array con la lista completa de clientes.
     *
     * @return static[]
     */
    public function all_full()
    {
        $sql = "SELECT *"
            . " FROM `" . $this->table_name() . "`"
            . " ORDER BY codagente;";
        return $this->all_from_cached(self::CACHE_KEY_ALL, $sql);
    }

    /**
     * Esta función es llamada al crear una tabla.
     * Permite insertar valores en la tabla.
     *
     * @return string
     */
    public function install()
    {
        $this->clean_cache();
        return "INSERT INTO `" . $this->table_name() . "` (codagente,nombre,apellidos,dnicif,f_alta)
         VALUES ('1','Nombre','Empleado','00000014Z','" . date("Y-m-d") . "');";
    }

    /**
     * Devuelve un agente por su RFID.
     *
     * @param string $rfid
     *
     * @return false|static
     */
    public function get_by_rfid($rfid)
    {
        $sql = "SELECT *"
            . " FROM `" . $this->table_name() . "`"
            . " WHERE rfid = " . $this->var2str($rfid)
            . ";";

        $data = $this->db->select($sql);
        if ($data) {
            return new static($data[0]);
        }

        return false;
    }

    public function get_fecha_alta()
    {
        if (empty($this->f_alta)) {
            return '';
        }
        return date('Y-m-d', strtotime($this->f_alta));
    }

    public function get_fecha_baja()
    {
        if (empty($this->f_baja)) {
            return '';
        }
        return date('Y-m-d', strtotime($this->f_baja));
    }

    /**
     * Realizamos algunas correcciones a la base de datos y devuelve su estado.
     * Principalmente debe corregir fallos en relaciones entre tablas.
     * NOTA: Algunos controladores llaman a este método, así que las consultas deben ser lo más eficientes posibles
     * hasta que se reemplace por otra forma.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2021.09
     *
     * @return bool
     */
    public function fix_db()
    {
        $status = true;

        // Creamos agentes que existen en otras tablas pero no en agentes
        $sqls = [];
        if ($this->db->table_exists('facturascli')) {
            $sqls[] = "SELECT DISTINCT(codagente) FROM `facturascli` WHERE codagente NOT IN (SELECT codagente FROM `agentes`)";
            $sqls[] = "SELECT DISTINCT(codagente_creacion) FROM `facturascli` WHERE codagente_creacion NOT IN (SELECT codagente FROM `agentes`)";
        }
        if ($this->db->table_exists('albaranescli')) {
            $sqls[] = "SELECT DISTINCT(codagente) FROM `albaranescli` WHERE codagente NOT IN (SELECT codagente FROM `agentes`)";
            $sqls[] = "SELECT DISTINCT(codagente_creacion) FROM `albaranescli` WHERE codagente_creacion NOT IN (SELECT codagente FROM `agentes`)";
        }
        if ($this->db->table_exists('servicioscli')) {
            $sqls[] = "SELECT DISTINCT(codagente) FROM `servicioscli` WHERE codagente NOT IN (SELECT codagente FROM `agentes`)";
            $sqls[] = "SELECT DISTINCT(codagente_creacion) FROM `servicioscli` WHERE codagente_creacion NOT IN (SELECT codagente FROM `agentes`)";
        }
        if ($this->db->table_exists('servicioscli_agentes')) {
            $sqls[] = "SELECT DISTINCT(codagente) FROM `servicioscli_agentes` WHERE codagente NOT IN (SELECT codagente FROM `agentes`)";
        }
        if ($this->db->table_exists('pedidoscli')) {
            $sqls[] = "SELECT DISTINCT(codagente) FROM `pedidoscli` WHERE codagente NOT IN (SELECT codagente FROM `agentes`)";
            $sqls[] = "SELECT DISTINCT(codagente_creacion) FROM `pedidoscli` WHERE codagente_creacion NOT IN (SELECT codagente FROM `agentes`)";
        }
        if ($this->db->table_exists('presupuestoscli')) {
            $sqls[] = "SELECT DISTINCT(codagente) FROM `presupuestoscli` WHERE codagente NOT IN (SELECT codagente FROM `agentes`)";
            $sqls[] = "SELECT DISTINCT(codagente_creacion) FROM `presupuestoscli` WHERE codagente_creacion NOT IN (SELECT codagente FROM `agentes`)";
        }
        if ($this->db->table_exists('facturasprov')) {
            $sqls[] = "SELECT DISTINCT(codagente) FROM `facturasprov` WHERE codagente NOT IN (SELECT codagente FROM `agentes`)";
            $sqls[] = "SELECT DISTINCT(codagente_creacion) FROM `facturasprov` WHERE codagente_creacion NOT IN (SELECT codagente FROM `agentes`)";
        }
        if ($this->db->table_exists('albaranesprov')) {
            $sqls[] = "SELECT DISTINCT(codagente) FROM `albaranesprov` WHERE codagente NOT IN (SELECT codagente FROM `agentes`)";
            $sqls[] = "SELECT DISTINCT(codagente_creacion) FROM `albaranesprov` WHERE codagente_creacion NOT IN (SELECT codagente FROM `agentes`)";
        }
        if ($this->db->table_exists('pedidosprov')) {
            $sqls[] = "SELECT DISTINCT(codagente) FROM `pedidosprov` WHERE codagente NOT IN (SELECT codagente FROM `agentes`)";
            $sqls[] = "SELECT DISTINCT(codagente_creacion) FROM `pedidosprov` WHERE codagente_creacion NOT IN (SELECT codagente FROM `agentes`)";
        }
        if ($this->db->table_exists('agentes_altas')) {
            $sqls[] = "SELECT DISTINCT(codagente) FROM `agentes_altas` WHERE codagente NOT IN (SELECT codagente FROM `agentes`)";
        }
        if ($this->db->table_exists('fs_users')) {
            $sqls[] = "SELECT DISTINCT(codagente) FROM `fs_users` WHERE codagente NOT IN (SELECT codagente FROM `agentes`)";
        }
        if ($this->db->table_exists('archivos')) {
            $sqls[] = "SELECT DISTINCT(created_by) FROM `archivos` WHERE created_by NOT IN (SELECT codagente FROM `agentes`)";
        }
        if ($this->db->table_exists('archivos_entidades')) {
            $sqls[] = "SELECT DISTINCT(created_by) FROM `archivos_entidades` WHERE created_by NOT IN (SELECT codagente FROM `agentes`)";
        }
        if ($this->db->table_exists('visitas_asistencia')) {
            $sqls[] = "SELECT DISTINCT(codagente) FROM `visitas_asistencia` WHERE codagente NOT IN (SELECT codagente FROM `agentes`)";
        }
        if ($this->db->table_exists('cajas_general')) {
            $sqls[] = "SELECT DISTINCT(codagente) FROM `cajas_general` WHERE codagente NOT IN (SELECT codagente FROM `agentes`)";
            $sqls[] = "SELECT DISTINCT(codagente_fin) FROM `cajas_general` WHERE codagente_fin NOT IN (SELECT codagente FROM `agentes`)";
        }
        if ($this->db->table_exists('cajas_general_mov')) {
            $sqls[] = "SELECT DISTINCT(codagente) FROM `cajas_general_mov` WHERE codagente NOT IN (SELECT codagente FROM `agentes`)";
        }
        if ($this->db->table_exists('crm_calendario')) {
            $sqls[] = "SELECT DISTINCT(codagente) FROM `crm_calendario` WHERE codagente NOT IN (SELECT codagente FROM `agentes`)";
        }
        if ($this->db->table_exists('crm_contactos')) {
            $sqls[] = "SELECT DISTINCT(codagente) FROM `crm_contactos` WHERE codagente NOT IN (SELECT codagente FROM `agentes`)";
        }
        if ($this->db->table_exists('crm_oportunidades')) {
            $sqls[] = "SELECT DISTINCT(codagente) FROM `crm_oportunidades` WHERE codagente NOT IN (SELECT codagente FROM `agentes`)";
        }
        if ($this->db->table_exists('candidatos')) {
            $sqls[] = "SELECT DISTINCT(codagente) FROM `candidatos` WHERE codagente NOT IN (SELECT codagente FROM `agentes`)";
        }
        if ($this->db->table_exists('powerbi')) {
            $sqls[] = "SELECT DISTINCT(codagente) FROM `powerbi` WHERE codagente NOT IN (SELECT codagente FROM `agentes`)";
        }
        if ($this->db->table_exists('visitas')) {
            $sqls[] = "SELECT DISTINCT(codagente) FROM `visitas` WHERE codagente NOT IN (SELECT codagente FROM `agentes`)";
        }
        if ($this->db->table_exists('altas_alojamientos')) {
            $sqls[] = "SELECT DISTINCT(codagente_creacion) FROM `altas_alojamientos` WHERE codagente_creacion NOT IN (SELECT codagente FROM `agentes`)";
        }
        if ($this->db->table_exists('mf_alojamientos')) {
            $sqls[] = "SELECT DISTINCT(codagente) FROM `mf_alojamientos` WHERE codagente NOT IN (SELECT codagente FROM `agentes`)";
        }
        // El plugins gastos_ingresos está obsoleto, no hace falta
        //        if ($this->db->table_exists('gastos')) {
        //            $sqls[] = "SELECT DISTINCT(codagente) FROM `gastos` WHERE codagente NOT IN (SELECT codagente FROM `agentes`)";
        //        }
        if ($this->db->table_exists('incentivos')) {
            $sqls[] = "SELECT DISTINCT(codagente) FROM `incentivos` WHERE codagente NOT IN (SELECT codagente FROM `agentes`)";
            $sqls[] = "SELECT DISTINCT(codagente_creacion) FROM `incentivos` WHERE codagente_creacion NOT IN (SELECT codagente FROM `agentes`)";
            $sqls[] = "SELECT DISTINCT(codagente_actualizacion) FROM `incentivos` WHERE codagente_actualizacion NOT IN (SELECT codagente FROM `agentes`)";
        }
        if ($this->db->table_exists('objetivos')) {
            $sqls[] = "SELECT DISTINCT(codagente) FROM `objetivos` WHERE codagente NOT IN (SELECT codagente FROM `agentes`)";
            $sqls[] = "SELECT DISTINCT(codagente_creacion) FROM `objetivos` WHERE codagente_creacion NOT IN (SELECT codagente FROM `agentes`)";
            $sqls[] = "SELECT DISTINCT(codagente_actualizacion) FROM `objetivos` WHERE codagente_actualizacion NOT IN (SELECT codagente FROM `agentes`)";
        }
        if ($this->db->table_exists('instalaciones_equipo')) {
            $sqls[] = "SELECT DISTINCT(codagente) FROM `instalaciones_equipo` WHERE codagente NOT IN (SELECT codagente FROM `agentes`)";
        }
        if ($this->db->table_exists('vigilantes')) {
            $sqls[] = "SELECT DISTINCT(codagente) FROM `vigilantes` WHERE codagente NOT IN (SELECT codagente FROM `agentes`)";
        }
        if ($this->db->table_exists('reserva')) {
            $sqls[] = "SELECT DISTINCT(codagente) FROM `reserva` WHERE codagente NOT IN (SELECT codagente FROM `agentes`)";
        }
        if ($this->db->table_exists('rutas_agentes')) {
            $sqls[] = "SELECT DISTINCT(codagente) FROM `rutas_agentes` WHERE codagente NOT IN (SELECT codagente FROM `agentes`)";
        }
        if ($this->db->table_exists('notes')) {
            $sqls[] = "SELECT DISTINCT(codagente) FROM `notes` WHERE codagente NOT IN (SELECT codagente FROM `agentes`)";
        }
        if ($this->db->table_exists('contratoservicioscli')) {
            $sqls[] = "SELECT DISTINCT(codagente) FROM `contratoservicioscli` WHERE codagente NOT IN (SELECT codagente FROM `agentes`)";
        }
        if ($this->db->table_exists('servicioscli_log')) {
            $sqls[] = "SELECT DISTINCT(codagente) FROM `servicioscli_log` WHERE codagente NOT IN (SELECT codagente FROM `agentes`)";
        }

        if (!empty($sqls)) {
            $sqls = [
                implode(' UNION ', $sqls) . ';',
            ];
        }

        foreach ($sqls as $sql) {
            $data = $this->db->select($sql);
            if ($data) {
                foreach ($data as $item) {
                    if (!empty($item['codagente'])) {
                        if (!(new agente())->get($item['codagente'])) {
                            $agente = new agente();
                            $agente->codagente = $item['codagente'];
                            $agente->nombre = 'Agente ' . $item['codagente'];
                            $status &= $agente->save();
                        }
                    }
                }
            }
        }

        return $status;
    }

    /**
     * Devuelve el empleado/agente con codagente = $cod
     *
     * @param string $cod
     *
     * @return false|static
     */
    public function get($cod)
    {
        $sql = "SELECT *"
            . " FROM `" . $this->table_name() . "`"
            . " WHERE codagente = " . $this->var2str($cod)
            . ";";

        $name = str_replace('{id}', $cod, self::CACHE_KEY_SINGLE);
        return $this->get_from_cached_pk($name, $sql);
    }

    /**
     * Devuelve true si ha añadido o actualizado el registro, sinó false
     *
     * @return bool
     */
    public function save()
    {
        // Se ejecuta aquí el exists, para que cumplimenta
        // la fecha de alta y baja anteriores al guardado.
        $exists = $this->exists();

        if ($this->test()) {
            $this->clean_cache();

            if (!is_null($this->get_ultimo_contrato()) && (isset($this->f_alta) || isset($this->f_baja))) {
                $contratos_model = new agente_alta();
                $contratos = $contratos_model->all($this->codagente);
                if (!$contratos) {
                    $contratos_model->codagente = $this->codagente;
                    $contratos_model->f_alta = $this->f_alta;
                    $contratos_model->f_baja = $this->f_baja;
                    $contratos_model->save();
                } else {
                    $alta = '';
                    foreach ($contratos as $key => $_contrato) {
                        $contrato = $_contrato;
                        if ($contratos[$key]->f_alta !== null) {
                            $contrato->f_alta = date('Y-m-d', strtotime($contratos[$key]->f_alta));
                            $contratos[$key]->f_alta = $contrato->f_alta;
                        }
                        if ($contratos[$key]->f_baja !== null) {
                            $contrato->f_baja = date('Y-m-d', strtotime($contratos[$key]->f_baja));
                            $contratos[$key]->f_baja = $contrato->f_baja;
                        }
                        if ($alta < $this->f_alta) {
                            $alta = $this->f_alta;
                        }
                    }
                }
            }

            if ($exists) {
                $sql = "UPDATE `" . $this->table_name() . "` SET "
                    . "nombre = " . $this->var2str($this->nombre)
                    . ", apellidos = " . $this->var2str($this->apellidos)
                    . ", dnicif = " . $this->var2str($this->dnicif)
                    . ", telefono = " . $this->var2str($this->telefono)
                    . ", telefono2 = " . $this->var2str($this->telefono2)
                    . ", email = " . $this->var2str($this->email)
                    . ", cargo = " . $this->var2str($this->cargo)
                    . ", provincia = " . $this->var2str($this->provincia)
                    . ", ciudad = " . $this->var2str($this->ciudad)
                    . ", direccion = " . $this->var2str($this->direccion)
                    . ", codpais = " . $this->var2str($this->codpais)
                    . ", codpostal = " . $this->var2str($this->codpostal)
                    . ", f_nacimiento = " . $this->var2str($this->f_nacimiento)
                    . ", f_alta = " . $this->var2str($this->f_alta)
                    . ", f_baja = " . $this->var2str($this->f_baja)
                    . ", seg_social = " . $this->var2str($this->seg_social)
                    . ", banco = " . $this->var2str($this->banco)
                    . ", porcomision = " . $this->var2str($this->porcomision)
                    . ", coste_hora = " . $this->var2str($this->coste_hora)
                    . ", nacionalidad = " . $this->var2str($this->nacionalidad)
                    . ", hijos = " . $this->var2str($this->hijos)
                    . ", carnet_conducir = " . $this->var2str($this->carnet_conducir)
                    . ", observaciones = " . $this->var2str($this->observaciones)
                    . ", numdocs = " . $this->var2str($this->numdocs)
                    . ", pin = " . $this->var2str($this->pin)
                    . ", rfid = " . $this->var2str($this->rfid)
                    . ", imputar_gasto = " . $this->var2str($this->imputar_gasto)
                    . ", color_calendario = " . $this->var2str($this->color_calendario)
                    . ", imagen = " . $this->var2str($this->imagen)
                    . " WHERE codagente = " . $this->var2str($this->codagente)
                    . ";";
            } else {
                if (is_null($this->codagente)) {
                    $this->codagente = $this->get_new_codigo();
                }
                $sql = "INSERT INTO `" . $this->table_name() . "` (
                    codagente,
                    nombre,
                    apellidos,
                    dnicif,
                    telefono,
                    telefono2,
                    email,
                    cargo,
                    provincia,
                    ciudad,
                    direccion,
                    codpais,
                    codpostal,
                    f_nacimiento,
                    f_alta,
                    f_baja,
                    seg_social,
                    banco,
                    coste_hora,
                    nacionalidad,
                    hijos,
                    carnet_conducir,
                    observaciones,
                    porcomision,
                    numdocs,
                    pin,
                    rfid,
                    imputar_gasto,
                    color_calendario,
                    imagen
                ) VALUES ("
                    . $this->var2str($this->codagente)
                    . ", " . $this->var2str($this->nombre)
                    . ", " . $this->var2str($this->apellidos)
                    . ", " . $this->var2str($this->dnicif)
                    . ", " . $this->var2str($this->telefono)
                    . ", " . $this->var2str($this->telefono2)
                    . ", " . $this->var2str($this->email)
                    . ", " . $this->var2str($this->cargo)
                    . ", " . $this->var2str($this->provincia)
                    . ", " . $this->var2str($this->ciudad)
                    . ", " . $this->var2str($this->direccion)
                    . ", " . $this->var2str($this->codpais)
                    . ", " . $this->var2str($this->codpostal)
                    . ", " . $this->var2str($this->f_nacimiento)
                    . ", " . $this->var2str($this->f_alta)
                    . ", " . $this->var2str($this->f_baja)
                    . ", " . $this->var2str($this->seg_social)
                    . ", " . $this->var2str($this->banco)
                    . ", " . $this->var2str($this->coste_hora)
                    . ", " . $this->var2str($this->nacionalidad)
                    . ", " . $this->var2str($this->hijos)
                    . ", " . $this->var2str($this->carnet_conducir)
                    . ", " . $this->var2str($this->observaciones)
                    . ", " . $this->var2str($this->porcomision)
                    . ", " . $this->var2str($this->numdocs)
                    . ", " . $this->var2str($this->pin)
                    . ", " . $this->var2str($this->rfid)
                    . ", " . $this->var2str($this->imputar_gasto)
                    . ", " . $this->var2str($this->color_calendario)
                    . ", " . $this->var2str($this->imagen)
                    . ");";
            }

            return $this->db->exec($sql);
        }

        return false;
    }

    /**
     * Esta función devuelve TRUE si los datos del objeto se encuentran
     * en la base de datos.
     *
     * @return bool
     */
    public function exists()
    {
        if (is_null($this->codagente)) {
            return false;
        }

        $sql = "SELECT *"
            . " FROM `" . $this->table_name() . "`"
            . " WHERE codagente = " . $this->var2str($this->codagente)
            . ";";
        $result = $this->db->select($sql);

        return $result;
    }

    /**
     * Comprueba los datos del empleado/agente, devuelve TRUE si son correctos
     *
     * @return bool
     */
    public function test()
    {
        $this->apellidos = $this->no_html($this->apellidos);
        $this->banco = $this->no_html($this->banco);
        $this->cargo = $this->no_html($this->cargo);
        $this->ciudad = $this->no_html($this->ciudad);
        $this->codpostal = $this->no_html($this->codpostal);
        $this->direccion = $this->no_html($this->direccion);
        $this->dnicif = $this->no_html($this->dnicif);
        $this->email = $this->no_html($this->email);
        $this->nombre = $this->no_html($this->nombre);
        $this->provincia = $this->no_html($this->provincia);
        $this->seg_social = $this->no_html($this->seg_social);
        $this->telefono = $this->no_html($this->telefono);
        $this->telefono2 = $this->no_html($this->telefono2);
        $this->coste_hora = (float) $this->coste_hora;
        $this->nacionalidad = $this->no_html($this->nacionalidad);
        $this->hijos = (integer) $this->hijos;
        $this->carnet_conducir = $this->str2bool($this->carnet_conducir);
        $this->observaciones = $this->no_html($this->observaciones);

        if (empty($this->imputar_gasto)) {
            $this->imputar_gasto = false;
        }

        if (strlen($this->nombre) < 1 || strlen($this->nombre) > 50) {
            $this->new_error_msg("El nombre del empleado debe tener entre 1 y 50 caracteres.");
            return false;
        }
        if (empty($this->color_calendario)) {
            $this->color_calendario = '#000000';
        }
        if (empty($this->f_alta)) {
            $this->f_alta = null;
        }
        if (empty($this->f_baja)) {
            $this->f_baja = null;
        }
        if (empty($this->codpais)) {
            $this->codpais = null;
        }

        return true;
    }

    /**
     * Genera un nuevo código de agente
     *
     * @return string
     */
    public function get_new_codigo()
    {
        $sql = "SELECT MAX(" . $this->db->sql_to_int('codagente') . ") as cod FROM `" . $this->table_name() . "`;";
        $data = $this->db->select($sql);
        if ($data) {
            return (string) (1 + (int) $data[0]['cod']);
        }

        return '1';
    }

    /**
     * Devuelve el ultimo codigo de Agente
     *
     * @return string
     */
    public function get_last_codigo()
    {
        $sql = "SELECT MAX(CAST(codagente as UNSIGNED)) as codagente FROM `" . $this->table_name() . "`;";
        $data = $this->db->select($sql);
        $ultimo_codigo = $data[0]['codagente'];
        return $ultimo_codigo;
    }

    /**
     * Si un modelo depende de otro, en este método deben hacerse las correspondientes instanciaciones
     * A su vez, debe invocar al fix_db de dicho modelo dependiente, para poder corregir al vuelo posibles errores que
     * se hubieran detectado con el tiempo.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2021.09
     */
    protected function check_model_dependencies()
    {
        (new pais())->fix_db();

        parent::check_model_dependencies();
    }

    /**
     * Retorna si existe o no, un agente con un determinado DNI/NIF
     *
     * @author  Leonardo M Gonzalez
     * @version 2022.02
     *
     * @return bool
     */
    public function exists_nif($nif)
    {
        $query = $this->var2str($nif);
        $sql = " FROM `agentes` ";
        $and = ' WHERE ';

        if ($query != '') {
            $sql .= $and . "( UPPER(dnicif) =  UPPER(" . $query . ") )";
        }
        $data = $this->db->select("SELECT COUNT(codagente) AS total" . $sql . ';');
        return intval($data[0]['total']) > 0;
    }

    /**
     * Devuelve la url relativa de la imagen de un empleado.
     *
     * @return string
     */
    public function imagen_url()
    {
        $image = FS_PATH . 'Templates/src/assets/images/users/user-dashboard.jpg';
        if (file_exists(FS_MYDOCS . 'images/empleados/' . $this->image_ref() . '-1.png')) {
            $image = FS_PATH . 'images/empleados/' . $this->image_ref() . '-1.png';
        } elseif (file_exists(FS_MYDOCS . 'images/empleados/' . $this->image_ref() . '-1.jpg')) {
            $image = FS_PATH . 'images/empleados/' . $this->image_ref() . '-1.jpg';
        }

        return $image;
    }

    /**
     * Devuelve si el agente tiene o no una imagen asignada.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2022.0808
     *
     * @return bool
     */
    public function has_imagen()
    {
        return $this->imagen_url();
    }

    /**
     * Devuelve la referencia codificada para poder ser usada en imágenes.
     * Evitamos así errores con caracteres especiales como / y \.
     *
     * @param string $ref
     *
     * @return string
     */
    public function image_ref($ref = false)
    {
        if ($ref === false) {
            $ref = $this->codagente;
        }

        return str_replace(['/', '\\'], ['_', '_'], $ref);
    }

    /**
     * Asigna una imagen a un empleado.
     *
     * @param string $img
     * @param bool   $png
     */
    public function set_imagen($img, $png = true)
    {
        $this->imagen = null;

        if (file_exists(FS_MYDOCS . 'images/empleados/' . $this->image_ref() . '-1.png')) {
            unlink(FS_MYDOCS . 'images/empleados/' . $this->image_ref() . '-1.png');
        } elseif (file_exists('images/empleados/' . $this->image_ref() . '-1.jpg')) {
            unlink(FS_MYDOCS . 'images/empleados/' . $this->image_ref() . '-1.jpg');
        }

        if ($img) {
            if (!file_exists(FS_MYDOCS . 'images/empleados')) {
                @mkdir(FS_MYDOCS . 'images/empleados', 0777, true);
            }

            if ($png) {
                $f = @fopen(FS_MYDOCS . 'images/empleados/' . $this->image_ref() . '-1.png', 'a');
            } else {
                $f = @fopen(FS_MYDOCS . 'images/empleados/' . $this->image_ref() . '-1.jpg', 'a');
            }

            if ($f) {
                fwrite($f, $img);
                fclose($f);
            }
        }
    }
}
