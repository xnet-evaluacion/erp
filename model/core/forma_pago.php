<?php

/*
 * This file is part of MiFactura.eu
 * Copyright (C) 2021 X-Net Software Solutions S.L. <xnetsoftware@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace MiFactura\model;

use fs_model;

/**
 * Forma de pago de una factura, albarán, pedido o presupuesto.
 */
class forma_pago extends fs_model
{
    /**
     * Nombre de la clave para almacenar/acceder a la consulta en caché
     */
    const CACHE_KEY_ALL = 'm_forma_pago_all';

    /**
     * Clave primaria. Varchar (10).
     *
     * @var string
     */
    public $codpago;

    /**
     * Descripción.
     *
     * @var null|string
     */
    public $descripcion;

    /**
     * Pagados -> marca las facturas generadas como pagadas.
     *
     * @var string
     */
    public $genrecibos;

    /**
     * Código de la cuenta bancaria asociada.
     *
     * @var string
     */
    public $codcuenta;

    /**
     * Para indicar si hay que mostrar la cuenta bancaria del cliente.
     *
     * @var bool
     */
    public $domiciliado;

    /**
     * TRUE (por defecto) -> mostrar los datos en documentos de venta,
     * incluida la cuenta bancaria asociada.
     *
     * @var bool
     */
    public $imprimir;

    /**
     * Sirve para generar la fecha de vencimiento de las facturas.
     *
     * @var string
     */
    public $vencimiento;

    /**
     * forma_pago constructor.
     *
     * @param false|array $data
     */
    public function __construct($data = false)
    {
        parent::__construct('formaspago');
        if ($data) {
            $this->codpago = $data['codpago'];
            $this->descripcion = $data['descripcion'];
            $this->genrecibos = $data['genrecibos'];
            $this->codcuenta = $data['codcuenta'];
            $this->domiciliado = $this->str2bool($data['domiciliado']);
            $this->imprimir = $this->str2bool($data['imprimir']);
            $this->vencimiento = $data['vencimiento'];
        } else {
            $this->codpago = null;
            $this->descripcion = '';
            $this->genrecibos = 'Emitidos';
            $this->codcuenta = '';
            $this->domiciliado = false;
            $this->imprimir = true;
            $this->vencimiento = '+1day';
        }
    }

    /**
     * Devuelve la url donde se pueden ver/modificar estos datos
     *
     * @return string
     */
    public function url()
    {
        return 'index.php?page=contabilidad_formas_pago';
    }

    /**
     * Devuelve TRUE si esta es la forma de pago predeterminada de la empresa
     *
     * @return bool
     */
    public function is_default()
    {
        return ($this->codpago == $this->default_items->codpago());
    }

    /**
     * Devuelve la forma de pago con codpago = $cod
     *
     * @param string $cod
     *
     * @return false|static
     */
    public function get($cod)
    {
        $sql = "SELECT *"
            . " FROM `" . $this->table_name() . "`"
            . " WHERE codpago = " . $this->var2str($cod)
            . ";";
        $pago = $this->db->select($sql);
        if ($pago) {
            return new static($pago[0]);
        }

        return false;
    }

    /**
     * Devuelve true si ha añadido o actualizado el registro, sinó false
     *
     * @return bool
     */
    public function save()
    {
        $this->clean_cache();
        $this->test();

        if ($this->exists()) {
            $sql = "UPDATE `" . $this->table_name() . "`"
                . " SET descripcion = " . $this->var2str($this->descripcion)
                . ", genrecibos = " . $this->var2str($this->genrecibos)
                . ", codcuenta = " . $this->var2str($this->codcuenta)
                . ", domiciliado = " . $this->var2str($this->domiciliado)
                . ", imprimir = " . $this->var2str($this->imprimir)
                . ", vencimiento = " . $this->var2str($this->vencimiento)
                . " WHERE codpago = " . $this->var2str($this->codpago)
                . ";";
        } else {
            $sql = "INSERT INTO `" . $this->table_name() . "` (codpago,descripcion,genrecibos,codcuenta,domiciliado,imprimir,vencimiento) VALUES ("
                . $this->var2str($this->codpago)
                . ", " . $this->var2str($this->descripcion)
                . ", " . $this->var2str($this->genrecibos)
                . ", " . $this->var2str($this->codcuenta)
                . ", " . $this->var2str($this->domiciliado)
                . ", " . $this->var2str($this->imprimir)
                . ", " . $this->var2str($this->vencimiento)
                . ");";
        }

        return $this->db->exec($sql);
    }

    /**
     * Limpia la caché
     */
    public function clean_cache()
    {
        $this->cache->delete(self::CACHE_KEY_ALL);
    }

    /**
     * Devuelve el campo descriptivo.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2021.10
     *
     * @return string
     */
    public function get_description()
    {
        $cuenta_bancaria = (new cuenta_banco())->get($this->codcuenta);
        return $this->descripcion . ($this->imprimir && $cuenta_bancaria ? ' - ' . $cuenta_bancaria->iban(true) : '');
    }

    /**
     * Comprueba la validez de los datos de la forma de pago.
     */
    public function test()
    {
        $this->descripcion = $this->no_html($this->descripcion);

        /// comprobamos la validez del vencimiento
        $fecha1 = date('Y-m-d');
        $fecha2 = date('Y-m-d', strtotime($this->vencimiento));
        if (strtotime($fecha1) > strtotime($fecha2)) {
            /// vencimiento no válido, asignamos el predeterminado
            $this->new_error_msg('Vencimiento no válido.');
            $this->vencimiento = '+0day';
        }
    }

    /**
     * Esta función devuelve TRUE si los datos del objeto se encuentran
     * en la base de datos.
     *
     * @return bool
     */
    public function exists()
    {
        if (is_null($this->codpago)) {
            return false;
        }

        $sql = "SELECT *"
            . " FROM `" . $this->table_name() . "`"
            . " WHERE codpago = " . $this->var2str($this->codpago)
            . ";";
        return $this->db->select($sql);
    }

    /**
     * Elimina el registro de la tabla, devuelve el resultado de la consulta.
     *
     * @return bool
     */
    public function delete()
    {
        $this->clean_cache();
        $sql = "DELETE FROM `" . $this->table_name() . "`"
            . " WHERE codpago = " . $this->var2str($this->codpago)
            . ";";
        return $this->db->exec($sql);
    }

    /**
     * Devuelve un array con todas las formas de pago
     *
     * @return static[]
     */
    public function all()
    {
        $sql = "SELECT *"
            . " FROM `" . $this->table_name() . "` ORDER BY descripcion ASC;";
        return $this->all_from_cached(self::CACHE_KEY_ALL, $sql);
    }

    /**
     * A partir de una fecha devuelve la nueva fecha de vencimiento en base a esta forma de pago.
     * Si se proporciona $dias_de_pago se usarán para la nueva fecha.
     *
     * @param string $fecha_inicio
     * @param string $dias_de_pago dias de pago específicos para el cliente (separados por comas).
     *
     * @return string
     */
    public function calcular_vencimiento($fecha_inicio, $dias_de_pago = '')
    {
        $fecha = $this->calcular_vencimiento2($fecha_inicio);

        /// validamos los días de pago
        $array_dias = [];
        foreach (str_getcsv($dias_de_pago) as $d) {
            if (intval($d) >= 1 && intval($d) <= 31) {
                $array_dias[] = intval($d);
            }
        }

        if (!empty($array_dias)) {
            foreach ($array_dias as $i => $dia_de_pago) {
                if ($i == 0) {
                    $fecha = $this->calcular_vencimiento2($fecha_inicio, $dia_de_pago);
                } else {
                    /// si hay varios dias de pago, elegimos la fecha más cercana
                    $fecha_temp = $this->calcular_vencimiento2($fecha_inicio, $dia_de_pago);
                    if (strtotime($fecha_temp) < strtotime($fecha)) {
                        $fecha = $fecha_temp;
                    }
                }
            }
        }

        return $fecha;
    }

    /**
     * Esta función es llamada al crear una tabla.
     * Permite insertar valores por defecto en la tabla.
     *
     * @return string
     */
    public function install()
    {
        $this->clean_cache();
        return "INSERT INTO `" . $this->table_name() . "` (codpago,descripcion,genrecibos,codcuenta,domiciliado,vencimiento)"
            . " VALUES ('CONT','Al contado','Pagados',NULL,FALSE,'+0day')"
            . ", ('TRANS','Transferencia bancaria','Emitidos',NULL,FALSE,'+1month')"
            . ", ('TARJETA','Tarjeta de crédito','Pagados',NULL,FALSE,'+0day')"
            . ", ('PAYPAL','PayPal','Pagados',NULL,FALSE,'+0day');";
    }

    /**
     * Realizamos algunas correcciones a la base de datos y devuelve su estado.
     * Principalmente debe corregir fallos en relaciones entre tablas.
     * NOTA: Algunos controladores llaman a este método, así que las consultas deben ser lo más eficientes posibles
     * hasta que se reemplace por otra forma.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2021.09
     *
     * @return bool
     */
    public function fix_db()
    {
        $fixes = [
            'albaranescli' => [
                'ca_albaranescli_formaspago' => "UPDATE `albaranescli` SET codpago = 'CONT' WHERE codpago NOT IN (SELECT DISTINCT(codpago) FROM `formaspago`);",
            ],
            'albaranesprov' => [
                'ca_albaranesprov_formaspago' => "UPDATE `albaranesprov` SET codpago = 'CONT' WHERE codpago NOT IN (SELECT DISTINCT(codpago) FROM `formaspago`);",
            ],
            'empresa' => [
                'ca_empresa_formaspago' => "UPDATE `empresa` SET codpago = 'CONT' WHERE codpago NOT IN (SELECT DISTINCT(codpago) FROM `formaspago`);",
            ],
            'facturascli' => [
                'ca_facturascli_formaspago' => "UPDATE `facturascli` SET codpago = 'CONT' WHERE codpago NOT IN (SELECT DISTINCT(codpago) FROM `formaspago`);",
            ],
            'facturasprov' => [
                'ca_facturasprov_formaspago' => "UPDATE `facturasprov` SET codpago = 'CONT' WHERE codpago NOT IN (SELECT DISTINCT(codpago) FROM `formaspago`);",
            ],
            'pedidoscli' => [
                'ca_pedidoscli_formaspago' => "UPDATE `pedidoscli` SET codpago = 'CONT' WHERE codpago NOT IN (SELECT DISTINCT(codpago) FROM `formaspago`);",
            ],
            'pedidosprov' => [
                'ca_pedidosprov_formaspago' => "UPDATE `pedidosprov` SET codpago = 'CONT' WHERE codpago NOT IN (SELECT DISTINCT(codpago) FROM `formaspago`);",
            ],
            'plazos' => [
                'ca_plazos_formaspago' => "UPDATE `plazos` SET codpago = NULL WHERE codpago NOT IN (SELECT codpago FROM `formaspago`);",
            ],
            'presupuestoscli' => [
                'ca_presupuestoscli_formaspago' => "UPDATE `presupuestoscli` SET codpago = 'CONT' WHERE codpago NOT IN (SELECT DISTINCT(codpago) FROM `formaspago`);",
            ],
            'proveedores' => [
                'ca_proveedores_formaspago' => "UPDATE `proveedores` SET codpago = 'CONT' WHERE codpago NOT IN (SELECT DISTINCT(codpago) FROM `formaspago`);",
            ],
            'servicioscli' => [
                'ca_servicioscli_formaspago' => "UPDATE `servicioscli` SET codpago = 'CONT' WHERE codpago NOT IN (SELECT DISTINCT(codpago) FROM `formaspago`);",
            ],
        ];
        return $this->exec_fix_queries($fixes);
    }

    /**
     * Si un modelo depende de otro, en este método deben hacerse las correspondientes instanciaciones
     * A su vez, debe invocar al fix_db de dicho modelo dependiente, para poder corregir al vuelo posibles errores que
     * se hubieran detectado con el tiempo.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2021.09
     */
    protected function check_model_dependencies()
    {
        (new cuenta_banco())->fix_db();

        parent::check_model_dependencies();
    }

    /**
     * Calcula el dái de vencimiento a partir de la fecha de inicio y el día de pago.
     *
     * @param string $fecha_inicio
     * @param int    $dia_de_pago
     *
     * @return false|string
     */
    private function calcular_vencimiento2($fecha_inicio, $dia_de_pago = 0)
    {
        if ($dia_de_pago == 0) {
            return date('Y-m-d', strtotime($fecha_inicio . ' ' . $this->vencimiento));
        }

        $fecha = date('Y-m-d', strtotime($fecha_inicio . ' ' . $this->vencimiento));
        $tmp_dia = date('d', strtotime($fecha));
        $tmp_mes = date('m', strtotime($fecha));
        $tmp_anyo = date('Y', strtotime($fecha));

        if ($tmp_dia > $dia_de_pago) {
            /// calculamos el dia de cobro para el mes siguiente
            $fecha = date('Y-m-d', strtotime($fecha . ' first day of next month'));
            $tmp_mes = date('m', strtotime($fecha));
            $tmp_anyo = date('Y', strtotime($fecha));
        }

        /// ahora elegimos un dia, pero que quepa en el mes, no puede ser 31 de febrero
        $tmp_dia = min([$dia_de_pago, intval(date('t', strtotime($fecha))),]);

        /// y por último generamos la fecha
        return date('Y-m-d', strtotime($tmp_dia . '-' . $tmp_mes . '-' . $tmp_anyo));
    }
}
