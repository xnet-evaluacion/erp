<?php

namespace Xnet\Test;

use Xnet\Model\User;
use Xnet\Test\XnetTestCase;

class UserTest extends XnetTestCase
{
    public function setUp(): void
    {
        parent::setUp();
    }

    public function testNewUser()
    {
        $data = [
            'name' => 'Usuario 1',
            'email' => 'email@xnet.com',
            'code' => 'accesscode',
            'password' => sha1('password'),
        ];

        $this->assertIsObject(User::create($data));

        $data = [
            'name' => 'Usuario 2',
            'email' => 'email2@xnet.com',
            'code' => 'accesscode2',
            'password' => sha1('password'),
        ];
        $this->assertIsObject(User::create($data));
    }

    public function testDeleteUser()
    {
        $userArray = User::getBy(['code' => 'accesscode']);
        $user = User::getById(reset($userArray)['id']);

        $this->assertIsNotString($user->deleted_at);
        $user->delete();
        $userArray = User::getBy(['code' => 'accesscode']);

        $this->assertIsString($user->deleted_at);
        $user->delete(false);

        $userArray = User::getBy(['code' => 'accesscode2']);
        $user = User::getById(reset($userArray)['id']);
        $user->delete(false);
    }

}
