<?php
/*
 * This file is part of MiFactura.eu
 * Copyright (C) 2021 X-Net Software Solutions S.L. <xnetsoftware@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace Xnet;

use Xnet\Core\DB;
use Xnet\Model\Version;

echo 'Iniciando cron de MiFactura.eu...';

/// accedemos al directorio de MiFactura.eu
chdir(__DIR__);

/**
 * Contiene la ruta principal del alojamiento.
 */
define('BASE_PATH', __DIR__); // Eliminar cuando no se use

/**
 * Contiene la ruta principal del alojamiento.
 *
 * @deprecated Utilice en su lugar BASE_PATH
 */
define('PATH', constant('BASE_PATH'));

/**
 * Contiene la ruta principal del alojamiento.
 *
 * No está deprecated, no es la misma ruta cuando es una multi instalación
 */
define('FS_FOLDER', constant('BASE_PATH'));

$autoload_file = constant('FS_FOLDER') . '/vendor/autoload.php';
if (!file_exists($autoload_file)) {
    die('<h1>COMPOSER ERROR</h1><p>You need to run: composer install</p>');
}
require_once $autoload_file;

// Si se ejecuta desde la consola, pasar los parámetros que se le han pasado a $_GET
define('CONSOLE_MODE', (bool) isset($_GET['console']) && $_GET['console'] || PHP_SAPI === 'cli');
if (PHP_SAPI === 'cli') {
    parse_str(implode('&', array_slice($argv, 1)), $_GET);
    define('EOL', "\n");
} else {
    define('EOL', constant('CONSOLE_MODE') ? "\n" : '<br>');
}

if (!defined('BASE_URL')) {
    /**
     * Define la constante con valor por defecto vacío
     *
     * @deprecated Creo que BASE_URL no se está usando.
     */
    define('BASE_URL', '');
}

/// cargamos las constantes de configuración
require_once constant('BASE_PATH') . '/config.php';
require_once constant('BASE_PATH') . '/base/config2.php';
// require_once constant('BASE_PATH') . '/base/fs_functions.php';
require_once constant('BASE_PATH') . '/src/Xnet/Core/Functions.php'; // Se cargaría con autoload

/// establecemos nuevos límites
set_temporaly_unlimited_limits();

$tiempo = explode(' ', microtime());
$uptime = $tiempo[1] + $tiempo[0];

new DB();
if (!DB::connect()) {
    die('Error al intentar conectar con la base de datos en cron.php');
}

loadPluginsConfig2();

require_once constant('BASE_PATH') . '/base/fs_app.php';
require_once constant('BASE_PATH') . '/base/fs_core_log.php';
require_once constant('BASE_PATH') . '/base/fs_db2.php';
$core_log = new \fs_core_log();
$db = new \fs_db2();

require_once constant('BASE_PATH') . '/base/fs_default_items.php';
require_once constant('BASE_PATH') . '/base/fs_extended_model.php';
require_once constant('BASE_PATH') . '/base/fs_log_manager.php';
require_all_models();

if ($db->connect() && $db->connected()) {
    $fsvar = new \fs_var();
    $cron_vars = $fsvar->array_get([
        'cron_exists' => false,
        'cron_lock' => false,
        'cron_error' => false,
        'cron_maximo_minutos' => false,
    ]);

    $execute = $cron_vars['cron_lock'] === false;
    /*
     * Servicio SER2021JR898
     *
     * Se procede a eliminar la limitación de ejecución (que no es que cancele)
     * Si el cron está en ejecución, no se lanza otro hasta que se de por finalizado.
     *
    if (!$execute) {
        $tiempo_estipulado = $cron_vars['cron_maximo_minutos'];
        if ($tiempo_estipulado === false) {
            $tiempo_estipulado = 5;
        } else {
            $tiempo_estipulado = (integer) $tiempo_estipulado;
        }
        $minutos_ejecucion = (float) (time() - $cron_vars['cron_lock']) / 60;
        $minutos_ejecucion_redondeado = round($minutos_ejecucion, 2);

        echo "\nHay un cron en ejecución desde hace {$minutos_ejecucion_redondeado} minutos";

        $cron_vars['cron_error'] = 'TRUE';
        $execute = ($minutos_ejecucion > $tiempo_estipulado);
        if ($execute) {
            echo "\nSe ha excedido el tiempo de ejecución estipulado de {$tiempo_estipulado} minutos";

            // TODO: Se notifica
            $cron_vars['cron_lock'] = false;
            $cron_vars['cron_error'] = false;
        }
    }
    */

    if ($execute) {
        /**
         * He detectado que a veces, con el plugin kiwimaru,
         * el proceso cron tarda más de una hora, y por tanto se encadenan varios
         * procesos a la vez. Para evitar esto, uso la entrada cron_lock.
         * Además uso la entrada cron_exists para marcar que alguna vez se ha ejecutado el cron,
         * y cron_error por si hubiese algún fallo.
         */
        $cron_vars['cron_lock'] = time();
        $cron_vars['cron_exists'] = 'TRUE';

        /// guardamos las variables
        $fsvar->array_save($cron_vars);

        /// indicamos el inicio en el log
        $core_log->save('Ejecutando el cron...', 'cron');

        /// establecemos los elementos por defecto
        $fs_default_items = new \fs_default_items();
        $empresa = (new \empresa())->get(1);
        $fs_default_items->set_codalmacen($empresa->codalmacen);
        $fs_default_items->set_coddivisa($empresa->coddivisa);
        $fs_default_items->set_codejercicio($empresa->codejercicio);
        $fs_default_items->set_codpago($empresa->codpago);
        $fs_default_items->set_codpais($empresa->codpais);
        $fs_default_items->set_codserie($empresa->codserie);

        /*
         * Ahora ejecutamos el cron de cada plugin que tenga cron y esté activado
         */
        foreach (Version::getEnabledPluginsArray() as $plugin) {
            if (file_exists('plugins/' . $plugin . '/cron.php')) {
                echo "\n***********************\nEjecutamos el cron.php del plugin " . $plugin . "\n";
                include 'plugins/' . $plugin . '/cron.php';
                echo "\n***********************";
            }
        }

        /// indicamos el fin en el log
        $core_log->save('Terminada la ejecución del cron.', 'cron');

        /// Eliminamos la variable cron_lock puesto que ya hemos terminado
        $cron_vars['cron_lock'] = false;
    }

    /// guardamos las variables
    $fsvar->array_save($cron_vars);

    /// mostramos el errores que se hayan podido producir
    foreach ($core_log->get_errors() as $err) {
        echo "\nERROR: " . $err . "\n";
    }

    /// guardamos los errores en el log
    $log_manager = new \fs_log_manager();
    $log_manager->save();

    $db->close();
} else {
    echo "¡Imposible conectar a la base de datos!\n";
    foreach ($core_log->get_errors() as $err) {
        echo $err . "\n";
    }
}

$tiempo2 = explode(' ', microtime());
echo "\nTiempo de ejecución: " . number_format($tiempo2[1] + $tiempo2[0] - $uptime, 3) . " s\n";
