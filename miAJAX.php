<?php

/*
 * This file is part of MiFactura.eu
 * Copyright (C) 2021 X-Net Software Solutions S.L. <xnetsoftware@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
error_reporting(E_ALL);
date_default_timezone_set('Europe/Madrid');

/**
 * Contiene la ruta principal del alojamiento.
 */
const BASE_PATH = __DIR__;

/**
 * Contiene la ruta principal del alojamiento.
 *
 * @deprecated Utilice en su lugar BASE_PATH
 */
define('PATH', constant('BASE_PATH'));

/**
 * Contiene la ruta principal del alojamiento.
 *
 * No está deprecated, no es la misma ruta cuando es una multi instalación
 */
define('FS_FOLDER', constant('BASE_PATH'));

$autoload_file = __DIR__ . '/vendor/autoload.php';
if (!file_exists($autoload_file)) {
    die('<h1>COMPOSER ERROR</h1><p>You need to run: composer install</p>');
}
require_once $autoload_file;

require_once('config.php');
require_once('base/config2.php');
// require_once constant('FS_PATH') . '/base/fs_functions.php';
//require_once constant('FS_PATH') . '/src/Xnet/Core/Functions.php'; // Se cargaría con autoload

// Nos permite recopilar crasheos en tiempo real sin que los usuarios deban realizar ningún reporte para hacernos conocedores del problema.
if (constant('USE_BUG_TRACKER')) {
    \Sentry\init([
        'dsn' => 'http://192f6ff7c1ec4bcf806b715483fba9bc@sentry.x-netdigital.com:9000/2',
        'environment' => constant('FS_DEBUG') ? 'production' : 'development',
    ]);
}

$db = new \Xnet\Core\DB();
$db->connect();

// It is only included if the plugin is activated and the file exists
if (is_plugin_enabled('ayuda_soporte')) {
    new \Xnet\Core\XnetDebugBar();
    require_once(constant('FS_FOLDER') . '/plugins/xnetcommonhelpers/functions.php');
    $filename = constant('FS_FOLDER') . '/plugins/ayuda_soporte/miAJAX.php';
    if (file_exists($filename)) {
        require_once($filename);
    }
}
