<?php return [
    /**
     * Estas dos opciones son las que aparecen como títulos de los bloques.
     * Habría que determinar si se seguirán usando así, o no
     */
    'admin' => [
        'icon' => null,
        'icon_open' => null,
        'title' => 'Administración',
    ],
    'erp' => [
        'icon' => null,
        'icon_open' => null,
        'title' => 'X-Net ERP',
    ],
    'rrhh' => [
        'icon' => null,
        'icon_open' => null,
        'title' => 'RR.HH.',
    ],
    'quality' => [
        'icon' => null,
        'icon_open' => null,
        'title' => 'Calidad',
    ],
    'admin|company' => [
        'icon' => 'fa-solid fa-industry fa-fw',
        'icon_open' => 'fa-solid fa-industry fa-fw',
        'title' => 'Empresa',
    ],
    'admin|dashboard' => [
        'icon' => 'fa-solid fa-solar-panel fa-fw',
        'icon_open' => 'fa-solid fa-solar-panel fa-fw',
        'title' => 'Panel de control',
    ],
    'admin|dashboard|config' => [
        'icon' => 'fa-solid fa-gear fa-fw',
        'icon_open' => 'fa-solid fa-gear fa-fw',
        'title' => 'Configuración',
    ],
];
