<?php

/*
 * This file is part of MiFactura.eu
 * Copyright (C) 2021 X-Net Software Solutions S.L. <xnetsoftware@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

require_once constant('BASE_PATH') . '/base/fs_list_filter.php';

/**
 * Description of fs_list_filter_checkbox
 */
class fs_list_filter_checkbox extends fs_list_filter
{
    /**
     * Debe coincidir con el valor?
     *
     * @var bool
     */
    protected $match_value;

    /**
     * Operación a contrastar.
     *
     * @var string
     */
    protected $operation;

    /**
     * fs_list_filter_checkbox constructor.
     *
     * @param string $col_name
     * @param string $label
     * @param string $operation
     * @param bool   $match_value
     */
    public function __construct($col_name, $label, $operation = '=', $match_value = true)
    {
        parent::__construct($col_name, $label);
        $this->match_value = $match_value;
        $this->operation = $operation;
    }

    /**
     * Genera la parte WHERE de la SQL.
     *
     * @return string
     */
    public function get_where()
    {
        /// necesitamos un modelo, el que sea, para llamar a su función var2str()
        $fs_log = new fs_log();
        return $this->value ? ' AND ' . $this->col_name . ' ' . $this->operation . ' ' . $fs_log->var2str($this->match_value) : '';
    }

    /**
     * Devuelve el código HTML para mostrar el componente.
     *
     * @return string
     */
    public function show()
    {
        $checked = $this->value ? ' checked=""' : '';
        return '<div class="form-check form-switch">'
            . '    <input class="form-check-input" type="checkbox" role="switch" name="' . $this->name() . '" value="TRUE" ' . $checked . ' />'
            . '    <label class="form-check-label">' . $this->label . '</label>'
            . '</div>';
    }
}
