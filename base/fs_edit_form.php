<?php

/*
 * This file is part of MiFactura.eu
 * Copyright (C) 2021 X-Net Software Solutions S.L. <xnetsoftware@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Description of fs_edit_form
 */
class fs_edit_form
{
    /**
     * Listado ce columnas.
     *
     * @var array
     */
    public $columns = [];

    /**
     * Listado de objetos para consultar a los datos de modelos.
     *
     * @var array
     */
    private $model_objects = [];

    /**
     * fs_edit_form constructor.
     *
     * @param fs_edit_form $old_decoration
     */
    public function __construct($old_decoration = null)
    {
        if ($old_decoration) {
            $this->columns = $old_decoration->columns;
        }
    }

    /**
     * Añade una columna de tipo select.
     *
     * @param string $col_name
     * @param array  $values
     * @param string $label
     * @param int    $num_cols
     * @param bool   $required
     */
    public function add_column_select($col_name, $values, $label = '', $num_cols = 2, $required = false, $readonly = false)
    {
        $this->add_column($col_name, 'select', $label, $num_cols, $required, $values, $readonly);
    }

    /**
     * Añade una columna con todos los detalles indicados.
     * ejemplo de autocomplete en https://drive.google.com/file/d/1AnsFY6S6A1mMIf69BEjgvvo1mda0hJyC/view?usp=sharing
     *
     * @param string     $col_name
     * @param string     $type      bool, date, hidden, select, textarea, number, money, text
     * @param string     $label
     * @param int|string $num_cols
     * @param bool       $required
     * @param array      $values
     * @param bool       $readonly
     * @param array      $extradata podemos especificar maxlength=n, min=n, max=n, step=n, autocomplete=url, inputid=id
     */
    public function add_column($col_name, $type = 'string', $label = '', $num_cols = 2, $required = false, $values = [], $readonly = false, $extradata = [])
    {
        $this->columns[$col_name] = [
            'label' => empty($label) ? $col_name : $label,
            'num_cols' => is_int($num_cols) ? 'col-sm-' . $num_cols : $num_cols,
            'required' => $required,
            'type' => $type,
            'values' => $values,
            'readonly' => $readonly,
            'extradata' => $extradata,
        ];
    }

    /**
     * Muestra los datos para una columna.
     *
     * @param string            $col_name
     * @param array             $col_config
     * @param fs_extended_model $model
     *
     * @return string
     */
    public function show($col_name, $col_config, $model)
    {
        $html = '<div class="form-group">'
            . ($col_config['type'] == 'bool' ? '' : '<label>' . $col_config['label'] . '</label>');
        $required = $col_config['required'] ? ' required' : '';
        $readonly = $col_config['readonly'] ? ' readonly' : '';
        if (isset($col_config['extradata'] ['maxlength'])) {
            $maxlength = ' maxlength="' . $col_config['extradata'] ['maxlength'] . '"';
        } else {
            $maxlength = '';
        }

        if (isset($col_config['extradata'] ['inputid'])) {
            $inputid = ' id="' . $col_config['extradata'] ['inputid'] . '"';
        } else {
            $inputid = '';
        }

        $name = $col_name;
        $esautocomplete=false;
        switch ($col_config['type']) {
            case 'bool':
                $checked = $model->{$col_name} ? ' checked=""' : '';
                $html .= '<div class="checkbox"><input type="checkbox" name="' . $name . '" value="TRUE"' . $checked . $readonly . $inputid . '/> ' . $col_config['label'] . '</div>';
                break;

            case 'date':
                $html .= '<div class="input-group">'
                    . '    <span class="input-group-text">'
                    . '        <i class="fa-solid fa-calendar fa-fw"></i>'
                    . '    </span>'
                    . '    <input class="form-control" type="date" name="' . $name . '" value="' . $model->{$col_name} . '" autocomplete="off"' . $required . $readonly . $inputid . '/>'
                    . '</div>';
                break;

            case 'datetime-local':
                $datetime_local = '';
                if($model->{$col_name}){
                   $datetime_local = substr(date(DATE_ATOM,strtotime($model->{$col_name})),0,16);
                }
                $html .= '<div class="input-group">'
                    . '    <span class="input-group-text">'
                    . '        <i class="fa-solid fa-calendar fa-fw"></i>'
                    . '    </span>'
                    . '    <input class="form-control" type="datetime-local" name="' . $name . '" value="' . $datetime_local . '" autocomplete="off"' . $required . $readonly . $inputid . '/>'
                    . '</div>';
                break;

            case 'money':
            case 'number':
                if (isset($col_config['extradata'] ['min'])) {
                    $min = ' min="' . $col_config['extradata'] ['min'] . '"';
                } else {
                    $min = '';
                }
                if (isset($col_config['extradata'] ['max'])) {
                    $max = ' max="' . $col_config['extradata'] ['max'] . '"';
                } else {
                    $max = '';
                }
                if (isset($col_config['extradata'] ['step'])) {
                    $step = ' step="' . $col_config['extradata'] ['step'] . '"';
                } else {
                    $step = '';
                }
                $html .= '<input class="form-control" type="number" step="any" name="' . $name . '" value="' . $model->{$col_name} . '" autocomplete="off"' . $required . $readonly . $min . $max . $step . $inputid . '/>';
                break;

            case 'hidden':
                $html .= '<input class="form-control" type="hidden" name="' . $name . '" value="' . $model->{$col_name} . '" autocomplete="off"  ' . $inputid . '/>';
                break;

            case 'select':
                $html .= $this->show_select($name, $col_config, $model);
                break;

            case 'textarea':
                $html .= '<textarea class="form-control" name="' . $name . '"' . $required . $readonly . $inputid . '>' . $model->{$col_name} . '</textarea>';
                break;

            case 'range':
                //Por defecto max=100, min=0, step = 1
                if (isset($col_config['extradata'] ['min'])) {
                    $min = ' min="' . $col_config['extradata'] ['min'] . '"';
                } else {
                    $min = '0';
                }
                if (isset($col_config['extradata'] ['max'])) {
                    $max = ' max="' . $col_config['extradata'] ['max'] . '"';
                } else {
                    $max = '100';
                }
                if (isset($col_config['extradata'] ['step'])) {
                    $step = ' step="' . $col_config['extradata'] ['step'] . '"';
                } else {
                    $step = '1';
                }
                $html .= '<input class="form-range" type="range" step="' . $step . '" name="' . $name . '" value="' . $model->{$col_name} . '" autocomplete="off"' . $required . $readonly . $min . $max . $step . $inputid . '/>';

                break;

            case 'autocomplete':
                $html .= '<input class="form-control" type="hidden" name="' . $col_name . '" value="' . $model->{$col_name} . '" autocomplete="off"  id="' . $col_config['extradata'] ['inputid'] . '" />';
                if (isset($col_config['extradata'] ['ac_noSuggestion'])) { //el mensaje que se mostrará si no se encuentran coincidencias
                    $noSuggestion = $col_config['extradata'] ['ac_noSuggestion'];
                } else {
                    $noSuggestion = '';
                }
                $name = "ac_" . $col_name; //si es de tipo autocomplete, el input mostrado se le pone otro nombre
                $inputid = ' id="ac_' . $col_config['extradata'] ['inputid'] . '"';
                $esautocomplete="fs_autocomplete";


                //no poner break, requiere continuar con lo que está en el default

            default:
                $restrictions = '';
                if(isset($col_config['extradata'])) {
                    foreach ($col_config['extradata'] as $key => $value) {
                        $restrictions .= ' ' . $key . '="' . $value . '"';
                    }
                }
                $html .= '<input class="form-control ' . $esautocomplete . '" type="text" name="' . $name . '" value="' . $model->{$col_name} . '" autocomplete="off"' . $required . $readonly . $maxlength . $inputid . $restrictions . ' />';
        }

        $html .= '</div>';
        return $html;
    }

    /**
     * Muestra los datos para una columna de tipo select.
     *
     * @param string            $col_name
     * @param array             $col_config
     * @param fs_extended_model $model
     *
     * @return string
     */
    protected function show_select($col_name, $col_config, $model)
    {
        $required = $col_config['required'] ? ' required' : '';
        $readonly = $col_config['readonly'] ? ' readonly' : '';

        if (isset($col_config['extradata'] ['inputid'])) {
            $inputid = ' id="' . $col_config['extradata'] ['inputid'] . '"';
        } else {
            $inputid = '';
        }

        $html = '<select id="' . $col_name . '" name="' . $col_name . '" class="form-control select2"' . $required . $readonly . $inputid . '>';

        foreach ($col_config['values'] as $key => $value) {
            if ($model->{$col_name} == $key) {
                $html .= '<option value="' . $key . '" selected="">' . $value . '</option>';
            } else {
                $html .= '<option value="' . $key . '">' . $value . '</option>';
            }
        }

        $html .= '</select>';
        return $html;
    }

    /**
     * Devuelve el objeto del modelo para la clave primaria indicada.
     *
     * @param string $model_class_name
     * @param string $code
     *
     * @return mixed
     */
    protected function get_model_object($model_class_name, $code)
    {
        if (isset($this->model_objects[$model_class_name][$code])) {
            return $this->model_objects[$model_class_name][$code];
        }

        $model = new $model_class_name();
        $object = $model->get($code);
        if ($object) {
            $this->model_objects[$model_class_name][$code] = $object;
            return $object;
        }

        return $model;
    }
}
