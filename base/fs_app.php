<?php
/*
 * This file is part of MiFactura.eu
 * Copyright (C) 2021 X-Net Software Solutions S.L. <xnetsoftware@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

require_once constant('BASE_PATH') . '/base/fs_cache.php';
require_once constant('BASE_PATH') . '/base/fs_core_log.php';
require_once constant('BASE_PATH') . '/base/fs_file_manager.php';
//require_once constant('BASE_PATH') . '/base/fs_functions.php';
require_once constant('BASE_PATH') . '/base/debugbar.php';

/**
 * Description of fs_app
 */
class fs_app
{
    /**
     * Identificador para la caché.
     *
     * @var string
     */
    public $id_cache;

    /**
     * Este objeto permite interactuar con memcache
     *
     * @var fs_cache
     */
    protected $cache;

    /**
     * Este objeto contiene los mensajes, errores y consejos volcados por controladores,
     * modelos y base de datos.
     *
     * @var fs_core_log
     */
    protected $core_log;

    /**
     * TODO: Missing documentation
     *
     * @var fs_log_manager
     */
    protected $log_manager;

    /**
     * Permite calcular cuanto tarda en procesarse la página.
     *
     * @var string
     */
    private $uptime;

    /**
     * Barra de depuración.
     *
     * @var debugbar
     */
    public $debugBar;

    /**
     * fs_app constructor.
     */
    public function __construct()
    {
        $tiempo = explode(' ', microtime());
        $this->uptime = $tiempo[1] + $tiempo[0];

        $this->debugBar = new debugbar();
        $this->cache = new fs_cache();
        $this->core_log = new fs_core_log(get_called_class());
        $this->log_manager = new fs_log_manager();

        $this->id_cache = $this->get_id_cache();
    }

    /**
     *
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2021.11
     *
     * @return mixed|string
     */
    private function get_id_cache()
    {
        /// necesitamos un id que se cambie al limpiar la caché
        $idcache = $this->cache->get('fs_idcache');
        if (!$idcache) {
            $idcache = $this->random_string(10);
            $this->cache->set('fs_idcache', $idcache, 30 * 24 * 60 * 60);
        }
        return $idcache;
    }

    /**
     * Devuelve un string aleatorio de longitud $length
     *
     * @param int $length la longitud del string
     *
     * @return string la cadena aleatoria
     */
    public function random_string($length = 30)
    {
        return mb_substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
    }

    /**
     * Devuelve la duración de la ejecución de la página
     *
     * @return string
     */
    public function duration()
    {
        $tiempo = explode(" ", microtime());
        return (number_format($tiempo[1] + $tiempo[0] - $this->uptime, 3) . ' s');
    }

    /**
     * Devuelve la lista de mensajes completa
     *
     * @return array lista de mensajes
     */
    public function get_all_logs()
    {
        return $this->core_log->get_all();
    }

    /**
     * Devuelve la lista de consejos
     *
     * @return array lista de consejos
     */
    public function get_advices()
    {
        return $this->core_log->get_advices();
    }

    /**
     * Devuelve el listado de consultas SQL que se han ejecutados
     *
     * @return array lista de consultas SQL
     */
    public function get_db_history()
    {
        $result = $this->core_log->get_sql_history();
        foreach ($result as $key => $value) {
            $this->debugBar->addQuery($key + 1, $value);
        }
        return $result;
    }

    /**
     * Devuelve la lista de errores
     *
     * @return array lista de errores
     */
    public function get_errors()
    {
        return $this->core_log->get_errors();
    }

    /**
     * Busca en la lista de plugins activos, en orden inverso de prioridad
     * (el último plugin activo tiene más prioridad que el primero)
     * y nos devuelve la ruta del archivo javascript que le solicitamos.
     * Así usamos el archivo del plugin con mayor prioridad.
     *
     * @param string $filename
     *
     * @return string
     */
    public function get_js_location($filename)
    {
        foreach ($GLOBALS['plugins'] as $plugin) {
            switch (ENGINE) {
                case 'raintpl':
                    if (file_exists('plugins/' . $plugin . '/view/js/' . $filename)) {
                        return constant('FS_PATH') . 'plugins/' . $plugin . '/view/js/' . $filename . '?idcache=' . $this->id_cache;
                    }
                    break;
                case 'blade':
                    if (file_exists('plugins/' . $plugin . '/Templates/js/' . $filename)) {
                        return constant('FS_PATH') . 'plugins/' . $plugin . '/Templates/js/' . $filename . '?idcache=' . $this->id_cache;
                    }
                    break;
            }
        }

        /// si no está en los plugins estará en el núcleo
        switch (ENGINE) {
            case 'raintpl':
                if (file_exists('view/js/' . $filename)) {
                    return constant('FS_PATH') . 'view/js/' . $filename . '?idcache=' . $this->id_cache;
                }
                return '#NOTFOUND-' . constant('FS_PATH') . 'view/js/' . $filename . '?idcache=' . $this->id_cache;
                break;
            case 'blade':
                if (file_exists('Templates/js/' . $filename)) {
                    return constant('FS_PATH') . 'Templates/js/' . $filename . '?idcache=' . $this->id_cache;
                }
                return '#NOTFOUND-' . constant('FS_PATH') . 'Templates/js/' . $filename . '?idcache=' . $this->id_cache;
                break;
        }

        return '#NOTFOUND-' . $filename . '?idcache=' . $this->id_cache;
    }

    /**
     * Busca en la lista de plugins activos, en orden inverso de prioridad
     * (el último plugin activo tiene más prioridad que el primero)
     * y nos devuelve la ruta del archivo css que le solicitamos.
     * Así usamos el archivo del plugin con mayor prioridad.
     *
     * @param string $filename
     *
     * @return string
     */
    public function get_css_location($filename)
    {
        foreach ($GLOBALS['plugins'] as $plugin) {
            switch (ENGINE) {
                case 'raintpl':
                    if (file_exists('plugins/' . $plugin . '/view/css/' . $filename)) {
                        return constant('FS_PATH') . 'plugins/' . $plugin . '/view/csss/' . $filename . '?idcache=' . $this->id_cache;
                    }
                    break;
                case 'blade':
                    if (file_exists('plugins/' . $plugin . '/Templates/css/' . $filename)) {
                        return constant('FS_PATH') . 'plugins/' . $plugin . '/Templates/css/' . $filename . '?idcache=' . $this->id_cache;
                    }
                    break;
            }
        }

        /// si no está en los plugins estará en el núcleo
        switch (ENGINE) {
            case 'raintpl':
                if (file_exists('view/css/' . $filename)) {
                    return constant('FS_PATH') . 'view/css/' . $filename . '?idcache=' . $this->id_cache;
                }
                return '#NOTFOUND-' . constant('FS_PATH') . 'view/css/' . $filename . '?idcache=' . $this->id_cache;

                break;
            case 'blade':
                if (file_exists('Templates/css/' . $filename)) {
                    return constant('FS_PATH') . 'Templates/css/' . $filename . '?idcache=' . $this->id_cache;
                }
                return '#NOTFOUND-' . constant('FS_PATH') . 'Templates/css/' . $filename . '?idcache=' . $this->id_cache;

                break;
        }
        return '#NOTFOUND-' . $filename . '?idcache=' . $this->id_cache;
    }

    /**
     * Devuelve el tamaño máximo permitido para subir archivos.
     *
     * @return int
     */
    public function get_max_file_upload()
    {
        return fs_get_max_file_upload();
    }

    /**
     * Devuelve la lista de mensajes
     *
     * @return array lista de mensajes
     */
    public function get_messages()
    {
        return $this->core_log->get_messages();
    }

    /**
     * Devuelve la hora actual
     *
     * @return string la hora en formato indicado
     */
    public function hour($format = 'H:i:s')
    {
        return date($format);
    }

    /**
     * Devuelve la fecha actual
     *
     * @return string la fecha en formato indicado
     */
    public function today($format = 'Y-m-d')
    {
        return date($format);
    }

    /**
     * Devuelve la fecha actual
     *
     * @author     Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version    2022.0427
     *
     * @param string $moment
     * @param string $format
     * @param string $diff_time
     *
     * @return string|null
     * @deprecated 2022.0427 Usar form_date en su lugar.
     */
    public function date($moment = 'now', $format = 'Y-m-d', $diff_time = ' + 0 days')
    {
        $date = $moment . $diff_time;
        return form_date($date, $format);
    }

    /**
     * Retorna el nombre de la base de datos.
     * En el caso de Windows, siempre en minúsculas.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0202
     *
     * @return mixed|string
     */
    static public function get_db_name()
    {
        /**
         * En el caso de que exista FS_DB_NAME_SOURCE es porque se desea una migración.
         * En ese caso, el sistema antiguo tomará la base de datos SOURCE y la migración
         * se hará hacia la nueva base de datos.
         *
         * De momento, sólo se está usando para pruebas de migración y tener la base de datos
         * de destino depurada y fácil de gestionar.
         */
        $database_name = defined('FS_DB_NAME_SOURCE')
            ? constant('FS_DB_NAME_SOURCE')
            : constant('FS_DB_NAME');

        if (strtoupper(substr(constant('PHP_OS'), 0, 3)) === 'WIN') {
            return mb_strtolower($database_name);
        }
        return $database_name;
    }

    /**
     * Devuelve la versión de la app.
     * Si está en modo depuración, devuelve la que debería contener el archivo VERSION.
     *
     * @return string
     */
    public function get_app_version()
    {
        $version = (string) number_format((float) $this->version(), 4, '.', '');
        if ($this->is_beta()) {
            $version .= "-beta";
        }
        if (defined('FS_DEBUG') && constant('FS_DEBUG')) {
            $number = date('Y') . '.'
                . str_pad(date('m'), 2, '0', STR_PAD_LEFT)
                . str_pad(date('d'), 2, '0', STR_PAD_LEFT);
            $version_dev = (string) number_format((float) $number, 4, '.', '');
            if ((float) $version < (float) $version_dev) {
                $version .= ' (' . $version_dev . '-dev)';
            }
        }
        return $version;
    }

    /**
     * Devuelve la versión de MiFactura.eu
     *
     * @return float versión de MiFactura.eu
     */
    public function version()
    {
        return (float) file_exists('MiVersion') ? trim(@file_get_contents('MiVersion')) : '0';
    }

    /**
     * Devuelve si la instalación en cuestión está en beta.
     *
     * @return false
     */
    public function is_beta()
    {
        $info = $this->get_community_info();
        return $info && isset($info->beta) ? $info->beta : false;
    }

    /**
     * Devuelve información sobre la comunidad configurada en la instalación.
     *
     * @return false|array|mixed|string|null
     */
    public function get_community_info()
    {
        $result = $this->cache->get('community_info');
        if (!isset($result)) {
            $result = false;
            $communityInfo = get_from_community('/info.json', 30);
            if (!empty($communityInfo)) {
                $this->cache->set('community_info', $communityInfo);
                $result = $communityInfo;
            }
            // No se está pudiendo contactar con la comunidad
        }
        return $result;
    }

    /**
     * He detectado que algunos navegadores, en algunos casos, envían varias veces la
     * misma petición del formulario. En consecuencia se crean varios modelos (asientos,
     * albaranes, etc...) con los mismos datos, es decir, duplicados.
     * Para solucionarlo añado al formulario un campo petition_id con una cadena
     * de texto aleatoria. Al llamar a esta función se comprueba si esa cadena
     * ya ha sido almacenada, de ser así devuelve TRUE, así no hay que gabar los datos,
     * si no, se almacena el ID y se devuelve FALSE.
     *
     * @param string $pid el identificador de la petición
     *
     * @return bool TRUE si la petición está duplicada
     */
    protected function duplicated_petition($pid)
    {
        $ids = $this->cache->get_array('petition_ids');
        if (in_array($pid, $ids)) {
            return true;
        }

        $ids[] = $pid;
        $this->cache->set('petition_ids', $ids, 300);
        return false;
    }
}
