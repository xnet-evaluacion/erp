<?php

/*
 * This file is part of MiFactura.eu
 * Copyright (C) 2021 X-Net Software Solutions S.L. <xnetsoftware@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Esta clase sólo sirve para que los modelos sepan que elementos son los
 * predeterminados para la sesión. Pero para guardar los valores hay que usar
 * las funciones fs_controller::save_lo_que_sea()
 */
class fs_default_items
{
    /**
     * Contiene la página por defecto.
     *
     * @var string
     */
    private static $default_page;

    /**
     * Contiene la página que se está mostrando.
     *
     * @var string
     */
    private static $showing_page;

    /**
     * Contiene el código de ejercicio por defecto.
     *
     * @var string
     */
    private static $codejercicio;

    /**
     * Contiene el código de almacén por defecto.
     *
     * @var string
     */
    private static $codalmacen;

    /**
     * Contiene el código de divisa por defecto.
     *
     * @var string
     */
    private static $coddivisa;

    /**
     * Contiene el código de la forma de pago por defecto.
     *
     * @var string
     */
    private static $codpago;

    /**
     * Contiene el código del impuesto por defecto.
     *
     * @var string
     */
    private static $codimpuesto;

    /**
     * Contiene el código del país por defecto.
     *
     * @var string
     */
    private static $codpais;

    /**
     * Contiene el código de la serie por defecto.
     *
     * @var string
     */
    private static $codserie;

    /**
     * Devuelve el código de ejercicio por defecto.
     *
     * @return string
     */
    public function codejercicio()
    {
        return self::$codejercicio;
    }

    /**
     * Asigna el código de ejercicio por defecto.
     *
     * @param string $cod
     */
    public function set_codejercicio($cod)
    {
        self::$codejercicio = $cod;
    }

    /**
     * Devuelve el código de almacén por defecto.
     *
     * @return string
     */
    public function codalmacen()
    {
        return self::$codalmacen;
    }

    /**
     * Asigna el código de almacén por defecto.
     *
     * @param string $cod
     */
    public function set_codalmacen($cod)
    {
        self::$codalmacen = $cod;
    }

    /**
     * Devuelve el código de divisa por defecto.
     *
     * @return string
     */
    public function coddivisa()
    {
        return self::$coddivisa;
    }

    /**
     * Asigna el código de divisa por defecto.
     *
     * @param string $cod
     */
    public function set_coddivisa($cod)
    {
        self::$coddivisa = $cod;
    }

    /**
     * Devuelve el código de la forma de pago por defecto.
     *
     * @return string
     */
    public function codpago()
    {
        return self::$codpago;
    }

    /**
     * Asigna el código de la forma de pago por defecto.
     *
     * @param string $cod
     */
    public function set_codpago($cod)
    {
        self::$codpago = $cod;
    }

    /**
     * Devuelve el código de impuesto por defecto.
     *
     * @return string
     */
    public function codimpuesto()
    {
        return self::$codimpuesto;
    }

    /**
     * Asigna el código de impuesto por defecto.
     *
     * @param string $cod
     */
    public function set_codimpuesto($cod)
    {
        self::$codimpuesto = $cod;
    }

    /**
     * Devuelve el código de país por defecto.
     *
     * @return string
     */
    public function codpais()
    {
        return self::$codpais;
    }

    /**
     * Asigna el código de país por defecto.
     *
     * @param string $cod
     */
    public function set_codpais($cod)
    {
        self::$codpais = $cod;
    }

    /**
     * Devuelve el código de serie por defecto.
     *
     * @return string
     */
    public function codserie()
    {
        return self::$codserie;
    }

    /**
     * Asigna el código de serie por defecto.
     *
     * @param string $cod
     */
    public function set_codserie($cod)
    {
        self::$codserie = $cod;
    }

    /**
     * Devuelve la página por defecto.
     *
     * @return string
     */
    public function default_page()
    {
        return self::$default_page;
    }

    /**
     * Asigna la página por defecto.
     *
     * @param string $name
     */
    public function set_default_page($name)
    {
        self::$default_page = $name;
    }

    /**
     * Devuelve la página que se está mostrando.
     *
     * @return string
     */
    public function showing_page()
    {
        return self::$showing_page;
    }

    /**
     * Asigna la página que se está mostrando.
     *
     * @param string $name
     */
    public function set_showing_page($name)
    {
        self::$showing_page = $name;
    }
}
