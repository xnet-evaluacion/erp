<?php

/*
 * This file is part of MiFactura.eu
 * Copyright (C) 2021 X-Net Software Solutions S.L. <xnetsoftware@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

require_once constant('BASE_PATH') . '/base/fs_core_log.php';
require_once constant('BASE_PATH') . '/base/fs_cache.php';
require_once constant('BASE_PATH') . '/base/fs_db2.php';
//require_once constant('BASE_PATH') . '/base/fs_functions.php';
require_once constant('BASE_PATH') . '/base/fs_default_items.php';

/**
 * La clase de la que heredan todos los modelos, conecta a la base de datos,
 * comprueba la estructura de la tabla y de ser necesario la crea o adapta.
 */
abstract class fs_model
{
    /**
     * Directorio donde se encuentra el directorio table con
     * el XML con la estructura de la tabla.
     *
     * @var string[]
     */
    private static $base_dir;

    /**
     * Lista de tablas ya comprobadas.
     *
     * @var array
     */
    private static $checked_tables;

    /**
     * Gestiona el log de todos los controladores, modelos y base de datos.
     *
     * @var fs_core_log
     */
    private static $core_log;

    /**
     * Permite conectar e interactuar con memcache.
     *
     * @var fs_cache
     */
    protected $cache;

    /**
     * Proporciona acceso directo a la base de datos.
     * Implementa la clase fs_mysql o fs_postgresql.
     *
     * @var fs_db2
     */
    protected $db;

    /**
     * Clase que se utiliza para definir algunos valores por defecto:
     * codejercicio, codserie, coddivisa, etc...
     *
     * @var fs_default_items
     */
    protected $default_items;

    /**
     * Nombre de la tabla en la base de datos.
     *
     * @var string
     */
    protected $table_name;

    /**
     * True si la tabla tiene el campo created_at
     *
     * @var bool
     */
    public $use_created_at;

    /**
     * True si la tabla tiene el campo updated_at
     *
     * @var bool
     */
    public $use_updated_at;

    /**
     * True si la tabla tiene el campo deleted_at
     *
     * @var bool
     */
    public $use_deleted_at;

    /**
     * Constructor.
     *
     * @param string $table_name nombre de la tabla de la base de datos.
     */
    public function __construct($table_name)
    {
        if (empty($table_name)) {
            echo '<p></p><strong>Empty table_name</strong></p>';
            echo '<pre>' . debug_backtrace() . '</pre>';
            die();
        }

        $this->cache = new fs_cache();
        $this->db = new fs_db2();
        if (constant('FS_DEBUG') && !$this->db->connected()) {
            echo 'Intento de instanciar el modelo de ' . $table_name . ' (' . get_class_name($this) . '), antes de conectar con la base de datos.<br>';
            debug_message(debug_backtrace());
            die('Verifique que no se haya instanciado antes de llamar al constructor del controlador.');
        }

        $this->use_created_at = false;
        $this->use_updated_at = false;
        $this->use_deleted_at = false;
        $columns = $this->db->get_columns($table_name);
        foreach ($columns as $column) {
            if ($column['name'] === 'created_at') {
                $this->use_created_at = ($column['type'] === 'datetime');
                if (!$this->use_created_at) {
                    $this->new_error_msg('La columna created_at tiene que ser de tipo datetime, no de tipo ' . $column['type']);
                }
                continue;
            }
            if ($column['name'] === 'updated_at') {
                $this->use_updated_at = ($column['type'] === 'datetime');
                if (!$this->use_updated_at) {
                    $this->new_error_msg('La columna updated_at tiene que ser de tipo datetime, no de tipo ' . $column['type']);
                }
            }
            if ($column['name'] === 'deleted_at') {
                $this->use_deleted_at = ($column['type'] === 'datetime');
                if (!$this->use_deleted_at) {
                    $this->new_error_msg('La columna deleted_at tiene que ser de tipo datetime, no de tipo ' . $column['type']);
                }
            }
        }

        $this->table_name = $table_name;
        $this->default_items = new fs_default_items();

        // La primera vez que se entre en un modelo, no existirá checked_tables, así que se cargará
        if (!isset(self::$checked_tables)) {
            self::$base_dir = [];
            self::$core_log = new fs_core_log();

            self::$checked_tables = $this->cache->get_array('fs_checked_tables');
            if (!empty(self::$checked_tables)) {
                /// nos aseguramos de que existan todas las tablas que se suponen comprobadas
                foreach (self::$checked_tables as $ckt => $value) {
                    if (!$this->db->table_exists($ckt)) {
                        $this->clean_checked_tables();
                        break;
                    }
                }
            }
        }

        if (!isset(self::$base_dir[$table_name])) {
            $this->get_base_dir($table_name);
        }

        // Sólo revisaremos si se ha limpiado caché.
        if (empty(self::$checked_tables)) {
            $this->check_collation();
        }

        // Si aún no se ha chequeado la tabla, o si se está verificando (porque se llame por segunda vez, o fallase la verificación)
        if (!$this->is_table_checked($table_name)) {
            // Se marca como que se está verificando. Así no se volverá a entrar aquí, y el proceso finalizará por su cuenta
            self::$checked_tables[$table_name] = 'Verifying';
            if ($this->check_table($table_name)) {
                // Si éxito se da por verificada...
                self::$checked_tables[$table_name] = 'Verified';
            }
            // Se guarda el resultado (verificada o no)
            $this->cache->set('fs_checked_tables', self::$checked_tables, 5400);
        }
    }

    /**
     * Borra los datos de las tablas verificadas para proceder a una nueva verificación
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0706
     *
     */
    public static function clear_cache()
    {
        self::$checked_tables = [];
    }

    /**
     * Retorna una cadena para la importación de registros.
     * Si existe un archivo CSV separado por punto y coma, genera la SQL para la importación de datos desde él.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0629
     *
     * @return string
     */
    public function install()
    {
        $result = '';

        $filename = self::$base_dir[$this->table_name] . 'model/seed/' . $this->table_name . '.csv';
        if (!file_exists($filename)) {
            return '';
        }

        $rows = 10; // Indicamos el número de registros que vamos a insertar de una vez
        $handle = fopen($filename, "r");
        if ($handle === false) {
            $this->new_error_msg('No ha sido posible abrir el archivo ' . $filename);
            return '';
        }

        // Asumimos que la primera fila es la cabecera...
        $header = fgetcsv($handle, 0, ';');
        if ($header === false) {
            $this->new_error_msg('No ha sido posible leer la primera línea del archivo ' . $filename);
            fclose($handle);
            return '';
        }

        $sqlHeader = "INSERT INTO `$this->table_name` (`" . implode('`, `', $header) . '`) VALUES ';
        $row = 0;
        $sqlData = [];
        while (($data = fgetcsv($handle, 0, ';')) !== false) {
            // Entrecomillamos lo que no sea null.
            foreach ($data as $key => $datum) {
                if (mb_strtoupper($datum) !== 'NULL') {
                    $data[$key] = "'$datum'";
                }
            }

            if ($row % $rows === 0) {
                if (count($sqlData) > 0) {
                    $result .= ($sqlHeader . implode(', ', $sqlData) . ';' . PHP_EOL);
                }
                $sqlData = [];
            }
            $sqlData[] = '(' . implode(', ', $data) . ')';
            $row++;
        }
        if (count($sqlData) > 0) {
            $result .= ($sqlHeader . implode(', ', $sqlData) . ';' . PHP_EOL);
        }
        fclose($handle);

        return $result;
    }

    /**
     * Realizamos algunas correcciones a la base de datos y devuelve su estado.
     * Principalmente debe corregir fallos en relaciones entre tablas.
     * NOTA: Algunos controladores llaman a este método, así que las consultas deben ser lo más eficientes posibles
     * hasta que se reemplace por otra forma.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2021.09
     *
     * @return bool
     */
    public function fix_db()
    {
        return true;
    }

    /**
     * Esta función sirve para eliminar los datos del objeto de la base de datos
     */
    abstract public function delete();

    /**
     * Esta función devuelve TRUE si los datos del objeto se encuentran
     * en la base de datos.
     */
    abstract public function exists();

    /**
     * Esta función sirve tanto para insertar como para actualizar
     * los datos del objeto en la base de datos.
     */
    abstract public function save();

    /**
     * Vacía la lista de errores de los modelos.
     */
    public function clean_errors()
    {
        self::$core_log->clean_errors();
    }

    /**
     * Vacía la lista de mensajes de los modelos.
     */
    public function clean_messages()
    {
        self::$core_log->clean_messages();
    }

    /**
     * Compara dos números en coma flotante con una precisión de $precision,
     * devuelve TRUE si son iguales, FALSE en caso contrario.
     *
     * @param float $f1
     * @param float $f2
     * @param int   $precision
     * @param bool  $round
     *
     * @return bool
     */
    public function floatcmp($f1, $f2, $precision = 10, $round = false)
    {
        if ($round || !function_exists('bccomp')) {
            return (abs($f1 - $f2) < 6 / pow(10, $precision + 1));
        }

        return (bccomp((string) $f1, (string) $f2, $precision) == 0);
    }

    /**
     * Devuelve la lista de mensajes de error de los modelos.
     *
     * @return array lista de errores.
     */
    public function get_errors()
    {
        return self::$core_log->get_errors();
    }

    /**
     * Devuelve la lista de mensajes de los modelos.
     *
     * @return array
     */
    public function get_messages()
    {
        return self::$core_log->get_messages();
    }

    /**
     * Devuelve el valor entero de la variable $s,
     * o NULL si es NULL. La función intval() del php devuelve 0 si es NULL.
     *
     * @param string $str
     *
     * @return int
     */
    public function intval($str)
    {
        return ($str === null) ? null : (int) $str;
    }

    /**
     * Esta función convierte:
     * < en &lt;
     * > en &gt;
     * " en &quot;
     * ' en &#39;
     *
     * No tengas la tentación de sustituirla por htmlentities o htmlspecialshars
     * porque te encontrarás con muchas sorpresas desagradables.
     *
     * TODO: Al utilizar el dato almacenado con este método, debe deshacerse con fs_fix_html(). Debería hacerse esto en el constructor para evitar problemas.
     *
     * @param string $txt
     *
     * @return string
     */
    public function no_html($txt)
    {
        $newt = str_replace(['<', '>', '"', "'",], ['&lt;', '&gt;', '&quot;', '&#39;',], $txt);

        return trim($newt);
    }

    /**
     * PostgreSQL guarda los valores TRUE como 't', MySQL como 1.
     * Esta función devuelve TRUE si el valor se corresponde con
     * alguno de los anteriores.
     *
     * @param string $val
     *
     * @return bool
     */
    public function str2bool($val)
    {
        return ($val == 't' || $val == '1');
    }

    /**
     * Transforma una variable en una cadena de texto válida para ser
     * utilizada en una consulta SQL.
     *
     * @param mixed $val
     *
     * @return string
     */
    public function var2str($val)
    {
        if (is_null($val)) {
            return 'NULL';
        } elseif (is_bool($val)) {
            return $val ? 'TRUE' : 'FALSE';
        } elseif (preg_match('/^([0-9]{1,2})-([0-9]{1,2})-([0-9]{4})$/i', $val)) {
            /// es una fecha
            return "'" . date($this->db->date_style(), strtotime($val)) . "'";
        } elseif (preg_match('/^([0-9]{4})-([0-9]{1,2})-([0-9]{1,2}) ([0-9]{1,2}):([0-9]{1,2}):([0-9]{1,2})$/i', $val)) {
            /// es una fecha+hora
            return "'" . date($this->db->date_style() . ' H:i:s', strtotime($val)) . "'";
        }

        return $this->db->escape_string($val);
    }

    /**
     * Devuelve el campo descriptivo.
     *
     * @return string
     */
    public function get_description()
    {
        return get_called_class() . ' no tiene get_description';
    }

    /**
     * Devuelve el nombre de la tabla de este modelo.
     *
     * @return string
     */
    public function table_name()
    {
        return $this->table_name;
    }

    /**
     * Devuelve si la tabla está comprobada.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2021.10
     *
     * @param string $table_name
     *
     * @return bool
     */
    public function is_table_checked($table_name)
    {
        return isset(self::$checked_tables[$table_name]);
    }

    /**
     * Ejecuta las consultas correspondientes.
     * Si no pudieran ejecutarse, se hará un rollback.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2021.10
     *
     * @param string[][] $fixes [
     *                          'table_name_1' => ['query1','query2' ],
     *                          'table_name_2' => ['query1','query2' ],
     *                          ]
     *
     * @return bool
     */
    public function exec_fix_queries($fixes = [])
    {
        $status = true;
        $sqls = [];
        foreach ($fixes as $table => $queries) {
            if ($this->db->table_exists($table)) {
                $constraints = [];
                foreach ($this->db->get_constraints($table) as $constraint) {
                    if ($constraint['type'] == "FOREIGN KEY") {
                        $constraints[] = $constraint['name'];
                    }
                }

                if (isset($constraint) && is_numeric($constraint)) {
                    $sqls[] = $query;
                } else {
                    foreach ($queries as $constraint => $query) {
                        if (!in_array($constraint, $constraints)) {
                            $sqls[] = $query;
                        }
                    }
                }
            }
        }

        if (!empty($sqls)) {
            $this->db->begin_transaction();
            $status = $this->db->exec(implode('', $sqls));
            if ($status) {
                $this->db->commit();
            } else {
                $this->db->rollback();
            }
        }

        return $status;
    }

    /**
     * Devuelve los datos como array de objetos.
     *
     * @param string $sql
     * @param int    $offset
     * @param int    $limit
     *
     * @return static[]
     */
    public function all_from($sql, $offset = 0, $limit = FS_ITEM_LIMIT)
    {
        $sql = rtrim($sql, ';');
        $data = $limit > 0 ? $this->db->select_limit($sql, $limit, $offset) : $this->db->select($sql . ';');
        return $this->all_from_data($data);
    }

    /**
     * Limpia la lista de tablas comprobadas.
     */
    protected function clean_checked_tables()
    {
        self::$checked_tables = [];
        $this->cache->delete('fs_checked_tables');
    }

    /**
     * Comprueba si hay cambios de charset/collation y los aplica si son necesarios.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2021.09
     *
     */
    protected function check_collation()
    {
        $this->db->update_collation();
    }

    /**
     * Comprueba y actualiza la estructura de la tabla si es necesario
     *
     * @param string $table_name
     *
     * @return bool
     */
    protected function check_table($table_name)
    {
        $sql = '';
        $xml_cols = [];
        $xml_cons = [];

        if (!$this->get_xml_table($table_name, $xml_cols, $xml_cons)) {
            $this->new_error_msg('Error con el xml.');
            return false;
        }

        // Se instancian modelos dependientes y se ejecutan sus correspondientes fix_db()
        $this->check_model_dependencies();

        if ($this->db->table_exists($table_name)) {
            if (!$this->db->check_table_aux($table_name)) {
                $this->new_error_msg('Error al convertir la tabla a InnoDB.');
            }

            /**
             * Si hay que hacer cambios en las restricciones, eliminamos todas las restricciones,
             * luego añadiremos las correctas. Lo hacemos así porque evita problemas en MySQL.
             */
            $db_cons = $this->db->get_constraints($table_name);
            $sql2 = $this->db->compare_constraints($table_name, $xml_cons, $db_cons, true);
            if ($sql2 != '') {
                if (!$this->db->exec($sql2)) {
                    $this->new_error_msg('Error al comprobar la tabla ' . $table_name);
                }

                /// leemos de nuevo las restricciones
                $db_cons = $this->db->get_constraints($table_name);
            }

            /// comparamos las columnas
            $db_cols = $this->db->get_columns($table_name, true);
            $sql .= $this->db->compare_columns($table_name, $xml_cols, $db_cols);

            /// comparamos las restricciones
            $sql .= $this->db->compare_constraints($table_name, $xml_cons, $db_cons);
        } else {
            /// generamos el sql para crear la tabla
            $sql .= $this->db->generate_table($table_name, $xml_cols, $xml_cons);
            $sql .= $this->install();
        }

        // Probamos a corregir datos antes de aplicar cambios
        if (!$this->fix_model_table_before()) {
            $this->new_error_msg('Error al reparar datos de la tabla ' . $table_name . ' antes de actualizarla.');
        }

        if (!empty($sql) && !$this->db->exec($sql)) {
            $this->new_error_msg('Error al comprobar la tabla ' . $table_name . ' después de actualizarla.');
            return false;
        }

        // Probamos a corregir datos después de aplicar cambios
        if (!$this->fix_model_table_after()) {
            $this->new_error_msg('Error al reparar datos de la tabla ' . $table_name);
        }

        return true;
    }

    /**
     * Obtiene las columnas y restricciones del fichero xml para una tabla
     *
     * @param string $table_name
     * @param array  $columns
     * @param array  $constraints
     *
     * @return bool
     */
    protected function get_xml_table($table_name, &$columns, &$constraints)
    {
        $filename = self::$base_dir[$table_name] . 'model/table/' . $table_name . '.xml';
        if (!file_exists($filename)) {
            $this->new_error_msg('Archivo ' . $filename . ' no encontrado.');
            return false;
        }

        $xml = simplexml_load_string(file_get_contents('./' . $filename, FILE_USE_INCLUDE_PATH));
        if (!$xml) {
            $this->new_error_msg('Error al leer el archivo ' . $filename);
            return false;
        }

        $return = false;
        if ($xml->columna) {
            $i = 0;
            foreach ($xml->columna as $col) {
                $columns[$i]['nombre'] = (string) $col->nombre;
                $columns[$i]['tipo'] = (string) $col->tipo;

                $columns[$i]['nulo'] = 'YES';
                if ($col->nulo && mb_strtolower($col->nulo) == 'no') {
                    $columns[$i]['nulo'] = 'NO';
                }

                if ($col->defecto == '') {
                    $columns[$i]['defecto'] = null;
                } else {
                    $columns[$i]['defecto'] = (string) $col->defecto;
                }

                if (isset($col->description)) {
                    debug_message('Cambie la etiqueta <description> por comentario en ' . $col->nombre . ' de ' . $table_name);
                    $columns[$i]['comentario'] = $col->description;
                } elseif (isset($col->comment)) {
                    debug_message('Cambie la etiqueta <comment> por comentario en ' . $col->nombre . ' de ' . $table_name);
                    $columns[$i]['comentario'] = $col->comment;
                } elseif (isset($col->comentario)) {
                    $columns[$i]['comentario'] = $col->comentario;
                }

                $i++;
            }

            /// debe de haber columnas, si no es un fallo
            $return = true;
        }

        if ($xml->restriccion) {
            $i = 0;
            foreach ($xml->restriccion as $col) {
                $constraints[$i]['nombre'] = (string) $col->nombre;
                $constraints[$i]['consulta'] = (string) $col->consulta;
                $i++;
            }
        }

        return $return;
    }

    /**
     * Muestra al usuario un mensaje de error
     *
     * @param string $msg mensaje de error
     */
    protected function new_error_msg($msg)
    {
        if (constant('FS_DEBUG')) {
            self::$core_log->new_error('<b>Error en el modelo <em>' . get_called_class() . '</em>:</b> ' . $msg);
        }
        self::$core_log->save($msg);
    }

    /**
     * Si un modelo depende de otro, en este método deben hacerse las correspondientes instanciaciones
     * A su vez, debe invocar al fix_db de dicho modelo dependiente, para poder corregir al vuelo posibles errores que
     * se hubieran detectado con el tiempo.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2021.09
     *
     * @return bool
     */
    protected function check_model_dependencies()
    {

    }

    /**
     * Revisa si hay instantes de creación o actualización a null, poniéndolos al instante actual UTC si los hay.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0629
     *
     */
    private function updateCreationDatetimes()
    {
        if (!$this->use_created_at && !$this->use_updated_at) {
            return true;
        }

        $sql = '';
        if ($this->use_created_at) {
            $sql .= 'UPDATE ' . $this->table_name . ' SET created_at = "' . gmdate('Y-m-d H:i:s') . '" WHERE created_at IS NULL;';
        }
        if ($this->use_updated_at) {
            $sql .= 'UPDATE ' . $this->table_name . ' SET updated_at = "' . gmdate('Y-m-d H:i:s') . '" WHERE updated_at IS NULL;';
        }

        return $this->db->exec($sql);
    }

    /**
     * Si una tabla de un modelo tiene problemas a nivel SQL que corregir, deben ejecutarse aquí.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2021.09
     *
     * @return bool
     */
    protected function fix_model_table_before()
    {
        return $this->fix_db();
    }

    /**
     * Si una tabla de un modelo tiene problemas a nivel SQL que corregir, deben ejecutarse aquí.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2022.0426
     *
     * @return bool
     */
    protected function fix_model_table_after()
    {
        return $this->updateCreationDatetimes();
    }

    /**
     * Convierte una variable con contenido binario a texto.
     * Lo hace en base64.
     *
     * @param mixed $val
     *
     * @return string
     */
    protected function bin2str($val)
    {
        return is_null($val) ? 'NULL' : "'" . base64_encode($val) . "'";
    }

    /**
     * Devuelve un array con todas las fechas entre $first y $last.
     *
     * @param string $first
     * @param string $last
     * @param string $step
     * @param string $format
     *
     * @return array
     */
    protected function date_range($first, $last, $step = '+1 day', $format = 'Y-m-d')
    {
        $dates = [];
        $current = strtotime($first);
        $last = strtotime($last);

        while ($current <= $last) {
            $dates[] = date($format, $current);
            $current = strtotime($step, $current);
        }

        return $dates;
    }

    /**
     * Escapa las comillas de una cadena de texto.
     *
     * @param string $str cadena de texto a escapar
     *
     * @return string cadena de texto resultante
     */
    protected function escape_string($str = '')
    {
        return $this->db->escape_string($str);
    }

    /**
     * Muestra al usuario un consejo.
     *
     * @param string $msg
     */
    protected function new_advice($msg)
    {
        if (constant('FS_DEBUG')) {
            self::$core_log->new_advice('<b>Aviso en el modelo <em>' . get_called_class() . '</em>:</b> ' . $msg);
        }
        self::$core_log->save($msg, 'advice');
    }

    /**
     * Muestra al usuario un mensaje.
     *
     * @param string $msg
     */
    protected function new_message($msg)
    {
        if (constant('FS_DEBUG')) {
            self::$core_log->new_message('<b>Mensaje en el modelo <em>' . get_called_class() . '</em>:</b> ' . $msg);
        }
        self::$core_log->save($msg, 'message');
    }

    /**
     * Devuelve una cadena de texto aleatorio de longitud $length
     *
     * @param int $length
     *
     * @return string
     */
    protected function random_string($length = 10)
    {
        return mb_substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
    }

    /**
     * Convierte un texto a binario.
     * Lo hace con base64.
     *
     * @param string $val
     *
     * @return null|string
     */
    protected function str2bin($val)
    {
        return is_null($val) ? null : base64_decode($val);
    }

    /**
     * Devuelve los datos como array de objetos haciendo uso de la caché.
     * IMPORTANTE: El uso de este método implica que el modelo final debe tener el método clean_cache definido.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2021.12
     *
     * @param string $name
     * @param string $sql
     * @param int    $offset
     * @param int    $limit
     *
     * @return static[]
     */
    protected function all_from_cached($name, $sql, $offset = 0, $limit = 0)
    {
        // Leemos esta lista de la caché
        $list = $this->cache->get_array($name);
        if (empty($list)) {
            // Si no está en caché, leemos de la base de datos
            $list = $this->all_from($sql, $offset, $limit);
            // Guardamos la lista en caché
            $this->cache->set($name, $list);
        }
        return $list;
    }

    /**
     * Devuelve los datos como objeto haciendo uso de la caché.
     * IMPORTANTE: El uso de este método implica que el modelo final debe tener el método clean_cache definido.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2021.12
     *
     * @param string $name
     * @param string $sql
     *
     * @return false|static
     */
    protected function get_from_cached_pk($name, $sql)
    {
        // Leemos el elemento de la caché
        $item = $this->cache->get($name);
        if (!empty($item)) {
            return new static($item);
        }
        if (empty($item)) {
            // Si no está en caché, leemos de la base de datos
            $data = $this->db->select($sql);
            if (isset($data[0])) {
                // Guardamos el elemento en caché
                $this->cache->set($name, $data[0]);
                return new static($data[0]);
            }
        }
        return false;
    }

    /**
     * Devuelve los datos como array de objetos.
     *
     * @param array $data
     *
     * @return static[]
     */
    protected function all_from_data(&$data)
    {
        $list = [];
        if ($data) {
            foreach ($data as $a) {
                $list[] = new static($a);
            }
        }
        return $list;
    }

    /**
     * Buscamos el xml de la tabla en los plugins
     *
     * @param string $table_name
     */
    private function get_base_dir($table_name)
    {
        self::$base_dir[$table_name] = '';
        foreach ($GLOBALS['plugins'] as $plugin) {
            if (file_exists('plugins/' . $plugin . '/model/table/' . $table_name . '.xml')) {
                self::$base_dir[$table_name] = 'plugins/' . $plugin . '/';
                break;
            }
        }
    }
}
