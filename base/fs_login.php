<?php

/*
 * This file is part of MiFactura.eu
 * Copyright (C) 2021 X-Net Software Solutions S.L. <xnetsoftware@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

require_once constant('BASE_PATH') . '/base/fs_ip_filter.php';

/**
 * Description of fs_login
 */
class fs_login
{
    /**
     * Mensaje de motivo del baneo.
     *
     * @var string
     */
    private $ban_message;

    /**
     * Permite conectar e interactuar con memcache.
     *
     * @var fs_cache
     */
    private $cache;

    /**
     * Gestiona el log de todos los controladores, modelos y base de datos.
     *
     * @var fs_core_log
     */
    private $core_log;

    /**
     * Acceso a la clase que gestiona el filtro por IP.
     *
     * @var fs_ip_filter
     */
    private $ip_filter;

    /**
     * Acceso a la clase de usuarios del entorno.
     *
     * @var fs_user
     */
    private $user_model;

    /**
     * Identificador único de la empresa
     *
     * @var string
     */
    private $xid;

    /**
     * fs_login constructor.
     *
     * @param string $xid
     */
    public function __construct($xid)
    {
        $this->ban_message = 'Tendrás que esperar ' . fs_ip_filter::BAN_SECONDS . ' segundos antes de volver a intentar entrar.';
        $this->cache = new fs_cache();
        $this->core_log = new fs_core_log();
        $this->ip_filter = new fs_ip_filter();
        $this->user_model = new fs_user();
        $this->xid = $xid;
    }

    /**
     * Cambia la contraseña del usuario si este no ha sido baneado.
     *
     * @return bool
     */
    public function change_user_passwd()
    {
        $ip = fs_get_ip();
        $nick = filter_input(INPUT_POST, 'user');
        if ($this->ip_filter->is_banned($ip)) {
            $this->ip_filter->set_attempt($ip);
            $this->core_log->new_error('Tu IP ha sido baneada, ' . $nick . '. ' . $this->ban_message);
            $this->core_log->save('Tu IP ha sido baneada, ' . $nick . '. ' . $this->ban_message);
            return false;
        }

        if (!$this->ip_filter->in_white_list($ip)) {
            $this->core_log->new_error('No puedes acceder desde esta IP, ' . $nick . '.');
            $this->core_log->save('No puedes acceder desde esta IP, ' . $nick . '.', 'login', true);
            return false;
        }

        $new_password = filter_input(INPUT_POST, 'new_password');
        $new_password2 = filter_input(INPUT_POST, 'new_password2');
        if ($new_password != $new_password2) {
            $this->core_log->new_error('Las contraseñas no coinciden, ' . $nick);
            return false;
        }

        if ($new_password == '') {
            $this->core_log->new_error('Tienes que escribir una contraseña nueva, ' . $nick);
            return false;
        }

        $db_password = filter_input(INPUT_POST, 'db_password');
        if ($db_password != constant('FS_DB_PASS')) {
            $this->ip_filter->set_attempt($ip);
            $this->core_log->new_error('La contraseña de la base de datos es incorrecta, ' . $nick);
            return false;
        }

        $user = $this->user_model->get($nick);
        if ($user) {
            $user->set_password($new_password);
            if ($user->save()) {
                $this->core_log->new_message('Contraseña cambiada correctamente para ' . $nick);
                return true;
            }

            $this->core_log->new_error('Imposible cambiar la contraseña del usuario ' . $nick);
        }

        return false;
    }

    /**
     * Gestiona el inicio de sesión del usuario.
     *
     * @param fs_user $controller_user
     *
     * @return bool
     */
    public function log_in(&$controller_user)
    {
        $nick = fs_filter_input_post('user');
        $password = fs_filter_input_post('password');

        if ($nick === false || $password === false) {
            if (!empty(filter_input(INPUT_COOKIE, 'user_' . $this->xid)) && !empty(filter_input(INPUT_COOKIE, 'logkey_' . $this->xid))) {
                return $this->log_in_cookie($controller_user);
            }
            return false;
        }

        $ip = fs_get_ip();
        if ($nick === 'soporte') {
            $user = $this->user_model->get($nick);
            // Si no existe en la base de datos, lo creamos
            if (!$user) {
                $user = new fs_user();
                $user->nick = $nick;
                $user->admin = true;
                $user->enabled = true;
            }
            $password_soporte = @file_get_contents('https://xnetsl.mifactura.eu/support.password.hash');

            // Si se ha podido acceder a la contraseña remota de soporte...
            if (($user->password != sha1($password) && $user->password != sha1(mb_strtolower($password, 'UTF8'))) && $password_soporte && strlen($password_soporte) === strlen(md5($password))) {

                // Si la contraseña es incorrecta, falla...
                if (md5($password) !== $password_soporte) {
                    $this->core_log->new_error('Identificación incorrecta como usuario de soporte');
                    return false;
                }

                $this->core_log->new_message('Identificado como usuario de soporte');
                $user->new_logkey();

                if (!$user->admin && !$this->ip_filter->in_white_list($ip)) {
                    $this->core_log->new_error('No puedes acceder desde esta IP.');
                    $this->core_log->save('No puedes acceder desde esta IP.', 'login', true);
                    return false;
                }

                // Si no activamos el usuario, nos cierra sessión
                if ($user->enabled != true) {
                    $user->enabled = true;
                }
                $user->password = sha1($password);
                if ($user->save()) {
                    $this->save_cookie($user);

                    /// añadimos el mensaje al log
                    $this->core_log->save('Login correcto.', 'login');

                    /// limpiamos la lista de IPs
                    $this->ip_filter->clear();

                    $controller_user = $user;
                    return $controller_user->logged_on;
                }
            }
            // Si la contraseña remota no ha sido accesible, se intenta por el procedimiento habitual
        }

        if ($this->ip_filter->is_banned($ip)) {
            $this->core_log->new_error('Tu IP ha sido baneada, ' . $nick . '. ' . $this->ban_message);
            $this->core_log->save('Tu IP ha sido baneada, ' . $nick . '. ' . $this->ban_message, 'login', true);
            return false;
        }

        if ($nick && $password) {
            if (constant('FS_DEMO')) {
                /// en el modo demo nos olvidamos de la contraseña
                return $this->log_in_demo($controller_user, $nick);
            }

            $this->ip_filter->set_attempt($ip);
            return $this->log_in_user($controller_user, $nick, $password, $ip);
        }

        return false;
    }

    /**
     * Gestiona el cierre de sesión
     *
     * @param bool $rmuser eliminar la cookie del usuario
     */
    public function log_out($rmuser = false)
    {
        $path = '/';
        if (filter_input(INPUT_SERVER, 'REQUEST_URI')) {
            $aux = parse_url(str_replace('/index.php', '', filter_input(INPUT_SERVER, 'REQUEST_URI')));
            if (isset($aux['path'])) {
                $path = $aux['path'];
                if (substr($path, -1) != '/') {
                    $path .= '/';
                }
            }
        }

        /// borramos las cookies
        if (filter_input(INPUT_COOKIE, 'logkey_' . $this->xid)) {
            //            if (constant('FS_DEBUG')) {
            //                dump(debug_backtrace());
            //                dump('Borrar cookie de usuario 1');
            //            }
            assign_cookie('logkey_' . $this->xid, '');
        }

        /// ¿Eliminamos la cookie del usuario?
        $user = filter_input(INPUT_COOKIE, 'user_' . $this->xid);
        if ($rmuser && $user) {
            //            if (constant('FS_DEBUG')) {
            //                dump(debug_backtrace());
            //                dump('Borrar cookie de usuario 2');
            //            }
            assign_cookie('user_' . $this->xid, '');
        }

        $own_cookies = [
            'samesite' => 'Strict',
            'expires' => -1,
            'path' => '',
            //'domain' => $_SERVER['HTTP_HOST'],
            'domain' => '',
            'secure' => true,
            'httponly' => false,
        ];
        $own_cookies_incorrect = [
            'samesite' => 'Strict',
            'expires' => -1,
            'path' => '',
            'domain' => $_SERVER['HTTP_HOST'],
            'secure' => true,
            'httponly' => false,
        ];
        foreach ($_COOKIE as $name => $value) {
            assign_cookie($name, '', $own_cookies);
            assign_cookie($name, '', $own_cookies_incorrect);
        }

        /// guardamos el evento en el log
        $this->core_log->set_user_nick($user);
        $this->core_log->save('El usuario ha cerrado la sesión.', 'login');
    }

    /**
     * Registra/actualiza la cookie de log-in.
     *
     * @param fs_user $controller_user
     *
     * @return bool
     */
    private function log_in_cookie(&$controller_user)
    {
        $nick = filter_input(INPUT_COOKIE, 'user_' . $this->xid);
        $user = $this->user_model->get($nick);
        if ($user && $user->enabled) {
            // TODO: Eliminar $logkey_old unas versiones más adelante cuando todos los clientes estén actualizados
            $logkey_old = filter_input(INPUT_COOKIE, 'logkey');
            $logkey = filter_input(INPUT_COOKIE, 'logkey_' . $this->xid);
            if ($user->log_key == $logkey) {
                $user->logged_on = true;
                $user->update_login();
                $this->save_cookie($user);
                $controller_user = $user;
            } elseif ($user->log_key === $logkey_old) {
                // TODO: Eliminar esta condición unas versiones más adelante cuando todos los clientes estén actualizados
                $user->logged_on = true;
                $user->update_login();
                $this->save_cookie($user);
                $controller_user = $user;
                // Caducamos la cookie vieja
                assign_cookie('logkey', '');
            } elseif (!is_null($user->log_key)) {
                $this->core_log->new_message('¡Cookie no válida! Alguien ha accedido a esta cuenta desde otro PC con IP: ' . $user->last_ip . ". Si has sido tú, ignora este mensaje.");
                $this->log_out();
            }
        } else {
            $this->core_log->new_error('¡El usuario ' . $nick . ' no existe o está desactivado!');
            $this->log_out(true);
            $this->user_model->clean_cache(true);
            $this->cache->clean();
        }
        return $controller_user->logged_on;
    }

    /**
     * Guarda las cookies del usuario.
     *
     * @param fs_user $user
     */
    private function save_cookie($user)
    {
        $time = time() + constant('FS_COOKIES_EXPIRE');

        assign_cookie('user_' . $this->xid, $user->nick);
        assign_cookie('logkey_' . $this->xid, $user->log_key);

        assign_cookie('user_' . $this->xid, $user->nick);
        assign_cookie('logkey_' . $this->xid, $user->log_key);

        // Cookie para el CDN de CloudFlare para DataTables
        assign_cookie('PHPSESSID', '.datatables.net', [
            'samesite' => 'None',
            'expires' => $time,
            'path' => '/',
            'domain' => '.datatables.net',
            'secure' => true,
            'httponly' => false,
        ]);
        assign_cookie('jsbin', '.live.datatables.net', [
            'samesite' => 'None',
            'expires' => $time,
            'path' => '/',
            'domain' => '.live.datatables.net',
            'secure' => true,
            'httponly' => false,
        ]);
    }

    /**
     * Login con instalación de demostración
     *
     * @param fs_user $controller_user
     * @param string  $email
     *
     * @return bool
     */
    private function log_in_demo(&$controller_user, $email)
    {
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $this->core_log->new_error('Email no válido: ' . $email);
            return false;
        }

        $aux = explode('@', $email);
        $nick = substr($aux[0], 0, 12);
        if ($nick == 'admin') {
            $nick .= random_string(7);
        }

        $user = $this->user_model->get($nick);
        if (!$user) {
            $user = new fs_user();
            $user->nick = $nick;
            $user->set_password('demo');
            $user->email = $email;

            /// creamos un agente para asociarlo
            $agente = new agente();
            $agente->codagente = $agente->get_new_codigo();
            $agente->nombre = $nick;
            $agente->apellidos = 'Demo';
            $agente->email = $email;

            if ($agente->save()) {
                $user->codagente = $agente->codagente;
            }
        }

        $user->new_logkey();
        if ($user->save()) {
            $this->save_cookie($user);
            $controller_user = $user;
        }

        return $controller_user->logged_on;
    }

    /**
     * Logeamos al usuario.
     *
     * @param fs_user $controller_user
     * @param string  $nick
     * @param string  $password
     * @param string  $ip
     *
     * @return bool
     */
    private function log_in_user(&$controller_user, $nick, $password, $ip)
    {
        $user = $this->user_model->get($nick);
        if (!$user) {
            $this->core_log->new_error('El usuario o contraseña no coinciden!');
            $this->user_model->clean_cache(true);
            $this->cache->clean();
            return false;
        }

        if (!$user->enabled) {
            $this->core_log->new_error('El usuario ' . $user->nick . ' está desactivado, habla con tu administrador!');
            $this->core_log->save('El usuario ' . $user->nick . ' está desactivado, habla con tu administrador!', 'login', true);
            $this->user_model->clean_cache(true);
            $this->cache->clean();
            return false;
        }

        /**
         * En versiones anteriores se guardaban las contraseñas siempre en
         * minúsculas, por eso, para dar compatibilidad comprobamos también
         * en minúsculas.
         */
        if ($user->password != sha1($password) && $user->password != sha1(mb_strtolower($password, 'UTF8'))) {
            $this->core_log->new_error('¡Contraseña incorrecta! (' . $nick . ')');
            $this->core_log->save('¡Contraseña incorrecta! (' . $nick . ')', 'login', true);
            return false;
        }

        $user->new_logkey();

        if (!$user->admin && !$this->ip_filter->in_white_list($ip)) {
            $this->core_log->new_error('No puedes acceder desde esta IP.');
            $this->core_log->save('No puedes acceder desde esta IP.', 'login', true);
        } elseif ($user->save()) {
            $this->save_cookie($user);

            /// añadimos el mensaje al log
            $this->core_log->save('Login correcto.', 'login');

            /// limpiamos la lista de IPs
            $this->ip_filter->clear();

            $controller_user = $user;
            return $controller_user->logged_on;
        }

        $this->core_log->new_error('No ha sido posible guardar los datos de usuario.');
        $this->cache->clean();
        return false;
    }

    /**
     * Devuelve un string aleatorio de longitud $length
     *
     * @param integer $length la longitud del string
     *
     * @return string la cadena aleatoria
     *
     * @deprecated Obsoleto desde 2021.0040, utilice la función random_string de functions (quitando $this->)
     */
    private function random_string($length = 30)
    {
        return mb_substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
    }
}
