<?php

/*
 * This file is part of MiFactura.eu
 * Copyright (C) 2021 X-Net Software Solutions S.L. <xnetsoftware@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * La clase principal de la que deben heredar todos los controladores (las páginas) de MiFactura.eu.
 */
abstract class fs_license_controller extends fs_controller
{
    /**
     * fs_license_controller constructor.
     *
     * @param string $name      sustituir por __CLASS__
     * @param string $title     es el título de la página, y el texto que aparecerá en el menú
     * @param string $folder    es el menú dónde quieres colocar el acceso directo
     * @param bool   $admin     OBSOLETO
     * @param bool   $shmenu    debe ser TRUE si quieres añadir el acceso directo en el menú
     * @param bool   $important debe ser TRUE si quieres que aparezca en el menú de destacado
     */
    public function __construct($name = __CLASS__, $title = 'home', $folder = ['Oculto'], $admin = false, $shmenu = true, $important = false)
    {
        parent::__construct($name, $title, $folder, $admin, $shmenu, $important);
    }

    /**
     * Código que se ejecutará en la parte privada
     */
    protected function private_core()
    {
        $this->template = "block/license";

        if ($this->is_cluf_accepted()) {
            $this->wizard_start();

            $this->check_menu($this->getDir());

            $this->run_wizard();

            $this->wizard_finish();
            if ($this->is_wizard_finished()) {
                $this->wizard_finished();
            }

            header('Location: ' . $this->getNewUrl());
        }
    }

    /**
     * Devuelve TRUE si el CLUF ha sido aceptado ahora o anteriormente o FALSE si nunca.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2021.06
     * @return bool
     *
     */
    public function is_cluf_accepted(): bool
    {
        $status = false;
        $fsvar = new fs_var();
        $continuar = (bool) fs_filter_input_req('continuar', (bool) $fsvar->simple_get($this->getCLUFName()));
        if ($continuar) {
            $fsvar->simple_save($this->getCLUFName(), date('Y/m/d H:i:s'));
            $status = true;
        } elseif ($fsvar->simple_get($this->getCLUFName())) {
            $status = true;
        }
        return $status;
    }

    /**
     * Devuelve el título principal de la página.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2022.0503
     *
     * @return string
     */
    abstract public function getTitlePrimary(): string;

    /**
     * Devuelve el título secundario de la página.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2022.0503
     *
     * @return string
     */
    abstract public function getTitleSecondary(): string;

    /**
     * Se ejecuta durante la ejecución del wizard.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2022.0510
     *
     * @return string
     */
    abstract protected function run_wizard();

    /**
     * Devuelve la ruta al archivo de licencia.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2022.0503
     *
     * @return string
     */
    abstract public function getLicenseFilePath(): string;

    /**
     * Devuelve el texto contenido en el archivo de licencia.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2022.0503
     *
     * @return string
     */
    public function getLicenseText(): string
    {
        $license = file_get_contents($this->getLicenseFilePath());
        $changes = [
            PHP_EOL . PHP_EOL => '2br',
            PHP_EOL => ' ',
            '2br' => PHP_EOL . PHP_EOL,
        ];
        $searches = [];
        $replaces = [];
        foreach ($changes as $search => $replace) {
            $searches[] = $search;
            $replaces[] = $replace;
        }

        return str_replace($searches, $replaces, $license);
    }

    /**
     * Devuelve el texto para el copyright.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2022.0503
     *
     * @return string
     */
    abstract public function getCopyright(): string;

    /**
     * Devuelve el texto reservado.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2022.0503
     *
     * @return string
     */
    abstract public function getReserved(): string;

    /**
     * Devuelve el nombre de la variable para la licencia.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2022.0503
     *
     * @return string
     */
    abstract public function getCLUFName(): string;

    /**
     * Devuelve la nueva URL donde redireccionar cuando la licencia es aceptada.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2022.0503
     *
     * @return string
     */
    abstract public function getNewUrl(): string;

    /**
     * Devuelve la nueva URL donde redireccionar cuando la licencia es aceptada.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2022.0503
     *
     * @return string
     */
    abstract public function getDir(): string;
}
