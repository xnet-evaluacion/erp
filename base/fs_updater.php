<?php

/*
 * This file is part of MiFactura.eu
 * Copyright (C) 2021 X-Net Software Solutions S.L. <xnetsoftware@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

require_once constant('BASE_PATH') . '/base/fs_app.php';
require_once constant('BASE_PATH') . '/base/fs_plugin_manager.php';
require_once constant('BASE_PATH') . '/base/fs_log_manager.php';

/**
 * Define la ruta donde se realizará la descompresión del nucleo
 */
define('FS_UNPACK_FOLDER', constant('FS_FOLDER') . DIRECTORY_SEPARATOR . 'tmp' . DIRECTORY_SEPARATOR . constant('FS_TMP_NAME') . 'updates' . DIRECTORY_SEPARATOR . 'mifactura-2020' . DIRECTORY_SEPARATOR);

/**
 * Carpeta temporal donde se almacenarán todos los archivos relativos al proceso de actualización automático.
 */
define('FS_UPDATES_FOLDER', constant('FS_FOLDER') . DIRECTORY_SEPARATOR . 'tmp' . DIRECTORY_SEPARATOR . constant('FS_TMP_NAME') . 'updates' . DIRECTORY_SEPARATOR);

/**
 * Ruta al archivo para gestionar datos en periodo de mantenimiento
 */
if (!defined('MAINTENANCE_FILE')) {
    define('MAINTENANCE_FILE', constant('FS_FOLDER') . DIRECTORY_SEPARATOR . 'in_maintenance');
}

/**
 * Controlador del actualizador de MiFactura.eu.
 */
class fs_updater extends fs_app
{
    /**
     * Contiene la/s URL del wizard al que hay que redirigir, si es que corresponde hacerlo.
     *
     * @var string[]
     */
    public $url_wizard;

    /**
     * Debe mostrarse el botón de fin?
     *
     * @var bool
     */
    public $btn_fin;

    /**
     * Clase para centralizar la gestión de plugins.
     *
     * @var fs_plugin_manager
     */
    public $plugin_manager;

    /**
     * Lista de plugins.
     *
     * @var array
     */
    public $plugins;

    /**
     * Opciones para la fila.
     *
     * @var string
     */
    public $tr_options;

    /**
     * Opciones para la fila.
     *
     * @var string
     */
    public $tr_updates;

    /**
     * Lista de actualizaciones.
     *
     * @var array
     */
    public $updates;

    /**
     * Identificador único de la instalación.
     *
     * @var string
     */
    public $xid;

    /**
     * Este objeto permite acceso directo a la base de datos.
     *
     * @var fs_db2
     */
    protected $db;

    /**
     * Lista de plugins con actualizaciones.
     *
     * @var array
     */
    private $plugin_updates;

    /**
     * Contiene la lista de plugins obtenida de la comunidad (array plugins_lists.json)
     *
     * @var array
     */
    private $lista_plugins;

    /**
     * Lista de plugins que serán actualizados.
     *
     * @var array
     */
    private $plugins_updated;

    /**
     * Lista de wizards que deberán ser ejecutados tras sus respectivas actualizaciones.
     *
     * @var array
     */
    private $plugins_wizards;

    /**
     * Lista de plugins que ya han sido procesados.
     *
     * @var array
     */
    private $plugins_procesados;

    /**
     * fs_updater constructor.
     */
    public function __construct()
    {
        parent::__construct(__CLASS__);

        $this->plugins_procesados = [];
        $this->url_wizard = [];
        $this->db = new fs_db2();
        if (!$this->db->connect()) {
            $this->new_error_msg('¡Imposible conectar con la base de datos <b>' . fs_app::get_db_name() . '</b>!');
        }

        $this->btn_fin = false;
        $this->plugin_manager = new fs_plugin_manager();
        $this->log_manager = new fs_log_manager();
        $this->plugins = [];
        $this->tr_options = '';
        $this->tr_updates = '';
        $this->xid();

        if (filter_input(INPUT_COOKIE, 'user_' . $this->xid) && filter_input(INPUT_COOKIE, 'logkey_' . $this->xid)) {
            $this->process();
        } else {
            $this->core_log->new_error('Debes <a href="index.php">iniciar sesión</a>.');
        }

        $this->log_manager->save();
    }

    /**
     * Devuelve un identificador único de la empresa en el atributo $this->xid.
     */
    private function xid()
    {
        $this->xid = '';
        $data = $this->cache->get_array('empresa');
        if (empty($data)) {
            $sql = "SELECT *"
                . " FROM `empresa` ;";
            $result = $this->db->select($sql);
            $data = $result[0];
            $this->cache->set('empresa', $data);
        }
        if (!empty($data)) {
            $this->xid = $data['xid'];
            if (!filter_input(INPUT_COOKIE, 'uxid')) {
                assign_cookie('uxid', $this->xid);
            }
        } elseif (filter_input(INPUT_COOKIE, 'uxid')) {
            $this->xid = filter_input(INPUT_COOKIE, 'uxid');
        }
    }

    /**
     * Punto de inicio a la clase.
     */
    private function process()
    {
        /// solamente comprobamos si no hay que hacer nada
        if (!filter_input(INPUT_GET, 'update') && !filter_input(INPUT_GET, 'reinstall') && !filter_input(INPUT_GET, 'plugin') && !filter_input(INPUT_GET, 'idplugin')) {
            /* // TODO: Esta revisión no debería ser nunca necesaria, a menos que haya alguien manipulando el código
            /// ¿Están todos los permisos correctos?
            foreach (fs_file_manager::not_writable_folders() as $dir) {
                $this->core_log->new_error('No se puede escribir sobre el directorio ' . $dir);
            }
            */
            /// ¿Sigue estando disponible ziparchive?
            if (!extension_loaded('zip')) {
                $this->core_log->new_error('No se encuentra la clase ZipArchive, debes instalar php-zip.');
            }
        }

        if (count($this->core_log->get_errors()) > 0) {
            $this->core_log->new_error('Tienes que corregir estos errores antes de continuar.');
        } elseif (filter_input(INPUT_GET, 'update') || filter_input(INPUT_GET, 'reinstall')) {
            $this->actualizar_nucleo(true);
        } elseif (filter_input(INPUT_GET, 'plugin')) {
            $this->actualizar_plugin(filter_input(INPUT_GET, 'plugin'));
            $this->redirect_if_possible(filter_input(INPUT_GET, 'plugin'));
        } elseif (filter_input(INPUT_GET, 'idplugin') && filter_input(INPUT_GET, 'name') && filter_input(INPUT_GET, 'key')) {
            $this->actualizar_plugin_pago(filter_input(INPUT_GET, 'idplugin'), filter_input(INPUT_GET, 'name'), filter_input(INPUT_GET, 'key'));
            $this->redirect_if_possible(filter_input(INPUT_GET, 'name'));
        } elseif (filter_input(INPUT_GET, 'idplugin') && filter_input(INPUT_GET, 'name') && filter_input(INPUT_POST, 'key')) {
            $this->guardar_key();
        }

        if (count($this->core_log->get_errors()) == 0) {
            $this->comprobar_actualizaciones();
        } else {
            $this->tr_updates = '<tr class="warning"><td colspan="5">Aplazada la comprobación' . ' de plugins hasta que resuelvas los problemas.</td></tr>';
        }

        $action = fs_filter_input_req('action');
        switch ($action) {
            case 'update_all':
                $this->plugins_updated = [];
                $this->plugins_updates = [];
                $this->plugins_wizards = [];
                $this->update_all();
                break;
            case 'disable_maintenance':
                // Sacar de modo mantenimiento
                $status = $this->change_maintenance_mode(false);
                if ($status) {
                    $this->core_log->new_message('Modo mantenimiento desactivado.');
                } else {
                    $this->core_log->new_error('Modo mantenimiento no ha podido ser desactivado.');
                    return false;
                }
                break;
            case 'test':
                echo '<pre>' . var_export($this->plugin_manager->get_plugin_priorities(), true) . '</pre>';
                die();
                break;
        }
    }

    /**
     * Se encarga de actualizar el núcleo.
     *
     * @param bool $desatendido
     */
    private function actualizar_nucleo($desatendido = true)
    {
        $parametros = '';
        if ($desatendido) {
            $parametros = '?disregarded=true';
        }
        header('Location: updater_core.php' . $parametros);
        die();
    }

    /**
     * Se encarga de actualizar un plugin.
     *
     * @param string $plugin_name
     *
     * @return bool
     */
    private function actualizar_plugin($plugin_name)
    {
        $plugins = [];
        foreach ($this->plugin_manager->downloads() as $plugin) {
            $plugins[$plugin['nombre']] = $plugin;
        }
        if (!isset($plugins[$plugin_name])) {
            $this->core_log->new_error('Descarga no encontrada para plugin "' . $plugin_name . '".');
            return false;
        }

        $plugin = $plugins[$plugin_name];

        $ok = $this->plugin_manager->download($plugin['id'], true);
        if (!$ok) {
            return false;
        }
        $this->actualizacion_correcta($plugin_name);
        $this->core_log->new_message('Plugin "' . $plugin_name . '" actualizado correctamente.');
        return true;
    }

    /**
     * Elimina la actualización de la lista de pendientes.
     *
     * @param string $plugin
     */
    private function actualizacion_correcta($plugin = '')
    {
        $pluginData = $this->plugin_manager->get_plugin_data($plugin);
        if ($pluginData['wizard']) {
            $this->url_wizard[] = $pluginData['wizard'];
        }

        if (!isset($this->updates)) {
            $this->get_updates();
        }

        if (!$this->updates) {
            return;
        }

        if (empty($plugin)) {
            /// hemos actualizado el core
            $this->updates['core'] = false;
        } else {
            foreach ($this->updates['plugins'] as $i => $pl) {
                if ($pl->nombre == $plugin) {
                    unset($this->updates['plugins'][$i]);
                    break;
                }
            }
        }

        /// guardamos la lista de actualizaciones en cache
        if (count($this->updates['plugins']) > 0) {
            $this->cache->set('updater_lista', $this->updates);
        }
        $this->cache->clean();
    }

    /**
     * Obtiene las actualizaciones del núcleo, y si no las hay, lo intenta con las de plugins.
     */
    private function get_updates()
    {
        // Si hay actualizaciones en la caché, se retornan directamente
        // $this->updates = $this->cache->get('updater_lista');
        // if ($this->updates) {
        // $this->plugin_updates = $this->updates['plugins'];
        // return;
        // }

        $this->updates = ['version' => '', 'core' => false, 'plugins' => [],];

        $version_actual = floatval($this->plugin_manager->version);
        $this->updates['version'] = $version_actual;

        // Se descargan las versiones del núcleo y si hay actualización, nos salimos...
        $nucleos = json_decode(fs_file_get_contents(FS_COMMUNITY_URL . '/core.json', 30));
        if ($nucleos == null) {
            $this->core_log->new_error('No se ha podido comunicar con el servidor para obtener las actualizaciones');
            return false;
        }

        if (count($nucleos) > 0) {
            $nuevo_nucleo = $nucleos[0];
            $nueva_version = floatval($nuevo_nucleo->version);
            if ($version_actual < $nueva_version) {
                $this->updates['core'] = $nueva_version;
                // return;
            }
        }

        // Se comprueban las actualizaciones de plugins...
        // $array_lista_plugins = $this->get_lista_plugins();

        /// comprobamos los plugins
        $plugins = $this->check_for_plugin_updates();
        /*
        if (constant('FS_DB_HISTORY')) {
            echo 'plugins<pre>' . print_r($plugins, true) . '</pre>';
        }
        foreach ($plugins as $plugin) {
             // * Esto no termino de verlo claro, porque me pide siempre la clave.
             // * Lo que vamos a hacer es descargar al información de la comunidad y actuar en consecuencia.
            $idplugin = $plugin['idplugin'];
            $data = $lista_plugins[$idplugin];
            $data->version_anterior = $plugin['version'];

            $this->updates['plugins'][] = $data;
        }
        // if (constant('FS_DB_HISTORY')) {
        // echo '<pre>' . print_r($this, true) . '</pre>';
        // }
        */
        $this->updates['plugins'] = $plugins;
        // echo '<pre>'.print_r($this->updates['plugins'],true).'</pre>';

        /// guardamos la lista de actualizaciones en cache
        if (count($this->updates['plugins']) > 0) {
            $this->cache->set('updater_lista', $this->updates);
        }
    }

    /**
     * Revisa si hay actualizaciones e plugins disponibles.
     *
     * @return array
     */
    public function check_for_plugin_updates()
    {
        // if (isset($this->plugin_updates)) {
        // return $this->plugin_updates;
        // }

        $this->plugin_updates = [];

        // Obtiene un array con los plugins instalados, tomando los datos del archivo mifactura.ini.ini
        $plugins_instalados = $this->plugin_manager->installed();

        $lista_plugins = $this->get_lista_plugins();
        foreach ($plugins_instalados as $plugin_instalado) {
            // $idplugin = $plugin_instalado['idplugin'];
            $nameplugin = $plugin_instalado['name'];
            if (!isset($nameplugin)) {
                if (constant('FS_DB_HISTORY')) {
                    // echo '<p>El plugin <strong>' . $plugin_instalado['name'] . '</strong> carece de idplugin.</p>';
                }
                continue;
            }

            // TODO: Ver el código que hay tras el continue. Ésto se hacía de forma muy distinta.

            // Si el plugin no está en la comunidad... Siguiente...
            if (!isset($lista_plugins[$nameplugin])) {
                // echo '<p>El plugin (' . $idplugin . ') <strong>' . $plugin->name . '</strong> no está en la comunidad.</p>';
                continue;
            }
            $plugin_comunidad = $lista_plugins[$nameplugin];
            // echo 'Comunidad<pre>' . print_r($plugin_comunidad, true) . '</pre>';
            // echo 'Instalado<pre>' . print_r($plugin_instalado, true) . '</pre>';

            $plugin = new stdClass();
            $plugin->idplugin = $plugin_instalado['idplugin'];
            $plugin->name = $plugin_instalado['name'];
            $plugin->nombre = $plugin->name;
            $plugin->descripcion_corta = $plugin_comunidad->descripcion_corta;
            $plugin->descripcion = $plugin_comunidad->descripcion;
            $plugin->tipo = $plugin_comunidad->tipo;
            $plugin->depago = false;
            $plugin->version_anterior = floatval($plugin_instalado['version']);
            $plugin->version = floatval($plugin_comunidad->version);
            $plugin->requeridos = $plugin_instalado['require'];
            $plugin->autoactivable = $plugin_instalado['self-activating'];
            $plugin->deactivable = $plugin_instalado['deactivable'];

            $this->plugins[] = $plugin->name;
            // echo $plugin['name'] . '<pre>' . print_r($plugin_instalado, true) . '</pre>';

            if ($plugin->version_anterior < $plugin->version) {
                switch ($plugin_comunidad->tipo) {
                    case 'gratis':
                    case 'oculto':
                        $this->plugin_updates[] = $plugin;
                        break;
                    case 'pago':
                        /**
                         * TODO: Hay que verificar el funcionamiento de los plugins de PAGO, que no está comprobado.
                         */ $plugin->depago = true;
                        if (floatval($plugin->version) > floatval($plugin->version_anterior)) {
                            $plugin->private_key = '';

                            if (file_exists('tmp/' . constant('FS_TMP_NAME') . 'private_keys/' . $plugin->idplugin)) {
                                $plugin->private_key = trim(@file_get_contents('tmp/' . constant('FS_TMP_NAME') . 'private_keys/' . $plugin->idplugin));
                            } elseif (!file_exists('tmp/' . constant('FS_TMP_NAME') . 'private_keys/') && mkdir('tmp/' . constant('FS_TMP_NAME') . 'private_keys/')) {
                                file_put_contents('tmp/' . constant('FS_TMP_NAME') . 'private_keys/.htaccess', 'Deny from all');
                            }

                            $this->plugin_updates[] = $plugin;
                        }
                        break;
                    default:
                        echo "<p>Falta soporte para tipo {$plugin_comunidad->tipo} </p>";
                }
            }

            /**
             * // TODO: SE MANTIENE ESTE CODIGO PARA SU REVISIÓN. Ahora se hace forma muy distinta.
             *
             * if ($plugin['version_url'] != '' && $plugin['update_url'] != '') {
             * echo '$plugin ' . $plugin['name'] . '<pre>' . print_r($plugin, true) . '</pre>';
             *
             * /// plugin con descarga gratuita
             * $internet_ini = @parse_ini_string(@fs_file_get_contents($plugin['version_url']));
             * echo '$plugin <pre>' . print_r($internet_ini, true) . '</pre>';
             * echo '$lista_plugin <pre>' . print_r($this->lista_plugins[$plugin['idplugin']], true) . '</pre>';
             * if ($internet_ini && floatval($plugin['version']) < floatval($internet_ini['version'])) {
             * $plugin['new_version'] = floatval($internet_ini['version']);
             * $plugin['depago'] = false;
             *
             * $this->plugin_updates[] = $plugin;
             * }
             * } elseif ($plugin['idplugin']) {
             * /// plugin de pago/oculto
             * foreach ($this->plugin_manager->downloads() as $ditem) {
             * if ($ditem['id'] != $plugin['idplugin']) {
             * continue;
             * }
             *
             * if (floatval($ditem['version']) > floatval($plugin['version'])) {
             *
             * $plugin['new_version'] = floatval($ditem['version']);
             * $plugin['depago'] = true;
             * $plugin['private_key'] = '';
             *
             * if (file_exists('tmp/' . constant('FS_TMP_NAME') . 'private_keys/' . $plugin['idplugin'])) {
             * $plugin['private_key'] = trim(@file_get_contents('tmp/' . constant('FS_TMP_NAME') . 'private_keys/' . $plugin['idplugin']));
             * } elseif (!file_exists('tmp/' . constant('FS_TMP_NAME') . 'private_keys/') && mkdir('tmp/' . constant('FS_TMP_NAME') . 'private_keys/')) {
             * file_put_contents('tmp/' . constant('FS_TMP_NAME') . 'private_keys/.htaccess', 'Deny from all');
             * }
             *
             * $this->plugin_updates[] = $plugin;
             * }
             * break;
             * }
             * }
             */

            // echo 'Añadido '.$idplugin.':<pre>'.print_r($plugin,true).'</pre>';
            // $this->plugin_updates[$idplugin] = $plugin;
        }

        return $this->plugin_updates;
    }

    /**
     * Se obtiene la lista de todos los plugins de la comunidad
     *
     * @return array
     */
    public function get_lista_plugins()
    {
        if ($this->lista_plugins != null) {
            return $this->lista_plugins;
        }

        $array_lista_plugins = json_decode(fs_file_get_contents(FS_COMMUNITY_URL . '/plugins-list.json', 30));
        // echo 'lista_plugins<pre>' . print_r($array_lista_plugins, true) . '</pre>';
        $lista_plugins = [];
        if (isset($array_lista_plugins) && $array_lista_plugins !== false) {
            foreach ($array_lista_plugins as $plugin) {
                $lista_plugins[$plugin->nombre] = $plugin;
            }
        }
        // if (constant('FS_DB_HISTORY')) {
        // echo 'lista_plugins<pre>' . print_r($lista_plugins, true) . '</pre>';
        // }
        $this->lista_plugins = $lista_plugins;
        return $lista_plugins;
    }

    /**
     * Redirigiremos si no hay errores, si hay errores no redirigimos para que se puedan ver.
     */
    private function redirect_if_possible($plugin_name)
    {
        $plugin = $this->plugin_manager->get_plugin_data($plugin_name);

        $wizard = false;
        if (!empty($plugin['wizard'])) {
            $wizard = $plugin['wizard'];
        }

        $is_enabled = false;
        $installed = $this->plugin_manager->installed();
        foreach ($installed as $plug) {
            if ($plug['name'] !== $plugin_name) {
                continue;
            }
            $is_enabled = $plug['enabled'];
        }

        $this->cache->clean();
        fs_file_manager::clear_views_cache();

        if (function_exists('clean_full_cache')) {
            clean_full_cache();
        }

        if ($is_enabled && $wizard !== false) {
            $this->core_log->new_advice('Ya puedes <a href="index.php?page=' . $wizard . '">configurar el plugin</a>.');
            header('Location: index.php?page=' . $wizard);
            die();
        }
    }

    /**
     * Se encarga de actualizar un plugin de pago si se tiene una clave válida.
     *
     * @param int    $idplugin
     * @param string $name
     * @param string $key
     *
     * @return bool
     */
    private function actualizar_plugin_pago($idplugin, $name, $key)
    {
        $url = constant('FS_COMMUNITY_URL') . '/DownloadBuild2017/' . $idplugin . '/stable?xid=' . $this->xid . '&key=' . $key;

        /// descargamos el zip
        if (!@fs_file_download($url, constant('FS_FOLDER') . '/update-pay.zip')) {
            $this->core_log->new_error('Error al descargar el archivo update-pay.zip. <a href="updater.php?idplugin=' . $idplugin . '&name=' . $name . '">¿Clave incorrecta?</a>');
            return false;
        }

        $zip = new ZipArchive();
        $zip_status = $zip->open(constant('FS_FOLDER') . '/update-pay.zip', ZipArchive::CHECKCONS);
        if ($zip_status !== true) {
            $this->core_log->new_error('Ha habido un error con el archivo update-pay.zip. Código: ' . $zip_status . '. Intente de nuevo en unos minutos.');
            return false;
        }

        /// eliminamos los archivos antiguos
        fs_file_manager::del_tree(constant('FS_FOLDER') . '/plugins/' . $name);

        /// descomprimimos
        $zip->extractTo(constant('FS_FOLDER') . '/plugins/');
        $zip->close();
        unlink(constant('FS_FOLDER') . '/update-pay.zip');

        if (file_exists(constant('FS_FOLDER') . '/plugins/' . $name)) {
            /// renombramos el directorio
            rename(constant('FS_FOLDER') . '/plugins/' . $name, 'plugins/' . $name);
        }

        $this->core_log->new_message('Plugin <b>' . $name . '</b> actualizado correctamente.');
        $this->actualizacion_correcta($name);
        return true;
    }

    /**
     * Guarda la clave privada para un plugin.
     */
    private function guardar_key()
    {
        $private_key = filter_input(INPUT_POST, 'key');
        if (file_put_contents('tmp/' . constant('FS_TMP_NAME') . 'private_keys/' . filter_input(INPUT_GET, 'idplugin'), $private_key)) {
            $this->core_log->new_message('Clave añadida correctamente.');
            $this->cache->clean();
        } else {
            $this->core_log->new_error('Error al guardar la clave.');
        }
    }

    /**
     * Comprobamos las actualizaciones que puedan estar disponibles.
     */
    private function comprobar_actualizaciones()
    {
        if (!isset($this->updates)) {
            $this->get_updates();
        }

        if ($this->updates['core']) {
            $this->tr_updates = '<tr>'
                . '<td><b>Núcleo</b></td>'
                . '<td>Núcleo de MiFactura.eu</td>'
                . '<td class="text-end">' . $this->plugin_manager->version . '</td>'
                . '<td class="text-end"><a href="' . constant('FS_COMMUNITY_URL') . '/index.php?page=community_changelog&version=' . $this->updates['core'] . '" target="_blank">' . $this->updates['core'] . '</a></td>'
                . '<td class="text-end">
                    <a class="btn btn-primary" href="updater.php?update=TRUE" role="button">
                        <i class="fa-solid fa-upload fa-fw"></i>Actualizar
                    </a></td>'
                . '</tr>';
        } else {
            $this->tr_options = '<tr>'
                . '<td><b>Núcleo</b></td>'
                . '<td>Núcleo de MiFactura.eu</td>'
                . '<td class="text-end">' . $this->plugin_manager->version . '</td>'
                . '<td class="text-end"><a href="' . constant('FS_COMMUNITY_URL') . '/index.php?page=community_changelog&version=' . $this->plugin_manager->version . '" target="_blank">' . $this->plugin_manager->version . '</a></td>'
                . '<td class="text-end">
                    <a class="btn btn-outline-secondary" href="updater.php?reinstall=TRUE" role="button">
                        <i class="fa-solid fa-repeat fa-fw"></i>Reinstalar
                    </a></td>'
                . '</tr>';

            $plugins = $this->updates['plugins'];

            $lista_plugins_actualizar = [];
            foreach ($plugins as $plugin) {
                $lista_plugins_actualizar[] = $plugin->name;
            }

            foreach ($plugins as $plugin) {

                if (!isset($plugin->nombre)) {
                    continue;
                }
                // Se comprueba que esté activo, para actualizar sólo los activos.
                if (!is_plugin_enabled($plugin->nombre)) {
                    continue;
                }

                // echo '<pre>' . print_r($plugin, true) . '</pre>';
                if ($plugin->version <= $plugin->version_anterior) {
                    continue;
                }

                $dependiente = false;
                foreach ($plugin->requeridos as $requerido) {
                    if (in_array($requerido, $lista_plugins_actualizar)) {
                        $dependiente = true;
                        break;
                    }
                }

                if ($dependiente) {
                    $button = ' <p class="help-block">' . '<span class="text-danger"><i class="fa-solid fa-circle-info fa-fw"></i>Actualiza las dependencias primero</span>' . ' </p>';
                } else {
                    $button = '<a href="updater.php?plugin=' . $plugin->nombre . '" class="btn btn-primary">' . '<i class="fa-solid fa-upload fa-fw"></i>Actualizar' . '</a>';
                }

                // $update_private_btn = ' <a href="index.php?task=updater&idplugin=' . $plugin['idplugin'] . '&name=' . $plugin['name'] . '&key=' . $plugin['private_key'] . '" class="btn d-flex justify-content-center align-items-center btn-xs btn-primary">'
                // . '  <i class="fa-solid fa-upload fa-fw"></i>Actualizar'
                // . ' </a>';

                $this->tr_updates .= '<tr>'
                    . '<td>' . $plugin->nombre . '</td>'
                    . '<td>' . $plugin->descripcion_corta . '</td>'
                    . '<td class="text-end">' . $plugin->version_anterior . '</td>'
                    . '<td class="text-end"><a href="' . constant('FS_COMMUNITY_URL') . '/index.php?page=community_changelog&version=' . $plugin->version . '&plugin=' . $plugin->nombre . '" target="_blank">' . $plugin->version . '</a></td>'
                    . '<td class="text-end">' . $button . '</td></tr>';
                /*
                // De momento no se contemplan los de pago...
                continue;

                if ($plugin->tipo == 'gratis') {
                    $this->tr_updates .= '<tr>'
                        . '<td>' . $plugin->nombre . '</td>'
                        . '<td>' . $plugin->descripcion . '</td>'
                        . '<td class="text-end">' . $plugin->version_anterior . '</td>'
                        . '<td class="text-end"><a href="' . constant('FS_COMMUNITY_URL') . '/index.php?page=community_changelog&version='
                        . $plugin->version . '&plugin=' . $plugin->nombre . '" target="_blank">' . $plugin->version . '</a></td>'
                        . '<td class="text-end">'
                        . '<a href="updater.php?plugin=' . $plugin->nombre . '" class="btn btn-primary">'
                        . '<i class="fa-solid fa-upload fa-fw"></i>Actualizar'
                        . '</a>'
                        . '</td></tr>';
                    continue;
                }

                if (!$this->xid) {
                    /// nada
                    continue;
                }


                if (empty($plugin['private_key'])) {
                    $this->tr_updates .= '<tr>'
                        . '<td>' . $plugin['name'] . '</td>'
                        . '<td>' . $plugin['description'] . '</td>'
                        . '<td class="text-end">' . $plugin['version'] . '</td>'
                        . '<td class="text-end"><a href="' . constant('FS_COMMUNITY_URL') . '/index.php?page=community_changelog&version='
                        . $plugin['new_version'] . '&plugin=' . $plugin['name'] . '" target="_blank">' . $plugin['new_version'] . '</a></td>'
                        . '<td class="text-end">'
                        . '<div class="d-flex btn-group" role="group">'
                        . '<a href="#" class="btn btn-warning" data-toggle="modal" data-target="#modal_key_' . $plugin['name'] . '">'
                        . '<i class="fa-solid fa-key fa-fw"></i>Añadir clave'
                        . '</a>'
                        . '</div>'
                        . '</td></tr>';
                    continue;
                }

                $this->tr_updates .= '<tr>'
                    . '<td>' . $plugin['name'] . '</td>'
                    . '<td>' . $plugin['description'] . '</td>'
                    . '<td class="text-end">' . $plugin['version'] . '</td>'
                    . '<td class="text-end"><a href="' . constant('FS_COMMUNITY_URL') . '/index.php?page=community_changelog&version='
                    . $plugin['new_version'] . '&plugin=' . $plugin['name'] . '" target="_blank">' . $plugin['new_version'] . '</a></td>'
                    . '<td class="text-center">'
                    . '<div class="d-flex btn-group" role="group">'
                    . '<a href="updater.php?idplugin=' . $plugin['idplugin'] . '&name=' . $plugin['name'] . '&key=' . $plugin['private_key']
                    . '" class="btn d-flex justify-content-center align-items-center btn-xs btn-primary">'
                    . '<i class="fa-solid fa-upload fa-fw"></i>Actualizar'
                    . '</a>'
                    . '<a href="#" data-toggle="modal" data-target="#modal_key_' . $plugin['name'] . '">'
                    . '<i class="fa-solid fa-edit fa-fw"></i>Cambiar la clave'
                    . '</a>'
                    . '</div>'
                    . '</td></tr>';
                */
            }

            if ($this->tr_updates == '') {
                $this->tr_updates = '<tr class="success"><td colspan="5">El sistema está actualizado.' . ' <a href="index.php?page=admin_home&updated=TRUE">Volver</a></td></tr>';
                $this->btn_fin = true;
            }
        }
    }

    /**
     * Se encarga de actualizar todos los plugins disponibles y devuelve el estado resultante.
     *
     * Este método contiene la lógica principal de la acción, luego se ha dividido en funciones menores.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2021.08
     *
     * @return false
     */
    public function update_all()
    {
        set_temporaly_unlimited_limits();

        $this->get_updates();

        // TODO: ÑAPA Quitamos el modo mantenimiento para obligar a borrar el archivo.
        $this->change_maintenance_mode(false);

        // Poner en modo mantenimiento
        $status = $this->change_maintenance_mode(true);
        if ($status) {
            $this->core_log->new_message('Modo mantenimiento activado.');
        } else {
            $this->core_log->new_error('Modo mantenimiento no ha podido ser activado.');
            return false;
        }

        // Descargamos todas las actualizaciones disponibles
        $status = $this->descargar_nucleo();
        if ($status) {
            if (is_file(FS_UPDATES_FOLDER . 'update-core.zip')) {
                $this->core_log->new_message('Actualización del núcleo descargada correctamente.');
            }

            foreach ($this->updates['plugins'] as $plugin) {
                $status = $this->descargar_plugin($plugin->nombre);
                if ($status) {
                    $this->core_log->new_message('Actualización del plugin "' . $plugin->nombre . '" descargada correctamente.');
                } else {
                    $message = 'Actualización del plugin "' . $plugin->nombre . '" con problemas durante la descarga.';
                    $this->core_log->new_error($message);

                    // No debemos consumir espacio al usuario, si necesitas depurarlo comenta la llamada a esta función
                    $this->clean_updates_folder();
                    $this->change_maintenance_mode(false);
                    $this->notify_support($message);

                    return false;
                }

                // También sus dependencias
                foreach ($plugin->requeridos as $plugin_requerido) {
                    $founded = false;
                    foreach ($this->plugin_manager->installed() as $installed_plugin) {
                        if ($installed_plugin['name'] != $plugin_requerido) {
                            continue;
                        }
                        if (!in_array($plugin_requerido, $this->plugin_manager->enabled())) {
                            $founded = true;
                            break;
                        }
                    }
                    if (!$founded) {
                        $status = $this->descargar_plugin($plugin_requerido);
                        if ($status) {
                            $this->core_log->new_message('Actualización del plugin dependencia "' . $plugin_requerido . '" descargada correctamente.');
                        } else {
                            $message = 'Actualización del plugin dependencia "' . $plugin_requerido . '" con problemas durante la descarga.';
                            $this->core_log->new_error($message);

                            // No debemos consumir espacio al usuario, si necesitas depurarlo comenta la llamada a esta función
                            $this->clean_updates_folder();
                            $this->change_maintenance_mode(false);
                            $this->notify_support($message);

                            return false;
                        }
                    }
                }
            }
        } else {
            $message = 'La actualización del núcleo no ha podido ser descargada o se ha descargado un archivo invalido.';
            $this->core_log->new_error($message);

            // No debemos consumir espacio al usuario, si necesitas depurarlo comenta la llamada a esta función
            $this->clean_updates_folder();
            $this->change_maintenance_mode(false);
            $this->notify_support($message);

            return false;
        }

        // Descomprimimos todos los zips descargados
        if ($status && file_exists(FS_UPDATES_FOLDER . '/update-core.zip')) {
            $status = $this->descomprimir_nucleo();
            if ($status) {
                $this->core_log->new_message('Núcleo descomprimido correctamente.');
            }
        }
        if ($status) {
            foreach ($this->updates['plugins'] as $plugin) {
                $status = $this->descomprimir_plugin($plugin->nombre);
                if ($status) {
                    $this->core_log->new_message('Actualización del plugin "' . $plugin->nombre . '" descomprimido correctamente.');
                } else {
                    $message = 'Actualización del plugin "' . $plugin->nombre . '" con problemas durante la descompresión.';
                    $this->core_log->new_error($message);

                    // No debemos consumir espacio al usuario, si necesitas depurarlo comenta la llamada a esta función
                    $this->clean_updates_folder();
                    $this->change_maintenance_mode(false);
                    $this->notify_support($message);

                    return false;
                }

                // También sus dependencias
                foreach ($plugin->requeridos as $plugin_requerido) {
                    $founded = false;
                    foreach ($this->plugin_manager->installed() as $installed_plugin) {
                        if ($installed_plugin['name'] != $plugin_requerido) {
                            continue;
                        }
                        if (!in_array($plugin_requerido, $this->plugin_manager->enabled())) {
                            $founded = true;
                            break;
                        }
                    }
                    if (!$founded) {
                        $status = $this->descomprimir_plugin($plugin_requerido);
                        if ($status) {
                            $this->core_log->new_message('Actualización del plugin dependencia "' . $plugin_requerido . '" descomprimido correctamente.');
                        } else {
                            $message = 'Actualización del plugin dependencia "' . $plugin_requerido . '" con problemas durante la descompresión.';
                            $this->core_log->new_error($message);

                            // No debemos consumir espacio al usuario, si necesitas depurarlo comenta la llamada a esta función
                            $this->clean_updates_folder();
                            $this->change_maintenance_mode(false);
                            $this->notify_support($message);

                            return false;
                        }
                    }
                }
            }
        } else {
            $message = 'El núcleo no ha podido ser descomprimido.';
            $this->core_log->new_error($message);

            // No debemos consumir espacio al usuario, si necesitas depurarlo comenta la llamada a esta función
            $this->clean_updates_folder();
            $this->change_maintenance_mode(false);
            $this->notify_support($message);

            return false;
        }

        // Hacer backup
        if ($status && (is_dir(constant('FS_UNPACK_FOLDER')))) {
            $status = $this->backup_old_nucleo();
        }
        if ($status) {
            $this->core_log->new_message('Copia del núcleo antes de actualización realizada correctamente.');
            foreach ($this->updates['plugins'] as $plugin) {
                $status = $this->backup_old_plugin($plugin->nombre);
                if ($status) {
                    $this->core_log->new_message('Copia del plugin "' . $plugin->nombre . '" antes de actualización realizada correctamente.');
                } else {
                    $message = 'La copia del plugin "' . $plugin->nombre . '" antes de actualización ha tenido problemas.';
                    $this->core_log->new_error($message);

                    // No debemos consumir espacio al usuario, si necesitas depurarlo comenta la llamada a esta función
                    $this->clean_updates_folder();
                    $this->change_maintenance_mode(false);
                    $this->notify_support($message);

                    return false;
                }

                // También sus dependencias
                foreach ($plugin->requeridos as $plugin_requerido) {
                    $founded = false;
                    foreach ($this->plugin_manager->installed() as $installed_plugin) {
                        if ($installed_plugin['name'] != $plugin_requerido) {
                            continue;
                        }
                        if (!in_array($plugin_requerido, $this->plugin_manager->enabled())) {
                            $founded = true;
                            break;
                        }
                    }
                    if (!$founded) {
                        $status = $this->backup_old_plugin($plugin_requerido);
                        if ($status) {
                            $this->core_log->new_message('Copia del plugin dependencia "' . $plugin_requerido . '" antes de actualización realizada correctamente.');
                        } else {
                            $message = 'La copia del plugin dependencia "' . $plugin_requerido . '" antes de actualización ha tenido problemas.';
                            $this->core_log->new_error($message);

                            // No debemos consumir espacio al usuario, si necesitas depurarlo comenta la llamada a esta función
                            $this->clean_updates_folder();
                            $this->change_maintenance_mode(false);
                            $this->notify_support($message);

                            return false;
                        }
                    }
                }
            }
        } else {
            $message = 'No se ha podido realizar copia del núcleo antes de la actualización.';
            $this->core_log->new_error($message);

            // No debemos consumir espacio al usuario, si necesitas depurarlo comenta la llamada a esta función
            $this->clean_updates_folder();
            $this->change_maintenance_mode(false);
            $this->notify_support($message);

            return false;
        }

        // Mover todas las actualizaciones a su destino final
        if ($status && (is_dir(constant('FS_UNPACK_FOLDER')))) {
            $status = $this->mover_nucleo();
        }
        if ($status) {
            if (is_dir(constant('FS_UNPACK_FOLDER'))) {
                $this->core_log->new_message('Actualización del nucleo aplicada correctamente.');
            }

            $stats = fs_file_manager::del_tree(constant('FS_UNPACK_FOLDER'));
            $this->core_log->new_message('Carpeta de nucleo "' . constant('FS_UNPACK_FOLDER') . '" eliminada: ' . ($stats ? 'Correctamente' : 'FALLO') . '.');

            foreach ($this->updates['plugins'] as $plugin) {
                $status = $this->mover_plugin($plugin->nombre);
                if ($status) {
                    $this->core_log->new_message('Actualización del plugin "' . $plugin->nombre . '" finalizada correctamente.');
                    $stats = fs_file_manager::del_tree(FS_UPDATES_FOLDER . $plugin->nombre);
                    $this->core_log->new_message('Carpeta de plugin "' . constant('FS_UPDATES_FOLDER') . $plugin->nombre . '" eliminada: ' . ($stats ? 'Correctamente' : 'FALLO') . '.');
                } else {
                    $message = 'Actualización del plugin "' . $plugin->nombre . '" finalizada con problemas durante la aplicación.';
                    $this->core_log->new_error($message);

                    $this->restore_old_core();
                    $this->restore_old_plugins();
                    $this->change_maintenance_mode(false);
                    $this->clean_updates_folder();
                    $this->notify_support($message);

                    return false;
                }

                // También sus dependencias
                foreach ($plugin->requeridos as $plugin_requerido) {
                    $founded = false;
                    foreach ($this->plugin_manager->installed() as $installed_plugin) {
                        if ($installed_plugin['name'] != $plugin_requerido) {
                            continue;
                        }
                    }
                    if (!$founded) {
                        $status = $this->mover_plugin($plugin_requerido);
                        if ($status) {
                            $this->core_log->new_message('Actualización del plugin dependencia "' . $plugin_requerido . '" finalizada correctamente.');
                            $stats = fs_file_manager::del_tree(FS_UPDATES_FOLDER . $plugin_requerido);
                            $this->core_log->new_message('Carpeta de plugin dependencia "' . constant('FS_UPDATES_FOLDER') . $plugin_requerido . '" eliminada: ' . ($stats ? 'Correctamente' : 'FALLO') . '.');
                            if (!in_array($plugin_requerido, $this->plugin_manager->enabled())) {
                                $this->plugin_manager->enable($plugin_requerido, true);
                            }
                        } else {
                            $message = 'Actualización del plugin dependencia "' . $plugin_requerido . '" finalizada con problemas durante la aplicación.';
                            $this->core_log->new_error($message);

                            $this->restore_old_core();
                            $this->restore_old_plugins();
                            $this->change_maintenance_mode(false);
                            $this->clean_updates_folder();
                            $this->notify_support($message);

                            return false;
                        }
                    }
                }
            }
        } else {
            $message = 'No se ha podido aplicar la actualización del nucleo.';
            $this->core_log->new_error($message);

            $this->restore_old_core();
            // $this->change_maintenance_mode(false);
            $this->clean_updates_folder();
            $this->notify_support($message);

            return false;
        }

        // Notificar que ha terminado de actualizar
        $details = [
            'is_updating' => false,
            'plugins' => array_reverse($this->plugins_updated),
            'updates' => array_reverse($this->plugins_updates),
            'wizards' => array_reverse($this->plugins_wizards),
        ];
        $status = (bool) file_put_contents(constant('MAINTENANCE_FILE'), json_encode($details));
        if ($status) {
            $this->core_log->new_message('Archivo de mantenimiento actualizado correctamente.');
        } else {
            $this->core_log->new_error('No se ha podido actualizar el archivo de mantenimiento.');
            // return false;
        }

        // Limpiar caché al terminar
        $status = $this->cache->clean();
        if ($status) {
            $this->core_log->new_message('Caché limpiada correctamente.');
        } else {
            $this->core_log->new_error('No se ha podido limpiar la caché.');
            // return false;
        }

        if ($status) {
            $status = $this->clean_updates_folder();

            if ($status) {
                $this->core_log->new_message('Carpeta temporal para actualizaciones eliminada correctamente.');
            } else {
                $this->core_log->new_error('No se ha podido eliminar la carpeta temporal para actualizaciones.');
                return false;
            }
        }

        return $status;
    }

    /**
     * Cambia el modo mantenimiento en base al parámetro de activación.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2021.08
     *
     * @param bool $maintenance_enabled
     *
     * @return bool
     */
    public function change_maintenance_mode($maintenance_enabled = false)
    {
        $this->core_log->new_message('Cambio de estado de mantenimiento iniciado.');

        if ($maintenance_enabled) {
            if (!file_exists(constant('MAINTENANCE_FILE'))) {
                foreach ($this->plugin_manager->get_plugin_priorities() as $plugin_priority) {
                    foreach ($this->updates['plugins'] as $plugin) {
                        if ($plugin_priority == $plugin->nombre) {
                            $data = $this->plugin_manager->get_plugin_data($plugin->nombre);
                            if ($data) {
                                $this->plugins_updated[] = $plugin->nombre;
                                if ($data['wizard']) {
                                    $this->plugins_wizards[] = $data['wizard'];
                                }
                            }
                        }
                    }
                }

                $details = [
                    'is_updating' => true,
                    'plugins' => array_reverse($this->plugins_updated),
                    'wizards' => array_reverse($this->plugins_wizards),
                ];
                $status = (bool) file_put_contents(constant('MAINTENANCE_FILE'), json_encode($details));
            } else {
                $status = true;
            }
        } else {
            if (file_exists(constant('MAINTENANCE_FILE'))) {
                $status = unlink(constant('MAINTENANCE_FILE'));
            } else {
                $status = true;
            }
        }

        $this->core_log->new_message('Estado de mantenimiento cambiado a ' . ($maintenance_enabled ? 'Activado' : 'Desactivado') . ' ' . ($status ? 'Correctamente' : 'FALLO') . '.');

        return $status;
    }

    /**
     * Se encarga de descargar el núcleo y devolver su estado.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2021.08
     *
     * @return bool
     */
    public function descargar_nucleo()
    {
        $status = true;

        $this->core_log->new_message('Descarga de núcleo iniciada.');

        $cores = get_from_community('/core.json', 10);
        if ($cores == null) {
            $this->core_log->new_error('No se ha podido comunicar con el servidor para obtener las actualizaciones.');
            return false;
        }

        if (count($cores) > 0) {
            $version_actual = (float) trim(@file_get_contents('MiVersion'));
            $new_core = $cores[0];

            $nueva_version = (float) $new_core->version;
            if ($version_actual < $nueva_version) {
                if (!is_dir(FS_UPDATES_FOLDER) && !mkdir(FS_UPDATES_FOLDER, 0777, true)) {
                    $this->core_log->new_error('No se ha podido crear la carpeta temporal para descargar las actualizaciones.');
                    return false;
                }
                $status = fs_file_download($new_core->link, FS_UPDATES_FOLDER . 'update-core.zip');

                $this->core_log->new_message('Nucleo descargado a "' . constant('FS_UPDATES_FOLDER') . 'update-core.zip": ' . ($status ? 'Correctamente' : 'FALLO') . '.');
            }
        }

        $this->core_log->new_message('No ha sido necesario descargar el núcleo.');

        return $status;
    }

    /**
     * Se encarga de descargar el plugin indicado, y devolver su estado.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2021.08
     *
     * @param $plugin_id
     *
     * @return false
     */
    public function descargar_plugin($plugin_name)
    {
        $this->core_log->new_message('Descarga de plugin: "' . $plugin_name . '" iniciada.');

        $plugins = [];
        foreach ($this->plugin_manager->downloads() as $plugin) {
            $plugins[$plugin['nombre']] = $plugin;
        }

        $item = $plugins[$plugin_name];

        if (!is_dir(FS_UPDATES_FOLDER) && !mkdir(FS_UPDATES_FOLDER, 0777, true)) {
            $this->core_log->new_error('No se ha podido crear la carpeta temporal para descargar las actualizaciones de plugins.');
            return false;
        }

        $status = @fs_file_download($item['link'], FS_UPDATES_FOLDER . $plugin_name . '.zip');

        $this->core_log->new_message('Plugin "' . $plugin_name . '" descargado desde "' . constant('FS_UPDATES_FOLDER') . $plugin_name . '.zip"' . ' : ' . ($status ? 'Correctamente' : 'FALLO') . '.');

        return $status;
    }

    /**
     * Limpia la carpeta de actualizaciones y devuelve su estado.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2021.09
     *
     * @return bool
     */
    public function clean_updates_folder()
    {
        $this->core_log->new_message('Limpiando "' . constant('FS_UPDATES_FOLDER') . '".');
        return fs_file_manager::del_tree(FS_UPDATES_FOLDER);
    }

    /**
     * Envía un email a soporte informando del problema.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2021.09
     *
     * @param $message
     */
    public function notify_support($message)
    {
        // if (function_exists('send_error')) {
        // send_error($message);
        // }
    }

    /**
     * Se encarga de descomprimir el núcleo y devolver su estado.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2021.08
     *
     * @return bool
     */
    public function descomprimir_nucleo()
    {
        $status = false;

        $this->core_log->new_message('Descomprimiendo núcleo.');

        if (!is_dir(FS_UPDATES_FOLDER) && !mkdir(FS_UPDATES_FOLDER, 0777, true)) {
            $this->core_log->new_error('No se ha podido crear la carpeta temporal para descargar las actualizaciones.');
            return false;
        }
        $filezip = FS_UPDATES_FOLDER . '/update-core.zip';
        if (file_exists($filezip) && is_file($filezip)) {
            if (file_exists(constant('FS_UNPACK_FOLDER')) && is_dir(constant('FS_UNPACK_FOLDER'))) {
                $status = $status && fs_file_manager::del_tree(constant('FS_UNPACK_FOLDER'));
                if (!$status) {
                    $this->core_log->new_error('No se ha podido eliminar la descompresión anterior del núcleo.');
                    return false;
                }
            }
            $zip = new ZipArchive();

            $zip_status = $zip->open($filezip, ZipArchive::CHECKCONS);
            if ($zip_status === true) {
                $status = $zip->extractTo(str_replace('mifactura-2020' . DIRECTORY_SEPARATOR, '', constant('FS_UNPACK_FOLDER')));
                $zip->close();

                $this->core_log->new_message('Núcleo descomprimido: ' . ($status ? 'Correctamente' : 'FALLO') . '.');

                $status &= unlink($filezip);
            }
        }

        return $status;
    }

    /**
     * Se encarga de descomprimir el plugin indicado y devolver su estado.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2021.08
     *
     * @param $plugin_id
     *
     * @return false
     */
    public function descomprimir_plugin($plugin_name)
    {
        $status = false;

        if (in_array($plugin_name, $this->plugins_procesados)) {
            $this->core_log->new_message('ZIP de plugin "' . $plugin_name . '" descomprimido previamente.');
            return true;
        }

        $this->core_log->new_message('Descomprimiendo plugin "' . $plugin_name . '".');

        $plugins = [];
        foreach ($this->plugin_manager->downloads() as $plugin) {
            $plugins[$plugin['nombre']] = $plugin;
        }

        $item = $plugins[$plugin_name];

        $filepath = FS_UPDATES_FOLDER . $plugin_name . '.zip';

        if (!file_exists($filepath)) {
            $this->core_log->new_error('El archivo ZIP del plugin "' . $item['nombre'] . '" no existe.');
            return false;
        }

        $zip = new ZipArchive();
        $res = $zip->open($filepath, ZipArchive::CHECKCONS);
        if ($res !== true) {
            $this->core_log->new_error('Error al abrir el ZIP del plugin "' . $item['nombre'] . '" . Código: ' . $res);
            return false;
        }

        if (!is_dir(FS_UPDATES_FOLDER) && !mkdir(FS_UPDATES_FOLDER, 0777, true)) {
            $this->core_log->new_error('No se ha podido crear la carpeta temporal para el plugin "' . $item['nombre'] . '".');
            return false;
        }

        $plugin_Folder = FS_UPDATES_FOLDER . $plugin_name;

        if (file_exists($plugin_Folder) && is_dir($plugin_Folder)) {
            $status &= fs_file_manager::del_tree($plugin_Folder);
            if (!$status) {
                $this->core_log->new_error('No se ha podido eliminar la descompresión anterior del plugin "' . $item['nombre'] . '".');
                return false;
            }
        }

        $status = $zip->extractTo(FS_UPDATES_FOLDER);

        $this->core_log->new_message('Plugin "' . $plugin_name . '" descomprimido: ' . ($status ? 'Correctamente' : 'FALLO') . '.');

        if (!$status) {
            $this->core_log->new_error('No se ha podido extraer el plugin "' . $item['nombre'] . '".');
            return false;
        }
        $this->plugins_procesados[] = $plugin_name;
        $zip->close();
        $status &= unlink($filepath);

        $this->core_log->new_message('ZIP de plugin "' . $plugin_name . '" eliminado: ' . ($status ? 'Correctamente' : 'FALLO') . '.');

        return $status;
    }

    /**
     * Se encarga de realizar un backup del núcleo, y devolver su estado.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2021.08
     *
     * @return bool
     */
    public function backup_old_nucleo()
    {
        $this->core_log->new_message('Backup de núcleo iniciado.');

        $backup_folder = FS_UPDATES_FOLDER . 'backup_core' . DIRECTORY_SEPARATOR;
        if (!is_dir($backup_folder) && !mkdir($backup_folder, 0777, true)) {
            $this->core_log->new_error('No se ha podido crear la carpeta temporal para backup.');
            return false;
        }

        $copyFolders = [
            'root',         // ???
            'base',
            'bin',
            'cache',        // ???
            'components',   // ???
            'config',
            'controller',
            'dataconfig',
            'docker',
            'docs',
            'e',            // ???
            'extras',       // ???
            'libraries',    // ???
            'model',
            'node_modules',
            // 'plugins',
            'raintpl',
            'resellers',
            'resources',
            'src',
            'vendor',
            'view',
            'api.php',
            'apigen.neon-sample',
            'build.sh',         // ???
            'CHANGELOG.md',
            'config.php',
            'composer.json',
            'composer.lock',
            'CONTRIBUTING.md',
            'COPYING',
            'cron.php',
            'gulpfile.js',
            'fixperm.sh',       // ???
            'htaccess-sample',
            'index.php',
            'install.php',
            'miAJAX.php',
            'MiVersion',
            'package.json',
            'package-lock.json',
            'phpunit.xml',
            'README.md',
            'robots.txt',
            'TODO.md',          // ???
            'update.php',
            'updater.php',
            'updater_core.php',
        ];
        $status = true;
        foreach ($copyFolders as $folder) {
            if (file_exists(constant('FS_FOLDER') . DIRECTORY_SEPARATOR . $folder)) {
                $status = $status && fs_file_manager::recurse_copy(constant('FS_FOLDER') . DIRECTORY_SEPARATOR . $folder, $backup_folder . $folder);
            }
        }

        $deleteFolders = [
            'root',
            'base',
            'controller',
            'extras',
            'model',
            'raintpl',
            'view',
        ];
        foreach ($deleteFolders as $folder) {
            $folder_name = constant('FS_FOLDER') . DIRECTORY_SEPARATOR . $folder;
            // Renombramos la carpeta original
            if ($status && file_exists($folder_name) && is_dir($folder_name)) {
                $status &= fs_file_manager::del_tree($folder_name);
            }
            if (!$status) {
                return $status;
            }
        }

        $this->core_log->new_message('Backup de núcleo restaurado: ' . ($status ? 'Correctamente' : 'FALLO') . '.');

        return $status;
    }

    /**
     * Se encarga de realizar un backup del plugin, y devolver su estado.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2021.08
     *
     * @param $plugin_id
     *
     * @return bool
     */
    public function backup_old_plugin($plugin_name)
    {
        $this->core_log->new_message('Backup de plugin "' . $plugin_name . '" iniciado.');

        $backup_folder = constant('FS_FOLDER') . DIRECTORY_SEPARATOR . 'tmp' . DIRECTORY_SEPARATOR . constant('FS_TMP_NAME') . 'updates' . DIRECTORY_SEPARATOR . 'backups' . DIRECTORY_SEPARATOR . date('Y-m-d');
        if (!is_dir($backup_folder) && !mkdir($backup_folder, 0777, true)) {
            $this->core_log->new_error('No se ha podido crear la carpeta temporal para backups de plugins.');
            return false;
        }
        // Guarda la copia del plugin antes de actualizarlo, por si hay que recuperarlo.
        if (is_dir(constant('FS_FOLDER') . DIRECTORY_SEPARATOR . 'plugins' . DIRECTORY_SEPARATOR . $plugin_name) && !rename(constant('FS_FOLDER') . DIRECTORY_SEPARATOR . 'plugins' . DIRECTORY_SEPARATOR . $plugin_name, $backup_folder . DIRECTORY_SEPARATOR . $plugin_name)) {
            $status = false;
        } else {
            $status = true;
        }

        $this->core_log->new_message('Backup de plugin "' . $plugin_name . '" realizado: ' . ($status ? 'Correctamente' : 'FALLO') . '.');

        return $status;
    }

    /**
     * Se encarga
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2021.08
     *
     * @return false
     */
    public function mover_nucleo()
    {
        $this->core_log->new_message('Moviendo actualización de núcleo iniciado.');

        $status = false;
        if (file_exists(constant('FS_UNPACK_FOLDER')) && is_dir(constant('FS_UNPACK_FOLDER'))) {
            $status = fs_file_manager::recurse_copy(constant('FS_UNPACK_FOLDER'), FS_FOLDER);
            $status &= fs_file_manager::del_tree(constant('FS_UNPACK_FOLDER'));
        }

        $this->core_log->new_message('Núcleo copiado desde "' . constant('FS_UNPACK_FOLDER') . '" a "' . constant('FS_FOLDER') . '": ' . ($status ? 'Correctamente' : 'FALLO') . '.');

        return $status;
    }

    /**
     * Se encarga de reemplazar el plugin
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2021.08
     *
     * @param $plugin_name
     *
     * @return bool
     */
    public function mover_plugin($plugin_name)
    {
        $this->core_log->new_message('Moviendo actualización de plugin "' . $plugin_name . '" iniciado.');

        // Transferimos la actualización descomprimida a su ruta final
        if (is_dir(FS_UPDATES_FOLDER . $plugin_name) && !rename(FS_UPDATES_FOLDER . $plugin_name, constant('FS_FOLDER') . DIRECTORY_SEPARATOR . 'plugins' . DIRECTORY_SEPARATOR . $plugin_name)) {
            $status = false;
        } else {
            $status = true;
            $status &= fs_file_manager::del_tree(FS_UPDATES_FOLDER . $plugin_name);

            $pluginData = $this->plugin_manager->get_plugin_data($plugin_name);
            if ($pluginData['wizard']) {
                $this->url_wizard[] = $pluginData['wizard'];
            }
        }

        $this->core_log->new_message('Plugin "' . $plugin_name . '" copiado desde "' . constant('FS_UPDATES_FOLDER') . $plugin_name . '" a "' . constant('FS_FOLDER') . DIRECTORY_SEPARATOR . 'plugins' . DIRECTORY_SEPARATOR . $plugin_name . '": ' . ($status ? 'Correctamente' : 'FALLO') . '.');

        return $status;
    }

    /**
     * Restaura el núcleo anterior y devuelve su estado.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2021.09
     *
     * @return bool
     */
    public function restore_old_core()
    {
        $this->core_log->new_message('Restauración de núcleo original iniciado.');

        $deleteFolders = [
            'root',
            'base',
            'controller',
            'extras',
            'model',
            'raintpl',
            'view',
        ];
        foreach ($deleteFolders as $folder) {
            $folder_name = constant('FS_FOLDER') . DIRECTORY_SEPARATOR . $folder;
            // Renombramos la carpeta original
            if (file_exists($folder_name) && is_dir($folder_name)) {
                fs_file_manager::del_tree($folder_name);
            }
        }

        $status = fs_file_manager::recurse_copy(FS_UPDATES_FOLDER . 'backup_core', FS_FOLDER);

        $this->core_log->new_message('Restauración de nucleo de "' . constant('FS_UPDATES_FOLDER') . 'backup_core" a ' . constant('FS_FOLDER') . ' finalizada: ' . ($status ? 'Correctamente' : 'FALLO') . '.');

        return $status;
    }

    /**
     * Restaura los plugins anteriores y devuelve su estado.
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2021.09
     *
     * @return bool
     */
    public function restore_old_plugins()
    {
        $this->core_log->new_message('Restauración de plugins de "' . constant('FS_UPDATES_FOLDER') . 'backups' . DIRECTORY_SEPARATOR . date('Y-m-d') . '" a "' . constant('FS_FOLDER') . DIRECTORY_SEPARATOR . 'plugins' . '".');

        $status = fs_file_manager::recurse_copy(FS_UPDATES_FOLDER . 'backups' . DIRECTORY_SEPARATOR . date('Y-m-d'), constant('FS_FOLDER') . DIRECTORY_SEPARATOR . 'plugins');

        $this->core_log->new_message('Restauración de plugins finalizada: ' . ($status ? 'Correctamente' : 'FALLO') . '.');

        return $status;
    }

    /**
     * Devuelve la lista de actualizaciones disponibles
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2021.08
     *
     * @return array
     */
    public function get_available_updates()
    {
        $this->get_updates();

        $updates = [
            'core' => ($this->updates['core'] ? [$this->updates['core']] : []),
            'plugins' => $this->updates['plugins'],
        ];

        return $updates;
    }
}
