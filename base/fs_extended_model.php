<?php

/*
 * This file is part of MiFactura.eu
 * Copyright (C) 2021 X-Net Software Solutions S.L. <xnetsoftware@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

require_once constant('BASE_PATH') . '/base/fs_model.php';

/**
 * La clase de la que heredan todos los modelos que a su vez están implicados con un fs_list_controller y/o fs_edit_controller.
 */
abstract class fs_extended_model extends fs_model
{
    /**
     * Lista de campos del modelo.
     *
     * @var array
     */
    private static $model_fields = [];

    /**
     * fs_extended_model constructor.
     *
     * @param string     $table_name
     * @param array|bool $data
     */
    public function __construct($table_name, $data = false)
    {
        parent::__construct($table_name);
        if (empty($data)) {
            $this->clear();
        } else {
            $this->load_from_data($data);
        }
    }

    /**
     * Limpia los valores del modelo, asignando sus propiedades a null.
     */
    public function clear()
    {
        foreach (array_keys($this->get_model_fields()) as $field) {
            $this->{$field} = null;
        }
    }

    /**
     * Devuelve los campos del modelo.
     *
     * @return array
     */
    public function get_model_fields()
    {
        $table_name = $this->table_name();
        if (!isset(self::$model_fields[$table_name])) {
            self::$model_fields[$table_name] = [];
            foreach ($this->db->get_columns($table_name) as $column) {
                self::$model_fields[$table_name][$column['name']] = $column['type'];
            }
        }

        return self::$model_fields[$table_name];
    }

    /**
     * Carga los datos recibidos en el modelo.
     *
     * @param array $data
     */
    public function load_from_data($data)
    {
        $fields = $this->get_model_fields();
        foreach ($data as $key => $value) {
            if (!isset($fields[$key])) {
                $this->{$key} = $value;
                continue;
            }

            $field_type = $fields[$key];
            $type = (strpos($field_type, '(') === false) ? $field_type : substr($field_type, 0,
                strpos($field_type, '('));
            switch ($type) {
                case 'tinyint':
                case 'boolean':
                    $this->{$key} = $this->str2bool($value);
                    break;

                case 'integer':
                case 'int':
                    $this->{$key} = $this->intval($value);
                    break;

                case 'double':
                case 'double precision':
                case 'float':
                    $this->{$key} = empty($value) ? 0.00 : (float) $value;
                    break;

                case 'date':
                    $this->{$key} = empty($value) ? null : date('Y-m-d', strtotime($value));
                    break;

                default:
                    $this->{$key} = $value;
            }
        }
    }

    /**
     * Elimina el registro de la tabla, devuelve el resultado de la consulta.
     *
     * @return bool
     */
    public function delete()
    {
        $sql = "DELETE FROM `" . $this->table_name() . "`"
            . " WHERE " . $this->primary_column() . " = " . $this->var2str($this->primary_column_value());
        return $this->db->exec($sql);
    }

    /**
     * Devuelve la clave primaria de la tabla.
     *
     * @return string
     */
    abstract public function primary_column();

    /**
     * Devuelve el valor de la clave primaria del modelo.
     *
     * @return mixed
     */
    public function primary_column_value()
    {
        return $this->{$this->primary_column()};
    }

    /**
     * Devuelve el objeto obtenido a partir del valor y clave indicados.
     * Por defecto la clave es la clave primaria del modelo.
     *
     * @param string $code
     * @param string $col_name
     *
     * @return fs_extended_model|bool
     */
    public function get($code, $col_name = '')
    {
        $column = empty($col_name) ? $this->primary_column() : $col_name;
        $sql = "SELECT *"
            . " FROM `" . $this->table_name() . "`"
            . " WHERE " . $column . " = " . $this->var2str($code)
            . ";";
        $data = $this->db->select($sql);
        if (empty($data)) {
            return false;
        }

        $model_class = $this->model_class_name();
        return new $model_class($data[0]);
    }

    public function getById($id)
    {
        return $this->get($id);
    }

    /**
     * Retorna el nombre de la clase modelo.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0305
     *
     * @return string
     */
    public function model_class_name()
    {
        return get_class_name($this);
    }

    /**
     * Carga un registro a partir de su clave primaria.
     *
     * @param string $code Valor de la clave primaria
     *
     * @return bool
     */
    public function load_from_code($code)
    {
        $sql = "SELECT *"
            . " FROM `" . $this->table_name() . "`"
            . " WHERE " . $this->primary_column() . " = " . $this->var2str($code)
            . ";";
        $data = $this->db->select($sql);
        if (empty($data)) {
            return false;
        }

        $this->load_from_data($data[0]);
        return true;
    }

    /**
     * Devuelve true si ha añadido o actualizado el registro, si no, false
     *
     * @return bool
     */
    public function save()
    {
        if (!$this->test()) {
            return false;
        }

        if ($this->use_created_at && !isset($this->created_at)) {
            $this->created_at = gmdate('Y-m-d H:i:s');
        }

        if ($this->use_updated_at) {
            $this->updated_at = gmdate('Y-m-d H:i:s');
        }

        if ($this->exists()) {
            return $this->save_update();
        }

        return $this->save_insert();
    }

    /**
     * Comprueba los datos del modelo, devuelve TRUE si son correctos
     *
     * @return bool
     */
    public abstract function test();

    /**
     * Esta función devuelve TRUE si los datos del objeto se encuentran
     * en la base de datos.
     *
     * @return bool
     */
    public function exists()
    {
        if (is_null($this->primary_column_value())) {
            return false;
        }

        $sql = "SELECT *"
            . " FROM `" . $this->table_name() . "`"
            . " WHERE " . $this->primary_column() . " = " . $this->var2str($this->primary_column_value())
            . ";";
        return (bool) $this->db->select($sql);
    }

    /**
     * Devuelve las url para la inserción, modificación y listado de los datos.
     *
     * @param string $type
     *
     * @return string
     */
    public function url($type = 'auto')
    {
        $edit_url = 'index.php?page=edit_' . $this->model_class_name() . '&code=' . $this->primary_column_value();
        $list_url = 'index.php?page=list_' . $this->model_class_name();

        switch ($type) {
            case 'edit':
                return $edit_url;

            case 'list':
                return $list_url;

            case 'new':
                return 'index.php?page=edit_' . $this->model_class_name();

            default:
                return is_null($this->primary_column_value()) ? $list_url : $edit_url;
        }
    }

    /**
     * Devuelve todos los registros de la tabla
     *
     * @return static[]
     */
    public function all()
    {
        $sql = "SELECT *"
            . " FROM `" . $this->table_name() . "`"
            . " ORDER BY " . $this->primary_column() . " ASC;";
        return $this->all_from($sql, 0, 0);
    }

    /**
     * Devuelve todos los registros de la tabla aplicando las cláusulas where, order, offset y limit indicadas
     * TODO: Este método podría sustituir perfectamente a all.
     *
     * Aquí hay algún tipo de inconsistencia, porque para formar los array se usan las clases getWhereValues y
     * getRequestArray y parece que no están funcionando bien.
     * El único ejemplo que he visto en el que se usa es en ApiModel, y retorna siempre un array vacío en ambos.
     *
     * TODO: Entiendo que este método tendría que leer los array y contatenar, pero podría estar mal implementado.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2021.10
     *
     * @param array $where
     * @param array $order
     * @param int   $offset
     * @param int   $limit
     *
     * @return static[]
     */
    public function all_query(array $where = [], array $order = [], int $offset = 0, int $limit = FS_ITEM_LIMIT)
    {
        $sql = 'SELECT *'
            . ' FROM ' . $this->table_name();
        if (!empty($where)) {
            $sql .= ' WHERE';
            $nexo = '';
            foreach ($where as $clause) {
                $sql .= $nexo . $clause;
                $nexo = ' AND ';
            }
        }
        if (!empty($order)) {
            $sql .= ' ORDER BY';
            $nexo = '';
            foreach ($order as $clause) {
                $sql .= $nexo . $clause;
                $nexo = ', ';
            }
        }
        $sql .= ' LIMIT ' . $limit . ' OFFSET ' . $offset . ';';

        return $this->all_from($sql, $offset, $limit);
    }

    /**
     * Realiza la operación update para el save.
     *
     * @return bool
     */
    protected function save_update()
    {
        $sql = 'UPDATE `' . $this->table_name() . '`';
        $coma = ' SET ';
        foreach (array_keys($this->get_model_fields()) as $field) {
            if ($field == $this->primary_column()) {
                continue;
            }

            $sql .= $coma . $field . ' = ' . $this->var2str($this->{$field});
            $coma = ', ';
        }

        $sql .= ' WHERE ' . $this->primary_column() . ' = ' . $this->var2str($this->primary_column_value())
            . ';';

        return $this->db->exec($sql);
    }

    /**
     * Realiza la operación insert para el save.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0630
     *
     * @return bool
     */
    protected function save_insert()
    {
        $columns = [];
        $values = [];

        $fields = $this->db->get_columns_details($this->table_name());
        foreach ($fields as $field => $value) {
            if ($field === $this->primary_column() && $value['extra'] === 'auto_increment') {
                continue;
            }
            $columns[] = $field;
            $values[] = $this->var2str($this->{$field});
        }

        $sql = 'INSERT INTO `' . $this->table_name() . '` (' . implode(', ', $columns) . ') VALUES (' . implode(', ',
                $values) . ');';

        if (!$this->db->exec($sql)) {
            return false;
        }

        if (null === $this->primary_column_value()) {
            $this->{$this->primary_column()} = $this->db->lastval();
        }

        return true;
    }

    /**
     * Retorna un array asociativo con las propiedades del modelo.
     * Usado por la API e importado de Alxarafe.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0610
     *
     * @param $id
     *
     * @return array
     */
    final public function getDataArray($id = null)
    {
        $result = [];
        if (!isset($id) && !isset($this->id)) {
            $result;
        }

        $data = $this->getById($id ?? $this->id);
        foreach (get_object_vars($data) as $propiedad => $value) {
            if (is_object($value) || $propiedad === 'table_name') {
                continue;
            }
            $result[$propiedad] = $data->{$propiedad};
        }
        return $result;
    }

}
