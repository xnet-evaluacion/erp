<?php

define('TMP_FOLDER', 'tmp');

if (!file_exists(constant('BASE_PATH') . '/config.php')) {
    /// si no hay config.php redirigimos al instalador
    header('Location: install.php');
    die('Redireccionando al instalador...');
}

/// cargamos las constantes de configuración
require_once constant('BASE_PATH') . '/config.php';
require_once constant('BASE_PATH') . '/base/config2.php';
require_once constant('BASE_PATH') . '/base/fs_controller.php';
require_once constant('BASE_PATH') . '/base/fs_license_controller.php';
require_once constant('BASE_PATH') . '/base/fs_edit_controller.php';
require_once constant('BASE_PATH') . '/base/fs_list_controller.php';
require_once constant('BASE_PATH') . '/base/fs_log_manager.php';
require_once constant('BASE_PATH') . '/raintpl/rain.tpl.class.php';
require_once constant('BASE_PATH') . '/base/Blade.php';

// Nos permite recopilar crasheos en tiempo real sin que los usuarios deban realizar ningún reporte para hacernos conocedores del problema.
if (constant('USE_BUG_TRACKER')) {
    \Sentry\init([
        'dsn' => 'http://192f6ff7c1ec4bcf806b715483fba9bc@sentry.x-netdigital.com:9000/2',
        'environment' => constant('FS_DEBUG') ? 'production' : 'development',
    ]);
}

/**
 * Registramos la función para capturar los fatal error.
 * Información importante a la hora de depurar errores.
 */
register_shutdown_function("fatal_handler");
