<?php

/*
 * This file is part of MiFactura.eu
 * Copyright (C) 2021 X-Net Software Solutions S.L. <xnetsoftware@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

require_once constant('BASE_PATH') . '/base/fs_list_filter.php';

/**
 * Description of fs_list_filter_date
 */
class fs_list_filter_date extends fs_list_filter
{
    /**
     * Operación a contrastar.
     *
     * @var string
     */
    protected $operation;

    /**
     * fs_list_filter_date constructor.
     *
     * @param string $col_name
     * @param string $label
     * @param string $operation
     */
    public function __construct($col_name, $label, $operation)
    {
        parent::__construct($col_name, $label);
        $this->operation = $operation;
    }

    /**
     * Genera la parte WHERE de la SQL.
     *
     * @return string
     */
    public function get_where()
    {
        /// necesitamos un modelo, el que sea, para llamar a su función var2str()
        $fs_log = new fs_log();
        return $this->value ? ' AND ' . $this->col_name . ' ' . $this->operation . ' ' . $fs_log->var2str($this->value) : '';
    }

    /**
     * Devuelve el código HTML para mostrar el componente.
     *
     * @return string
     */
    public function show()
    {
        return '<div class="form-group">'
            . '    <div class="input-group">'
            . '        <span class="input-group-text">'
            . '            <i class="fa-solid fa-calendar fa-fw"></i>'
            . '        </span>'
            . '        <input type="date" name="' . $this->name() . '" value="' . $this->value . '" class="form-control" placeholder="' . $this->label . '" autocomplete="off">'
            . '    </div>'
            . '</div>';
    }

    /**
     * Devuelve el nombre del filtro.
     *
     * @return string
     */
    public function name()
    {
        switch ($this->operation) {
            case '>':
            case '>=':
                return parent::name() . '_gt';

            case '<':
            case '<=':
                return parent::name() . '_lt';

            default:
                return parent::name();
        }
    }
}
