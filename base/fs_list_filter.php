<?php

/*
 * This file is part of MiFactura.eu
 * Copyright (C) 2021 X-Net Software Solutions S.L. <xnetsoftware@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Description of fs_list_filter
 */
abstract class fs_list_filter
{
    /**
     * Nombre de la columna a filtrar.
     *
     * @var string
     */
    public $col_name;

    /**
     * Etiqueta a mostrar para la columna.
     *
     * @var string
     */
    public $label;

    /**
     * Valor a filtrar.
     *
     * @var mixed
     */
    public $value;

    /**
     * fs_list_filter constructor.
     *
     * @param string $col_name
     * @param string $label
     */
    public function __construct($col_name, $label)
    {
        $this->col_name = $col_name;
        $this->label = $label;
    }

    /**
     * Genera la parte WHERE de la SQL.
     *
     * @return mixed
     */
    abstract public function get_where();

    /**
     * Devuelve el código HTML para mostrar el componente.
     *
     * @return mixed
     */
    abstract public function show();

    /**
     * Devuelve el nombre del filtro.
     *
     * @return string
     */
    public function name()
    {
        return 'filter_' . $this->col_name;
    }
}
