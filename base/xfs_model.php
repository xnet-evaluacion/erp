<?php

/*
 * This file is part of plugin xnetcommonhelpers for MiFactura.eu
 * Copyright (C) 2021 X-Net Software Solutions S.L. <xnetsoftware@gmail.com>
 */

require_once constant('BASE_PATH') . '/base/fs_model.php';

/**
 * Class xfs_model
 *
 * Ante la inconveniencia de modificar los archivos originales de FS, se procede a extender
 * la clase fs_extended_model como xfs_model, con idea de poder realizar adaptaciones de los
 * modelos para el uso particular de los plugins de XFS.
 *
 * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
 * @version 2021.04
 */
abstract class xfs_model extends fs_extended_model
{
    public $created_at;
    public $updated_at;
    public $deleted_at;

    /**
     * Crea un segmento de código SQL para filtrar un campo de fecha ($field) entre dos fechas.
     * Las fechas pueden ser false si se quiere filtrar sólo por una de ellas.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2021.04
     *
     * @param string|false $fecha_hasta
     *
     * @param string       $field
     * @param string|false $fecha_desde
     *
     * @return string
     *
     */
    static public function filter_date($field, $fecha_desde, $fecha_hasta)
    {
        // Si no hay filtros, se retorna la cadena vacía (nada que añadir)
        if (!$fecha_desde && !$fecha_hasta) {
            return '';
        }

        if ($fecha_desde) {
            $fecha_desde = date('Y-m-d', strtotime($fecha_desde));
        }

        if ($fecha_hasta) {
            $fecha_hasta = date('Y-m-d', strtotime($fecha_hasta));
        }

        $sql = "date($field) ";

        if ($fecha_desde && $fecha_hasta) {
            $sql .= "BETWEEN date('$fecha_desde') AND date('$fecha_hasta')";
        } elseif ($fecha_desde) {
            $sql .= ">= date('$fecha_desde')";
        } else {
            $sql .= "<= date('$fecha_hasta')";
        }

        return $sql;
    }

    /**
     * Crea un segmento de código SQL para filtrar un campo de fecha ($field) entre dos fechas.
     * Las fechas pueden ser false si se quiere filtrar sólo por una de ellas.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2021.04
     *
     * @param string|false $fecha_hasta
     *
     * @param string       $field
     * @param string|false $fecha_desde
     *
     * @return string
     *
     */
    static public function filter_datetime($field, $fecha_desde, $fecha_hasta)
    {
        // Si no hay filtros, se retorna la cadena vacía (nada que añadir)
        if (!$fecha_desde && !$fecha_hasta) {
            return '';
        }

        if ($fecha_desde) {
            $fecha_desde = date('Y-m-d H:i:s', strtotime($fecha_desde));
        }

        if ($fecha_hasta) {
            $fecha_hasta = date('Y-m-d 23:59:59', strtotime($fecha_hasta));
        }

        $sql = "$field ";

        if ($fecha_desde && $fecha_hasta) {
            $sql .= "BETWEEN '$fecha_desde' AND '$fecha_hasta'";
        } elseif ($fecha_desde) {
            $sql .= ">= '$fecha_desde'";
        } else {
            $sql .= "<= '$fecha_hasta'";
        }

        return $sql;
    }

    /**
     * Realiza una consulta, obteniendo un array con todos los registros que coincidan con el array asociativo
     * $key=>value proporcionado, siendo $key el campo y $value, el valor esperado para el campo. Los campos se
     * relacionan siempre con un 'AND'.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2021.05
     *
     * @param array $fields
     * @param array $orderby
     *
     * @return array
     */
    public function get_by($fields, $orderby = [])
    {
        if (!is_array($fields)) {
            $this->new_error_msg('xfs_model::get_by necesita como parámetro un array asociativo');
        }

        $where_clause = '';
        $concat = '';
        foreach ($fields as $field => $_value) {
            switch ($_value) {
                case null:
                    $value = $_value . ' IS NULL';
                    break;
                default:
                    if (is_array($_value)) {
                        $value = ' IN(' . implode(',', $_value) . ')';
                    } else {
                        $value = "= '$_value'";
                    }
            }
            $where_clause .= "$concat {$field} {$value}";
            $concat = ' AND ';
        }

        if ($where_clause == '') {
            $where_clause = 'TRUE';
        }

        $sql = "SELECT *"
            . " FROM `" . $this->table_name() . "`"
            . " WHERE " . $where_clause;
        if (count($orderby) > 0) {
            $sql .= ' ORDER BY';
            $concat = ' ';
            foreach ($orderby as $order) {
                $sql .= $concat . $order;
                $concat = ', ';
            }
            $sql .= ';';
        }

        $data = $this->db->select($sql);
        if (empty($data)) {
            return [];
        }

        $index = $this->primary_column();
        $result = [];
        foreach ($data as $record) {
            $result[$record[$index]] = $record;
        }
        return $result;
    }

    /**
     * Realiza una consulta, obteniendo un array con todos los registros que coincidan con el array asociativo
     * $index=>[$key=>value] proporcionado, siendo $key el campo y $value, el valor esperado para el campo. Cada $index
     * significa una operación OR, mientras que lo que hay dentro del array se une con AND.
     *
     * @example $fields = [[id => 1, name => example1], [id=>2]] es equivalente a WHERE (id==1 AND name==example1) OR (id==2)
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2021.05
     *
     * @param array $fields
     * @param array $orderby
     *
     * @return array
     */
    public function get_by_with_or($fields, $orderby = [])
    {
        if (!is_array($fields)) {
            $this->new_error_msg('xfs_model::get_by necesita como parámetro un array asociativo');
        }

        $where_clause = '';
        $concat = '';
        foreach ($fields as $_value){
            $where_clause .= "$concat (";
            $concat = '';
            foreach ($_value as $f => $v) {
                switch ($v) {
                    case null:
                        $value = $v . ' IS NULL';
                        break;
                    default:
                        if (is_array($v)) {
                            $value = ' IN(' . implode(',', $_value) . ')';
                        } else {
                            $value = "= '$v'";
                        }
                }
                $where_clause .= "$concat {$f} {$value}";
                $concat = ' AND ';
            }
            $where_clause .= ')';
            $concat = ' OR ';
        }

        if ($where_clause == '') {
            $where_clause = 'TRUE';
        }

        $sql = "SELECT *"
            . " FROM `" . $this->table_name() . "`"
            . " WHERE " . $where_clause;
        if (count($orderby) > 0) {
            $sql .= ' ORDER BY';
            $concat = ' ';
            foreach ($orderby as $order) {
                $sql .= $concat . $order;
                $concat = ', ';
            }
            $sql .= ';';
        }

        $data = $this->db->select($sql);
        if (empty($data)) {
            return [];
        }

        $index = $this->primary_column();
        $result = [];
        foreach ($data as $record) {
            $result[$record[$index]] = $record;
        }
        return $result;
    }

    /**
     * Obtiene un array con los registros cuyo campo $field, tenga el valor $value.
     * Opcionalmente, se puede pasar un campo $fields, con el listado de campos a retornar separados por coma.
     * El índice del array, será el ID del archivo.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2021.04
     *
     * @param string $fields
     *
     * @param string $field
     * @param mixed  $value
     *
     * @return array
     *
     */
    public function get_all($field, $value, $fields = '*')
    {
        $sql = "SELECT {$fields} FROM `" . $this->table_name() . "`"
            . " WHERE {$field} = " . $this->var2str($value);
        $data = $this->db->select($sql);
        if (empty($data)) {
            return [];
        }

        $index = $this->primary_column();
        $result = [];
        foreach ($data as $record) {
            $result[$record[$index]] = $record;
        }
        return $result;
    }

    /**
     * Retorna un array asociativo con id=>nombre
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2021.04
     * @return array
     *
     */
    public function get_all_names()
    {
        $pk = $this->primary_column();
        $todos = $this->all();
        $return = [];
        foreach ($todos as $value) {
            $return[$value->{$pk}] = $value->get_name();
        }
        return $return;
    }

    /**
     * Devuelve un array con todos las registros.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2021.04
     * @return static[]
     *
     */
    public function all($deleted = false)
    {
        $where = '';
        if (!$deleted && $this->use_deleted_at) {
            $where = ' WHERE deleted_at IS NULL';
        }

        $list = [];

        $sql = "SELECT *"
            . " FROM `" . $this->table_name() . "`" . $where
            . ";";

        $data = $this->db->select($sql);
        if ($data) {
            foreach ($data as $d) {
                $list[] = new static($d);
            }
        }
        return $list;
    }

    /**
     * Define un método para obtener el nombre del registro actual
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2021.04
     * @return string
     *
     */
    abstract public function get_name();

    /**
     * Retorna un array asociativo con id=>registro
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2021.04
     * @return array
     *
     */
    public function get_all_values()
    {
        $pk = $this->primary_column();
        $todos = $this->all();
        $return = [];
        foreach ($todos as $value) {
            $return[$value->{$pk}] = $value;
        }
        return $return;
    }

    /**
     * Devuelve el numero total registros.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2021.04
     * @return int
     *
     */
    public function count()
    {
        $sql = "SELECT COUNT(id) AS total FROM " . $this->table_name()
            . ";";
        $data = $this->db->select($sql);
        return (empty($data) ? 0 : (int) $data[0]['total']);
    }

    /**
     * Retorna el valor de la clave primaria para el registro en curso
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0305
     *
     * @return mixed
     */
    public function get_id()
    {
        return $this->{$this->primary_column()};
    }

    /**
     * Comprueba los datos del modelo, devuelve TRUE si son correctos
     *
     * @return bool
     */
    public function test()
    {
        return true;
    }

    /**
     * Inserta o actualiza un registro.
     * Lo busca primero por los datos proporcionados en unique.
     * Si existe lo actualiza, y si no existe (o unique es un array vacío), lo crea.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0704
     *
     * @param $data
     * @param $unique
     */
    public function import_record($data, $unique = [])
    {
        $record = clone $this;
        if (!empty($unique)) {
            $search = $record->get_by($unique);
            if (count($search > 0)) {
                $datum = reset($search);
                dump($datum);
                dump($this->primary_column());
                $record = $record->get($datum[$this->primary_column()]);
            }
        }
        foreach ($data as $key => $datum) {
            $record->{$key} = $datum;
        }
        return $record->save();
    }
}
