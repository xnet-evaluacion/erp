<?php

/*
 * This file is part of MiFactura.eu
 * Copyright (C) 2021 X-Net Software Solutions S.L. <xnetsoftware@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

require_once constant('BASE_PATH') . '/base/fs_list_filter.php';

/**
 * Description of fs_list_filter_select
 */
class fs_list_filter_select extends fs_list_filter
{
    /**
     * Valores a mostrar para poder seleccionar.
     *
     * @var array
     */
    protected $values;

    /**
     * fs_list_filter_select constructor.
     *
     * @param string $col_name
     * @param string $label
     * @param array  $values
     */
    public function __construct($col_name, $label, $values)
    {
        parent::__construct($col_name, $label);
        $this->values = $values;
    }

    /**
     * Genera la parte WHERE de la SQL.
     *
     * @return string
     */
    public function get_where()
    {
        /// necesitamos un modelo, el que sea, para llamar a su función var2str()
        $fs_log = new fs_log();
        return empty($this->value) ? '' : ' AND ' . $this->col_name . ' = ' . $fs_log->var2str($this->value);
    }

    /**
     * Devuelve el código HTML para mostrar el componente.
     *
     * @return string
     */
    public function show()
    {
        $html = '<label>' . $this->label. '</label><div class="mb-2"><select class="form-control select2" name="' . $this->name() . '">'
            . '<option value="">Cualquier ' . $this->label . '</option>';

        foreach ($this->values as $key => $value) {
            if ($key === $this->value) {
                $html .= '<option value="' . $key . '" selected="">' . $value . '</option>';
            } else {
                $html .= '<option value="' . $key . '">' . $value . '</option>';
            }
        }

        $html .= '</select></div>';
        return $html;
    }
}
