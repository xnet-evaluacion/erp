@if (!$fsc->hide_title)
    <div class="container-fluid">
        <div class="row mb-3">
            <div class="col-12">
                <div class="page-title-box d-sm-flex align-items-center justify-content-between pb-2">
                    <h4 class="mb-sm-0 font-size-18">
                        {!! $fsc->page->title !!}
                    </h4>

                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item text-capitalize">
                                {!! $fsc->page->folder !!}
                            </li>
                            <li class="breadcrumb-item active">
                                {!! $fsc->page->title !!}
                            </li>
                            <li class="breadcrumb-item active">
                                @if (!isset($fsc->mainModel))
                                    <b>Alta de registro</b>
                                @elseif (empty($fsc->mainModel->primaryKeyValue()))
                                    <b>Alta de registro.</b>
                                @else
                                    {!! $fsc->mainModel->primaryKeyValue() !!}
                                @endif
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
            @if (view_exists('block/description/' . $fsc->page->name))
                <div class="col-12">
                    @include('block/description/' . $fsc->page->name)
                </div>
            @endif
        </div>

        <div class="row">
            <div class="col-12">
                <div class="card border">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12">
                                <div class="d-sm-flex align-items-center justify-content-between p-0">
                                    <div class="buttons-left p-0">
                                        <div class="d-flex flex-wrap gap-1 p-0">
                                            <div class="d-flex btn-group" role="group">
                                                <button class="d-flex justify-content-center align-items-center btn btn-outline-secondary" type="submit" name="action" value="Back" title="Volver" formnovalidate>
                                                    <i class="fa-solid fa-chevron-left fa-fw"></i>
                                                    <span>Todo</span>
                                                </button>
                                                @include('block/menu_buttons/refresh_action')
                                            </div>

                                            @if (view_exists('block/menu_buttons/left/' . $fsc->page->name))
                                                @include('block/menu_buttons/left/' . $fsc->page->name)
                                            @else
                                                @include('block/extensions/buttons', ['page_params' => ''])
                                            @endif

                                            @if ($fsc->haveAccessTo('New'))
                                                <button class="d-flex justify-content-center align-items-center btn btn-success" type="submit" name="action" value="New" title="Nuevo">
                                                    <i class="fa-solid fa-plus fa-fw"></i>
                                                    <span>Nuevo</span>
                                                </button>
                                            @endif
                                        </div>
                                    </div>
                                    @if (view_exists('block/menu_buttons/right/' . $fsc->page->name))
                                        <div class="buttons-right mt-3 mt-sm-0 mt-md-0 mt-xl-0 p-0">
                                            <div class="d-flex flex-wrap gap-1 px-1">
                                                @include('block/menu_buttons/right/' . $fsc->page->name)
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif
