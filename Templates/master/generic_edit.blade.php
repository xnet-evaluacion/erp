@extends('layouts/main')

@section('main-content')
    <form action="{!! $fsc->url() !!}" method="post" class="form">
        @include('master/generic_template_header_edit')

        @if ($fsc->mainModel)
            @if (isset($fsc->template_top))
                @include($fsc->template_top)
            @endif

            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card border">
                            @include('block/card_page_header')
                            <div class="card-body">
                                <input type="hidden" name="petition_id" value="{!! $fsc->random_string() !!}"/>
                                @if (isset($fsc->code))
                                <input type="hidden" name="record_code" value="{!! $fsc->code !!}"/>
                                @endif
                                @foreach ($fsc->fields as $row)
                                    <div class="row">
                                        @foreach ($row as $key1 => $value1)
                                            {!! fs_fix_html($fsc->show($key1, $value1, $fsc->mainModel)) !!}
                                        @endforeach
                                    </div>
                                @endforeach
                            </div>
                            <div class="card-footer">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="d-flex justify-content-start">
                                            @if ($fsc->haveAccessTo('Delete'))
                                                <button class="btn btn-danger flex-grow-1 flex-sm-grow-0" type="submit" name="action" value="Delete" title="Eliminar" onclick="allowClose();confirmDeleteRecord('{{ $fsc->url() }}&code={{ $fsc->code }}&action=delete');return false;">
                                                    <i class="fa-solid fa-trash-can fa-fw"></i>
                                                    <span>Eliminar</span>
                                                </button>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="d-flex justify-content-end mt-2 mt-sm-0">
                                            <button class="btn btn-outline-secondary waves-effect waves-light flex-grow-1 flex-sm-grow-0 mx-1" type="submit" name="action" value="Cancel" title="Guardar" onclick="allowClose();" formnovalidate>
                                                <i class="fa-solid fa-undo fa-fw"></i>
                                                <span>Deshacer</span>
                                            </button>
                                            @if ($fsc->haveAccessTo('Save'))
                                                <button class="btn btn-primary flex-grow-1 flex-sm-grow-0 mx-1" type="submit" name="action" value="Save" title="Guardar" onclick="allowClose();">
                                                    <i class="fa-solid fa-save fa-fw"></i>
                                                    <span>Guardar</span>
                                                </button>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            @if (isset($fsc->template_bottom))
                @include($fsc->template_bottom)
            @endif
        @endif
    </form>
@endsection

@section('javascripts')
    @parent
    @include('block/javascripts/generic_edit')
@endsection
