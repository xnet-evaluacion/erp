@extends('layouts/main')

@section('main-content')
    @include('master/template_header_edit')

    @if ($fsc->model)
        @if (isset($fsc->template_top))
            @include($fsc->template_top)
        @endif

        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <form action="{!! $fsc->model->url('edit') !!}" method="post" class="form">
                        <div class="card border">
                            @include('block/card_page_header')
                            <div class="card-body">
                                <input type="hidden" name="action" value="edit"/>
                                <input type="hidden" name="petition_id" value="{!! $fsc->random_string() !!}"/>
                                <div class="row">
                                    @foreach ($fsc->form->columns as $key1 => $value1)
                                        <div class="{!! $value1['num_cols'] !!}">
                                            {!! fs_fix_html($fsc->form->show($key1, $value1, $fsc->model)) !!}
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="d-flex justify-content-start">
                                            <a href="#" id="btn_delete_model" class="btn btn-danger flex-grow-1 flex-sm-grow-0">
                                                <i class="fa-solid fa-trash-can fa-fw"></i>
                                                <span>Eliminar</span>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="d-flex justify-content-end mt-2 mt-sm-0">
                                            <a href="{!! $fsc->model->url() !!}" class="btn btn-outline-secondary waves-effect waves-light flex-grow-1 flex-sm-grow-0 mx-1" title="Deshacer" aria-label="Deshacer">
                                                <i class="fa-solid fa-undo fa-fw"></i>
                                                <span>Deshacer</span>
                                            </a>
                                            <button class="btn btn-primary flex-grow-1 flex-sm-grow-0 mx-1" type="submit" title="Guardar">
                                                <i class="fa-solid fa-save fa-fw"></i>
                                                <span>Guardar</span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        @if (isset($fsc->template_bottom))
            @include($fsc->template_bottom)
        @endif
    @endif
@endsection

@section('javascripts')
    @parent
    @include('block/javascripts/common/edit_controller')
@endsection
