<div class="row">
    <div class="col-12">
        <div class="table-responsive-datatable">
            <table id="table_{!! $fsc->page->name !!}" class="table align-middle table-datatable table-borderless table-striped table-hover w-100">
                <thead class="table-dark">
                <tr>
                    @foreach ($fsc->decoration->get_columns($fsc->active_tab) as $key1 => $value1)
                        <th class="{!! !is_array($value1['class']) ? $value1['class'] : '' !!}">{!! ucfirst($value1['title']) !!}</th>
                    @endforeach
                </tr>
                </thead>
                <tbody>
                @foreach ($fsc->get_current_tab('cursor') as $key1 => $value1)
                    <tr class="{!! $fsc->decoration->row_class($fsc->active_tab, $value1) !!}">
                        @foreach ($fsc->decoration->get_columns($fsc->active_tab) as $key2 => $value2)
                            {!! fs_fix_html($fsc->decoration->show($key2, $value2, $value1)) !!}
                        @endforeach
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="row mt-3">
    <div class="col-12">
        <nav aria-label="Paginación">
            <ul class="pagination justify-content-center m-0">
                @foreach ($fsc->get_pagination() as $key1 => $value1)
                    <li class="page-item @if ($value1['active']) active @endif ">
                        <a onclick="document.f_custom_search.offset.value = '{!! $value1['offset'] !!}';document.f_custom_search.submit();" class="page-link">{!! $value1['num'] !!}</a>
                    </li>
                @endforeach
            </ul>
        </nav>
    </div>
</div>
