@extends('layouts/main')

@section('main-content')
    @include('master/generic_template_header')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card border">
                    <div class="card-header mt-2 text-truncate">
                        <i class="fa-solid fa-copy fa-fw"></i>
                        {!! $fsc->title !!}
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12 text-justify">
                                <small>
                                    <a href="{!! $fsc->url() !!}" class="link-primary">{!! $fsc->articulo->referencia !!}</a>
                                </small>
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-12">
                                <div class="table-responsive-datatable">
                                    <table id="table_compras_pedidos_articulo" class="table align-middle table-datatable table-borderless table-striped table-hover w-100">
                                        <thead class="table-dark">
                                        <tr>
                                            @foreach($fsc->getHeaders() as $value1)
                                            <th class="text-center">{!! $value1 !!}</th>
                                            @endforeach
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($fsc->mainModel->all() as $key1 => $value1)
                                            @php
                                                $class = '';
                                                if ($value1->cantidad<1) {
                                                    $class = 'table-warning';
                                                }
                                            @endphp
                                            <tr class="{!! $class !!} clickableRow" href="{!! $fsc->mainModel->url($fsc->getClassName()) !!}&code={!! $value1->{$fsc->mainModel->primaryKey()} !!}">
                                                @foreach($fsc->getHeaders() as $key2=>$value2)
                                                <td class="text-center text-nowrap text-truncate">
                                                    {!! $value1->{$key2} !!}
                                                </td>
                                                @endforeach
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-12">
                                <ul class="pager">
                                    @if ($fsc->anterior_url()!='')
                                        <li class="previous">
                                            <a href="{!! $fsc->anterior_url() !!}" class="link-primary">
                                                <i class="fa-solid fa-chevron-left fa-fw"></i>
                                                <span>Anteriores</span>
                                            </a>
                                        </li>
                                    @endif
                                    @if ($fsc->siguiente_url()!='')
                                        <li class="next">
                                            <a href="{!! $fsc->siguiente_url() !!}" class="link-primary">
                                                Siguientes<i class="fa-solid fa-chevron-right fa-fw"></i>
                                            </a>
                                        </li>
                                    @endif
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
