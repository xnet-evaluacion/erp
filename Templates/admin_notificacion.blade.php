@extends('layouts/main')

@section('main-content')
    @include('master/template_header')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card border">
                    @include('block/card_page_header')
                    <div class="card-body px-0">
                        <div class="row">
                            <div class="col-12">
                                <div class="tab-content nav-content">
                                    <div class="row my-3">
                                        <div class="col-12">
                                            <div class="row">
                                                <div class="col-sm-8 col-md-9 col-lg-10">
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="d-flex justify-content-end p-0">
                                                                <a href="#" class="flex-grow-1 flex-sm-grow-0 btn btn-success" data-bs-toggle="modal" data-bs-target="#modal_nuevo_notificacion">
                                                                    <i class="fa-solid fa-plus fa-fw"></i>
                                                                    Añadir
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row my-3">
                                        <div class="col-12">
                                            <div class="table-responsive-datatable">
                                                <table id="table_admin_users_usuarios" class="table align-middle table-datatable table-borderless table-striped table-hover w-100">
                                                    <thead class="table-dark">
                                                    <tr>
                                                        <th class="text-start">ID</th>
                                                        <th class="text-start">Fecha</th>
                                                        <th class="text-start">Mensaje</th>
                                                        <th class="text-start">Enlace</th>
                                                        <th class="text-start">Asignado</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach ($fsc->lista_notificaciones as $item_notificacion)
                                                        <tr class="">
                                                            <td class="text-start text-nowrap text-truncate">
                                                                <a href="index.php?page=admin_notificacion&action=edit-detalle&id={!! $item_notificacion->id !!}" class="link-primary">{!! $item_notificacion->id !!}</a>
                                                            </td>
                                                            <td class="text-start text-nowrap text-truncate">{!! $item_notificacion->f_emision !!}</td>
                                                            <td class="text-start text-nowrap text-truncate">{!! $item_notificacion->mensaje !!}</td>
                                                            <td class="text-start text-nowrap text-truncate">{!! $item_notificacion->enlace !!}</td>
                                                            <td class="text-start text-nowrap text-truncate">{!! $item_notificacion->codagente_destino !!}</td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="container-fluid">
        <form class="form" role="form" name="f_nuevo_notificacion" action="index.php?page=admin_notificacion" method="post">
            <div class="modal" id="modal_nuevo_notificacion" style="display: none;" aria-hidden="true">
                <div class="modal-dialog modal-xl modal-fullscreen-xl-down">
                    <div class="modal-content">
                        <input class="form-control" type="hidden" name="action" value="guardar_notificacion">
                        <div class="modal-header">
                            <h4 class="modal-title">
                                <i class="fa-solid fa-user fa-fw"></i>
                                Nueva notificación
                            </h4>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Cerrar"></button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-6">
                                    <label>
                                        Tipo
                                    </label>
                                    <select class="form-select" aria-label="Default select example" name="vidtipo_notificacion">
                                        @foreach($fsc->lista_notificaciones_tipo as $item_tipo)
                                            <option value="{!! $item_tipo->id !!}">{!! $item_tipo->nombre !!}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="col-12 col-sm-12 col-md-6">
                                    <label>
                                        Dirigida a
                                    </label>
                                    <select class="form-select" aria-label="Default select example" name="vcodagente_destino">
                                        @foreach($fsc->list_empleado as $item_empleado)
                                            <option value="{!! $item_empleado->codagente !!}">{!! $item_empleado->nombre.' '.$item_empleado->apellidos !!}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="col-12 col-sm-12 col-md-6">
                                    <label>
                                        Mensaje
                                    </label>
                                    <input class="form-control" type="text" name="vmensaje" autocomplete="off">
                                </div>
                                <div class="col-12 col-sm-12 col-md-6">
                                    <label>
                                        Enlace
                                    </label>
                                    <input class="form-control" type="text" name="venlace" autocomplete="off">
                                </div>
                                <div class="col-12 col-sm-12 col-md-6">
                                    <div class="form-group"><label>Fecha Aviso</label>
                                        <div class="input-group">
                                            <span class="input-group-text">
                                                <i class="fa-solid fa-calendar fa-fw"></i>
                                             </span>
                                            <input class="form-control" type="datetime-local" name="f_aviso" value="" autocomplete="off">
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-primary flex-grow-1 flex-sm-grow-0" type="submit" title="Guardar">
                                <i class="fa-solid fa-save fa-fw"></i>
                                <span>Guardar</span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection
