@extends('layouts/main')

@section('main-content')
    @include('master/template_header')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card border">
                    <div class="card-header mt-2 text-truncate">
                        <i class="{!! $fsc->notificaciones_tipo->estilo !!} {!! $fsc->notificaciones_tipo->icono !!}"></i>
                        Notificación #{!! $fsc->notificacion->id !!}
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12">
                                <div class="mb-2">
                                    <span class="font-size-14">
                                        <i class="fa-solid fa-comment fa-fw text-secondary"></i>
                                        Mensaje notificación
                                    </span>
                                    <p class="form-text ms-2">
                                        {!! $fsc->notificacion->mensaje !!}
                                    </p>
                                </div>
                            </div>
                        </div>

                        @if(!empty($fsc->notificacion->enlace))
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="mb-2">
                                        <span class="font-size-14">
                                            <i class="fa-solid fa-shapes fa-fw text-secondary"></i>
                                            Tipo
                                        </span>
                                        <p class="form-text ms-2">
                                            {!! $fsc->notificaciones_tipo->nombre !!}
                                        </p>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="mb-2">
                                        <span class="font-size-14">
                                            <i class="fa-solid fa-link fa-fw text-secondary"></i>
                                            Enlace
                                        </span>
                                        <p class="form-text ms-2">
                                            <a href="{!! $fsc->notificacion->enlace !!}" target="blank_">{!! $fsc->notificacion->enlace !!}</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        @endif

                        <div class="row mt-3">
                            <div class="col-12">
                                <div class="mb-2">
                                    <span class="font-size-14">
                                        <i class="fa-solid fa-square-arrow-up-right fa-fw text-secondary"></i>
                                        Emitido
                                    </span>
                                    <p class="form-text ms-2">
                                        {!! ($fsc->notificacion->f_emision) !!}
                                    </p>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="mb-2">
                                    <span class="font-size-14">
                                        <i class="fa-solid fa-check-double fa-fw text-secondary"></i>
                                        Estado lectura
                                    </span>
                                    <p class="form-text ms-2">
                                        {!! ($fsc->notificacion->f_lectura)? 'Leído el ' . show_date($fsc->notificacion->f_lectura) : 'Sin leer' !!}
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="row mt-3">
                            <div class="mb-2">
                                <form method="post" role="form" action="index.php?page=admin_notificacion&action=actualizar_leido" name="form" id="form">
                                    <input class="form-control" type="hidden" name="id" value="{!! $fsc->notificacion->id !!}"/>
                                    <input class="form-check-input" type="checkbox" value="leido" name="f_lectura" {!! ($fsc->notificacion->f_lectura)?'checked':'' !!}>
                                    <label class="form-check-span" for="f_lectura">
                                        Marcar como leído </label>
                                </form>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12">
                                <div class="d-flex justify-content-end">
                                    <a class="btn btn-danger mx-1 flex-grow-1 flex-sm-grow-0" type="button" href="index.php?{!! http_build_query(['page' => 'admin_notificacion', 'action' => 'eliminar', 'id' => $fsc->notificacion->id]) !!}">
                                        <i class="fa-solid fa-trash-can fa-fw"></i>
                                        <span>Eliminar Notificacion</span>
                                    </a>
                                    <button class="btn btn-primary mx-1 flex-grow-1 flex-sm-grow-0" type="submit" onclick="document.getElementById('form').submit();">
                                        <i class="fa-solid fa-save fa-fw"></i>
                                        <span>Guardar Cambio</span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
