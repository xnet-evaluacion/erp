@extends('layouts/main')

@section('main-content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card border">
                    <div class="card-header mt-2 text-truncate">
                        <i class="fa-solid fa-user fa-fw"></i>
                        Su cuenta está bloqueada
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-3"></div>
                            <div class="col-sm-6">
                                <h4 class="fw-light">
                                    Puede ponerse en contacto con nosotros a través del chat de soporte o llamando al {!! $fsc->reseller_data->telefono !!}.
                                </h4>
                            </div>
                            <div class="col-sm-3"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
