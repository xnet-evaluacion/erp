<footer class="footer">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 col-sm-6">
                <a href="{!! $fsc->reseller_data->url !!}" rel="noopener" class="link-secondary text-decoration-none" target="_blank">{!! $fsc->reseller_data->name !!}</a>
                {!! $fsc->get_app_version() !!}
            </div>
            <div class="col-12 col-sm-6 text-end link-secondary d-none d-sm-inline">
                <i class="fa-solid fa-user fa-fw" title="Identificado como {!! $fsc->user->nick !!}"></i>
                <span>{!! $fsc->user->nick !!} |</span>

                <i class="fa-solid fa-database fa-fw" title="{!! $fsc->selects() !!} consultas"></i>
                <span>{!! $fsc->selects() !!} |</span>

                <i class="fa-solid fa-tasks fa-fw" title="{!! $fsc->transactions() !!} transacciones"></i>
                <span>{!! $fsc->transactions() !!} |</span>

                <i class="fa-solid fa-clock fa-fw" title="Página procesada en {!! $fsc->duration() !!}"></i>
                <span>{!! $fsc->duration() !!}</span>
            </div>
        </div>
    </div>
</footer>

@include('block/extensions/hidden_iframes', ['page_params' => ''])
