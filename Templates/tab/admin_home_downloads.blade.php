<div class="row">
    <div class="col-12">
        <div class="table-responsive-datatable">
            <table id="table_admin_home_downloads" class="table align-middle table-datatable table-borderless table-striped table-hover w-100">
                <thead class="table-dark">
                <tr>
                    <th class="text-center">#</th>
                    <th class="text-center">Puntuación</th>
                    <th class="text-start">Plugin</th>
                    <th class="text-end">Versión</th>
                    <th class="text-start">Descripción</th>
                    <th class="text-center" style="width: 125px; max-width: 125px;">Acciones</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($fsc->plugin_manager->downloads() as $key_plugin => $plugin)
                    @php
                        $tr_class = ' class= "table-warning"';
                    @endphp
                    @if ($plugin['instalado'])
                        @php
                            $tr_class = '';
                        @endphp
                    @elseif ($plugin['tipo']=='oculto')
                        @php
                            $tr_class = ' class= "hidden"';
                        @endphp
                    @elseif (!$plugin['estable'])
                        @php
                            $tr_class = ' class= "table-danger"';
                        @endphp
                    @elseif ($plugin['tipo']=='gratis' || $plugin['tipo']=='regalo')
                        @php
                            $tr_class = ' class= "table-success"';
                        @endphp
                    @endif
                    <tr{!! $tr_class !!}>
                        <td class="text-center text-nowrap text-truncate">{!! $plugin['id'] !!}</td>
                        <td class="text-start text-nowrap text-truncate">
                            @if (!$plugin['estable'])
                                <span class="badge bg-danger" title="inestable: no lo instales a menos que sepas lo que haces">inestable</span>
                            @else
                                @for ($i = 0; $i < 5; $i++)
                                    <i class="fa-solid fa-star"></i>
                                    {{--<i class="{!! ('fa-solid fa-star fa-fw' . ($i < $plugin['valoracion'] ? '' : '-empty')) !!}"></i>--}}
                                @endfor
                            @endif
                        </td>
                        <td class="text-start text-nowrap text-truncate">{!! nl2br($plugin['nombre']) !!}</td>
                        <td class="text-end text-nowrap text-truncate">{!! number_format($plugin['version'],4,'.','') !!}</td>
                        <td class="text-start text-wrap text-truncate" style="max-width: 300px">{!! nl2br($plugin['descripcion_corta']) !!}</td>
                        <td class="text-center text-nowrap">
                            @php
                                $params = [
                                    'buttons' => [],
                                    'type' => 'btn-group'
                                ];

                                $button = [
                                    'type' => 'a',
                                    'onclick' => "mostrar_info('" . $plugin['id'] . "','" . $plugin['descripcion_corta'] . "','" . $plugin['descripcion'] . "');",
                                    'link' => '#',
                                    'class' => 'secondary',
                                    'icon' => '<i class="fa-solid fa-circle-info fa-fw"></i>',
                                    'text' => '<span>Más información</span>',
                                    'title' => 'Más información',
                                ];
                                $params['buttons'][] = $button;

                                if ($plugin['instalado']) {
                                    $button = [
                                        'type' => 'a',
                                        'onclick' => "",
                                        'link' => $fsc->url() . '&caca=' . $fsc->random_string(4) . '#plugins-tab',
                                        'class' => 'secondary',
                                        'icon' => '<i class="fa-solid fa-check-square fa-fw"></i>',
                                        'text' => '<span>Instalado</span>',
                                        'title' => 'Instalado',
                                    ];
                                    $params['buttons'][] = $button;
                                } elseif ($plugin['tipo']=='pago' || $plugin['tipo']=='externo') {
                                    $button = [
                                        'type' => 'a',
                                        'onclick' => "",
                                        'target' => "_blank",
                                        'link' => constant('FS_COMMUNITY_URL') . '/plugin/' . $plugin['nombre'],
                                        'class' => 'info',
                                        'icon' => '<i class="fa-solid fa-shopping-cart fa-fw"></i>',
                                        'text' => '<span>Comprar</span>',
                                        'title' => 'Comprar',
                                    ];
                                    $params['buttons'][] = $button;
                                } elseif ($plugin['tipo']=='regalo') {
                                    $button = [
                                        'type' => 'a',
                                        'onclick' => "",
                                        'target' => "_blank",
                                        'link' => constant('FS_COMMUNITY_URL') . '/plugin/' . $plugin['nombre'],
                                        'class' => 'info',
                                        'icon' => '<i class="fa-solid fa-gift fa-fw"></i>',
                                        'text' => '<span>Obtener</span>',
                                        'title' => 'Obtener',
                                    ];
                                    $params['buttons'][] = $button;
                                } elseif ($plugin['estable']) {
                                    $button = [
                                        'type' => 'a',
                                        'onclick' => "",
                                        'link' => $fsc->url() . "&caca=" . $fsc->random_string(4) . "&download=" .  $plugin['id'] . "#plugins-tab",
                                        'class' => 'info',
                                        'icon' => '<i class="fa-solid fa-download fa-fw"></i>',
                                        'text' => '<span>Descargar</span>',
                                        'title' => 'Descargar',
                                    ];
                                    $params['buttons'][] = $button;
                                } else {
                                    $button = [
                                        'type' => 'a',
                                        'onclick' => "descargar_plugin_inestable('" . $plugin['id'] . "')",
                                        'link' => "#",
                                        'class' => 'primary',
                                        'icon' => '<i class="fa-solid fa-download fa-fw"></i>',
                                        'text' => '<span>Descargar</span>',
                                        'title' => 'Descargar',
                                    ];
                                    $params['buttons'][] = $button;
                                }
                            @endphp
                            @include('block/table_td_actions', $params)
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
