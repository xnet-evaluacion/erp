<form id="f_enable_pages" action="{!! $fsc->url() !!}&caca={!! $fsc->random_string() !!}" method="post" class="form">
    <div class="row">
        <div class="col-12">
            @php
                $enabled_pages = 0;
            @endphp
            @foreach ($fsc->paginas as $key1 => $value1)
                @if ($value1->enabled)
                    @php
                        $enabled_pages++;
                    @endphp
                @endif
            @endforeach
            @if ($enabled_pages != count($fsc->paginas))
                <div class="alert alert-warning" role="alert">
                    <p class="form-text">
                        {!! $enabled_pages !!} páginas activadas de {!! count($fsc->paginas) !!} disponibles.
                    </p>
                    <p class="form-text">
                        Puedes seleccionar todas las páginas desde la cabecera de la tabla, y pulsar en guardar para activarlas todas rápida y fácilmente.
                    </p>
                </div>
            @endif
        </div>
    </div>
    <div class="row mt-3">
        <div class="col-12">
            <div class="d-flex justify-content-end">
                <button class="btn btn-primary flex-grow-1 flex-sm-grow-0" type="submit" onclick="this.disabled = true;this.form.submit();">
                    <i class="fa-solid fa-save fa-fw"></i>
                    <span>Guardar</span>
                </button>
            </div>
        </div>
    </div>
    <div class="row mt-3">
        <div class="col-12">
            <input type="hidden" name="modpages" value="TRUE"/>
            <div class="table-responsive-datatable">
                <table id="table_admin_home_pages" class="table align-middle table-datatable table-borderless table-striped table-hover w-100">
                    <thead class="table-dark">
                    <tr>
                        <th class="text-center" style="width: 40px;">
                            <div class="form-check form-switch" title="Marcar/desmarcar todo">
                                <input class="form-check-input" type="checkbox" role="switch" id="marcar_todo_enabled" value="">
                                <span class="visually-hidden">Marcar/Desmarcar</span>
                                <label class="form-check-label" for="marcar_todo_enabled"> </label>
                            </div>
                        </th>
                        <th class="text-start">Página</th>
                        <th class="text-start">Alias</th>
                        <th class="text-start">Menú</th>
                        <th class="text-start">Descripción</th>
                        <th class="text-center">Existe</th>
                        <th class="text-center">Acción</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($fsc->paginas as $key1 => $value1)
                        <tr @if (!$value1->exists) class="table-danger" @endif >
                            <td class="text-center td-switch">
                                <div class="h-100 d-flex justify-content-center align-items-center">
                                    <div class="form-check form-switch">
                                        <input class="form-check-input" type="checkbox" role="switch" name="enabled[]" value="{!! $value1->name !!}" @if ($value1->enabled) checked="" @endif >
                                        <label class="form-check-label">&nbsp;</label>
                                    </div>
                                </div>
                            </td>
                            <td class="text-start text-nowrap text-truncate">
                                <a href="{!! $value1->url() !!}" target="_blank">{!! $value1->name !!}</a>
                            </td>
                            <td class="text-start text-nowrap text-truncate">
                                {!! $value1->alias !!}
                            </td>
                            <td class="text-start text-nowrap text-truncate">
                                @if ($value1->important)
                                    <i class="fa-solid fa-star fa-fw"></i>» {!! $value1->title !!}
                                    @endif
                                @if ($value1->show_on_menu)
                                    <span class="text-capitalize">{!! $value1->get_folder() !!}</span>
                                    » {!! $value1->alias !!}
                                @else
                                    -
                                @endif
                            </td>
                            <td class="text-start text-nowrap text-truncate">
                                {!! $value1->description !!}
                            </td>
                            <td class="text-center text-nowrap text-truncate">
                                @if ($value1->exists)
                                    <i class="fa-solid fa-check-square fa-fw"></i>
                                @else
                                    <i class="fa-solid fa-exclamation fa-fw" title="No se encuentra el controlador o pertenece a un plugin inactivo"></i>
                                @endif
                            </td>
                            <td class="text-start text-nowrap text-truncate">
                                <div class="d-flex btn-group d-grid text-center" role="group">
                                    <a class="btn btn-success" onClick="editar_pagina('{!! $fsc->url() !!}', '{!! $value1->name !!}')">
                                        <i class="fa-solid fa-pencil-alt fa-fw"></i>
                                        <span>Editar</span>
                                    </a>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="row mt-3">
        <div class="col-12">
            <div class="d-flex justify-content-end">
                <button class="btn btn-primary flex-grow-1 flex-sm-grow-0" type="submit" onclick="this.disabled = true;this.form.submit();">
                    <i class="fa-solid fa-save fa-fw"></i>
                    <span>Guardar</span>
                </button>
            </div>
        </div>
    </div>
</form>
