@extends('layouts/main')

@section('main-content')
    @include('master/template_header')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card border">
                    @include('block/card_page_header')
                    <div class="card-body px-0">
                        <div class="row">
                            <div class="col-12">
                                <nav>
                                    <ul id="tab_usuarios" class="nav nav-tabs nav-tabs-custom nav-justified" role="tablist">
                                        <li class="nav-item">
                                            <a id="nav-usuarios-tab" href="#" class="nav-link d-flex flex-column align-items-center active" data-bs-target="#nav-usuarios" aria-controls="nav-usuarios" role="tab" data-bs-toggle="tab">
                                                {!! get_icon_with_badge('fa-solid fa-users fa-fw', count($fsc->user->all())) !!}
                                                <span>Usuarios</span>
                                            </a>
                                        </li>
                                        @if (!FS_DEMO)
                                            <li class="nav-item">
                                                <a id="nav-permisos-tab" href="#" class="nav-link d-flex flex-column align-items-center" data-bs-target="#nav-permisos" aria-controls="nav-permisos" role="tab" data-bs-toggle="tab">
                                                    <i class="fa-solid fa-check-square fa-fw"></i>
                                                    <span>Permisos</span>
                                                </a>
                                            </li>
                                            <li class="nav-item">
                                                <a id="nav-roles-tab" href="#" class="nav-link d-flex flex-column align-items-center" data-bs-target="#nav-roles" aria-controls="nav-roles" role="tab" data-bs-toggle="tab">
                                                    <i class="fa-solid fa-address-card fa-fw"></i>
                                                    <span>Roles</span>
                                                </a>
                                            </li>
                                        @endif
                                        <li class="nav-item">
                                            <a id="nav-historial-tab" href="#" class="nav-link d-flex flex-column align-items-center" data-bs-target="#nav-historial" aria-controls="nav-historial" role="tab" data-bs-toggle="tab">
                                                <i class="fa-solid fa-history fa-fw"></i>
                                                <span>Historial</span>
                                            </a>
                                        </li>
                                    </ul>
                                </nav>
                                <div class="tab-content nav-content">
                                    <div class="tab-pane fade show active" id="nav-usuarios" aria-labelledby="nav-usuarios-tab" role="tabpanel">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="d-flex justify-content-end">
                                                    <a href="#" id="b_nuevo_usuario" class="btn btn-success flex-grow-1 flex-sm-grow-0">
                                                        <i class="fa-solid fa-plus fa-fw"></i>
                                                        <span>Nuevo</span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row mt-3">
                                            <div class="col-12">
                                                <div class="table-responsive-datatable">
                                                    <table id="table_admin_users_usuarios" class="table align-middle table-datatable table-borderless table-striped table-hover w-100">
                                                        <thead class="table-dark">
                                                        <tr>
                                                            <th class="text-start">Nick</th>
                                                            <th class="text-start">Email</th>
                                                            <th class="text-start">Empleado</th>
                                                            <th class="text-center">Activado</th>
                                                            <th class="text-center">Administrador</th>
                                                            <th class="text-start">IP</th>
                                                            <th class="text-start">Página de inicio</th>
                                                            <th class="text-end">Último login</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @foreach ($fsc->user->all() as $key1 => $value1)
                                                            @php
                                                                $class = '';
                                                                if ($value1->show_last_login()=='-') {
                                                                    $class = 'table-warning';
                                                                }
                                                            @endphp
                                                            <tr class="{!! $class !!}">
                                                                <td class="text-start text-nowrap text-truncate">
                                                                    <a href="{!! $value1->url() !!}" class="link-primary">{!! $value1->nick !!}</a>
                                                                </td>
                                                                <td class="text-start text-nowrap text-truncate">
                                                                    @if (FS_DEMO)
                                                                        XXX@XXX.com
                                                                    @else
                                                                        {!! $value1->email !!}
                                                                    @endif
                                                                </td>
                                                                <td class="text-start text-nowrap text-truncate">{!! $value1->get_agente_fullname() !!}</td>
                                                                <td class="text-center text-nowrap text-truncate">
                                                                    @if ($value1->enabled)
                                                                        <i class="fa-solid fa-check-square fa-fw"></i>
                                                                    @else
                                                                        <i class="fa-solid fa-lock fa-fw"></i>
                                                                    @endif
                                                                </td>
                                                                <td class="text-center text-nowrap text-truncate">
                                                                    @if ($value1->admin)
                                                                        <i class="fa-solid fa-check-square fa-fw"></i>
                                                                    @endif
                                                                </td>
                                                                <td class="text-start text-nowrap text-truncate">
                                                                    @if (FS_DEMO)
                                                                        XX.XX.XX.XX
                                                                    @else
                                                                        {!! $value1->last_ip !!}
                                                                    @endif
                                                                </td>
                                                                <td class="text-start text-nowrap text-truncate">{!! $value1->fs_page !!}</td>
                                                                <td class="text-end text-nowrap text-truncate">{!! $value1->show_last_login() !!}</td>
                                                            </tr>
                                                        @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @if (!FS_DEMO)
                                        <div class="tab-pane fade" id="nav-permisos" aria-labelledby="nav-permisos-tab" role="tabpanel">
                                            <div class="row">
                                                <div class="col-12">
                                                    <p class="form-text">
                                                        <i class="fa-solid fa-circle-info fa-fw"></i>
                                                        Aquí puedes ver rápidamente qué usuarios tienen permiso para acceder
                                                        a cada página. En
                                                        <span class="badge bg-warning">destacado</span>
                                                        los que tienen permisos para ver, modificar y eliminar, el resto
                                                        solamente tienen permisos para ver y modificar.
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="row mt-3">
                                                <div class="col-12">
                                                    <div class="table-responsive-datatable">
                                                        <table id="table_admin_users_permisos" class="table align-middle table-datatable table-borderless table-striped table-hover w-100">
                                                            <thead class="table-dark">
                                                            <tr>
                                                                <th class="text-start">Página</th>
                                                                <th class="text-start">Usuarios con permiso</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            @foreach ($fsc->all_pages() as $key1 => $value1)
                                                                <tr>
                                                                    <td class="text-start text-nowrap text-truncate">{!! $value1->name !!}</td>
                                                                    <td class="text-start text-nowrap text-truncate">
                                                                        @foreach ($value1->users as $key2 => $value2)
                                                                            @if ($value2['delete'])
                                                                                <a href="index.php?page=admin_user&snick={!! $key2 !!}" class="badge bg-warning" title="{!! $key2 !!} puede ver, modificar y eliminar en {!! $value1->name !!}">
                                                                                    {!! $key2 !!}
                                                                                </a>
                                                                            @elseif ($value2['modify'])
                                                                                <a href="index.php?page=admin_user&snick={!! $key2 !!}" class="badge bg-secondary" title="{!! $key2 !!} puede ver y modificar en {!! $value1->name !!}, pero no eliminar">
                                                                                    {!! $key2 !!}
                                                                                </a>
                                                                            @endif
                                                                        @endforeach
                                                                    </td>
                                                                </tr>
                                                            @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="nav-roles" aria-labelledby="nav-roles-tab" role="tabpanel">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <p class="form-text">
                                                        <i class="fa-solid fa-circle-info fa-fw"></i>
                                                        Los roles permiten definir paquetes de permisos para aplicar
                                                        rápidamente
                                                        a usuarios, en lugar de ir uno por uno.
                                                    </p>
                                                </div>
                                                <div class="col-sm-6 text-end">
                                                    <div class="d-flex justify-content-end">
                                                        <a href="#" id="b_nuevo_rol" class="flex-grow-1 flex-sm-grow-0 btn btn-success">
                                                            <i class="fa-solid fa-plus fa-fw"></i>
                                                            <span>Nuevo</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row mt-3">
                                                <div class="col-12">
                                                    <div class="table-responsive-datatable">
                                                        <table id="table_admin_users_roles" class="table align-middle table-datatable table-borderless table-striped table-hover w-100">
                                                            <thead class="table-dark">
                                                            <tr>
                                                                <th style="width: 100px;">Código</th>
                                                                <th style="min-width: 300px;">Descripción</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            @foreach ($fsc->rol->all() as $key1 => $value1)
                                                                <tr>
                                                                    <td class="text-start text-nowrap text-truncate">
                                                                        <a href="{!! $value1->url() !!}" class="link-primary">
                                                                            {!! $value1->codrol !!}
                                                                        </a>
                                                                    </td>
                                                                    <td class="text-start text-nowrap text-truncate">
                                                                        {!! nl2br($value1->descripcion) !!}
                                                                    </td>
                                                                </tr>
                                                            @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    <div class="tab-pane fade" id="nav-historial" aria-labelledby="nav-historial-tab" role="tabpanel">
                                        <div class="row">
                                            <div class="col-sm-8">
                                                <p class="form-text">
                                                    <i class="fa-solid fa-circle-info fa-fw"></i>
                                                    Puedes ver más detalles desde Admin > Información del sistema.
                                                </p>
                                            </div>
                                            <div class="col-sm-4 text-end">
                                                <a href="index.php?page=admin_info" class="d-flex justify-content-center align-items-center btn btn-outline-secondary">
                                                    <i class="fa-solid fa-book fa-fw"></i>
                                                    <span>Historial completo</span>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="row mt-3">
                                            <div class="col-12">
                                                <div class="table-responsive-datatable">
                                                    <table id="table_admin_users_historial" class="table align-middle table-datatable table-borderless table-striped table-hover w-100">
                                                        <thead class="table-dark">
                                                        <tr>
                                                            <th class="text-start">Usuario</th>
                                                            <th class="text-start"></th>
                                                            <th class="text-start">Detalle</th>
                                                            <th class="text-start">IP</th>
                                                            <th class="text-end">Fecha</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @foreach ($fsc->historial as $key1 => $value1)
                                                            <tr @if ($value1->alerta) class="table-danger" @endif >
                                                                <td class="text-start text-nowrap text-truncate">
                                                                    <a href="index.php?page=admin_user&snick={!! $value1->usuario !!}" class="link-primary">{!! $value1->usuario !!}</a>
                                                                </td>
                                                                <td class="text-end text-nowrap text-truncate">
                                                                    @if ($value1->alerta)
                                                                        <i class="fa-solid fa-exclamation-triangle fa-fw" title="Podría ser importante"></i>
                                                                    @endif
                                                                </td>
                                                                <td class="text-start text-nowrap text-truncate">{!! $value1->detalle !!}</td>
                                                                <td class="text-start text-nowrap text-truncate">{!! $value1->ip !!}</td>
                                                                <td class="text-end text-nowrap text-truncate">{!! show_date($value1->fecha) !!}</td>
                                                            </tr>
                                                        @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal" id="modal_nuevo_usuario">
        <div class="modal-dialog modal-xl modal-fullscreen-xl-down">
            <div class="modal-content">
                <form name="f_nuevo_usuario" class="form" role="form" action="{!! $fsc->url() !!}" method="post">
                    <div class="modal-header">
                        <h4 class="modal-title">
                            <i class="fa-solid fa-user fa-fw"></i>
                            Nuevo usuario
                        </h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Cerrar"></button>
                    </div>
                    <div class="modal-body">
                        <div class="mb-2">
                            <label>
                                Nick
                            </label>
                            <input class="form-control" type="text" name="nnick" maxlength="12" autocomplete="off" required/>
                        </div>
                        <div class="mb-2">
                            <label>
                                Roles
                            </label>
                            <div class="d-flex justify-content-between">
                                <div class="form-check form-switch form-check-inline">
                                    <input class="form-check-input" type="checkbox" role="switch" name="nadmin" value="TRUE"/>
                                    <label class="form-check-label">
                                        Administrador
                                    </label>
                                </div>
                                @foreach ($fsc->rol->all() as $key1 => $value1)
                                    <div class="form-check form-switch form-check-inline">
                                        <input class="form-check-input" type="checkbox" role="switch" name="roles[]" value="{!! $value1->codrol !!}"/>
                                        <label class="form-check-label">
                                            {!! $value1->codrol !!}
                                        </label>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="mb-2">
                            <label>
                                Contraseña
                            </label>
                            <input class="form-control" type="password" name="npassword" maxlength="32" autocomplete="new-password"/>
                        </div>
                        <div class="mb-2">
                            <label>
                                Email
                            </label>
                            <div class="input-group">
                                <div class="input-group-text"><i class="fa-solid fa-envelope fa-fw"></i></div>
                                <input class="form-control" type="text" name="nemail" autocomplete="new-email"/>
                            </div>
                        </div>
                        <div class="mb-2">
                            <label>
                                <a href="{!! $fsc->agente->url() !!}" target="_blank">Empleado</a>
                            </label>
                            <select name="ncodagente" class="form-select select2">
                                <option value="">Ninguno</option>
                                @foreach ($fsc->agente->all() as $key1 => $value1)
                                    <option value="{!! $value1->codagente !!}">{!! $value1->get_fullname() !!}</option>
                                @endforeach
                            </select>
                            <p class="form-text">
                                Puedes tener empleados que no tengan acceso a {!! $fsc->reseller_data->name !!},
                                o bien usuarios que no sean empleados, por eso está separado.
                            </p>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-primary flex-grow-1 flex-sm-grow-0" type="submit" title="Guardar">
                            <i class="fa-solid fa-save fa-fw"></i>
                            <span>Guardar</span>
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal" id="modal_nuevo_rol">
        <div class="modal-dialog modal-xl modal-fullscreen-xl-down">
            <div class="modal-content">
                <form name="f_nuevo_rol" class="form" role="form" action="{!! $fsc->url() !!}" method="post">
                    <div class="modal-header">
                        <h4 class="modal-title">
                            <i class="fa-solid fa-address-card fa-fw"></i>
                            Nuevo rol
                        </h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Cerrar"></button>
                    </div>
                    <div class="modal-body">
                        <div class="mb-2">
                            <label>
                                Código
                            </label>
                            <input type="text" name="nrol" class="form-control" maxlength="20" autocomplete="off" required/>
                        </div>
                        <div class="mb-2">
                            <label>
                                Descripcion
                            </label>
                            <input type="text" name="descripcion" class="form-control" autocomplete="off" required/>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-primary flex-grow-1 flex-sm-grow-0" type="submit" title="Guardar">
                            <i class="fa-solid fa-save fa-fw"></i>
                            <span>Guardar</span>
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
