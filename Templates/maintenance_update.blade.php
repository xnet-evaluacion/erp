<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="es" xml:lang="es">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>MiFactura.eu</title>
    <meta name="description" content="MiFactura.eu es un software de facturación y contabilidad para pymes. Es software libre bajo licencia GNU/LGPL."/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <link rel="shortcut icon" href="{!! FS_PATH !!}Templates/img/favicon.ico?idcache={!! $fsc->id_cache !!}"/>
    <link rel="stylesheet" href="{!! FS_PATH !!}node_modules/@fortawesome/fontawesome-free/css/all.min.css?idcache={!! $fsc->id_cache !!}"/>
    <link rel="stylesheet" href="{!! FS_PATH !!}Templates/css/custom.css?idcache={!! $fsc->id_cache !!}"/>

    <script type="text/javascript" src="{!! FS_PATH !!}node_modules/jquery/dist/jquery.min.js?idcache={!! $fsc->id_cache !!}"></script>
    <script type="text/javascript" src="{!! FS_PATH !!}node_modules/bootstrap/dist/js/bootstrap.bundle.min.js?idcache={!! $fsc->id_cache !!}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            setTimeout(function () {
                location.reload();
            }, 60000);
        });
    </script>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-12">
            <div class="card border">
                <div class="card-header mt-2 text-truncate">
                    <i class="fa-solid fa-wrench fa-fw"></i>
                    En mantenimiento
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12 text-justify">
                            <p class="form-text">
                                El entorno se encuentra en mantenimiento debido a la instalación de actualizaciones, manténgase a la espera.
                            </p>
                        </div>
                        <div class="col-12 text-justify">
                            <p class="form-text">
                                La página se cargará automáticamente cuando haya terminado el proceso, el proceso puede tardar unos minutos.
                            </p>
                        </div>
                        <div class="col-12 text-justify">
                            <p class="form-text">
                                <b>IMPORTANTE:</b> Si estaba realizando alguna operación y la actualización interrumpió su trabajo, una vez haya terminado la actualización puede pulsar el botón de atrás del navegador para poder realizarla de nuevo.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
