@extends('layouts/main')

@section('main-content')
    @include('master/template_header')
    <div class="container-fluid">
        <div class="card border">
            @include('block/card_page_header')
            <div class="card-body">
                <form action="{!! $fsc->url() !!}" method="post" class="form">
                    <input type="hidden" name="guardar" value="TRUE"/>
                    <div class="row">
                        @foreach ($fsc->folders() as $key1 => $value1)
                            @if ($loop->index%4==0)
                    </div>
                    <div class="row">
                        @endif
                        <div class="col-12 col-sm-6 col-lg-4 col-xl-3 mb-3 ">
                            <div class="card border">
                                <div class="card-body">
                                    <h3 class="fw-light">
                                        <i class="fa-solid fa-folder-open fa-fw"></i>
                                        <span class="text-capitalize">{!! $value1 !!}</span>
                                    </h3>
                                    <ul class="list-group sortable">
                                        @foreach ($fsc->pages($value1) as $key2 => $value2)
                                            <li class="list-group-item clickable">
                                                <i class="fa-solid fa-arrows-v fa-fw"></i>{!! $value2->title !!}
                                                <input type="hidden" name="{!! $value1 !!}_{!! $loop->index !!}" value="{!! $value2->name !!}"/>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    <div class="row">
                        <div class="col-12 d-flex justify-content-end">
                            <button class="btn btn-primary flex-grow-1 flex-sm-grow-0" type="submit" title="Guardar">
                                <i class="fa-solid fa-save fa-fw"></i>
                                <span>Guardar</span>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
