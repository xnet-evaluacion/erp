@if ($fsc->hide_footer == true)
    @include('parts/footer_sin_creditos')
@else
    @switch(THEME)
        @case('skote')
            @include('parts/footer-menu-left-skote')
            @break

        @default
            @if ($fsc->use_sidebar_menu == true)
                @include('parts/footer-menu-left')
            @else
                @include('parts/footer')
            @endif
    @endswitch
@endif
