@extends('layouts/main')

@section('main-content')
    @include('master/template_header')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card border">
                    @include('block/card_page_header')
                    <div class="card-body px-0">
                        <form name="f_empresa" action="{!! $fsc->page->url() !!}" method="post" class="form" role="form" enctype="multipart/form-data">
                            <div class="row">
                                <div class="col-12">
                                    <ul class="nav nav-tabs nav-tabs-custom nav-justified" role="tablist">
                                        <li class="nav-item" role="presentation">
                                            <a id="v-pills-generales-tab" href="#" class="nav-link d-flex flex-column align-items-center active" role="tab" data-bs-toggle="pill" data-bs-target="#v-pills-generales" type="button" aria-controls="v-pills-generales" aria-selected="true">
                                                <i class="fa-solid fa-tachometer-alt fa-fw"></i>
                                                <span>Datos</span>
                                            </a>
                                        </li>
                                        <li class="nav-item" role="presentation">
                                            <a id="v-pills-personalizacion-tab" href="#" class="nav-link d-flex flex-column align-items-center" role="tab" data-bs-toggle="pill" data-bs-target="#v-pills-personalizacion" type="button" aria-controls="v-pills-personalizacion" aria-selected="false">
                                                <i class="fa-solid fa-images fa-fw"></i>
                                                <span>Personalización</span>
                                            </a>
                                        </li>
                                        <li class="nav-item" role="presentation">
                                            <a id="v-pills-email-tab" href="#" class="nav-link d-flex flex-column align-items-center" role="tab" data-bs-toggle="pill" data-bs-target="#v-pills-email" type="button" aria-controls="v-pills-email" aria-selected="false">
                                                <i class="fa-solid fa-envelope fa-fw"></i>
                                                <span>Email</span>
                                            </a>
                                        </li>
                                    </ul>
                                    <div class="tab-content nav-content" id="v-pills-tabContent">
                                        @include('block/admin_empresa_generales')
                                        @include('block/admin_empresa_personalizacion')
                                        @include('block/admin_empresa_email')
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
