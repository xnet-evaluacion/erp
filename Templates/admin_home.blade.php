@extends('layouts/main')

@section('main-content')
    @include('master/template_header')

    @if (!$fsc->step)
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card border">
                        <div class="card-header mt-2 text-truncate">
                            <h1 class="fw-light">
                                Bienvenido a #{!! $fsc->reseller_data->name !!}
                                <small>{!! $fsc->version() !!}</small>
                            </h1>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <p class="form-text">
                                    Ya tienes instalado el núcleo. Como ves, el núcleo permite gestionar páginas,
                                    plugins y usuarios. Pulsa continuar para seguir con la instalación.
                                </p>
                                <a class="btn btn-primary flex-grow-1 flex-sm-grow-0" onclick="fs_marcar_todo();f_enable_pages.submit();">
                                    <i class="fa-solid fa-chevron-right fa-fw"></i>
                                    <span>Continuar</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @elseif ($fsc->step=='1')
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card border">
                        @php

                            @endphp
                        @include('block/card_page_header')
                        {{--<div class="card-header mt-2 text-truncate">
                            <i class="fa-solid fa-plug fa-fw"></i>
                            Plugins
                        </div>--}}
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <p class="form-text">
                                        El núcleo solamente se encarga de la gestión de usuarios, plugins y páginas.
                                        Para todo lo demás tienes los plugins.
                                        En la pestaña <b>descargas</b> tienes disponibles los principales plugins.
                                        Elige los que necesites. Hay de todo y la lista se actualiza periódicamente.
                                        <br/>
                                        Los plugins instalados los tienes en la pestaña <b>plugins</b>. Puedes añadir
                                        plugins manualmente, si lo deseas, y también puedes activar o desactivar,
                                        incluso eliminarlos.
                                        <br/>
                                        Además, toda la facturación y contabilidad básica ha sido movida al plugin
                                        <b>facturacion_base</b>. Puedes descargarlo automáticamente e instalarlo
                                        pulsando
                                        este botón.
                                    </p>
                                   <div class="d-flex justify-content-end flex-column flex-sm-row gap-1">
                                       @if (file_exists('plugins/facturacion_base'))
                                           <a href="{!! $fsc->url() !!}&caca=CCC&enable=facturacion_base#plugins-tab" class="btn btn-primary flex-grow-1 flex-sm-grow-0">
                                               <i class="fa-solid fa-check-square fa-fw"></i>
                                               <span>Activar facturacion_base</span>
                                           </a>
                                       @else
                                           <a href="{!! $fsc->url() !!}&caca={!! $fsc->random_string(4) !!}&download=87#plugins-tab" class="btn btn-primary flex-grow-1 flex-sm-grow-0">
                                               <i class="fa-solid fa-download fa-fw"></i>
                                               <span>Descargar facturacion_base</span>
                                           </a>
                                       @endif
                                       <a href="{!! $fsc->url() !!}&caca={!! $fsc->random_string(4) !!}&skip=TRUE#descargas" class="d-flex justify-content-center align-items-center btn btn-warning">
                                           <i class="fa-solid fa-times-circle fa-fw"></i>
                                           <span>No, gracias</span>
                                       </a>
                                   </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card border">
                    @include('block/card_page_header')
                    <div class="card-body px-0">
                        <div class="row">
                            <div class="col-12">
                                <div id="tab_panel" role="tabpanel">
                                    <ul class="nav nav-tabs nav-tabs-custom nav-justified" role="tablist">
                                        <li class="nav-item" role="presentation">
                                            <a id="paginas-tab" href="#" class="nav-link d-flex flex-column align-items-center active" role="tab" data-bs-toggle="tab" data-bs-target="#t_paginas" type="button" aria-controls="paginas" aria-selected="true">
                                                {!! get_icon_with_badge('fa-solid fa-check-square fa-fw', count($fsc->paginas)) !!}
                                                <span>Menú</span>
                                            </a>
                                        </li>
                                        <li class="nav-item" role="presentation">
                                            <a id="plugins-tab" href="#" class="nav-link d-flex flex-column align-items-center" role="tab" data-bs-toggle="tab" data-bs-target="#t_plugins" type="button" aria-controls="plugins" aria-selected="true">
                                                {!! get_icon_with_badge('fa-solid fa-plug fa-fw', count($fsc->plugin_manager->installed(false))) !!}
                                                <span>Plugins</span>
                                            </a>
                                        </li>
                                        @if (!$fsc->plugin_manager->disable_mod_plugins)
                                            <li class="nav-item" role="presentation">
                                                <a id="descargas-tab" href="#" class="nav-link d-flex flex-column align-items-center" role="tab" data-bs-toggle="tab" data-bs-target="#t_descargas" type="button" aria-controls="descargas" aria-selected="true">
                                                    {!! get_icon_with_badge('fa-solid fa-download fa-fw', count($fsc->plugin_manager->downloads())) !!}
                                                    <span>Descargas</span>
                                                </a>
                                            </li>
                                        @endif
                                        <li class="nav-item" role="presentation">
                                            <a id="avanzado-tab" href="#" class="nav-link d-flex flex-column align-items-center" role="tab" data-bs-toggle="tab" data-bs-target="#t_avanzado" type="button" aria-controls="avanzado" aria-selected="true">
                                                <i class="fa-solid fa-wrench fa-fw"></i>
                                                <span>Avanzado</span>
                                            </a>
                                        </li>
                                    </ul>
                                    <div class="tab-content nav-content">
                                        <div class="tab-pane fade show active" id="t_paginas" aria-labelledby="paginas-tab" role="tabpanel">
                                            @include('tab/admin_home_pages')
                                        </div>
                                        <div class="tab-pane fade" id="t_plugins" aria-labelledby="plugins-tab" role="tabpanel">
                                            @include('tab/admin_home_plugins')
                                        </div>
                                        <div class="tab-pane fade" id="t_descargas" aria-labelledby="descargas-tab" role="tabpanel">
                                            @if (!$fsc->plugin_manager->disable_mod_plugins)
                                                @include('tab/admin_home_downloads')
                                            @endif
                                        </div>
                                        <div class="tab-pane fade" id="t_avanzado" aria-labelledby="avanzado-tab" role="tabpanel">
                                            @include('tab/admin_home_advanced')
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('block/edit_page')

    <div class="modal fade" id="modal_add_plugin" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-xl modal-fullscreen-xl-down">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">
                        <i class="fa-solid fa-plug fa-fw"></i>
                        Añadir un plugin
                    </h4>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Cerrar"></button>
                </div>
                <div class="modal-body">
                    <p class="form-text">
                        Si tienes un plugin en un archivo .zip, puedes subirlo e instalarlo desde aquí.
                    </p>
                    <form class="form" action="{!! $fsc->url() !!}#plugins-tab" enctype="multipart/form-data" method="post">
                        <input type="hidden" name="install" value="TRUE"/>
                        <div class="mb-2">
                            <div class="row">
                                <div class="col-sm-4">
                                    <input type="file" class="form-control" name="fplugin" accept="application/zip"/>
                                </div>
                            </div>
                        </div>
                        <p class="form-text">
                            Este servidor admite un tamaño máximo de {!! $fsc->get_max_file_upload() !!} MB.
                            Si el plugin ocupa más, dará error al subirlo, incluso puede que no salga
                            nada.
                        </p>
                        <button type="submit" class="btn btn-primary flex-grow-1 flex-sm-grow-0" onclick="this.disabled=true;this.form.submit();">
                            <i class="fa-solid fa-file-import fa-fw"></i>
                            <span>Añadir</span>
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal" id="modallog">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">
                        <i class="fa-solid fa-cloud-upload fa-fw"></i>
                        Proceso de instalación/actualización
                    </h4>
                </div>
                <div class="modal-body">
                    <div class="mt-3 col-sm-12">
                        <textarea class="form-control" id="messages" rows="10" style="width: 100%; resize: none" readonly></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="mt-3 col-sm-12">
                        <button type="button" class="btn btn-outline-primary" data-bs-dismiss="modal" id="close_button">
                            <i class="fa-solid fa-close fa-fw"></i>
                            <span class="d-none d-sm-inline">Cerrar ventana</span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
