@extends('layouts/main')

@section('main-content')
    @if ($fsc->suser)
        <form class="form" role="form" id="f_user_pages" action="{!! $fsc->url() !!}" method="post">
            @include('master/template_header')
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card border">
                            @include('block/card_page_header')
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-6 col-md-6 col-lg-4 col-xl-2">
                                        <div class="mb-2">
                                            <label>
                                                Nick
                                            </label>
                                            <input class="form-control" type="text" name="snick" value="{!! $fsc->suser->nick !!}" disabled="disabled"/>
                                            <label>

                                            </label>
                                            <div class="form-check form-switch">
                                                <input class="form-check-input" type="checkbox" role="switch" name="sadmin" value="TRUE" @if ($fsc->suser->admin) checked="" @endif />
                                                <label class="form-check-label">
                                                    Administrador
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-6 col-lg-4">
                                        <div class="mb-2">
                                            <label>
                                                Contraseña
                                            </label>
                                            <div class="d-flex flex-column flex-sm-column flex-md-row">
                                                <input class="form-control mb-sm-2 mb-2 mb-md-0 me-sm-0 me-0 me-md-2" type="password" name="spassword" maxlength="32" autocomplete="new-password" placeholder="Nueva contraseña"/>
                                                <input class="form-control" type="password" name="spassword2" maxlength="32" placeholder="Repite la contraseña"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-6 col-lg-4 col-xl-2">
                                        <div class="mb-2">
                                            <label>
                                                Email
                                            </label>
                                            @if (FS_DEMO)
                                                <input type="email" name="email" class="form-control" autocomplete="off"/>
                                            @else
                                                <input type="email" name="email" value="{!! $fsc->suser->email !!}" class="form-control" autocomplete="off"/>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-6 col-lg-4 col-xl-2">
                                        <div class="mb-2">
                                            <label>
                                                <a href="{!! $fsc->agente->url() !!}" class="link-primary">Empleado</a>
                                            </label>
                                            <select name="scodagente" class="form-select select2">
                                                <option data-comment="Placeholder"></option>
                                                @if ($fsc->user->admin)
                                                    <option value="">Ninguno</option>
                                                    @foreach ($fsc->agente->all() as $key1 => $value1)
                                                        @if ($fsc->suser->codagente==$value1->codagente)
                                                            <option value="{!! $value1->codagente !!}" selected="">{!! $value1->get_fullname() !!}</option>
                                                        @else
                                                            <option value="{!! $value1->codagente !!}">{!! $value1->get_fullname() !!}</option>
                                                        @endif
                                                    @endforeach
                                                @else
                                                    @foreach ($fsc->agente->all() as $key1 => $value1)
                                                        @if ($fsc->suser->codagente==$value1->codagente)
                                                            <option value="{!! $value1->codagente !!}" selected="">{!! $value1->get_fullname() !!}</option>
                                                        @endif
                                                    @endforeach
                                                @endif
                                            </select>
                                            @if ($fsc->user->admin)
                                                <p class="form-text">
                                                    <a href="#" id="b_nuevo_agente">Crear un nuevo empleado y asignarlo a este usuario</a>
                                                </p>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-6 col-lg-4 col-xl-2">
                                        <div class="mb-2">
                                            <label>
                                                Página de inicio
                                            </label>
                                            <select name="udpage" class="form-select select2">
                                                <option data-comment="Placeholder"></option>
                                                @foreach ($fsc->suser->get_menu() as $key1 => $value1)
                                                    @if ($value1->show_on_menu)
                                                        @if ($value1->name==$fsc->suser->fs_page)
                                                            <option value="{!! $value1->name !!}" selected="">{!! $value1->folder !!} - {!! $value1->alias !!}</option>
                                                        @else
                                                            <option value="{!! $value1->name !!}">{!! $value1->folder !!} - {!! $value1->alias !!}</option>
                                                        @endif
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-6 col-lg-4 col-xl-2">
                                        {{-- TODO: Anulado porqué en skote se prefirió forzar el vertical, aunque si tiene uno en horizontal --}}
                                        {{--
                                        <div class="mb-2">
                                            <label>
                                                Estilo visual
                                            </label>
                                            <select name="css" class="form-select select2">
                                                <option data-comment="Placeholder"></option>
                                                @foreach ($fsc->extensions as $key1 => $value1)
                                                    @if ($value1->type=='css')
                                                        <option value="{!! $value1->text !!}" @if ($value1->text==$fsc->suser->css) selected="" @endif >{!! $value1->name !!}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                            <select name="use_sidebar_menu" class="form-select select2">
                                                <option data-comment="Placeholder"></option>
                                                <option value="0" @if ($fsc->suser->use_sidebar_menu==false) selected="" @endif >
                                                    Menú horizontal
                                                </option>
                                                <option value="1" @if ($fsc->suser->use_sidebar_menu==true) selected="" @endif >
                                                    Menú vertical
                                                </option>
                                            </select>
                                        </div>
                                        --}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <input type="hidden" name="modupages" value="TRUE"/>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card border">
                            <div class="card-body">
                                <div class="row">
                                    <div role="tabpanel">
                                        <ul class="nav nav-tabs nav-tabs-custom nav-justified" role="tablist">
                                            <li class="nav-item" role="presentation">
                                                <a id="tab_admin_user_autorizar" href="#autorizar" aria-controls="autorizar" role="tab" data-bs-toggle="tab" class="nav-link d-flex flex-column align-items-center active">
                                                    <i class="fa-solid fa-check-square fa-fw"></i>
                                                    <span>Autorizar</span>
                                                </a>
                                            </li>
                                            <li class="nav-item" role="presentation">
                                                <a id="tab_admin_user_historial" href="#historial" aria-controls="historial" role="tab" data-bs-toggle="tab" class="nav-link d-flex flex-column align-items-center">
                                                    <i class="fa-solid fa-history fa-fw"></i>
                                                    <span>Historial</span>
                                                </a>
                                            </li>
                                            @include('block/extensions/tabs', ['page_params' => ''])
                                        </ul>
                                        <div class="tab-content nav-content">
                                            <div class="tab-pane fade show active" id="autorizar" role="tabpanel" aria-labelledby="autorizar-tab">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div class="table-responsive-datatable">
                                                            <table id="table_admin_user_autorizaciones" class="table align-middle table-datatable table-borderless table-striped table-hover w-100">
                                                                <thead class="table-dark">
                                                                <tr>
                                                                    <th class="text-start">Página</th>
                                                                    <th class="text-start">Menú</th>
                                                                    <th class="text-center">Ver / Modificar</th>
                                                                    <th class="text-center">Permiso de eliminación</th>
                                                                </tr>
                                                                </thead>
                                                                @if ($fsc->user->admin && !$fsc->suser->admin)
                                                                    <tr class="table-warning">
                                                                        <td class="text-start text-nowrap text-truncate"></td>
                                                                        <td class="text-start text-nowrap text-truncate"></td>
                                                                        <td class="text-center td-switch">
                                                                            <div class="h-100 d-flex justify-content-center align-items-center" title="Marcar/desmarcar todos">
                                                                                <div class="form-check form-switch">
                                                                                    <input class="form-check-input" type="checkbox" role="switch" id="marcar_todo_ver" name="p_ver_modificar" value=""/>
                                                                                    <label class="form-check-label" for="marcar_todo_ver"> </label>
                                                                                </div>
                                                                            </div>
                                                                        </td>
                                                                        <td class="text-center td-switch">
                                                                            <div class="h-100 d-flex justify-content-center align-items-center" title="Marcar/desmarcar todos">
                                                                                <div class="form-check form-switch">
                                                                                    <input class="form-check-input" type="checkbox" role="switch" id="marcar_todo_eliminar" name="p_eliminar" value=""/>
                                                                                    <label class="form-check-label" for="marcar_todo_eliminar"> </label>
                                                                                </div>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                @endif
                                                                @if (!$fsc->user->admin)
                                                                    @foreach ($fsc->all_pages() as $key1 => $value1)
                                                                        <tr>
                                                                            <td class="text-start text-nowrap text-truncate">
                                                                                {!! $value1->name !!}
                                                                            </td>
                                                                            <td class="text-start text-nowrap text-truncate">
                                                                                @if ($value1->important)
                                                                                    <i class="fa-solid fa-star fa-fw"></i>
                                                                                    » {!! $value1->title !!}
                                                                                @elseif ($value1->show_on_menu)
                                                                                    <span class="text-capitalize">{!! $value1->folder !!}</span>
                                                                                    » {!! $value1->title !!}
                                                                                @else
                                                                                    -
                                                                                @endif
                                                                            </td>
                                                                            <td class="text-center text-nowrap text-truncate">
                                                                                @if ($value1->enabled)
                                                                                    <i class="fa-solid fa-check-square fa-fw"></i>
                                                                                @else
                                                                                    <i class="fa-solid fa-lock fa-fw"></i>
                                                                                @endif
                                                                            </td>
                                                                            <td class="text-center text-nowrap text-truncate">
                                                                                @if ($value1->allow_delete)
                                                                                    <i class="fa-solid fa-check-square fa-fw"></i>
                                                                                @else
                                                                                    <i class="fa-solid fa-lock fa-fw"></i>
                                                                                @endif
                                                                            </td>
                                                                        </tr>
                                                                    @endforeach
                                                                @else
                                                                    @foreach ($fsc->all_pages() as $key1 => $value1)
                                                                        <tr>
                                                                            <td class="text-start text-nowrap text-truncate">
                                                                                {!! $value1->name !!}
                                                                            </td>
                                                                            <td class="text-start text-nowrap text-truncate">
                                                                                @if ($value1->important)
                                                                                    <i class="fa-solid fa-star fa-fw"></i>
                                                                                    » {!! $value1->title !!}
                                                                                @elseif ($value1->show_on_menu)
                                                                                    <span class="text-capitalize">{!! $value1->folder !!}</span>
                                                                                    » {!! $value1->title !!}
                                                                                @else
                                                                                    -
                                                                                @endif
                                                                            </td>
                                                                            <td class="text-center td-switch">
                                                                                <div class="h-100 d-flex justify-content-center align-items-center" title="Marcar/desmarcar todos">
                                                                                    <div class="form-check form-switch">
                                                                                        <input class="form-check-input" type="checkbox" role="switch" id="enabled_{!! $loop->index !!}" name="enabled[]" value="{!! $value1->name !!}" onclick="check_allow_delete('{!! $loop->index !!}')" @if ($value1->enabled)checked="" @endif />
                                                                                    </div>
                                                                                </div>
                                                                            </td>
                                                                            <td class="text-center td-switch" title="el usuario tiene permisos para eliminar en esta página">
                                                                                <div class="h-100 d-flex justify-content-center align-items-center" title="Marcar/desmarcar todos">
                                                                                    <div class="form-check form-switch">
                                                                                        <input class="form-check-input" type="checkbox" role="switch" id="allow_delete_{!! $loop->index !!}" name="allow_delete[]" value="{!! $value1->name !!}" @if ($value1->allow_delete) checked="" @endif />
                                                                                    </div>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    @endforeach
                                                                @endif
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane fade" id="historial" role="tabpanel" aria-labelledby="historial-tab">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div class="table-responsive-datatable">
                                                            <table id="table_admin_user_last_login" class="table align-middle table-datatable table-borderless table-striped table-hover w-100">
                                                                <thead class="table-dark">
                                                                <tr>
                                                                    <th class="text-start">Último login</th>
                                                                    <th class="text-start">IP</th>
                                                                    <th class="text-start">Navegador</th>
                                                                </tr>
                                                                </thead>
                                                                <tr>
                                                                    <td class="text-start text-nowrap text-truncate">{!! $fsc->suser->show_last_login() !!}</td>
                                                                    <td class="text-start text-nowrap text-truncate">{!! $fsc->suser->last_ip !!}</td>
                                                                    <td class="text-start text-nowrap text-truncate">{!! $fsc->suser->last_browser !!}</td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-8">
                                                        <p class="form-text">
                                                            <i class="fa-solid fa-circle-info fa-fw"></i>
                                                            Puedes ver más detalles desde Admin > Información del
                                                            sistema.
                                                        </p>
                                                    </div>
                                                    <div class="col-sm-4 text-end">
                                                        <a href="index.php?page=admin_info" class="d-flex justify-content-center align-items-center btn btn-outline-secondary">
                                                            <i class="fa-solid fa-book fa-fw"></i>
                                                            <span>Historial completo</span>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="row mt-3">
                                                    <div class="col-12">
                                                        <div class="table-responsive-datatable">
                                                            <table id="table_admin_user_detalles" class="table align-middle table-datatable table-borderless table-striped table-hover w-100">
                                                                <thead class="table-dark">
                                                                <tr>
                                                                    <th class="text-start">Tipo</th>
                                                                    <th class="text-start"></th>
                                                                    <th class="text-start">Detalle</th>
                                                                    <th class="text-start">IP</th>
                                                                    <th class="text-end">Fecha</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                @foreach ($fsc->user_log as $key1 => $value1)
                                                                    <tr @if ($value1->alerta) class="table-danger" @endif >
                                                                        <td class="text-start text-nowrap text-truncate">{!! $value1->tipo !!}</td>
                                                                        <td class="text-end text-nowrap text-truncate">
                                                                            @if ($value1->alerta)
                                                                                <i class="fa-solid fa-exclamation-triangle fa-fw" title="Podría ser importante"></i>
                                                                            @endif
                                                                        </td>
                                                                        <td class="text-start text-nowrap text-truncate">{!! $value1->detalle !!}</td>
                                                                        <td class="text-start text-nowrap text-truncate">{!! $value1->ip !!}</td>
                                                                        <td class="text-end text-nowrap text-truncate">{!! show_date($value1->fecha) !!}</td>
                                                                    </tr>
                                                                @endforeach
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            @include('block/extensions/tabpanes', ['page_params' => '&snick=' . $fsc->suser->nick])
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>

        <form class="form-horizontal" role="form" name="f_nuevo_agente" action="{!! $fsc->url() !!}" method="post">
            <div class="modal" id="modal_nuevo_agente">
                <div class="modal-dialog modal-xl modal-fullscreen-xl-down">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">
                                <i class="fa-solid fa-user fa-fw"></i>
                                Nuevo empleado
                            </h4>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Cerrar"></button>
                        </div>
                        <div class="modal-body">
                            <p class="form-text">Se creará un empleado y se asignará a este usuario.</p>
                            <div class="mb-2">
                                <label>
                                    Nombre
                                </label>
                                <div class="col-sm-10">
                                    <input class="form-control" type="text" name="nnombre" autocomplete="off" required/>
                                </div>
                            </div>
                            <div class="mb-2">
                                <label>
                                    Apellidos
                                </label>
                                <div class="col-sm-10">
                                    <input class="form-control" type="text" name="napellidos" autocomplete="off"/>
                                </div>
                            </div>
                            <div class="mb-2">
                                <label>
                                    {!! ucfirst(constant('FS_CIFNIF')) !!}
                                </label>
                                <div class="col-sm-10">
                                    <input class="form-control" type="text" name="ndnicif" autocomplete="off"/>
                                </div>
                            </div>
                            <div class="mb-2">
                                <label>
                                    Teléfono
                                </label>
                                <div class="col-sm-10">
                                    <input class="form-control" type="text" name="ntelefono" autocomplete="off"/>
                                </div>
                            </div>
                            <div class="mb-2">
                                <label>
                                    Email
                                </label>
                                <div class="col-sm-10">
                                    <input class="form-control" type="text" name="nemail" autocomplete="new-email"/>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-primary flex-grow-1 flex-sm-grow-0" type="submit" title="Guardar">
                                <i class="fa-solid fa-save fa-fw"></i>
                                <span>Guardar</span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    @else
        @include('parts/errors/coming-soon')
    @endif
@endsection
