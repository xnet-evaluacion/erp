<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="es" xml:lang="es">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>MiFactura.eu</title>
    <meta name="description" content="MiFactura.eu es un software de facturación y contabilidad para pymes. Es software libre bajo licencia GNU/LGPL."/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <link rel="shortcut icon" href="{!! FS_PATH !!}Templates/img/favicon.ico?idcache={!! $fsc->id_cache !!}"/>
    <link rel="stylesheet" href="{!! FS_PATH !!}node_modules/@fortawesome/fontawesome-free/css/all.min.css?idcache={!! $fsc->id_cache !!}"/>
    <link rel="stylesheet" href="{!! FS_PATH !!}Templates/css/custom.css?idcache={!! $fsc->id_cache !!}"/>

    <script type="text/javascript" src="{!! FS_PATH !!}node_modules/jquery/dist/jquery.min.js?idcache={!! $fsc->id_cache !!}"></script>
    <script type="text/javascript" src="{!! FS_PATH !!}node_modules/bootstrap/dist/js/bootstrap.bundle.min.js?idcache={!! $fsc->id_cache !!}"></script>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-12">
            <div class="card border">
                <div class="card-header mt-2 text-truncate">
                    <i class="fa-solid fa-exclamation fa-fw"></i>
                    Error
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12 text-justify">
                            @include('parts/header-messages')
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 text-justify">
                            <p class="form-text">
                                Ha sido imposible conectar a la base de datos. Los motivos suelen ser distintos
                                en función de dónde tengas instalado MiFactura.eu.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card border">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <h3 class="fw-light">
                                <i class="fa-solid fa-desktop fa-fw"></i>
                                Instalado en tu PC...
                            </h3>
                            <ul class="m-0">
                                <li>
                                    Se ha desconectado {!! FS_DB_TYPE !!}. Para solucionarlo debes reiniciar {!! FS_DB_TYPE !!}.
                                    Si estás usando MiFactura.eu portable, ve a la carpeta de MiFactura.eu, haz clic
                                    en <b>xampp-control</b> y pulsa el botón
                                    <b>start</b>, al lado de {!! FS_DB_TYPE !!}.
                                </li>
                                <li>
                                    Has cambiado la contraseña de la base de datos. Para solucionarlo debes volver a poner
                                    la contraseña original de la base de datos, o bien poner la nueva contraseña en el archivo
                                    config.php de MiFactura.eu.
                                </li>
                            </ul>
                        </div>
                        <div class="col-sm-6">
                            <h3 class="fw-light">
                                <i class="fa-solid fa-globe fa-fw"></i>
                                Instalado en un hosting...
                            </h3>
                            <ul class="m-0">
                                <li>
                                    Se ha bloqueado {!! FS_DB_TYPE !!}. Contacta con tu proveedor para que lo reinicie.
                                </li>
                                <li>
                                    Has excedido la cuota del hosting. Muchos hostings tienen límites de uso en la base
                                    de datos, y si los excedes te bloquean.
                                </li>
                                <li>Se ha cambiado la contraseña o la configuración de la base de datos.</li>
                            </ul>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <br/>
                            <a href="{!! FS_COMMUNITY_URL !!}/contacto" target="_blank" class="d-flex justify-content-center align-items-center btn btn-info">
                                <i class="fa-solid fa-link fa-fw"></i>
                                <span>Soporte oficial</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
