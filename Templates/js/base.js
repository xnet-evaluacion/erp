/**
 * This file is part of MiFactura.eu
 * Copyright (C) 2021 X-Net Software Solutions S.L. <xnetsoftware@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

let formUrl = '';

let input_number = "number";
if (navigator.userAgent.toLowerCase().indexOf("firefox") > -1) {
    input_number = "text";
}

function fs_round(value, precision) {
    var m, f, isHalf, sgn;
    precision |= 0;
    m = Math.pow(10, precision);
    value *= m;
    sgn = (value > 0) | -(value < 0);
    isHalf = value % 1 === 0.5 * sgn;
    f = Math.floor(value);

    if (isHalf) {
        value = f + (sgn > 0);
    }

    return (isHalf ? value : Math.round(value)) / m;
}

function number_format(number, decimals, dec_point, thousands_sep) {
    var n = number, c = isNaN(decimals = Math.abs(decimals)) ? 2 : decimals;
    var d = (dec_point == undefined) ? "," : dec_point;
    var t = (thousands_sep == undefined) ? "." : thousands_sep, s = n < 0 ? "-" : "";
    var i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
}

var Base64 = {
    // private property
    _keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",

    // public method for encoding
    encode: function (input) {
        var output = "";
        var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
        var i = 0;

        input = Base64._utf8_encode(input);

        while (i < input.length) {
            chr1 = input.charCodeAt(i++);
            chr2 = input.charCodeAt(i++);
            chr3 = input.charCodeAt(i++);

            enc1 = chr1 >> 2;
            enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
            enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
            enc4 = chr3 & 63;

            if (isNaN(chr2)) {
                enc3 = enc4 = 64;
            } else if (isNaN(chr3)) {
                enc4 = 64;
            }

            output = output +
                this._keyStr.charAt(enc1) + this._keyStr.charAt(enc2) +
                this._keyStr.charAt(enc3) + this._keyStr.charAt(enc4);
        }

        return output;
    },

    // public method for decoding
    decode: function (input) {
        var output = "";
        var chr1, chr2, chr3;
        var enc1, enc2, enc3, enc4;
        var i = 0;

        input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

        while (i < input.length) {
            enc1 = this._keyStr.indexOf(input.charAt(i++));
            enc2 = this._keyStr.indexOf(input.charAt(i++));
            enc3 = this._keyStr.indexOf(input.charAt(i++));
            enc4 = this._keyStr.indexOf(input.charAt(i++));

            chr1 = (enc1 << 2) | (enc2 >> 4);
            chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
            chr3 = ((enc3 & 3) << 6) | enc4;

            output = output + String.fromCharCode(chr1);

            if (enc3 != 64) {
                output = output + String.fromCharCode(chr2);
            }
            if (enc4 != 64) {
                output = output + String.fromCharCode(chr3);
            }
        }

        output = Base64._utf8_decode(output);

        return output;

    },

    // private method for UTF-8 encoding
    _utf8_encode: function (string) {
        string = string.replace(/\r\n/g, "\n");
        var utftext = "";

        for (var n = 0; n < string.length; n++) {
            var c = string.charCodeAt(n);

            if (c < 128) {
                utftext += String.fromCharCode(c);
            } else if ((c > 127) && (c < 2048)) {
                utftext += String.fromCharCode((c >> 6) | 192);
                utftext += String.fromCharCode((c & 63) | 128);
            } else {
                utftext += String.fromCharCode((c >> 12) | 224);
                utftext += String.fromCharCode(((c >> 6) & 63) | 128);
                utftext += String.fromCharCode((c & 63) | 128);
            }
        }

        return utftext;
    },

    // private method for UTF-8 decoding
    _utf8_decode: function (utftext) {
        var string = "";
        var i = 0;
        var c = c1 = c2 = 0;

        while (i < utftext.length) {
            c = utftext.charCodeAt(i);

            if (c < 128) {
                string += String.fromCharCode(c);
                i++;
            } else if ((c > 191) && (c < 224)) {
                c2 = utftext.charCodeAt(i + 1);
                string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
                i += 2;
            } else {
                c2 = utftext.charCodeAt(i + 1);
                c3 = utftext.charCodeAt(i + 2);
                string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
                i += 3;
            }
        }

        return string;
    }

}

/**
 * Esta función sirve para las extensiones tipo modal.
 */
function fs_modal(txt, url) {
    $("#modal_iframe h4.modal-title").html(Base64.decode(txt));
    $("#modal_iframe iframe").attr("src", url);
    $("#modal_iframe").modal("show");
}

/**
 * Redimensiona todos los textarea al contenido del texto
 */
function redimensionar_textareas() {
    $('textarea').not('.editor').each(function () {
        this.style.height = 'auto';
        var height = this.scrollHeight;
        /*
        // Anteriormente se solicitó mínimo 3 líneas, ahora ya no se quiere así
        if (this.val === "") {
            height = Math.max(93, this.scrollHeight);
        }
        */
        this.setAttribute('style', 'height:' + (height) + 'px;overflow-y:hidden;');
    }).on('input', function () {
        this.style.height = 'auto';
        this.style.height = (this.scrollHeight) + 'px';
    });
}

/**
 * Redimensiona todos los iframes encontrados a su contenido
 */
function redimensionar_iframes() {
    $('iframe').not('.tox-edit-area__iframe').each(function () {
        let altura = $(this).contents().find("html").height();
        if (altura !== 0) {
            $(this).height(altura + 100);
        }
    });
}

/**
 * Redimensiona todos los datatables encontrados a su contenido
 */
function redimensionar_datatables() {
    if ($.fn.dataTable !== undefined) {
        $.fn.dataTable.tables({visible: true, api: true}).columns.adjust().draw();
    }
}

/**
 * Se quiere forzar a volver a la pestaña activa tras enviar un formulario
 * por tanto necesitamos cambiar su action url al vuelo.
 *
 * TODO 05/04/2022: Es posible que en algunas páginas se requiera una funcionalidad parecida a como se gestiona con los datatables:
 *  - De forma genérica: datatables_initialisation()
 *  - De forma específica: datatables_page_{{ $fsc->page->name }}()
 */
function refrescar_form_url(selector) {
    if (formUrl === '') {
        // Si no hay formulario omitimos la comprobación
        let items = selector.closest('form');
        if (items.length === 0) {
            return;
        }
        formUrl = items[0].action;
    }
    if (selector[0].id !== undefined && selector[0].id !== '' && selector.parents('form:first')[0].action) {
        selector.parents('form:first')[0].action = formUrl + '#' + selector[0].id;
    } else {
        bootbox.alert({
            message: '<p>No hay ID para ser usado desde refrescar_from_url, con lo que no se volverá a la pestaña seleccionada.</p>'
                + '<p>NOTA: No afecta al guardado de datos.</p>',
            title: '<b>Atención</b>'
        });
    }
}

/**
 * Mostramos la pestaña activa si la hay.
 */
function show_selected_tab() {
    let selectedHash = window.location.hash.substring(1);
    if (selectedHash !== undefined && selectedHash !== '') {
        console.log('Hay notas en "show_selected_tab" en "Templates/js/base.js", si estás leyendo este mensaje y está fallando algo de esta página, deberás completar esta función para solucionarlo.');
        /*
         * TODO: Esto sólo soporta 1 nivel de pestañas actualmente
         *  Faltaría detectar si este link, está dentro de otra tablist,
         *  y así darle visibilidad por orden
         *      - TABS_1r nivel show
         *      - TABS_2o nivel show
         *      - TABS_3r nivel show
         */
        $('ul[role="tablist"] li[role="presentation"] a#' + selectedHash).tab('show');
    }
}

/**
 *
 */
function check_tab_links() {
    selector = 'ul[role="tablist"] li[role="presentation"] a';

    document.querySelectorAll(selector).forEach(function (node) {
        if (!node.id) {
            bootbox.alert({
                message: 'La pestaña <b>' + node.text + '</b> no tiene ID, es requerida para el cambio automático de pestaña.',
                title: '<b>ERROR</b>',
                size: 'large'
            });
            throw new Error('La pestaña ' + node.text + ' no tiene ID, es requerida para el cambio automático de pestaña.');
        }
    });
}

/**
 * Función para unificar todas las respuestas via AJAX que devuelven código HTML.
 *
 * @param method
 * @param url
 * @param datos
 * @param target
 */
function processAjaxQuery(method, url, datos, target) {
    $.ajax({
        type: method,
        url: url,
        dataType: 'html',
        data: datos,
        success: function (response) {
            if (response !== undefined) {
                target.html(response);
            } else {
                bootbox.alert({
                    message: '<b>No se ha recibido una respuesta válida:</b> <br>' + response,
                    title: "<b>Atención</b>"
                });
            }
        },
        error: function (error) {
            bootbox.alert({
                message: '<b>ERROR:</b> <br>' + error,
                title: "<b>Atención</b>"
            });
        }
    });
}

/**
 * Inicialización de los textarea que deben visualizarse con TinyMCE
 */
function initializeTinyMCE() {
    tinymce.init({
        selector: "textarea.editor",
        language: 'es',
        language_url: 'Templates/src/assets/js/tinymce/es.js',
        height: 300,
        menubar: false,
        plugins: [
            'advlist', 'autolink', 'lists', 'link', 'image', 'charmap', 'preview', 'anchor',
            'searchreplace', 'visualblocks', 'code', 'fullscreen',
            'insertdatetime', 'media', 'table', 'code', 'help', 'wordcount'
        ],
        toolbar: 'undo redo | blocks | ' +
            'bold italic backcolor | alignleft aligncenter ' +
            'alignright alignjustify | bullist numlist outdent indent | ' +
            'removeformat | help',
        content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }',
        // callback function
        init_instance_callback: (editor) => {
            // console.log(`Editor: ${editor.id} is now initialized.`);
            // $('#' + editor.id).css({'display': 'none'});
        }
    });
}

function  activar_autocomplets_fs() {
    $( ".fs_autocomplete" ).each( function () {
        $("#" + $(this).attr("id")).devbridgeAutocomplete({
            serviceUrl: $(this).attr("ac_url"), //la url con el ajax de respuesta para los posibles valores
            dataType: "json",
            minLength: 2,
            deferRequestBy: 200,
            paramName: "query",
            showNoSuggestionNotice: true,
            noSuggestionNotice: $(this).attr("ac_noSuggestion"),
            onSearchStart: function(params) {
                $("#" + $(this).attr("inputid")).val("");
            },
            onSelect: function(suggestion) {
                $("#"+$(this).attr("inputid")).val(suggestion.id);
            }
        });
    });

    $(".fs_autocomplete").each( function() {
        $.get( $(this).attr("ac_url") , {
            "get": $("#" + $(this).attr("inputid")).val(),
            "input": $(this).attr("id")
        }).done(function (data) {
            if (data) {
                $("#" + data.input).val(data.campo.name);
            }
        });
    });
}

/* se encarga de guardar en una cookie el valor de los marcos de filtrado de busqueda y recuperar dicho valor para
    aplicarlo
 */
function collapse_expand_searchfilters() {
    if (localStorage.getItem(idtab_searchFilters()) == "0") {
        $("#collapseSearchFilters").collapse("hide");
    }
    $("#searchFilters").click( function() {
        if (! localStorage.getItem(idtab_searchFilters()) || localStorage.getItem(idtab_searchFilters()) == "1") {
            localStorage.setItem(idtab_searchFilters(), "0");
        } else {
            localStorage.setItem(idtab_searchFilters(), "1");
        }
    });
}

function idtab_searchFilters() {
    var urlParams = new URLSearchParams(window.location.search);
    return k = "SF_" + urlParams.get('page') + "_" + window.location.hash.substring(1);
}

$(document).ready(function () {
    check_tab_links();
    show_selected_tab();

    $("#b_feedback").click(function (event) {
        event.preventDefault();
        $("#modal_feedback").modal("show");
        document.f_feedback.feedback_text.focus();
    });
    $(".clickableRow").mousedown(function (event) {
        if (event.which === 1) {
            var href = $(this).attr("href");
            var target = $(this).attr("target");
            if (typeof href !== typeof undefined && href !== false) {
                if (typeof target !== typeof undefined && target === "_blank") {
                    window.open($(this).attr("href"));
                } else {
                    parent.document.location = $(this).attr("href");
                }
            }
        }
    });

    redimensionar_textareas();
    redimensionar_iframes();

    activar_autocomplets_fs();

    /**
     * Evento al cambiar de pestaña"
     */
    $('a[role="tab"]').on('shown.bs.tab', function () {
        redimensionar_textareas();
        redimensionar_datatables();
        redimensionar_iframes();
        refrescar_form_url($(this));
    });

    // Cuando se ajuste el tamaño del iframe, se invocará este desde el iframe
    $(window).resize(function () {
        redimensionar_textareas();
        redimensionar_datatables();
        redimensionar_iframes();
    });

    collapse_expand_searchfilters();

    initializeTinyMCE();
});
