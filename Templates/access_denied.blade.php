@extends('layouts/main')

@section('main-content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card border">
                    <div class="card-header mt-2 text-truncate">
                        <i class="fa-solid fa-lock fa-fw"></i>
                        Acceso denegado
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12 text-justify">
                                <ul>
                                    <li>No tienes permiso para acceder a esta página.</li>
                                    <li>
                                        @if ($fsc->user->admin)
                                            <b>¿Has movido de sitio la instalación de #{!! $fsc->reseller_data->name !!} o has eliminado plugins manualmente?</b>
                                            El error de acceso denegado a una página también sucede cuando la página a la que se intenta
                                            acceder ya no existe o <b>el plugin está desactivado</b>.
                                            <ul>
                                                <li>Ve a <strong>ADMINISTRACIÓN</strong> -&gt; Panel de control -&gt; Ajustes, y desde la pestaña de plugins, activa los que necesites.</li>
                                            </ul>
                                        @else
                                            Consulta con el administrador <u>para que te dé acceso</u> a esta página.
                                        @endif
                                    </li>
                                    <li>
                                        Si crees que es un error de #{!! $fsc->reseller_data->name !!}, no dudes en notificármelo.
                                        Ve a <strong>X-NET GROUP</strong> -&gt; Mi cuenta -&gt; Contactar, y <b>notifícame el error</b>.
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
