<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="es" xml:lang="es">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>
        {!! $fsc->page->title !!} &lsaquo;
        @if ($fsc->empresa->nombrecorto)
            {!! $fsc->empresa->nombrecorto !!}
        @else
            {!! $fsc->empresa->nombre !!}
        @endif
    </title>
    <meta name="description" content="#{!! $fsc->reseller_data->name !!} es un software de facturación y contabilidad para pymes. Es software libre bajo licencia GNU/LGPL."/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <meta name="generator" content="#{!! $fsc->reseller_data->name !!}"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="mobile-web-app-capable" content="yes">

    @include('parts/header-styles')

    @include('block/extensions/style')
</head>
<body>
    <div>
        <div class="page-content">

            @include('parts/header-messages')
