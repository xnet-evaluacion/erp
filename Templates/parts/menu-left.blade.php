{{--
Basado en: https://codepen.io/diego-q/pen/qBWjZwr
--}}

<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
    <div class="container-fluid">
        <a href="index.php" class="navbar-brand">
            <i class="fa-solid fa-home fa-fw"></i>
            @if (FS_DEMO)
                DEMO
            @elseif ($fsc->empresa->nombrecorto)
                {!! $fsc->empresa->nombrecorto !!}
            @else
                {!! $fsc->empresa->nombre !!}
            @endif
        </a>

        <button class="text-secondary me-auto" type="button" data-bs-toggle="collapse" data-bs-target="#left-menu" aria-controls="left-menu" aria-expanded="true" aria-label="Toggle left menu">
            <span class="navbar-toggler-icon"></span>
        </button>

        <button class="navbar-toggler collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#mf-navbar-collapse-1" aria-controls="mf-navbar-collapse-1" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="navbar-collapse collapse" id="mf-navbar-collapse-1">
            <ul class="ms-auto mb-md-0 navbar-nav">
                <li class="nav-item d-md-flex justify-content-center align-items-center" role="presentation">
                    <a rel="noopener" href="https://api.whatsapp.com/send?phone=34637582364&text=Necesito%20Soporte" target="_blank" class="nav-link d-flex flex-column align-items-center" title="Soporte vía WhatsApp">
                        <i style="background-size: auto 20px; height: 20px; overflow: hidden; padding-right: 0; width: 20px; background-image: url(https://static.whatsapp.net/rsrc.php/yv/r/-r3j-x8ZnM7.svg); background-repeat: no-repeat; display: block; float: left;"></i>
                        <span class="d-block d-sm-none">Soporte vía WhatsApp</span>
                    </a>
                </li>
                <li class="nav-item dropdown d-md-flex align-items-center justify-content-center">
                    <a href="#" data-bs-toggle="dropdown" id="navbarDropdown_ayuda" class="nav-link dropdown-toggle d-flex">
                        <i class="fa-solid fa-circle-question fa-fw"></i>
                        <span class="d-block d-sm-none">{!! $fsc->reseller_data->name !!}</span>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown_ayuda">
                        @if (isset($reseller->url))
                            <li>
                                <a href="{!! $fsc->reseller_data->url !!}" target="_blank" class="dropdown-item text-secondary d-flex">
                                    <i class="fa-solid fa-book fa-fw"></i>{!! $fsc->reseller_data->name !!}
                                </a>
                            </li>
                        @endif
                        @if (isset($reseller->url_contacto))
                            <li>
                                <a href="{!! $fsc->reseller_data->url_contacto !!}" target="_blank" class="dropdown-item text-secondary d-flex">
                                    <i class="fa-solid fa-shield fa-fw"></i>
                                    Contactar
                                </a>
                            </li>
                        @endif
                        <li class="divider"></li>
                        <li>
                            <a href="#" id="b_feedback" class="dropdown-item text-secondary d-flex">
                                <i class="fa-solid fa-edit fa-fw"></i>
                                Informar de error...
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="nav-item dropdown d-md-flex align-items-center justify-content-center">
                    <a id="navbarDropdown_usuario" class="nav-link dropdown-toggle d-flex" href="#" data-bs-toggle="dropdown" title="{!! $fsc->user->nick !!}">
                        <i class="fa-solid fa-user-circle fa-fw"></i>
                        <span class="d-block d-sm-none">Usuario</span>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-end" data-bs-popper="static" aria-labelledby="navbarDropdown_usuario">
                        <li>
                            <a href="{!! $fsc->user->url() !!}" class="dropdown-item text-secondary d-flex">
                                <i class="fa-solid fa-user-circle fa-fw"></i>{!! $fsc->user->nick !!}
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="{!! $fsc->url() !!}&logout=TRUE" class="dropdown-item text-secondary d-flex">
                                <i class="fa-solid fa-sign-out fa-fw"></i>
                                Cerrar sesión
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>

<div class="d-flex">
    <div class="nav-style p-2 bg-dark collapse-horizontal show collapse" id="left-menu">
        <nav class="sidebar-nav">
            <ul class="metismenu scrollarea" id="main-menu">
                @if (count($GLOBALS['plugins'])>0)
                    <li>
                        <a href="#" class="text-white has-arrow" title="Acceso rápido">
                            <i class="fa-solid fa-star fa-fw"></i>
                            Acceso rápido
                        </a>
                        <ul class="mm-collapse">
                            @php
                                $menu_ar_vacio = true;
                            @endphp
                            @foreach ($fsc->user->get_menu() as $key1 => $value1)
                                @if ($value1->important)
                                    <li>
                                        <a href="{!! $value1->url() !!}" class="text-white">
                                            {{--<i class="fa-solid fa-link fa-fw"></i>--}}
                                            <span class="text-capitalize">{!! $value1->title !!}</span>
                                        </a>
                                    </li>
                                    @php
                                        $menu_ar_vacio = false;
                                    @endphp
                                @endif
                            @endforeach
                            @if ($menu_ar_vacio)
                                <li>
                                    <a href="#" class="dropdown-toggle" data-bs-toggle="dropdown">
                                        Vacío
                                    </a>
                                </li>
                            @endif
                        </ul>
                    </li>
                @endif
                @foreach ($fsc->folders() as $key1 => $value1)
                    @php
                        $class_folder = '';
                        $icon_folder = 'fa-solid fa-folder fa-fw';
                        $open_folder = '';
                        $expanded_folder = 'false';

                    @endphp
                    @if ($value1==$fsc->page->folder)
                        @php
                            $class_folder = 'fw-bold';
                            $icon_folder = 'fa-solid fa-folder-open fa-fw';
                            $open_folder = 'mm-active';
                            $expanded_folder = 'true';
                        @endphp
                    @endif

                    <li class="{!! $open_folder !!}">
                        <a href="#" aria-expanded="{!! $expanded_folder !!}" class="text-white has-arrow {!! $class_folder !!}">
                            <i class="{!! $icon_folder !!}" title="{!! $value1 !!}"></i>
                            <span class="text-capitalize">{!! $value1 !!}</span>
                        </a>
                        <ul class="mm-collapse">
                            @foreach ($fsc->pages($value1) as $key2 => $value2)
                                @php
                                    $class_link = '';
                                @endphp
                                @if ($value2->showing())
                                    @php
                                        $class_link = 'fw-bold';
                                    @endphp
                                @endif
                                <li>
                                    <a href="{!! $value2->url() !!}" class="text-white {!! $class_link !!}">
                                        {{--<i class="fa-solid fa-list fa-fw" title="Administración"></i>--}}
                                        <span style="text-transform: none;">{!! $value2->title !!}</span>
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </li>
                @endforeach
            </ul>
        </nav>
    </div>

    <div class="d-flex flex-column scrollarea" id="content">
