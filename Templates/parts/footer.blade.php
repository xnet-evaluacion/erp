{{--
<div class="m-3"> </div>
--}}
@if (FS_DEMO)
    <br/>
    <div class="container-fluid bg-success">
        <div class="row">
            <div class="col-12">
                <h3 class="fw-light">
                    <i class="fa-solid fa-circle-question fa-fw"></i>
                    ¿Te ha convencido?
                </h3>
                <p class="form-text">
                    Esta demo sirve para tener una idea rápida de cómo funciona #{!! $fsc->reseller_data->name !!},
                    con un número reducido de plugins activos y muchas opciones desactivadas.
                    Ten en cuenta que hay docenas de personas usando esta demo a la vez.
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <a href='https://www.mifactura.eu/alta.php' target="_blank" class="d-flex justify-content-center align-items-center btn btn-lg d-flex justify-content-center align-items-center btn-primary">
                    <i class="fa-solid fa-download fa-fw"></i>
                    <span>Empieza a Usarlo es Gratis</span>
                </a>
                <i class="fa-solid fa-cloud fa-fw"></i>
                <i class="fa-solid fa-windows fa-fw"></i>
                <i class="fa-solid fa-linux fa-fw"></i>
                <i class="fa-solid fa-apple fa-fw"></i>
                Disponible para todas las plataformas desde la Nube.
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <br/><br/><br/>
            </div>
        </div>
    </div>
@else
    <div class="modal fade" id="modal_mf">
        <div class="modal-dialog modal-xl modal-fullscreen-xl-down">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">

                    </h4>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Cerrar"></button>
                </div>
                <div class="modal-body">
                </div>
            </div>
        </div>
    </div>

    @if (FS_DEMO)
        <hr class="mt-3"/>
        @endif
        @endif

        </main>

        @include('credits')

        <style rel="stylesheet">
            .float {
                position: fixed;
                width: 60px;
                height: 60px;
                bottom: 13px;
                right: 254px;
                background-color: #25d366;
                color: #FFF;
                border-radius: 50px;
                text-align: center;
                font-size: 35px;
                box-shadow: 2px 2px 3px #999;
                z-index: 100;
            }

            .my-float {
                margin-top: 16px;
            }
        </style>

        @include('parts/footer-javascripts')
        </body>
        </html>
