<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="text-center mb-2">
                <img src="{!! FS_PATH !!}Templates/dist/assets/images/logo-dark.svg" alt="" style="height: 50px;">
                <h4 class="mt-2 text-uppercase">En mantenimiento</h4>
                <div class="mt-3 text-center">
                    <a href="index.php" class="btn btn-primary waves-effect waves-light flex-grow-1 flex-sm-grow-0">
                        Ir al Inicio
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-8 col-xl-6">
            <img src="{!! FS_PATH !!}Templates/dist/assets/images/maintenance.svg" alt="" class="img-fluid">
        </div>
    </div>
</div>
