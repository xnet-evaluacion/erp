@if (file_exists('images/favicon.ico'))
    <link rel="shortcut icon" href="{!! FS_PATH !!}images/favicon.ico?idcache={!! $fsc->id_cache !!}"/>
@else
    <link rel="shortcut icon" href="{!! FS_PATH !!}Templates/dist/assets/images/favicon.ico?idcache={!! $fsc->id_cache !!}"/>
@endif
{{-- La añadimos para evitar ver un cuadrado hasta que cargan los SVG --}}
<link rel="stylesheet" href="{!! FS_PATH !!}node_modules/@fortawesome/fontawesome-free/css/all.min.css?idcache={!! $fsc->id_cache !!}"/>
<link rel="stylesheet" href="{!! FS_PATH !!}node_modules/toastr/build/toastr.min.css?idcache={!! $fsc->id_cache !!}"/>
{{-- <link rel="stylesheet" href="{!! FS_PATH !!}node_modules/jquery-ui-dist/jquery-ui.min.css?idcache={!! $fsc->id_cache !!}"/> --}}

@include('block/extensions/style')

@if (view_exists('block/style/' . $fsc->page->name))
    @include('block/style/' . $fsc->page->name)
@endif

@section('styles')
    <!-- Sección styles -->
@show

@switch(THEME)
    @case('skote')
        <link rel="stylesheet" href="{!! FS_PATH !!}node_modules/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css?idcache={!! $fsc->id_cache !!}"/>
        <link rel="stylesheet" href="{!! FS_PATH !!}Templates/dist/assets/css/icons.min.css?idcache={!! $fsc->id_cache !!}">
        @switch($fsc->user->theme_body)
            @case('dark')
                <link rel="stylesheet" href="{!! FS_PATH !!}Templates/dist/assets/css/bootstrap-dark.min.css?idcache={!! $fsc->id_cache !!}" id="bootstrap-style">
                <link rel="stylesheet" href="{!! FS_PATH !!}node_modules/select2/dist/css/select2.min.css?idcache={!! $fsc->id_cache !!}"/>
                <link rel="stylesheet" href="{!! FS_PATH !!}node_modules/select2-bootstrap-5-theme/dist/select2-bootstrap-5-theme.min.css?idcache={!! $fsc->id_cache !!}"/>
                <link rel="stylesheet" href="{!! FS_PATH !!}Templates/dist/assets/css/app-dark.min.css?idcache={!! $fsc->id_cache !!}" id="app-style">
                @break

            @default
            @case('light')
                <link rel="stylesheet" href="{!! FS_PATH !!}Templates/dist/assets/css/bootstrap.min.css?idcache={!! $fsc->id_cache !!}" id="bootstrap-style">
                <link rel="stylesheet" href="{!! FS_PATH !!}node_modules/select2/dist/css/select2.min.css?idcache={!! $fsc->id_cache !!}"/>
                <link rel="stylesheet" href="{!! FS_PATH !!}node_modules/select2-bootstrap-5-theme/dist/select2-bootstrap-5-theme.min.css?idcache={!! $fsc->id_cache !!}"/>
                <link rel="stylesheet" href="{!! FS_PATH !!}Templates/dist/assets/css/app.min.css?idcache={!! $fsc->id_cache !!}" id="app-style">
                @break
        @endswitch
        @break

    @default
        <link rel="stylesheet" href="{!! FS_PATH !!}Templates/css/custom.css?idcache={!! $fsc->id_cache !!}"/>
        <link rel="stylesheet" href="{!! FS_PATH !!}node_modules/bootstrap/dist/css/bootstrap.min.css?idcache={!! $fsc->id_cache !!}">
        <link rel="stylesheet" href="{!! FS_PATH !!}node_modules/select2/dist/css/select2.min.css?idcache={!! $fsc->id_cache !!}"/>
        <link rel="stylesheet" href="{!! FS_PATH !!}node_modules/select2-bootstrap-5-theme/dist/select2-bootstrap-5-theme.min.css?idcache={!! $fsc->id_cache !!}"/>

        @if (!empty($fsc->user->css) && file_exists($fsc->user->css))
            <link rel="stylesheet" href="{!! FS_PATH !!}{!! $fsc->user->css !!}?idcache={!! $fsc->id_cache !!}"/>
        @endif
        @if ($fsc->use_sidebar_menu == true)
            <link rel="stylesheet" href="{!! FS_PATH !!}node_modules/metismenu/dist/metisMenu.min.css?idcache={!! $fsc->id_cache !!}"/>
            <link rel="stylesheet" href="{!! FS_PATH !!}Templates/css/sidebar.css?idcache={!! $fsc->id_cache !!}" type="text/css" title="no title" charset="utf-8">
        @endif
@endswitch

{{-- TODO: Esto va a servir de ñapa mientras tengamos iframes para ganar el máximo espacio posible --}}
@if ($fsc->hide_title == true && $fsc->hide_footer == true)
    <style rel="stylesheet">
        body {
            background-color: transparent;
        }

        body > div > div.page-content {
            padding: 0 !important;
        }

        body > div > div.page-content > div.container-fluid > div.row > div.col {
            padding: 0 !important;
        }

        /*
        body > div > div.page-content > div.container-fluid > div.row > div.col > div.card.border,
        body > div > div.page-content > form > div.container-fluid > div.row > div.col > div.card.border
        {
            border: 0 !important;
        }
        */
    </style>
@endif

@if (FS_DEBUG)
    <!-- debugBar->getHeader -->
    {!! $fsc->debugBar->getHeader() !!}
@endif
