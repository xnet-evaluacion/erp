<div id="layout-wrapper">
    <header id="page-topbar">
        <div class="navbar-header">
            <div class="d-flex">
                <div class="navbar-brand-box">
                    <a href="index.php" class="logo logo-dark">
                        <span class="logo-sm"><img src="{!! FS_PATH !!}Templates/dist/assets/images/logo-icon-dark.svg" alt="Logo" style="height: 22px;"></span>
                        <span class="logo-lg"><img src="{!! FS_PATH !!}Templates/dist/assets/images/logo-dark.svg" alt="Logo" style="height: 24px;"></span>
                    </a>

                    <a href="index.php" class="logo logo-light">
                        <span class="logo-sm"><img src="{!! FS_PATH !!}Templates/dist/assets/images/logo-icon-light.svg" alt="Logo" style="height: 22px;"></span>
                        <span class="logo-lg"><img src="{!! FS_PATH !!}Templates/dist/assets/images/logo-light.svg" alt="Logo" style="height: 24px;"></span>
                    </a>
                </div>

                <button type="button" class="btn px-3 font-size-16 header-item waves-effect" id="vertical-menu-btn" aria-label="Menú principal">
                    <i class="fa-solid  fa-bars"></i>
                </button>

                <form class="app-search d-none d-lg-block" onsubmit="alert('Pendiente de implementar');">
                    <div class="position-relative">
                        <input type="text" class="form-control" placeholder="Buscar...">
                        <span class="bx bx-search-alt"></span>
                    </div>
                </form>

                <div class="dropdown dropdown-mega d-none d-lg-block">
                    <button type="button" class="btn header-item waves-effect" data-bs-toggle="dropdown" aria-haspopup="false" aria-expanded="false">
                        <span>Menu</span>
                        <i class="fa-solid fa-chevron-down "></i>
                    </button>
                    <div class="dropdown-menu dropdown-megamenu">
                        <div class="row">
                            <div class="col-6">
                                <div class="row">
                                    <div class="col-md-4">
                                        <h5 class="font-size-14">X-Net Software</h5>
                                        <ul class="list-unstyled megamenu-list">
                                            <li>
                                                <a href="#" class="link-primary"> X-Net ERP</a>
                                            </li>
                                            <li>
                                                <a href="#" class="link-primary">XDCP Control Horario</a>
                                            </li>
                                            <li>
                                                <a href="#" class="link-primary">XDES Satisfacción</a>
                                            </li>
                                            <li>
                                                <a href="#" class="link-primary">XCF Control de Flotas</a>
                                            </li>
                                            <li>
                                                <a href="#" class="link-primary">XBI Business Intelligence</a>
                                            </li>
                                            <li>
                                                <a href="#" class="link-primary">XBX IA</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-md-4">
                                        <h5 class="font-size-14">X-Net Digital</h5>
                                        <ul class="list-unstyled megamenu-list">
                                            <li>
                                                <a href="#" class="link-primary">Diseño Web</a>
                                            </li>
                                            <li>
                                                <a href="#" class="link-primary">E-Commerce</a>
                                            </li>
                                            <li>
                                                <a href="#" class="link-primary">Dominios</a>
                                            </li>
                                            <li>
                                                <a href="#" class="link-primary">Hosting</a>
                                            </li>
                                            <li>
                                                <a href="#" class="link-primary">Dedicados</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-md-4">
                                        <h5 class="font-size-14">X-Net Support</h5>
                                        <ul class="list-unstyled megamenu-list">
                                            <li>
                                                <a href="#" class="link-primary">Mantenimiento</a>
                                            </li>
                                            <li>
                                                <a href="#" class="link-primary">Redes</a>
                                            </li>
                                            <li>
                                                <a href="#" class="link-primary">Seguridad</a>
                                            </li>
                                            <li>
                                                <a href="#" class="link-primary">Maintenance</a>
                                            </li>
                                            <li>
                                                <a href="#" class="link-primary">Redes Cableadas y
                                                    Wifi</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="row">
                                    <div class="col-6">
                                        <h5 class="font-size-14">X-Net Fundation</h5>
                                        <ul class="list-unstyled megamenu-list">
                                            <li>
                                                <a href="#" class="link-primary">¿Qué es X-Net
                                                    Fundation?</a>
                                            </li>
                                            <li>
                                                <a href="#" class="link-primary">Innovación
                                                    Social</a>
                                            </li>
                                            <li>
                                                <a href="#" class="link-primary">Nuestro Reto</a>
                                            </li>
                                            <li>
                                                <a href="#" class="link-primary">XXVOZ - La Voz de la
                                                    Mujer</a>
                                            </li>
                                        </ul>
                                    </div>

                                    <div class="col-6">
                                        <div>
                                            <img src="{!! FS_PATH !!}Templates/img/loading_spinner.gif" data-src="{!! FS_PATH !!}Templates/dist/assets/images/x-net-group.jpg" alt="" class="lazyload img-fluid mx-auto d-block">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="d-flex">
                <div class="dropdown d-inline-block d-lg-none">
                    <button type="button" class="btn header-item noti-icon waves-effect" id="page-header-search-dropdown" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false" aria-label="Buscar">
                        <i class="fa-solid fa-magnifying-glass "></i>
                    </button>
                    <div class="dropdown-menu dropdown-menu-lg dropdown-menu-end p-0" aria-labelledby="page-header-search-dropdown">
                        <form class="p-3">
                            <div class="mb-2 m-0">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Buscar ...">
                                    <button class="btn btn-primary flex-grow-1 flex-sm-grow-0" type="submit" aria-label="Buscar">
                                        <i class="fa-solid fa-magnifying-glass "></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="dropdown d-inline-block">
                    <button type="button" class="btn header-item waves-effect" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <img id="header-lang-img" src="{!! FS_PATH !!}Templates/dist/assets/images/flags/spain.jpg" alt="Español" style="height: 16px;">
                    </button>
                    <div class="dropdown-menu dropdown-menu-end">
                        <a href="#" class="dropdown-item text-secondary notify-item language" data-lang="es">
                            <img src="{!! FS_PATH !!}Templates/dist/assets/images/flags/spain.jpg" alt="Idioma español" class="me-1" style="height: 12px;">
                            <span class="align-middle">Español</span>
                        </a>
                        {{--
                        <a href="#" class="dropdown-item text-secondary notify-item language" data-lang="en">
                            <img src="{!! FS_PATH !!}Templates/dist/assets/images/flags/us.jpg" alt="Idioma inglés" class="me-1" style="height: 12px;">
                            <span class="align-middle">Inglés</span>
                        </a>
                        <a href="#" class="dropdown-item text-secondary notify-item language" data-lang="gr">
                            <img src="{!! FS_PATH !!}Templates/dist/assets/images/flags/germany.jpg" alt="Idioma alemán" class="me-1" style="height: 12px;">
                            <span class="align-middle">Alemán</span>
                        </a>
                        <a href="#" class="dropdown-item text-secondary notify-item language" data-lang="it">
                            <img src="{!! FS_PATH !!}Templates/dist/assets/images/flags/italy.jpg" alt="Idioma italiano" class="me-1" style="height: 12px;">
                            <span class="align-middle">Italiano</span>
                        </a>
                        <a href="#" class="dropdown-item text-secondary notify-item language" data-lang="ru">
                            <img src="{!! FS_PATH !!}Templates/dist/assets/images/flags/russia.jpg" alt="Idioma ruso" class="me-1" style="height: 12px;">
                            <span class="align-middle">Ruso</span>
                        </a>
                        --}}
                    </div>
                </div>

                <div class="dropdown d-none d-lg-inline-block">
                    <button type="button" class="btn header-item noti-icon waves-effect" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false" aria-label="Enlaces externos">
                        <i class="fa-solid fa-box-open" style="font-size: 1.25rem"></i>
                    </button>
                    <div class="dropdown-menu dropdown-menu-lg dropdown-menu-end">
                        <div class="px-lg-2">
                            <div class="row g-0">
                                <div class="col-12">
                                    <a href="https://n8n.io/" class="dropdown-icon-item">
                                        <img src="{!! FS_PATH !!}Templates/dist/assets/images/brands/logo-n8n.png" alt="GitHub">
                                        <span>n8n.io</span>
                                    </a>
                                </div>
                                <div class="col-12">
                                    <a href="https://cashdrotenerife.com/" class="dropdown-icon-item">
                                        <img src="{!! FS_PATH !!}Templates/dist/assets/images/brands/logo-cashdro.jpg" alt="Bitbucket">
                                        <span>CashDro</span>
                                    </a>
                                </div>
                                <div class="col-12">
                                    <a href="https://www.avg.com/es-es/" class="dropdown-icon-item">
                                        <img src="{!! FS_PATH !!}Templates/dist/assets/images/brands/logo-avg.png" alt="Dribbble">
                                        <span>AVG</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="dropdown d-none d-lg-inline-block">
                    <button type="button" class="btn header-item noti-icon waves-effect" data-toggle="fullscreen" aria-label="Entrar/salir de pantalla completa">
                        <i class="fa-solid fa-expand" style="font-size: 1.25rem"></i>
                    </button>
                </div>

                <div class="dropdown d-inline-block">
                    <button type="button" class="btn header-item noti-icon waves-effect" id="page-header-notifications-dropdown" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false" aria-label="Notificaciones nuevas">
                        <i class="fa-regular fa-bell" style="--fa-animation-duration: 3s;" id="user_notifications"></i>
                        <span class="badge bg-danger rounded-pill visually-hidden" id="user_total_notifications">0</span>
                    </button>
                    <div class="dropdown-menu dropdown-menu-lg dropdown-menu-end p-0" aria-labelledby="page-header-notifications-dropdown">
                        <div class="p-3">
                            <div class="row align-items-center">
                                <div class="col-12">
                                    <h6 class="m-0">Notificaciones</h6>
                                </div>
                                <div class="col-auto">
                                    @if($fsc->user->admin)
                                        <a href="index.php?page=admin_notificaciones" class="small">Ver todo</a>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="simplebar-content-wrapper" data-simplebar style="min-height: 230px; max-height: 265px; overflow-y: auto;" id="div_notificaciones">
                        </div>

                        <div class="p-2 border-top d-grid">
                            <a href="index.php?page=admin_notificaciones" class="d-flex justify-content-center align-items-center btn btn-link font-size-14 text-center">
                                <i class="mdi mdi-arrow-right-circle me-1"></i>
                                <span>Ver más</span>
                            </a>
                        </div>
                    </div>
                </div>

                <div class="dropdown d-inline-block">
                    <button type="button" class="btn header-item waves-effect" id="page-header-user-dropdown" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="rounded-circle">
                            <i class="fa-solid fa-user-large "></i>
                        </span>
                        <span class="d-none d-xl-inline-block">
                            {!! $fsc->getUserName() !!}
                        </span>
                        <i class="fa-solid fa-chevron-down  d-none d-xl-inline-block"></i>
                    </button>
                    @if (isset($fsc->user))
                        <div class="dropdown-menu dropdown-menu-end">
                            <a id="user_profile_link" href="{!! $fsc->getUserUrl() !!}" class="dropdown-item text-secondary" data-codagente="{!! $fsc->user->codagente !!}">
                                <i class="fa-solid fa-user font-size-16 align-middle me-1"></i>
                                <span>Usuario</span>
                            </a>

                            <div class="dropdown-divider"></div>

                            <a href="{!! $fsc->getLogoutUrl() !!}" class="dropdown-item text-secondary text-danger">
                                <i class="fa-solid fa-power-off font-size-16 align-middle me-1 text-danger"></i>
                                <span>Cerrar sesión</span>
                            </a>
                        </div>
                    @endif
                </div>

                {{-- --}}
                <div class="dropdown d-inline-block">
                    <button type="button" class="btn header-item noti-icon right-bar-toggle waves-effect" aria-label="Ajustes">
                        <i class="fa-solid fa-circle-half-stroke " style="font-size: 1.25rem"></i>
                    </button>
                </div>
            </div>
        </div>
    </header>

    @if (isset($fsc->user))
        <div class="vertical-menu">
            <div data-simplebar class="h-100">

                <div id="sidebar-menu">
                    <ul class="metismenu list-unstyled" id="side-menu">
                        <li class="menu-title">Menu</li>
                        @php
                            $template_page = "index.php?page=";

                        @endphp
                        @if (count($GLOBALS['plugins'])>0)
                            <li>
                                <a href="#" class="has-arrow d-flex" title="Acceso rápido">
                                    <i class="fa-regular fa-star fa-fw"></i>
                                    <span class="w-100">Acceso rápido</span>
                                </a>
                                <ul class="sub-menu" aria-expanded="true">
                                    @php
                                        $menu_ar_vacio = true;
                                    @endphp
                                    @foreach ($fsc->getMenu() as $key1 => $value1)
                                        @if ($value1->important)
                                            <li>
                                                <a href="{!! $value1->url() !!}">
                                                    <span class="text-capitalize">{!! $value1->title !!}</span>
                                                </a>
                                            </li>
                                            @php
                                                $menu_ar_vacio = false;
                                            @endphp
                                        @endif
                                    @endforeach
                                    @if ($menu_ar_vacio)
                                        <li>
                                            <a href="" class="dropdown-toggle" data-bs-toggle="dropdown">
                                                Vacío
                                            </a>
                                        </li>
                                    @endif
                                </ul>
                            </li>
                        @endif
                        @php
                            // Si existe menuHtml(), es que es un controlador nuevo, lo usamos.
                            if (method_exists($fsc, 'menuHtml')) {
                                $menuElements = $fsc->menuHtml();
                            } else {
                                // $menuElements = $fsc->user->get_array_menu();
                                $menuElements = $fsc->getArrayMenu();
                                $menuElements = generate_menu_elements($menuElements,$fsc->page->folder, 1, 1,'');
                            }
                        @endphp
                        {!! $menuElements !!}
                    </ul>
                </div>
            </div>
        </div>
    @endif

    <div class="main-content">
        <div class="page-content">

