<span class="form-text-description">
    Aquí tienes información básica sobre esta instalación de MiFactura.eu,
    base de datos, listado de tablas y el historial de errores con múltiples
    filtros, para facilitarte el trabajo a la hora de encontrar un fallo.
</span>
