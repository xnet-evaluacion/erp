<!-- PhpStorm bug -->
{{--
USO:
@php
    $params = [
        'buttons' => [],
        'type' => 'btn-group/dropdown'
    ];
    $button = [
        'type' => 'a/button',
        'onclick' => '',
        'link' => 'index.php?page=XXX',
        'target' => '_blank',
        'class' => 'secondary ',
        'outline'=>true,
        'icon' => '<i class="fa-solid fa-cog fa-fw"></i>',
        'text' => '<span>Configurar</span>',
        'title' => 'Configurar',
    ];
    $params['buttons'][] = $button;
@endphp
@include('block/table_td_actions', $params)
--}}
@switch ($type)
    @case ('dropdown')
        <div class="dropdown">
            <a href="#" class="dropdown-toggle card-drop" data-bs-toggle="dropdown" aria-expanded="false">
                <i class="mdi mdi-dots-horizontal font-size-18"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-end " style="">
                <div class="d-flex flex-column justify-content-center">
                    @foreach ($buttons as $button)
                        @switch ($button['type'])
                            @case ('button')
                                <button class="btn btn-{!! ($button['outline'] ?? false) == false ? '' : 'outline-' !!}{!! $button['class'] ?? 'secondary' !!} dropdown-item text-secondary"
                                        @isset($button['onclick']) onclick="{!! $button['onclick'] !!}" @endisset
                                        @isset($button['title']) title="{!! $button['title'] !!}" @endisset
                                >
                                    @isset($button['icon'])
                                        {!! $button['icon'] !!}
                                    @endisset
                                    @isset($button['text'])
                                        {!! $button['text'] !!}
                                    @endisset
                                </button>
                                @break

                            @case ('a')
                                <a class="link-{!! $button['class'] ?? 'secondary' !!} dropdown-item text-secondary" href="{!! $button['link'] ?? '#' !!}"
                                   @isset($button['title']) title="{!! $button['title'] !!}" @endisset
                                   @isset($button['target']) target="{!! $button['target'] !!}" @endisset
                                   @isset($button['onclick']) onclick="{!! $button['onclick'] !!}" @endisset
                                >
                                    @isset($button['icon'])
                                        {!! $button['icon'] !!}
                                    @endisset
                                    @isset($button['text'])
                                        {!! $button['text'] !!}
                                    @endisset
                                </a>
                                @break

                            @default
                                <span>No compatible: {!! dump($button) !!}</span>
                        @endswitch
                    @endforeach
                </div>
            </div>
        </div>
        @break

    @case ('btn-group')
    @default
        <div class="d-flex btn-group d-grid text-center" role="group">
            @foreach ($buttons as $button)
                @switch ($button['type'])
                    @case ('button')
                        <button class="btn btn-{!! ($button['outline'] ?? false) == false ? '' : 'outline-' !!}{!! $button['class'] ?? 'secondary' !!}"
                                @isset($button['onclick']) title="{!! $button['onclick'] !!}" @endisset
                                @isset($button['title']) title="{!! $button['title'] !!}" @endisset
                        >
                            @isset($button['icon'])
                                {!! $button['icon'] !!}
                            @endisset
                            @isset($button['text'])
                                {!! $button['text'] !!}
                            @endisset
                        </button>
                        @break

                    @case ('a')
                        <a class="btn btn-{!! ($button['outline'] ?? false)==false? '' : 'outline-' !!}{!! $button['class'] ?? 'secondary' !!}" href="{!! $button['link'] ?? '#' !!}"
                           @isset($button['title']) title="{!! $button['title'] !!}" @endisset
                           @isset($button['target']) target="{!! $button['target'] !!}" @endisset
                           @isset($button['onclick']) onclick="{!! $button['onclick'] !!}" @endisset
                        >
                            @isset($button['icon'])
                                {!! $button['icon'] !!}
                            @endisset
                            @isset($button['text'])
                                {!! $button['text'] !!}
                            @endisset
                        </a>
                        @break

                    @default
                        <span>No compatible: {!! dump($button) !!}</span>
                @endswitch
            @endforeach
        </div>
        @break

@endswitch
