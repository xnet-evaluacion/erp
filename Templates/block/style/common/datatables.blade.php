<!-- PhpStorm bug -->
{{-- @include('block/style/common/datatables') --}}

{{-- LOCAL --}}
{{--<link rel="stylesheet" type="text/css" href="{!! FS_PATH !!}Templates/dist/assets/css/datatables.min.css?idcache={!! $fsc->id_cache !!}"/>
<link rel="stylesheet" type="text/css" href="{!! FS_PATH !!}node_modules/datatables.net-autofill-bs4/css/autoFill.bootstrap4.min.css?idcache={!! $fsc->id_cache !!}"/>
<link rel="stylesheet" type="text/css" href="{!! FS_PATH !!}node_modules/datatables.net-bs4/css/dataTables.bootstrap4.min.css?idcache={!! $fsc->id_cache !!}"/>
<link rel="stylesheet" type="text/css" href="{!! FS_PATH !!}node_modules/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css?idcache={!! $fsc->id_cache !!}"/>
<link rel="stylesheet" type="text/css" href="{!! FS_PATH !!}node_modules/datatables.net-colreorder-bs4/css/colReorder.bootstrap4.min.css?idcache={!! $fsc->id_cache !!}"/>
<link rel="stylesheet" type="text/css" href="{!! FS_PATH !!}node_modules/datatables.net-datetime/dist/dataTables.dateTime.min.css?idcache={!! $fsc->id_cache !!}"/>
<link rel="stylesheet" type="text/css" href="{!! FS_PATH !!}node_modules/datatables.net-fixedheader-bs4/css/fixedHeader.bootstrap4.min.css?idcache={!! $fsc->id_cache !!}"/>
<link rel="stylesheet" type="text/css" href="{!! FS_PATH !!}node_modules/datatables.net-keytable-bs4/css/keyTable.bootstrap4.min.css?idcache={!! $fsc->id_cache !!}"/>
<link rel="stylesheet" type="text/css" href="{!! FS_PATH !!}node_modules/datatables.net-responsive-bs4/css/responsive.bootstrap4.css?idcache={!! $fsc->id_cache !!}"/>
<link rel="stylesheet" type="text/css" href="{!! FS_PATH !!}node_modules/datatables.net-rowgroup-bs4/css/rowGroup.bootstrap4.min.css?idcache={!! $fsc->id_cache !!}"/>
<link rel="stylesheet" type="text/css" href="{!! FS_PATH !!}node_modules/datatables.net-rowreorder-bs4/css/rowReorder.bootstrap4.min.css?idcache={!! $fsc->id_cache !!}"/>
<link rel="stylesheet" type="text/css" href="{!! FS_PATH !!}node_modules/datatables.net-scroller-bs4/css/scroller.bootstrap4.min.css?idcache={!! $fsc->id_cache !!}"/>
<link rel="stylesheet" type="text/css" href="{!! FS_PATH !!}node_modules/datatables.net-select-bs4/css/select.bootstrap4.min.css?idcache={!! $fsc->id_cache !!}"/>
<link rel="stylesheet" type="text/css" href="{!! FS_PATH !!}node_modules/datatables.net-staterestore-bs4/css/stateRestore.bootstrap4.min.css?idcache={!! $fsc->id_cache !!}"/>--}}


{{-- REMOTO: https://cdn.datatables.net/ --}}
<link rel="stylesheet" type="text/css"
      href="https://cdn.datatables.net/v/bs5/jszip-2.5.0/dt-1.11.5/b-2.2.2/b-colvis-2.2.2/b-html5-2.2.2/b-print-2.2.2/cr-1.5.5/date-1.1.2/fc-4.0.2/fh-3.2.2/kt-2.6.4/r-2.2.9/rg-1.1.4/rr-1.2.8/sc-2.0.5/sb-1.3.2/sp-2.0.0/sl-1.3.4/sr-1.1.0/datatables.min.css"/>
<link rel="stylesheet" type="text/css"
      href="{!! FS_PATH !!}Templates/dist/assets/css/datatables.min.css?idcache={!! $fsc->id_cache !!}"/>
