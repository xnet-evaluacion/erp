@extends('layouts/main')

@section('styles')
    @parent
    <style rel="stylesheet">
        div.license {
            width: 100%;
            height: 480px;
            scrolling: "yes";
            overflow-y: auto;
            text-align: justify;
        }
    </style>
@endsection

@section('main-content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card border">
                    <div class="card-header mt-2 text-truncate">
                        <h1 class="fw-light">
                            <i class="fa-solid fa-file-contract fa-fw"></i>
                            {!! $fsc->getTitlePrimary() !!}
                            <br>
                            <small>{!! $fsc->getTitleSecondary() !!}</small>
                        </h1>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12 license">
                                @if (empty($fsc->getLicenseText()))
                                    Sin archivo de licencia en {!! $fsc->getLicenseFilePath() !!}
                                @else
                                    {!! nl2br($fsc->getLicenseText()) !!}
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <form name="f_cluf" id="f_cluf" class="form" action="{!! $fsc->url() !!}" role="form" method="post">
                            <div class="row">
                                <div class="col-sm-8">
                                    <div class="form-check form-switch">
                                        <input class="form-check-input" type="checkbox" role="switch" id="aceptado" name="aceptado" required="required" @if ($fsc->is_cluf_accepted()) checked="" @endif />
                                        <label class="form-check-label" for="aceptado">
                                            He leído y acepto los términos y condiciones del CLUF
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-4 text-end">
                                    <input type="submit" role="button" id="continuar" name="continuar" value="Continuar" class="d-flex justify-content-center align-items-center btn btn-success onclick=" return IsTermChecked();">
                                </div>
                            </div>
                        </form>
                        <div class="row">
                            <div class="col-12 text-center">
                                <p class="form-text">
                                    <copyright>{!! $fsc->getCopyright() !!}</copyright>
                                    <span>{!! $fsc->getReserved() !!}</span>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
