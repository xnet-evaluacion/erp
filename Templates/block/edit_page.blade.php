<form name="edit_page_form" action="{!! $fsc->url() !!}#" method="post" class="form">
    <input type="hidden" name="action" value="edit_page"/>
    <input type="hidden" id="page_name" name="name" value=""/>
    <div class="modal" id="edit_page_modal">
        <div class="modal-dialog" role="document" style="width: 95%; max-width: 950px;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><span class="glyphicon glyphicon-road"></span>
                        &nbsp; Editar página
                    </h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        Alias:
                        <input class="form-control" type="text" id="page_alias" name="alias"/>
                    </div>
                    <div class="form-group">
                        Descripción:
                        <input class="form-control" type="text" id="page_description" name="description"/>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-sm btn-primary" type="submit" onclick="this.disabled=true;this.form.submit();">
                        <span class="glyphicon glyphicon-floppy-disk"></span>&nbsp; Guardar
                    </button>
                </div>
            </div>
        </div>
    </div>
</form>
