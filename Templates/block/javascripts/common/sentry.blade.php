<!-- PhpStorm bug -->
{{--
    @include('block/javascripts/common/sentry')
--}}
@if (USE_BUG_TRACKER)
    <script src="https://browser.sentry-cdn.com/6.19.1/bundle.min.js"
            integrity="sha384-GRagWAKYasaEEyhq5NqRz9Hs7zZOXt+DwzY/WGbWZBkpvt5+lZxITNyU3bD7SFk5"
            crossorigin="anonymous"></script>
    <script src="https://browser.sentry-cdn.com/6.19.1/bundle.tracing.min.js"
            integrity="sha384-hthbsqa5mLHmR+ZUw29J9qGt8SdDRD7AVpG02i/NAsu5cv9G9I9D9IhEtSVyoPv2"
            crossorigin="anonymous"></script>
    <script type="text/javascript">
        Sentry.init({
            dsn: "http://4cf0ab99e80645d4b3398ba12b6b78ff@sentry.x-netdigital.com:9000/3",
            integrations: [new Sentry.BrowserTracing()],
            tracesSampleRate: 1.0,
            environment: ({!! (int)FS_DEBUG !!} ? 'production' : 'development')
        });
    </script>
@endif
