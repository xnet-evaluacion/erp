<script src="{!! FS_PATH !!}Templates/js/chart.bundle.min.js?idcache={!! $fsc->id_cache !!}"></script>

<script type="text/javascript">

    function generar_chart_column(xAxisChart, seriesData, idChart, nameChart) {
        let options = {
            series: [{
                data: seriesData
            }],
            chart: {
                height: 350,
                type: 'bar',
                events: {
                    click: function (chart, w, e) {
                        // console.log(chart, w, e)
                    }
                }
            },
            title: {
                text: nameChart
            },
            plotOptions: {
                bar: {
                    columnWidth: '45%',
                    distributed: true,
                }
            },
            dataLabels: {
                enabled: false
            },
            legend: {
                show: false
            },
            xaxis: {
                categories: xAxisChart
            }
        };

        var chart = new ApexCharts(document.querySelector("#" + idChart), options);
        chart.render();
    }

    function generar_chart_pie(seriesLabel, seriesData, idChart, nameChart, chartWidth) {
        console.log(seriesLabel, seriesData, idChart, nameChart, chartWidth);
        nameChart = (nameChart === undefined) ? 'Sin título' : nameChart;
        chartWidth = (chartWidth === undefined) ? 520 : chartWidth;
        let errorChecker = check_data_chart(seriesLabel, seriesData, idChart, 'Pie');
        if (!errorChecker) {
            let options = {
                series: seriesData,
                chart: {
                    width: chartWidth,
                    height: 350,
                    type: 'pie',
                },
                colors: ['#008ffb', '#f46a6a', '#00e396', '#feb019', '#775dd0'],
                labels: seriesLabel,
                title: {
                    text: nameChart
                },
                responsive: [{
                    breakpoint: 576,
                    options: {
                        chart: {
                            height: 300,
                            width: 300
                        }
                    }
                }],
                legend: {
                    position: 'bottom',
                    height: 50,
                },

            };

            let chart = new ApexCharts(document.querySelector("#" + idChart), options);
            chart.render();
        }
    }

    function generar_chart_semi_donut(seriesLabel, seriesData, idChart, nameChart, chartWidth) {
        nameChart = (nameChart === undefined) ? 'Sin título' : nameChart;
        chartWidth = (chartWidth === undefined) ? 200 : chartWidth;
        let options = {
            series: seriesData,
            chart: {
                type: 'donut',
            },
            labels: seriesLabel,
            colors: ['#008ffb', '#f46a6a', '#00e396', '#feb019', '#775dd0'],
            plotOptions: {
                pie: {
                    startAngle: -90,
                    endAngle: 90,
                    offsetY: 10
                }
            },
            grid: {
                padding: {
                    bottom: -80
                }
            },
            responsive: [{
                breakpoint: 480,
                options: {
                    chart: {
                        width: chartWidth
                    },
                    legend: {
                        position: 'bottom'
                    }
                }
            }]
        };

        var chart = new ApexCharts(document.querySelector("#" + idChart), options);
        chart.render();
    }


    function generar_chart_line(seriesData, idChart, nameChart, xAxisChart) {
        nameChart = (nameChart === undefined) ? 'Sin título' : nameChart;
        let errorChecker = check_data_chart(seriesData, idChart, 'Line');
        if (!errorChecker) {
            let options = {
                series: seriesData,
                chart: {
                    height: 350,
                    type: 'area',
                    zoom: {
                        enabled: false
                    }
                },
                colors: ['#f46a6a', '#008ffb', '#00e396', '#feb019', '#775dd0'],
                dataLabels: {
                    enabled: false,
                },
                stroke: {
                    curve: 'smooth',
                    width: 0.3
                },
                xaxis: {
                    categories: xAxisChart
                },
                title: {
                    text: nameChart,
                    align: 'left'
                },

            };

            let chart = new ApexCharts(document.querySelector("#" + idChart), options);
            chart.render();
        }
    }
    function generar_chart_radial_bar(seriesLabel, seriesData, idChart, extraOption) {
        extraOption = extraOption || {};
        let errorChecker = check_data_chart(seriesData, idChart, 'radialBar');
        if (!errorChecker) {
            let total = seriesData.reduce((acc, field) => acc + (parseInt(field) || 0), 0);
            let max = seriesData.reduce((acc, field) => acc > (parseInt(field) || 0)?acc: (parseInt(field) || 0), 0);
            let seriesDataProcess =seriesData.map(function(v){ return parseFloat(((parseFloat(v)*100)/total).toFixed(2));})

            let options = {
                chart: { 
                    height: 370, 
                    type: "radialBar" 
                },
                plotOptions: {
                    radialBar: {
                        dataLabels: {
                            name: { fontSize: "22px" },
                            value: { fontSize: "16px" },
                            total: {
                                show: true,
                                label: "Total",
                                formatter: function (e,v) {
                                    return total + " ("+max+")";
                                },
                            },
                            value: {
                                show: true,
                                formatter: (seriesName, opts,v) => {
                                    var totalSerie = 0;
                                    seriesDataProcess.map(function(value,key){ 
                                        if(seriesName == value){
                                            totalSerie = (parseFloat(seriesData[key])||0);
                                        }
                                    })
                                    return ((totalSerie*100)/total).toFixed(2)+"% ("+totalSerie+")";
                                },
                            },
                        },
                    },
                },
                series: seriesDataProcess,
                labels: seriesLabel,
                colors: ['#556ee6', '#34c38f', '#f46a6a', '#f1b44c'],
            };
            let chart = new ApexCharts(document.querySelector("#" + idChart), $.extend(true,{},options,extraOption));
            chart.render();
        }
    }
    function generar_chart_bar(xAxisChart, seriesData, idChart, nameChart,extraOption) {
        extraOption = extraOption || {};
        let errorChecker = check_data_chart(seriesData, idChart, 'bar');
        if (!errorChecker) {
            let options = {
                chart: { height: 350, type: "bar", toolbar: { show: !1 } },
                plotOptions: { bar: { horizontal: !1, columnWidth: "45%", endingShape: "rounded" } },
                dataLabels: { enabled: !1 },
                stroke: { show: !0, width: 2, colors: ["transparent"] },
                series: seriesData,
                xaxis: { categories: xAxisChart },
                fill: { opacity: 1 },
            };
            var chart = new ApexCharts(document.querySelector("#" + idChart), $.extend(true,{},options,extraOption));
            chart.render();
        }
    }
    
    function generar_chart_datetime(type,seriesData, idContainerChart,extraOption) {
        extraOption = extraOption || {};
        let errorChecker = check_data_chart(seriesData, idContainerChart, 'bar');
        let hoy = moment().format('YYYY-MM-DD');
        if (errorChecker) { 
            return false;
        }
        let $divChar = $('#' + idContainerChart);
        var options = {
            series: seriesData,
            chart: {
                type: type,
                height: 350,
                zoom: {
                    autoScaleYaxis: true
                }
            },
            annotations: {
                xaxis: [{
                    x: moment(hoy).valueOf(),
                    borderColor: '#999',
                    yAxisIndex: 0,
                    label: {
                        show: true,
                        text: 'Hoy',
                        style: {
                            color: "#fff",
                            background: '#775DD0'
                        }
                    }
                }]
            },
            dataLabels: {
                enabled: false
            },
            markers: {
                size: 0,
                style: 'hollow',
            },
            xaxis: {
                type: 'datetime',
                tickAmount: 6,
            },
            tooltip: {
                x: {
                    format: 'dd-MMM-yyyy'
                }
            },
        };
        
        var chart = new ApexCharts($divChar.find(".chart-timeline").get(0),  $.extend(true,{},options,extraOption));
        chart.render();
        
        
        var resetCssClasses = function(activeEl) {
            $divChar.find('button').removeClass('active');
            $(activeEl.target).addClass('active');
        }

        $divChar.find('.ten_days').click(function (e) {
            resetCssClasses(e)
            chart.zoomX(
                moment(hoy).add(-5, 'days').valueOf(),
                moment(hoy).add(5, 'days').valueOf()
            )
        });
        $divChar.find('.one_month').click(function (e) {
            resetCssClasses(e)
            chart.zoomX(
                moment(hoy).add(-0.5, 'months').valueOf(),
                moment(hoy).add(0.5, 'months').valueOf()
            )
        });

        $divChar.find('.six_months').click(function (e) {
            resetCssClasses(e)
            chart.zoomX(
                moment(hoy).add(-3, 'months').valueOf(),
                moment(hoy).add(3, 'months').valueOf()
            )
        });
        $divChar.find('.one_year').click(function (e) {
            resetCssClasses(e)
            chart.zoomX(
                moment(hoy).startOf('year').valueOf(),
                moment(hoy).endOf('year').valueOf()
            )
        });
    }

    function check_data_chart(seriesLabel, seriesData, idChart, typeChart) {
        let errorChecker = false;
        typeChart = (typeChart === undefined) ? 'No definido' : typeChart;
        if (seriesLabel === undefined) {
            bootbox.alert({
                message: 'No se han indicado los nombres de datos para la gráfica de tipo : ' + typeChart,
                title: '<b>ERROR</b>',
                size: 'large'
            });
            errorChecker = true;

        } else if (seriesData === undefined) {
            bootbox.alert({
                message: 'No se han indicado los datos para la gráfica de tipo : ' + typeChart,
                title: '<b>ERROR</b>',
                size: 'large'
            });
            errorChecker = true;
        } else if (idChart === undefined) {
            bootbox.alert({
                message: 'No se ha indicado la referencia para la gráfica de tipo : ' + typeChart,
                title: '<b>ERROR</b>',
                size: 'large'
            });
            errorChecker = true;
        }
        return errorChecker;
    }


</script>
