@if ($fsc->model)
    <script type="text/javascript">
        $(document).ready(function () {
            $("#btn_delete_model").click(function (event) {
                event.preventDefault();
                bootbox.confirm({
                    message: '¿Realmente desea eliminar? Esta acción no se puede deshacer',
                    title: '<b>Atención</b>',
                    callback: function (result) {
                        if (result) {
                            window.location.href = '{!! $fsc->model->url() !!}&action=delete';
                        }
                    }
                });
            });
        });
    </script>
@endif