<script type="text/javascript">
    function confirmDeleteRecord(url) {
        bootbox.confirm({
            title: "<b>Confirme el borrado</b>",
            message: "Está seguro de querer eliminar el registro",
            buttons: {
                cancel: {
                    label: '<i class="fa fa-times"></i> Cancelar'
                },
                confirm: {
                    label: '<i class="fa fa-check"></i> Confirmar'
                }
            },
            callback: function (result) {
                console.log(result)
                if (result) {
                    $(location).attr('href', url);
                }
            }
        });
    }
</script>