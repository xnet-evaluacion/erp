<script type="text/javascript">
    $(document).ready(function () {
        if (window.location.hash.substring(1) == 'nuevo') {
            $("#modal_nuevo_usuario").modal('show');
            document.f_nuevo_usuario.nnick.focus();
        } else if (window.location.hash.substring(1) == 'roles') {
            $('#tab_usuarios a[href="#roles"]').tab('show');
        } else if (window.location.hash.substring(1) == 'nuevorol') {
            $('#tab_usuarios a[href="#roles"]').tab('show');
            $("#modal_nuevo_rol").modal('show');
            document.f_nuevo_rol.descripcion.focus();
        }
        $("#b_nuevo_usuario").click(function (event) {
            event.preventDefault();
            $("#modal_nuevo_usuario").modal('show');
            document.f_nuevo_usuario.nnick.focus();
        });
        $("#b_nuevo_rol").click(function (event) {
            event.preventDefault();
            $("#modal_nuevo_rol").modal('show');
            document.f_nuevo_rol.nrol.focus();
        });
    });
</script>
