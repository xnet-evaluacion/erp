@if ($fsc->rol)
    <script type="text/javascript">
        function check_allow_delete(counter) {
            if ($("#enabled_" + counter).is(':checked')) {
                $("#allow_delete_" + counter).prop('checked', true);
            } else {
                $("#allow_delete_" + counter).prop('checked', false);
            }
        }

        $(document).ready(function () {
            $("#b_eliminar_rol").click(function (event) {
                event.preventDefault();
                bootbox.confirm({
                    message: '¿Realmente desea eliminar el rol {!! $fsc->rol->codrol !!}?',
                    title: '<b>Atención</b>',
                    callback: function (result) {
                        if (result) {
                            window.location.href = "index.php?page=admin_users&delete_rol=" + encodeURIComponent("{!! $fsc->rol->codrol !!}") + "#roles";
                        }
                    }
                });
            });
            $('#marcar_todo_ver').change(function () {
                var checked = $(this).prop('checked');
                $("#f_rol_pages input[name='enabled[]']").prop('checked', checked);
            });
            $('#marcar_todo_eliminar').change(function () {
                var checked = $(this).prop('checked');
                $("#f_rol_pages input[name='allow_delete[]']").prop('checked', checked);
            });
        });
    </script>
@endif
