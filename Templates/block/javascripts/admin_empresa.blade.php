<script type="text/javascript">
    function comprobar_url() {
        switch (window.location.hash.substring(1)) {
            case 'personalizacion':
                mostrar_seccion('personalizacion');
                break;
            case 'email':
                mostrar_seccion('email');
                break;
            default:
                mostrar_seccion();
                break;
        }
    }

    function mostrar_seccion(id) {
        $("#panel_generales").hide();
        $("#panel_personalizacion").hide();
        $("#panel_email").hide();

        $("#b_generales").removeClass('active');
        $("#b_personalizacion").removeClass('active');
        $("#b_email").removeClass('active');

        switch (id) {
            case 'personalizacion':
                $("#panel_personalizacion").show();
                $("#b_personalizacion").addClass('active');
                break;
            case 'email':
                $("#panel_email").show();
                $("#b_email").addClass('active');
                document.f_empresa.email.focus();
                break;
            default:
                $("#panel_generales").show();
                $("#b_generales").addClass('active');
                document.f_empresa.nombre.focus();
                break;
        }
    }

    $(document).ready(function () {
        comprobar_url();
        window.onpopstate = function () {
            comprobar_url();
        };
    });
</script>