<!-- PhpStorm bug -->
{{--
Ejecuta un cronjob al visitar la página.

USO:
@include('block/extensions/minicrons', ['page_params' => ''])
--}}
@foreach ($fsc->extensions as $key1 => $extension)
    @if ($extension->type=='minicron')
        <iframe src="index.php?page={!! $extension->from !!}{!! $extension->params !!}{!! $page_params ?? '' !!}&hide_title_iframe=true&hide_footer_iframe=true" style="display: none;"></iframe>
    @endif
@endforeach
