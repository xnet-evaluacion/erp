<!-- PhpStorm bug -->
{{--
Esto se utiliza sólo en ventas_articulo.

TODO: Creada extensión "as-is", debería revisarse si esto se utiliza o puede ser eliminado.

USO:
@include('block/extensions/tipo_art')
--}}
@foreach ($fsc->extensions as $key1 => $extension)
    @if ($extension->type=='tipo_art')
        @if ($fsc->articulo->tipo==$extension->params)
            <option value="{!! $extension->params !!}" selected="">{!! $extension->text !!}</option>
        @else
            <option value="{!! $extension->params !!}">{!! $extension->text !!}</option>
        @endif
    @endif
@endforeach
