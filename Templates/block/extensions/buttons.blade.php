<!-- PhpStorm bug -->
{{--
Extensión para cargar botones en páginas.
Generalmente estos botones se encuentran en la parte superior izquierda.

USO:
@include('block/extensions/buttons', ['page_params' => ''])
--}}
@foreach ($fsc->extensions as $key1 => $extension)
    @if ($extension->type=='button')
        <a href="index.php?page={!! $extension->from !!}{!! $page_params ?? '' !!}{!! $extension->params !!}" class="d-flex justify-content-center align-items-center btn btn-outline-secondary"
            {{--
            tabindex="0"
            data-bs-container="body" data-bs-toggle="popover" data-bs-trigger="hover" data-bs-placement="bottom" data-bs-content="{!! strip_tags($extension->text) !!}"
            --}}
        >
            {!! $extension->text !!}
        </a>
    @endif
@endforeach
