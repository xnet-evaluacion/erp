<!-- PhpStorm bug -->
{{--
Añade un botón para enviar el PDF de impresión.

USO:
@include('block/extensions/emails', ['page_params' => ''])
--}}
@foreach ($fsc->extensions as $key1 => $extension)
    @if ($extension->type=='email')
        <button class="btn btn-primary flex-grow-1 flex-sm-grow-0" type="submit" onclick="this.disabled=true;this.form.action='index.php?page={!! $extension->from !!}{!! $extension->params !!}{!! $page_params ?? '' !!}';this.form.submit();">
            <i class="fa-solid fa-paper-plane fa-fw"></i>
            {!! $extension->text !!}
        </button>
    @endif
@endforeach
