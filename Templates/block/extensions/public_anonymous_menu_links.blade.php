<!-- PhpStorm bug -->
{{--
Opciones de menú que ve el "cliente" cuando no está identificado.

USO:
@include('block/extensions/public_anonymous_menu_links', ['page_params' => ''])
--}}
@foreach ($fsc->extensions as $key1 => $extension)
    @if ($extension->type=='anonymous_menu_link')
        <li class="nav-item @if ($extension->from==$fsc->class_name) active @endif ">
            <a href="index.php?page={!! $extension->from !!}" class="nav-link d-flex flex-column align-items-center">
                {!! $extension->text !!}
                <span class="sr-only">(current)</span></a>
        </li>
    @endif
@endforeach
