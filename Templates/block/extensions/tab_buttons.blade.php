<!-- PhpStorm bug -->
{{--
Esto se utiliza sólo en ventas_articulo.

TODO: Creada extensión "as-is", en su lugar sería más coherente una extensión para generar dropdowns y que esta se incluya con los botones de acciones.

USO:
@include('block/extensions/tab_buttons', ['page_params' => ''])
--}}
@foreach ($fsc->extensions as $key1 => $extension)
    @if ($extension->type=='tab_button')
        <li>
            <a href="index.php?page={!! $extension->from !!}{!! $page_params ?? '' !!}{!! $extension->params !!}" class="dropdown-item text-secondary">
                {!! $extension->text !!}
            </a>
        </li>
    @endif
@endforeach
