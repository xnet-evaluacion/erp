<!-- PhpStorm bug -->
{{--
Añade una pestaña visible para agrupar contenido.

USO:
@include('block/extensions/tabs', ['page_params' => ''])
--}}
@foreach ($fsc->extensions as $key1 => $extension)
    @if ($extension->type=='tab')
        @php
            $show_param = '';
            if (isset($fsc->mostrar) && $fsc->mostrar=='ext_' . $extension->name) {
                $show_param = '&mostrar=ext_' . $extension->name;
            }
        @endphp
        <li class="nav-item" role="presentation">
            <a id="tab_{!! $fsc->page->name !!}_ext_{!! $extension->name !!}" href="#ext_{!! $extension->name !!}{!! $show_param !!}" aria-controls="ext_{!! $extension->name !!}" role="tab" data-bs-toggle="tab"
               class="nav-link d-flex flex-column align-items-center text-start @if (isset($fsc->mostrar) && $fsc->mostrar=='ext_'.$extension->name) active @endif ">
                {!! $extension->text !!}
            </a>
        </li>
    @endif
@endforeach
