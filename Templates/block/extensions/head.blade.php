<!-- PhpStorm bug -->
{{--
Esta extensión permite cargar elementos en la cabecera.

TODO: En su lugar sería más oportuno derivarlo a 2 tipos de extensiones nuevas para "css" y para "javascript"

USO:
@include('block/extensions/head')
--}}
@foreach ($fsc->extensions as $key1 => $extension)
    @if ($extension->type=='head')
        {!! $extension->text !!}

        <script type="text/javascript">
            {{-- TODO: Avisar de esto mediante bootbox --}}
            console.table({!! json_encode($extension) !!});
        </script>
    @endif
@endforeach
