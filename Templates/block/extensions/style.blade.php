<!-- PhpStorm bug -->
{{--
Esta extensión permite cargar elementos CSS.

USO:
@include('block/extensions/style')
--}}
@foreach ($fsc->extensions as $key1 => $extension)
    @if ($extension->type === 'style')
        {!! $extension->text !!}
    @endif
@endforeach
