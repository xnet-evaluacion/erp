<!-- PhpStorm bug -->
{{--
Extensión para añadir ventanas modales en páginas.

USO:
@include('block/extensions/modals', ['page_params' => ''])
--}}
@foreach ($fsc->extensions as $key1 => $extension)
    @if ($extension->type=='modal')
        <a class="d-flex justify-content-center align-items-center btn btn-outline-secondary" onclick="fs_modal('{!! base64_encode($extension->text) !!}','{!! 'index.php?page=' . $extension->from . ($page_params ?? '') . $extension->params !!}')">
            {!! $extension->text !!}
        </a>
    @endif
@endforeach
