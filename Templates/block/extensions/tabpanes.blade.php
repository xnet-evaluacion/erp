<!-- PhpStorm bug -->
{{--
Añade el contenido de una pestaña para agrupar contenido.
Este contenido está gestionado por un controlador independendiente.

USO:
@include('block/extensions/tabpanes', ['page_params' => ''])
--}}
@foreach ($fsc->extensions as $key1 => $extension)
    @if ($extension->type=='tab')
        <div role="tabpanel" class="tab-pane fade" id="ext_{!! $extension->name !!}" aria-labelledby="ext_{!! $extension->name !!}-tab">
            <iframe src="index.php?page={!! $extension->from !!}{!! $extension->params !!}{!! $page_params ?? '' !!}&hide_title_iframe=true&hide_footer_iframe=true" style="width: 100%;" frameborder="0"></iframe>
        </div>
    @endif
@endforeach
