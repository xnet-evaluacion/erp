<!-- PhpStorm bug -->
{{--
Esta extensión se utiliza únicamente para generar datos mediante el FSDK.

USO:
@include('block/extensions/generates', ['page_params' => ''])
--}}
@foreach ($fsc->extensions as $key1 => $extension)
    @if ($extension->type=='generate')
        <div class="col-sm-4">
            <a href="index.php?page={!! $extension->from !!}{!! $extension->params !!}" class="btn d-flex justify-content-center align-items-center btn-outline-secondary">
                {!! $extension->text !!}
            </a>
        </div>
    @endif
@endforeach
