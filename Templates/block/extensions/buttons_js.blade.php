<!-- PhpStorm bug -->
{{--
Extensión para cargar botonesen páginas con acciones en JS.

USO:
@include('block/extensions/buttons_js')
--}}
@foreach ($fsc->extensions as $key1 => $extension)
    @if ($extension->type=='btn_javascript')
        <button class="d-flex justify-content-center align-items-center btn btn-outline-secondary" type="button" onclick="{!! $extension->params !!}">
            <i class="fa-solid fa-exclamation fa-fw"></i>
            {!! $extension->text !!}
        </button>
    @endif
@endforeach
