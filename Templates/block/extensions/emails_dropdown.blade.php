<!-- PhpStorm bug -->
{{--
Añade un botón para generar el PDF de impresión en el formato escogido desde el desplegable.

USO:
@include('block/extensions/emails_dropdown', ['page_params' => ''])
--}}
@foreach ($fsc->extensions as $key1 => $extension)
    @if ($extension->type=='email')
        <li>
            <a onclick="this.disabled=true;enviar_email('index.php?page={!! $extension->from !!}{!! $extension->params !!}{!! $page_params ?? '' !!}');" class="dropdown-item text-secondary">
                {!! $extension->text !!}
            </a>
        </li>
    @endif
@endforeach
