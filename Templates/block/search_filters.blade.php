<!-- PhpStorm bug -->
{{--
USO:
@php
    $params = [
        'show_filter' => true,
        'show_order' => true,
    ];
@endphp
@include('block/search_filters', $params)
--}}

@if (view_exists('block/search_filters/' . $fsc->page->name))
    @php
        $show_filter = $show_filter ?? true;
        $show_order = $show_order ?? true;
    @endphp
    <div class="row">
        <div class="col-12">
            <div class="accordion" id="searchFilters">
                <div class="accordion-item">
                    <h2 class="accordion-header" id="searchHeading">
                        <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseSearchFilters" aria-expanded="true" aria-controls="collapseSearchFilters">
                            <i class="fa-solid fa-filter fa-fw"></i>
                            <span class="text-nowrap">Filtros de búsqueda</span>
                        </button>
                    </h2>
                    <div id="collapseSearchFilters" class="accordion-collapse collapse show" aria-labelledby="searchHeading" data-bs-parent="#searchFilters">
                        <div class="accordion-body">
                            @include('block/search_filters/' . $fsc->page->name)
                            @if ($show_order == true || $show_filter == true)
                                <div class="row mt-3">
                                    <div class="col-sm-9">
                                        <div class="row justify-content-start">
                                            <div class="col-sm-8 col-md-6 col-xl-3">
                                                @if ($show_order == true)
                                                    @if (method_exists($fsc, 'orden'))
                                                        <select name="order" class="form-select select2 input-sm" onchange="this.form.submit()">
                                                            @foreach ($fsc->orden() as $key_orden => $orden)
                                                                <option value="{!! $key_orden !!}" @if ($fsc->order_key==$key_orden) selected="" @endif >
                                                                    {!! $orden['icono'] !!}
                                                                    {!! $orden['texto'] !!}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    @else
                                                        <span class="text-danger">Falta función orden en el controlador</span>
                                                        <script type="text/javascript">
                                                            throw new Error('La página {!! $fsc->page->name !!} no tiene la función orden.');
                                                        </script>
                                                    @endif
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-3 d-sm-flex justify-content-end mt-2 mt-sm-0">
                                        <div>
                                            @if ($show_filter == true)
                                                <button class="d-flex justify-content-center align-items-center btn btn-primary d-sm-flex text-nowrap w-100" type="submit" aria-label="Filtrar">
                                                    <i class="fa-solid fa-filter fa-fw"></i>
                                                    <span>Filtrar</span>
                                                </button>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif
