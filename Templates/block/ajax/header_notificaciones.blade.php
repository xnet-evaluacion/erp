@forelse ($fsc->resultados as $item)
    <div class="notification-item text-reset">
        <div class="d-flex py-0 px-2">
            <a href="index.php?page=admin_notificacion&action=detalle&id={!! $item->id !!}" class="d-flex flex-grow-1 p-0">
                <div class="d-flex flex-column justify-content-center align-items-center p-0">
                    <div>
                        <span class="{!! $fsc->tipos_notificaciones[$item->idtipo_notificacion]['estilo'] !!}">
                            <i class="{!! $fsc->tipos_notificaciones[$item->idtipo_notificacion]['icono'] !!} fa-2x"></i>
                        </span>
                    </div>
                </div>
                <div class="d-flex flex-grow-1 p-0 justify-content-between">
                    <div class="d-flex flex-column">
                        <h6 class="mb-1">
                            {!! $item->get_mensaje_resume() !!}
                        </h6>
                        <div class="font-size-12 text-muted">
                            <p class="mb-1">
                                {!! $item->nombempleado !!}
                            </p>
                            <p class="mb-0">
                                <i class="mdi mdi-clock-outline"></i>
                                <span class="moment-from-now">
                                    {!! $item->get_tiempo_transcurrido() !!}
                                </span>
                            </p>
                        </div>
                    </div>
                </div>
            </a>
            @if(!empty($item->enlace))
                <div class="d-flex flex-column justify-content-center align-items-center p-0">
                    <span>
                        <a href="index.php?page=admin_notificacion&action=enlace&id={!! $item->id !!}">
                            <i class="fa-xl fa-fw fa-regular fa-circle-right text-primary"></i>
                        </a>
                    </span>
                </div>
            @endif
        </div>
    </div>
@empty
    <div class="notification-item text-reset">
        <div class="d-flex py-0 px-2">
            <a href="#" class="d-flex flex-grow-1 p-0">
                <div class="d-flex flex-column justify-content-center align-items-center p-0">
                    <div>
                        <span class="text-success">
                            <i class="fa-solid fa-broom fa-fw fa-2x"></i>
                        </span>
                    </div>
                </div>
                <div class="d-flex flex-grow-1 p-0 justify-content-between">
                    <div class="d-flex flex-column">
                        <h6 class="mb-1">
                            Ninguna notificación pendiente.
                        </h6>
                    </div>
                </div>
            </a>
        </div>
    </div>
@endforelse
