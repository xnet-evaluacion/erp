<div class="tab-pane fade show active" id="v-pills-generales" role="tabpanel" aria-labelledby="v-pills-generales-tab">
    <div class="row">
        <div class="col-12">
            <div class="card border">
                <div class="card-header mt-2 text-truncate">
                    Datos generales
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-4 mb-2">
                            <label>
                                Nombre
                            </label>
                            <input class="form-control" type="text" name="nombre" value="{!! $fsc->empresa->nombre !!}" autocomplete="off" autofocus/>
                        </div>
                        <div class="col-sm-4 mb-2">
                            <label>
                                Nombre corto
                            </label>
                            <input class="form-control" type="text" name="nombrecorto" value="{!! $fsc->empresa->nombrecorto !!}" autocomplete="off"/>
                        </div>
                        <div class="col-sm-4 mb-2">
                            <label>
                                Web
                            </label>
                            <input class="form-control" type="text" name="web" value="{!! $fsc->empresa->web !!}" autocomplete="off"/>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="row">
                        <div class="col-12 text-end">
                            <button class="btn btn-primary flex-grow-1 flex-sm-grow-0" type="submit" onclick="this.disabled=true;this.form.submit();">
                                <i class="fa-solid fa-save fa-fw"></i>
                                <span>Guardar</span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
