@if ($fsc->allow_delete)
    <a href="#" id="b_eliminar_usuario" class="btn btn-danger flex-grow-1 flex-sm-grow-0">
        <i class="fa-solid fa-trash-can fa-fw"></i>
        <span><span class="d-none d-sm-inline d-sm-none d-md-inline">Eliminar</span></span>
    </a>
@endif
@if ($fsc->suser->enabled)
    <a href="{!! $fsc->url() !!}&senabled=FALSE" id="b_desactivar_usuario" class="d-flex justify-content-center align-items-center btn btn-warning">
        <i class="fa-solid fa-lock fa-fw"></i>
        <span>Desactivar</span>
    </a>
@else
    <a href="{!! $fsc->url() !!}&senabled=TRUE" id="b_activar_usuario" class="d-flex justify-content-center align-items-center btn btn-outline-secondary">
        <i class="fa-solid fa-check-square fa-fw"></i>
        <span>Activar</span>
    </a>
@endif
<button class="btn btn-primary flex-grow-1 flex-sm-grow-0" type="submit" onclick="this.disabled=true;this.form.submit();">
    <i class="fa-solid fa-save fa-fw"></i>
    <span>Guardar</span>
</button>
