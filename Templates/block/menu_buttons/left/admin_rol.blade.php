<div class="d-flex btn-group" role="group">
    <a href="index.php?page=admin_users#roles" class="d-flex justify-content-center align-items-center btn btn-outline-secondary">
        <i class="fa-solid fa-arrow-left fa-fw"></i>
        <span>Roles</span>
    </a>
    @include('block/menu_buttons/refresh')
</div>

<div class="d-flex btn-group" role="group">
    @include('block/extensions/buttons', ['page_params' => '&codrol=' . $fsc->rol->codrol])
</div>

<a href="index.php?page=admin_users#nuevorol" class="d-flex justify-content-center align-items-center btn btn-success" title="Nuevo rol">
    <i class="fa-solid fa-plus fa-fw"></i>
</a>
