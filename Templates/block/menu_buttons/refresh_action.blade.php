@if (method_exists($fsc, 'url'))
    <button class="d-flex justify-content-center align-items-center btn btn-outline-secondary waves-effect waves-light" type="submit" name="action" value="Refresh" title="Recargar la página" aria-label="Recargar la página">
        <i class="fa-solid fa-sync fa-fw"></i>
    </button>
@endif
