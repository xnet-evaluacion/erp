@if (method_exists($fsc, 'url'))
    <a href="{!! $fsc->url() !!}" class="d-flex justify-content-center align-items-center btn btn-outline-secondary waves-effect waves-light" title="Recargar la página" aria-label="Recargar la página">
        <i class="fa-solid fa-sync fa-fw"></i>
    </a>
@endif
