@if (method_exists($fsc, 'url'))
    @php
        $title = 'Marcar como página de inicio';
        $class = '';
        $default_page = 'TRUE';
        $default_icon = 'fa-regular fa-bookmark fa-fw';
        if ($fsc->page->is_default()){
            $title = 'Desmarcar como página de inicio';
            $class = 'active';
            $default_page = 'FALSE';
            $default_icon = 'fa-solid fa-bookmark fa-fw';
        }
    @endphp

    <a href="{!! $fsc->url() !!}&default_page={!! $default_page !!}"
       class="d-flex justify-content-center align-items-center btn btn-outline-secondary {!! $class !!}"
       title="{!! $title !!}"
       aria-label="{!! $title !!}">
        <i class="{!! $default_icon !!}"></i>
    </a>
@endif
