<div class="tab-pane fade" id="v-pills-personalizacion" role="tabpanel" aria-labelledby="v-pills-personalizacion-tab">
    <div class="row">
        <div class="col-12">
            <div class="card border">
                <div class="card-header mt-2 text-truncate">
                    <i class="fa-solid fa-images fa-fw"></i>
                    Personalización
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12 col-md-4 col-lg-5 col-xl-4">
                            <div class="mb-2">
                                <label>
                                    Tipo de formulario de acceso
                                </label>
                                <select name="plantilla_login" class="form-select select2">
                                    <option data-comment="Placeholder"></option>
                                    @foreach ($fsc->plantillas as $key1 => $value1)
                                        @if ($fsc->empresa->plantilla_login==$value1->plantilla_login)
                                            <option value="{!! $value1->plantilla_login !!}" selected="">{!! $value1->nombre !!}</option>
                                        @else
                                            <option value="{!! $value1->plantilla_login !!}">{!! $value1->nombre !!}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-6 col-xl-5">
                            <div class="mb-2">
                                <label>
                                    Imagen de fondo
                                </label>
                                <div class="mb-2">
                                    @php
                                        $dashboardgb = FS_MYDOCS.'images/dashboardbg.jpg';
                                    @endphp
                                    <input class="form-control" id="dashboardbg" name="dashboardbg" type="file" accept="image/jpeg"/>
                                    @if (file_exists($dashboardgb))
                                        <img class="img-thumbnail img-fluid" src="{!! $dashboardgb !!}" style="width: 100px;" alt="Fondo">
                                        <a href="{!! $fsc->url() !!}&action=delete_dashboardbg" class="btn btn-danger flex-grow-1 flex-sm-grow-0" title="Eliminar fondo">
                                            <i class="fa-solid fa-trash-can fa-fw"></i>
                                            <span>Eliminar fondo</span>
                                        </a>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="row">
                        <div class="col-12">
                            <div class="d-flex justify-content-end">
                                <button class="btn btn-primary flex-grow-1 flex-sm-grow-0" type="submit" onclick="this.disabled = true; this.form.submit();">
                                    <i class="fa-solid fa-save fa-fw"></i>
                                    <span>Guardar</span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
