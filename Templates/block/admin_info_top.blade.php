@include('master/template_header')

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card border">
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <div class="table-responsive">
                                <table id="table_admin_info_environtment" class="table {{-- table-datatable --}} table-borderless table-striped table-hover w-100">
                                    <thead class="table-dark">
                                    <tr>
                                        <th class="text-start">MiFactura.eu</th>
                                        <th class="text-center">PHP</th>
                                        <th class="text-center">Base de datos</th>
                                        <th class="text-center">Motor de base de datos</th>
                                        <th class="text-center">Comunidad</th>
                                        <th class="text-end">Caché</th>
                                    </tr>
                                    </thead>
                                    <tr>
                                        <td class="text-start text-nowrap text-truncate">
                                            <a href="{!! FS_COMMUNITY_URL !!}/index.php?page=community_changelog&version={!! $fsc->version() !!}" target="_blank" class="link-primary">{!! $fsc->version() !!}</a>
                                        </td>
                                        <td class="text-center text-nowrap text-truncate">{!! $fsc->php_version() !!}</td>
                                        <td class="text-center text-nowrap text-truncate">{!! $fsc->fs_db_name() !!}</td>
                                        <td class="text-center text-nowrap text-truncate">{!! $fsc->fs_db_version() !!}</td>
                                        <td class="text-center text-nowrap text-truncate">{!! FS_COMMUNITY_URL !!}</td>
                                        <td class="text-end text-nowrap text-truncate">{!! $fsc->cache_version() !!}</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="d-flex flex-wrap gap-1 p-0">
                                    @include('block/extensions/buttons', ['page_params' => ''])
                                </div>
                            </div>
                        </div>
                        @if ($fsc->get_locks())
                            <div class="row mt-3">
                                <div class="col-12">
                                    <div class="card border">
                                        <div class="card-header mt-2 text-truncate">
                                            Bloqueos en la base de datos
                                        </div>
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="table-responsive-datatable">
                                                        <table id="table_admin_info_locks" class="table align-middle table-datatable table-borderless table-striped table-hover w-100">
                                                            <thead class="table-dark">
                                                            <tr>
                                                                <th class="text-start">Base de datos</th>
                                                                <th class="text-start">relname</th>
                                                                <th class="text-start">relation</th>
                                                                <th class="text-start">transaction ID</th>
                                                                <th class="text-start">PID</th>
                                                                <th class="text-start">modo</th>
                                                                <th class="text-start">granted</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            @foreach ($fsc->get_locks() as $key1 => $value1)
                                                                <tr>
                                                                    <td class="text-start text-nowrap text-truncate">{!! $value1['database'] !!}</td>
                                                                    <td class="text-start text-nowrap text-truncate">{!! $value1['relname'] !!}</td>
                                                                    <td class="text-start text-nowrap text-truncate">{!! $value1['relation'] !!}</td>
                                                                    <td class="text-start text-nowrap text-truncate">{!! $value1['transactionid'] !!}</td>
                                                                    <td class="text-start text-nowrap text-truncate">{!! $value1['pid'] !!}</td>
                                                                    <td class="text-start text-nowrap text-truncate">{!! $value1['mode'] !!}</td>
                                                                    <td class="text-start text-nowrap text-truncate">{!! $value1['granted'] !!}</td>
                                                                </tr>
                                                            @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
