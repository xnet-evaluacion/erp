<form name="f_feedback" action="{!! FS_COMMUNITY_URL !!}/feedback" method="post" target="_blank" class="form" role="form">
    <input type="hidden" name="feedback_info" value="{!! $fsc->system_info() !!}"/>
    <input type="hidden" name="feedback_type" value="error"/>
    <div class="modal" id="modal_feedback">
        <div class="modal-dialog modal-xl modal-fullscreen-xl-down">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">
                        <i class="fa-solid fa-edit fa-fw"></i>
                        Informar de error...
                    </h4>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Cerrar"></button>
                </div>
                @if ($fsc->check_for_updates())
                    <div class="modal-body bg-info bg-soft">
                        <p class='form-text'>
                            Tienes
                            <a href="updater.php" target="_blank" class="link-primary">actualizaciones pendientes</a>.
                            Las actualizaciones corrigen errores y añaden nuevas características.
                            No recibirás soporte en la web a menos que actualices.
                        </p>
                    </div>
                @endif
                <div class="modal-body">
                    <p class="form-text">
                        Usa este formulario para informarnos de cualquier error o duda que hayas encontrado.
                        Para facilitarnos el trabajo este formulario también nos informa de la versión de
                        MiFactura.eu que usas, lista de plugins activos, versión de php, etc...
                    </p>
                    <div class="mb-2">
                        <textarea rows="4" class="form-control" name="feedback_text" placeholder="Detalla tu duda o problema..."></textarea>
                    </div>
                    <div class="mb-2">
                        <div class="input-group">
                            <div class="input-group-text"><i class="fa-solid fa-envelope fa-fw"></i></div>
                            <input type="email" class="form-control" name="feedback_email" placeholder="Tu email"
                                   @if ($fsc->empresa && $fsc->user->logged_on)
                                       value="{!! $fsc->empresa->email !!}"
                                @endif
                            />
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary flex-grow-1 flex-sm-grow-0">
                        <i class="fa-solid fa-paper-plane fa-fw"></i>
                        <span>Enviar</span>
                    </button>
                </div>
            </div>
        </div>
    </div>
</form>

{{--
 @if ($fsc->empresa && !FS_DEMO && mt_rand(0,49)==0)
<div style="display: none;">
   @if (mt_rand(0,2)>0 && $fsc->user->logged_on)
    <iframe src="index.php?page=admin_home&check4updates=TRUE" style="height: 0;"></iframe>
    @endif
</div>
 @endif
--}}
