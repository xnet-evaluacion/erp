<?php

if (!defined('BASE_PATH')) {
    die(':)');
}

/*
 * This file is part of MiFactura.eu
 * Copyright (C) 2022 X-Net Software Solutions S.L. <xnetsoftware@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

// Cargamos el fichero de configuración para tener la información sobre la base de datos, aplicación, etc.
require_once constant('BASE_PATH') . '/config.php';

// Usamos el modelo "version" para acceder a la tabla versiones y saber los plugins instalados y su versión de base de datos.
require_once constant('BASE_PATH') . '/updater/model/version.php';

/**
 * Class PluginManager
 *
 * Esta clase analiza el estado de la instalación actual y permite descargar, instalar y readaptar la base de datos
 * para las versiones más recientes.
 *
 * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
 * @version 2022.0621
 *
 */
class PluginManager
{
    public const CORE_FOLDERS = [
        'base',
        'bin',
        'components',
        'controller',
        'dataconfig',
        'docker',
        'model',
        'node_modules',
        'raintpl',
        'resellers',
        'resources',
        'src',
        'Templates',
        'vendor',
        'view',
    ];

    /**
     * Modelo de la tabla versions, que contiene el listado de plugins instalados y sus versiones.
     *
     * @var version
     */
    private $versiones;

    /**
     * Contiene el nombre del archivo que contiene la versión del núcleo.
     * Gracias al nombre del archivo, podemos saber la aplicación que hay instalada.
     *
     * @var string
     */
    private $appVersionFile;

    /**
     * @var array
     */
    private $installed;

    /**
     * @var string
     */
    private $tmpFolder;

    /**
     * PluginManager constructor.
     */
    public function __construct()
    {
        $this->versiones = new version();
        $this->installed = $this->getVersions();
        $tmpname = defined('XFS_TMP_NAME') ? constant('XFS_TMP_NAME') : constant('FS_TMP_NAME');
        $this->tmpFolder = constant('BASE_PATH') . '/tmp/' . $tmpname . 'plugins/';
    }

    public function getInstalled(): array
    {
        return $this->installed;
    }

    /**
     * Retorna la aplicación que está instalada (por su nombre de archivo de configuración)
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0620
     *
     * @return string
     */
    private function getApplication()
    {
        // Se comprueba si es una instalación de MiFactura
        if (file_exists('MiVersion')) {
            return 'MiVersion';
        }

        // Se comprueba si es una instalación de XFS
        if (file_exists('XfsVersion')) {
            return 'XfsVersion';
        }

        // Se comprueba si es una instalación de FS
        if (file_exists('version')) {
            return 'version';
        }

        return 'unknown';
    }

    /**
     * Retorna el nombre del archivo de versión del programa instalado.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0621
     *
     * @return string
     */
    public function getApp()
    {
        return $this->appVersionFile;
    }

    /**
     * Obtiene la lista de plugins que existen en la tabla de versiones del núcleo.
     * Si la tabla no existe, retorna un array vacío y no habrá información sobre la versión actual
     * de la base de datos.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0624
     *
     * @return array
     */
    private function getPluginsInTableVersions()
    {
        $plugins = [];
        if ($this->versiones->tableExists) {
            $all = $this->versiones->select('SELECT * FROM `' . $this->versiones->tableName . '`');
            foreach ($all as $plugin) {
                $plugins[$plugin['plugin']] = [
                    'plugin' => $plugin['plugin'],
                    'version' => $plugin['version'],
                    'db_version' => $plugin['db_version'],
                ];
            }
        }
        return $plugins;
    }

    /**
     * Obtiene información REAL sobre los plugins instalados.
     * La información se obtiene recorriendo cada plugin y comprobando la versión instalada.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0624
     *
     * @return array
     */
    public function getVersions()
    {
        $pluginsInVersions = $this->getPluginsInTableVersions();

        // Tomamos el nombre del archivo de configuración según la aplicación
        $this->appVersionFile = $this->getApplication();
        switch ($this->appVersionFile) {
            case 'MiVersion':
                $configFilename = 'mifactura.ini';
                break;
            case 'XfsVersion':
                $configFilename = 'xfs.ini';
                break;
            case 'version':
                $configFilename = 'facturascripts.ini';
                break;
            default:
                $configFilename = 'config.ini';
        }

        $version = file_get_contents(constant('BASE_PATH') . '/' . $this->appVersionFile);
        if (isset($pluginsInVersions['core'])) {
            $this->installed['core'] = $pluginsInVersions['core'];
        } else {
            $this->installed['core']['plugin'] = 'core';
            $this->installed['core']['db_version'] = 0;
        }
        $this->installed['core']['version'] = $version;

        $plugins = scandir(constant('BASE_PATH') . '/plugins');
        foreach ($plugins as $pluginName) {
            // Nos aseguramos de que sea una carpeta real, y no haga referencia a sí mismo, o a la anterior
            if (!is_dir(constant('BASE_PATH') . '/plugins/' . $pluginName) || in_array($pluginName, ['.', '..'])) {
                continue;
            }

            // Leemos la versión del código.
            $filename = constant('BASE_PATH') . '/plugins/' . $pluginName . '/' . $configFilename;
            if (!file_exists($filename)) {
                continue;
            }

            $parameters = parse_ini_file($filename);
            $version = $parameters['version'];

            if (isset($pluginsInVersions[$pluginName])) {
                $this->installed[$pluginName] = $pluginsInVersions[$pluginName];
            } else {
                $this->installed[$pluginName]['plugin'] = $pluginName;
                $this->installed[$pluginName]['db_version'] = 0;

                $newVersion = new version();
                $newVersion->id = null;
                $newVersion->plugin = $pluginName;
                $newVersion->version = $version;
                $newVersion->db_version = 0;
                $newVersion->core_version = 0;
                if (!$newVersion->save()) {
                    json_response([
                        'status' => false,
                        'error' => "No se ha podido crear el registro en versions para el plugin '$pluginName'",
                    ]);
                }
            }
            $this->installed[$pluginName]['version'] = $version;
        }
        return $this->installed;
    }

    /**
     * Obtiene un archivo desde una URL.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0624
     *
     * @param $url
     * @param $timeout
     *
     * @return bool|string
     */
    public function fileGetContents($url, $timeout = 25)
    {
        if (function_exists('curl_init')) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
            curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla / 5.0 (X11; Linux x86_64) AppleWebKit / 537.36 (KHTML, like Gecko) Chrome / 56.0.2924.87 Safari / 537.36');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            if (ini_get('open_basedir') === null) {
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            }

            /**
             * En algunas configuraciones de php es necesario desactivar estos flags,
             * en otras es necesario activarlos. habrá que buscar una solución mejor.
             */
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);

            if (defined('FS_PROXY_TYPE')) {
                curl_setopt($ch, CURLOPT_PROXYTYPE, FS_PROXY_TYPE);
                curl_setopt($ch, CURLOPT_PROXY, FS_PROXY_HOST);
                curl_setopt($ch, CURLOPT_PROXYPORT, FS_PROXY_PORT);
            }
            $data = curl_exec($ch);
            $info = curl_getinfo($ch);

            if ($info['http_code'] == 200) {
                curl_close($ch);
                return $data;
            } elseif ($info['http_code'] == 301 || $info['http_code'] == 302) {
                $redirs = 0;
                return fs_curl_redirect_exec($ch, $redirs);
            }

            /// guardamos en el log
            if (class_exists('fs_core_log') && $info['http_code'] != 404) {
                $error = curl_error($ch);
                if ($error == '') {
                    $error = 'ERROR ' . $info['http_code'];
                    // if (constant('FS_DB_HISTORY')) {
                    // $error .= ' en fs_file_get_contents con url "' . $url . '"';
                    // $error .= ' < pre>' . print_r(debug_backtrace(), true) . ' </pre > ';
                    // }
                }

                $core_log = new fs_core_log();
                $core_log->new_error($error);
                $core_log->save($url . ' - ' . $error);
            }

            curl_close($ch);
            return "ERROR ($url)";
        }

        return file_get_contents($url);
    }

    /**
     * Obtiene un archivo de la comunidad.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0624
     *
     * @param $url
     * @param $timeout
     *
     * @return mixed
     */
    public function getFromCommunity($url, $timeout = 25)
    {
        switch ($this->appVersionFile) {
            case 'XfsVersion':
                $urlPath = defined('XFS_COMMUNITY_URL') ? constant('XFS_COMMUNITY_URL') : 'https://community.xfs.cloud';
                break;
            default:
                $urlPath = defined('FS_COMMUNITY_URL') ? constant('FS_COMMUNITY_URL') : 'https://community.mifactura.eu';
        }
        $completeUrl = $urlPath . $url;
        $fileContent = $this->fileGetContents($completeUrl, $timeout);
        return json_decode($fileContent);
    }

    /**
     * Inicializa el array communityCore con la información sobre la actualización del núcleo
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0621
     *
     */
    private function getCommunityCoreData()
    {
        $this->communityCore = [];
        switch ($this->appVersionFile) {
            case 'MiVersion':
                $aplicacion = 'MiFactura';
                foreach ($this->getFromCommunity('/core.json') as $item) {
                    $this->communityCore[$item->nombre] = [
                        'folder' => 'mifactura-2020',
                        'description' => $item->descripcion,
                        'version' => $this->installed['core']['version'],
                        'new_version' => $item->version,
                        'db_version' => $this->installed['core']['db_version'],
                        'link' => $item->link,
                    ];
                }
                break;
            case 'XfsVersion':
                $nucleo = $this->versiones->get(1);
                $aplicacion = 'XfsCloud';
                $item = $this->getFromCommunity('/index.php?page=download_plugin&action=details&id=1');
                $this->communityCore['core'] = [
                    'folder' => 'xfs-2017',
                    'description' => $item->descripcion,
                    'version' => $this->installed['core']['version'],
                    'new_version' => $item->version,
                    'db_version' => $this->installed['core']['db_version'],
                    'link' => $item->link,
                ];
                break;
        }
    }

    /**
     * Inicializa las variables con información sobre las actualizaciones de los plugins.
     * La variable communityInstalledPlugins nos da información sobre los plugins instalados, mientras que
     * la variable communityAvailablePlugins, nos la da sobre los disponibles.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0621
     *
     */
    private function getCommunityPluginsData()
    {
        $this->communityInstalledPlugins = [];
        $this->communityAvailablePlugins = [];

        foreach ($this->getFromCommunity('/plugins-list.json') as $item) {
            if (isset($this->installed[$item->nombre])) {
                $this->communityInstalledPlugins[$item->nombre] = [
                    'description' => $item->descripcion,
                    'version' => $this->installed[$item->nombre]['version'],
                    'new_version' => $item->version,
                    'db_version' => $this->installed[$item->nombre]['db_version'],
                    'link' => $item->link,
                ];
                continue;
            }
            $this->communityAvailablePlugins[$item->tipo][$item->nombre] = [
                'description' => $item->descripcion,
                'version' => 0,
                'new_version' => $item->version,
                'db_version' => 0,
                'link' => $item->link,
            ];
        }
    }

    /**
     * Obtiene la información para la descarga del núcleo
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0622
     *
     * @return array
     */
    public function getCoreInfo()
    {
        $downloads = [];
        $this->getCommunityCoreData();
        foreach ($this->communityCore as $nombre => $item) {
            if ((float) $item['version'] < (float) $item['new_version']) {
                $downloads[$item['folder']] = $item['link'];
            }
        }
        return $downloads;
    }

    /**
     * Obtiene la información para la descarga de los plugins
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0622
     *
     * @return array
     */
    public function getPluginsInfo()
    {
        $downloads = [];
        $this->getCommunityPluginsData();
        foreach ($this->communityInstalledPlugins as $nombre => $item) {
            if ((float) $item['version'] < (float) $item['new_version']) {
                $downloads[$nombre] = $item['link'];
            }
        }
        return $downloads;
    }

    public static function scanFolder($folder, $recursive = false, $exclude = ['.', '..', '.DS_Store', '.well-known',])
    {
        $scan = scandir($folder, SCANDIR_SORT_ASCENDING);
        if (!is_array($scan)) {
            return [];
        }

        $rootFolder = array_diff($scan, $exclude);
        natcasesort($rootFolder);
        if (!$recursive) {
            return $rootFolder;
        }

        $result = [];
        foreach ($rootFolder as $item) {
            $newItem = $folder . DIRECTORY_SEPARATOR . $item;
            if (is_file($newItem)) {
                $result[] = $item;
                continue;
            }
            $result[] = $item;
            foreach (static::scanFolder($newItem, true, $exclude) as $item2) {
                $result[] = $item . DIRECTORY_SEPARATOR . $item2;
            }
        }

        return $result;
    }

    public static function delTree($folder)
    {
        $folder = rtrim($folder, '\/');
        if (!file_exists($folder)) {
            return true;
        }

        $files = is_dir($folder) ? static::scanFolder($folder, false, ['.', '..']) : [];
        foreach ($files as $file) {
            $path = $folder . DIRECTORY_SEPARATOR . $file;
            is_dir($path) ? static::delTree($path) : unlink($path);
        }

        return is_dir($folder) ? rmdir($folder) : unlink($folder);
    }

    function fileDownload($url, $filename, $timeout = 60)
    {
        $ok = false;

        try {
            $data = $this->fileGetContents($url, $timeout);
            if ($data && $data != 'ERROR' && file_put_contents($filename, $data) !== false) {
                $ok = true;
            }
        } catch (Exception $e) {
            /// nada
        }

        return $ok;
    }

    private function descomprimir($name)
    {
        // Lo descomprimimos
        $zip = new ZipArchive();
        $res = $zip->open(constant('BASE_PATH') . '/tmp/' . $name . '.zip', ZipArchive::CHECKCONS);

        if ($res !== true) {
            $result['error'] = 'Error al abrir el ZIP del plugin ' . $name . '. Código: ' . $res;
            return $result;
        }

        if (!is_dir($this->tmpFolder) && !mkdir($this->tmpFolder, 0777, true)) {
            $result['error'] = 'No se ha podido crear la carpeta temporal para el plugin ' . $name . '.';
            return $result;
        }
        if (!$zip->extractTo($this->tmpFolder)) {
            $result['error'] = 'No se ha podido extraer el plugin ' . $name . '.';
            return $result;
        }
        $zip->close();
        unlink(constant('BASE_PATH') . '/tmp/' . $name . '.zip');

        return ['ok' => 'Descompresión realizada correctamente'];
    }

    public static function recursiveCopy($src, $dst)
    {
        if (!file_exists($src) || (is_dir($src) && !file_exists($dst) && !@mkdir($dst))) {
            return false;
        }

        if (is_file($src)) {
            copy($src, $dst);
        } elseif (is_dir($src)) {
            $folder = opendir($src);

            while (false !== ($file = readdir($folder))) {
                if ($file === '.' || $file === '..') {
                    continue;
                } elseif (is_dir($src . DIRECTORY_SEPARATOR . $file)) {
                    static::recursiveCopy($src . DIRECTORY_SEPARATOR . $file, $dst . DIRECTORY_SEPARATOR . $file);
                } else {
                    if (file_exists($src . DIRECTORY_SEPARATOR . $file)) {
                        copy($src . DIRECTORY_SEPARATOR . $file, $dst . DIRECTORY_SEPARATOR . $file);
                    }
                }
            }

            closedir($folder);
        }

        return true;
    }

    /**
     * Descarga el archivo del núcleo desde la comunidad correspondiente.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0622
     *
     * @param $link
     * @param $name
     *
     * @return string[]
     */
    public function downloadCore($link, $name)
    {
        if (!$this->fileDownload($link, constant('BASE_PATH') . '/tmp/' . $name . '.zip')) {
            $result['error'] = 'Error al descargar el núcleo. Inténtelo más tarde y contacte con el proveedor si el error persiste.';
            return $result;
        }
        $result['ok'] = 'Descarga correcta';
        return $result;
    }

    /**
     * Renombra las carpetas temporales del núcleo a su nombre original.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0819
     *
     * @return bool
     */
    public function restoreCore()
    {
        $ok = true;
        /**
         * Se restauran las carpetas
         */
        foreach (self::CORE_FOLDERS as $folder) {
            $folder_name = constant('FS_FOLDER') . '/' . $folder;
            $old_folder = $folder_name . '_old';
            if (file_exists($old_folder) && !file_exists($folder_name)) {
                $ok = $ok && rename($old_folder, $folder_name);
            }
        }
        return $ok;
    }

    /**
     * Renombra las carpetas del núcleo añadiéndole _old detrás.
     * Si había alguna carpeta de copia previa, la elimina antes de proceder.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0622
     *
     * @param $link
     * @param $name
     *
     * @return bool
     */
    public function backupCore()
    {
        $ok = true;
        /**
         * Eliminación de carpetas obsoletas
         */
        foreach (self::CORE_FOLDERS as $folder) {
            $folder_name = constant('FS_FOLDER') . '/' . $folder;
            $old_folder = $folder_name . '_old';
            $this->delTree($old_folder);
            if (file_exists($folder_name)) {
                $ok = $ok && rename($folder_name, $old_folder);
            }
        }
        return $ok;
    }

    /**
     * Descomprime el archivo ZIP con el núcleo.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0622
     *
     * @param $link
     * @param $name
     *
     * @return bool
     */
    public function decompressCore($name)
    {
        $msg = $this->descomprimir($name);
        if (isset($msg['error'])) {
            return $msg;
        }

        $result['ok'] = 'Descarga correcta';
        return $result;
    }

    /**
     * Mueve el núcleo de la carpeta temporal a la ubicación de la aplicación.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0624
     *
     * @param $link
     * @param $name
     *
     * @return bool
     */
    public function installCore($name)
    {
        $ok = true;

        $names = [
            $this->tmpFolder . $name . '/',
            $this->tmpFolder . $name . '-main/',
            $this->tmpFolder . $name . '-master/',
        ];

        foreach ($names as $folder_name) {
            if (file_exists($folder_name)) {
                $ok = $ok && $this->recursiveCopy($folder_name, constant('FS_FOLDER'));
                $ok = $ok && $this->delTree($folder_name);
            }
        }

        return $ok;
    }

    /**
     *
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0625
     *
     * @param $link
     * @param $name
     *
     * @return array|string[]
     */
    public function downloadPlugin($link, $name)
    {
        if (!$this->fileDownload($link, constant('BASE_PATH') . '/tmp/' . $name . '.zip')) {
            $result['error'] = 'Error al descargar el plugin ' . $name . '. Se ignora este plugin hasta la siguiente actualización.';
            return $result;
        }
        $result['ok'] = 'Descarga correcta';
        return $result;
    }

    /**
     * Descomprime un plugin recién descargado.
     *
     * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
     * @version 2022.0624
     *
     * @param $name
     *
     * @return array
     */
    public function decompressPlugin($name)
    {
        if (!is_writable(constant('BASE_PATH') . '/plugins')) {
            $result['error'] = 'No se puede escribir en la carpeta que contiene los plugins.';
            return $result;
        }

        $msg = $this->descomprimir($name);
        if (isset($msg['error'])) {
            return $msg;
        }

        $result['ok'] = 'Descarga correcta';
        return $result;
    }

    public function installPlugin($name)
    {
        $names = [
            $this->tmpFolder . $name . '/',
            $this->tmpFolder . $name . '-main/',
            $this->tmpFolder . $name . '-master/',
        ];

        foreach ($names as $folder_name) {
            if (file_exists($folder_name)) {
                // Se elimina el plugin antes de descomprimir el nuevo.
                $existe = is_dir(constant('BASE_PATH') . '/plugins/' . $name);
                if ($existe) {
                    // Guarda la copia del plugin antes de actualizarlo, por si hay que recuperarlo.
                    if (is_dir(constant('BASE_PATH') . '/plugins/' . $name) && !rename(constant('BASE_PATH') . '/plugins/' . $name, constant('BASE_PATH') . '/tmp/' . constant('FS_TMP_NAME') . '/plugins/' . $name . '.old')) {
                        $result['error'] = 'No se ha podido renombrar la carpeta de plugin ' . $name . '.';
                        return $result;
                    }
                }

                // Movemos el plugin de la carpeta temporal a la del plugin
                if (!rename($folder_name, constant('BASE_PATH') . '/plugins/' . $name)) {
                    // Si falla, recuperamos el original
                    rename(constant('BASE_PATH') . '/tmp/' . constant('FS_TMP_NAME') . '/plugins/' . $name . '.old', constant('BASE_PATH') . '/plugins/' . $name);
                    $result['error'] = 'No se ha podido restaurar la carpeta del nuevo plugin ' . $name . '.';
                    return $result;
                }

                // Eliminamos la carpeta del plugin original que ya ha sido actualizado
                if ($existe) {
                    if (!$this->delTree(constant('BASE_PATH') . '/tmp/' . constant('FS_TMP_NAME') . '/plugins/' . $name . '.old') || !$this->delTree($folder_name)) {
                        $result['error'] = 'No se han podido eliminar los archivos temporales del plugin ' . $name . '.';
                    }
                }

                $pluginVersion = $this->versiones->getByName($name);
                if ($pluginVersion) {
                    $this->installed = $this->getVersions();
                    $pluginVersion->version = $this->installed[$name]['version'];
                    if (!$pluginVersion->save()) {
                        $result['error'] = 'No se ha podido actualizar la versión del plugin en versions';
                    }
                }

                $result['ok'] = 'Proceso finalizado';
                return $result;
            }
        }

        $result['error'] = 'No se han encontrado los archivos temporales del plugin ' . $name . '.';
        return $result;
    }
}
