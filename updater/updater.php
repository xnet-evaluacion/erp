<?php
/*
 * This file is part of MiFactura.eu
 * Copyright (C) 2022 X-Net Software Solutions S.L. <xnetsoftware@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

if (!defined('BASE_PATH')) {
    die(':)');
}

define('FS_FOLDER', constant('BASE_PATH'));

if (!file_exists(constant('BASE_PATH') . '/config.php')) {
    die('Archivo config.php no encontrado. No puedes actualizar sin instalar.');
}

if (file_exists(constant('BASE_PATH') . '/vendor/autoload.php')) {
    require_once constant('BASE_PATH') . '/vendor/autoload.php';
}

if (!function_exists('json_response')) {
    /**
     * Devuelve una respuesta en formato JSON.
     * Se duplica aquí la función, porque al no cargarse el autoload, no está disponible Functions.php
     *
     * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
     * @version 2022.0902
     *
     * @param array|stdClass $data
     * @param null|int       $flags
     *
     * @return void
     */
    function json_response($data, $flags = null): void
    {
        if ($flags == null) {
            $flags = constant('FS_DEBUG') ? JSON_PRETTY_PRINT : 0;
        }
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($data, $flags);
        die();
    }
}

// Cargamos el fichero de configuración para tener la información sobre la base de datos, aplicación, etc.
require_once constant('BASE_PATH') . '/config.php';

// Usamos el modelo "version" para acceder a la tabla versiones y saber los plugins instalados y su versión de base de datos.
require_once constant('BASE_PATH') . '/updater/model/version.php';

// Usamos también la librería que nos permite manejar los plugins.
require_once constant('BASE_PATH') . '/updater/PluginManager.php';

$versiones = new version();
$plugins = new PluginManager();
$installed = $plugins->getInstalled();

$nucleo = $versiones->get(1);

$community_core = [];
$community_plugins = [];
$community_availables = [];

$app = $plugins->getApp();

switch ($app) {
    case 'MiVersion':
        $aplicacion = 'MiFactura';
        foreach ($plugins->getFromCommunity('/core.json') as $item) {
            $changelogFile = 'CHANGELOG.md';
            $changelog = file_exists($changelogFile) ? nl2br(file_get_contents($changelogFile)) : '';
            $changelog = (new Parsedown())->text($changelog);

            $community_core[$item->nombre] = [
                'description' => $item->descripcion,
                'version' => $installed['core']['version'],
                'new_version' => $item->version,
                'db_version' => $installed['core']['db_version'],
                'link' => $item->link,
                'changelog' => $changelog,
            ];
        }
        break;
    case 'XfsVersion':
        $aplicacion = 'XfsCloud';
        $item = $plugins->getFromCommunity('/index.php?page=download_plugin&action=details&id=1');
        $community_core['core'] = [
            'description' => $item->descripcion,
            'version' => $installed['core']['version'],
            'new_version' => $item->version,
            'db_version' => $installed['core']['db_version'],
            'link' => $item->link,
        ];
        break;
}

$new_core = reset($community_core);

foreach ($plugins->getFromCommunity('/plugins-list.json') as $item) {
    if (isset($installed[$item->nombre])) {
        $changelogFile = 'plugins/' . $item->nombre . '/CHANGELOG.md';
        $changelog = file_exists($changelogFile) ? nl2br(file_get_contents($changelogFile)) : '';
        $changelog = (new Parsedown())->text($changelog);

        $community_plugins[$item->nombre] = [
            'description' => $item->descripcion,
            'version' => $installed[$item->nombre]['version'],
            'new_version' => $item->version,
            'db_version' => $installed[$item->nombre]['db_version'],
            'link' => $item->link,
            'changelog' => $changelog,
        ];
        continue;
    }
    $community_availables[$item->nombre] = [
        'description' => $item->descripcion,
        'version' => 0,
        'new_version' => $item->version,
        'db_version' => 0,
        'link' => $item->link,
    ];
}

function printTableLine($name, $description, $version, $newVersion, $dbVersion)
{
    $actualizada = (float) $version >= (float) $newVersion;
    $dbActualizada = (float) $dbVersion >= (float) $version;

    $versionColor = $actualizada ? 'green' : 'red';
    $newVersionColor = 'black';
    $dbVersionColor = $dbActualizada ? 'green' : 'black';

    if ($actualizada) {
        $textoEstado = '<span style="color:green">Actualizada</span>';
    } else {
        $textoEstado = '<span style="color:indianred">Pendiente</span>';
    }

    return '
        <tr>
            <td><b>' . $name . '</b></td>
            <td style="min-width: 900px;"><textarea class="form-control" rows="10" style="resize: none" readonly>' . strip_tags($description) . '</textarea></td>
            <td class="text-end" style="color:' . $versionColor . '">' . $version . '</td>
            <td class="text-end" style="color:' . $newVersionColor . '">' . $newVersion . '</td>
            <td class="text-end" style="color:' . $dbVersionColor . '">' . $dbVersion . '</td>
            <td class="text-end">' . $textoEstado . '</td>
        </tr>
    ';
}

require_once(constant('FS_FOLDER') . '/updater/html/updater.html.php');
