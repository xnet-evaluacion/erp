<?php
/*
 * This file is part of MiFactura.eu
 * Copyright (C) 2022 X-Net Software Solutions S.L. <xnetsoftware@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Contiene la ruta principal del alojamiento.
 *
 * No está deprecated, no es la misma ruta cuando es una multi instalación
 */
define('FS_FOLDER', constant('BASE_PATH'));

/**
 * Ruta al archivo para gestionar datos en periodo de mantenimiento
 *
 * TODO: Este nombre de archivo tendrá que ser el mismo que se haya utilizado en el núcleo.
 */
define('MAINTENANCE_FILE', constant('FS_FOLDER') . DIRECTORY_SEPARATOR . 'in_maintenance');

/**
 * Ruta al archivo que se usa para avisar al núcleo de que la base de datos necesita actualizarse
 *
 * TODO: Este nombre de archivo tendrá que ser el mismo que se haya utilizado en el núcleo.
 */
define('DATABASE_UPDATE_FLAG', constant('FS_FOLDER') . DIRECTORY_SEPARATOR . 'database_update_flag');

// Cargamos la librería que nos permite manejar los plugins.
require_once constant('BASE_PATH') . '/updater/PluginManager.php';

ignore_user_abort(true);

/**
 * Guarda los datos en el archivo de procesamiento (semáforo)
 *
 * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
 * @version 2022.0622
 *
 * @param $semaphore
 *
 * @return bool
 */
function save_semaphore($semaphore)
{
    if (!isset($semaphore->info->begin)) {
        $semaphore->info->begin = time();
    }
    return false !== file_put_contents(constant('MAINTENANCE_FILE'), json_encode($semaphore), LOCK_EX);
}

/**
 * Genera un semáforo con la información a procesar.
 *
 * El fichero generado es un json con la siguiente estructura:
 *      - begin / now : Son el resultado de time() en el instante de inicio y actual (última actualización)
 *      - seconds: Segundos desde la última actualización (now - begin)
 *      - core: Es un array con la información sobre el core (*)
 *      - plugins: Es un array con la información sobre los plugins (*)
 *
 * Una vez descomprimido correctamente, el archivo de copia de seguridad será eliminado.
 *
 * (*) Es un array asociativo de la forma: nombre => link.
 *
 * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
 * @version 2022.0622
 */
function actionSetUpdating()
{
    $pluginManager = new PluginManager();
    $result = new stdClass();
    $result->info = new stdClass();

    $result->info->begin = time();    // Si ya existe, begin será reescrito con el valor que tenga guardado.
    if (file_exists(constant('MAINTENANCE_FILE'))) {
        $result = json_decode(file_get_contents(constant('MAINTENANCE_FILE')));
        if (isset($_GET['do']) && $_GET['do'] === 'reset') {
            unlink(constant('MAINTENANCE_FILE'));
        }
    }
    $result->info->core = $pluginManager->getCoreInfo();
    $result->info->plugins = $pluginManager->getPluginsInfo();
    $status = save_semaphore($result);

    return [
        'status' => $status,
        'message' => $status ? 'Información para actualizar recopilada correctamente.' : 'No se ha podido recopilar la información para gestionar las actualizaciones.',
        'info' => $result->info,
    ];
}

/**
 * Una vez finalizado el proceso, elimina el archivo de actualización.
 *
 * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
 * @version 2022.0622
 *
 */
function actionUnsetUpdating()
{
    $result = new stdClass();
    $result->status = false;
    $result->message = 'No se ha encontrado archivo con información para gestionar las actualizaciones.';

    // Se elimina el semáforo una vez finalizado el proceso.
    if (file_exists(constant('MAINTENANCE_FILE'))) {
        $result = json_decode(file_get_contents(constant('MAINTENANCE_FILE')));
        $result->info->now = time();
        $result->info->seconds = (int) $result->info->now - (int) $result->info->begin;
        $result->info->unlink = unlink(constant('MAINTENANCE_FILE'));
        $result->status = true;
        $result->message = $result->info->unlink ? 'Se ha eliminado el archivo con información para gestionar las actualizaciones.' : 'No se ha podido eliminar el archivo con información para gestionar las actualizaciones.';
    }
    return $result;
}

/**
 * Se descarga el archivo ZIP del núcleo desde la comunidad correspondiente.
 *
 * El fichero json añadirá la siguiente información a la previa:
 *      - action: 'Downloading core'
 *      - now : El instante en el que se inicia la descarga
 *      - seconds: Segundos hasta el inicio de la descarga
 *      - download_name: Es el nombre del último archivo descargado, mientras su procesado esté en curso
 *      - download_link: Es el enlace para descargar el archivo que está siendo procesado.
 *      - download_backup: 1 si ya se ha sacado la copia de seguridad.
 *      - download_installed: 1 si ya se ha descomprimido correctamente.
 *
 * Una vez descomprimido correctamente, el archivo de copia de seguridad será eliminado.
 *
 * (*) Es un array asociativo de la forma: nombre => link.
 *
 * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
 * @version 2022.0622
 *
 */
function actionDownloadCore($name)
{
    $pluginManager = new PluginManager();
    $result = json_decode(file_get_contents(constant('MAINTENANCE_FILE')));
    $result->status = true;
    $result->message = 'Núcleo descargado correctamente.';
    $downloads = (array) $result->info->core;
    $link = $downloads[$name];
    $msg = $pluginManager->downloadCore($link, $name);
    if (isset($msg['error'])) {
        $result->status = false;
        $result->message = $msg['error'];
    }
    return $result;
}

/**
 * Hace una copia de seguridad de las carpetas del núcleo, renombrándolas terminando en _old
 *
 * El fichero json añadirá la siguiente información a la previa:
 *      - download_backup: 1 si ha tenido éxito y 0 si no.
 *
 * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
 * @version 2022.0623
 *
 * @return mixed
 */
function actionBackupCore()
{
    $pluginManager = new PluginManager();
    $result = json_decode(file_get_contents(constant('MAINTENANCE_FILE')));
    $result->status = true;
    $result->message = 'Backup del núcleo realizado correctamente.';
    if (!$pluginManager->backupCore()) {
        $result->status = false;
        $result->message = 'Error al hacer copia de seguridad del núcleo';
    }
    return $result;
}

/**
 * Descarga el siguiente plugin disponible en la comunidad.
 *
 * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
 * @version 2022.0624
 *
 * @return mixed
 */
function actionDownloadPlugin($name)
{
    $pluginManager = new PluginManager();
    $result = json_decode(file_get_contents(constant('MAINTENANCE_FILE')));
    $result->status = true;
    $result->message = 'Plugin ' . $name . ' descargado correctamente.';
    $downloads = (array) $result->info->plugins;
    $link = $downloads[$name];
    $msg = $pluginManager->downloadPlugin($link, $name);
    if (isset($msg['error'])) {
        if (isset($result->info->plugins->{$name})) {
            unset($result->info->plugins->{$name});
            save_semaphore($result);
        }
        $result->status = false;
        $result->message = $msg['error'];
    }
    return $result;
}

/**
 * Descomprime el plugin recientemente descargado.
 *
 * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
 * @version 2022.0624
 *
 * @return mixed
 */
function actionDecompressPlugin($name)
{
    $pluginManager = new PluginManager();
    $result = json_decode(file_get_contents(constant('MAINTENANCE_FILE')));
    $result->status = true;
    $result->message = 'Plugin ' . $name . ' descomprimido correctamente.';
    $msg = $pluginManager->decompressPlugin($name);
    $result->now = time();
    if (isset($msg['error'])) {
        $result->status = false;
        $result->message = $msg['error'];
    }
    return $result;
}

function actionInstallPlugin($name)
{
    $pluginManager = new PluginManager();
    $result = json_decode(file_get_contents(constant('MAINTENANCE_FILE')));
    $result->status = true;
    $result->message = 'Plugin ' . $name . ' instalado correctamente.';
    $msg = $pluginManager->installPlugin($name);
    $result->now = time();
    if (isset($msg['error'])) {
        $result->status = false;
        $result->message = $msg['error'];
    }
    return $result;
}

/**
 * Genera un archivo, que indica al núcleo que tiene que actualizar las bases
 * de datos tras la instalación de actualizaciones.
 *
 * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
 * @version 2022.0624
 *
 * @return mixed
 */
function actionSetDatabaseUpdateFlag()
{
    $result = [
        'info' => ['now' => time()],
    ];

    $status = file_put_contents(constant('DATABASE_UPDATE_FLAG'), json_encode($result));
    $result['status'] = (bool) $status;
    $result['message'] = $status ? 'Establecido testigo para la actualización de la base de datos.' : 'No se ha podido establecer el testigo para la actualización de la base de datos.';

    return $result;
}

function actionUnsetDatabaseUpdateFlag()
{
    $result = [
        'info' => ['now' => time()],
        'status' => false,
        'message' => 'No se ha podido quitar el testigo para la actualización de la base de datos.',
    ];

    // Se elimina el semáforo una vez finalizado el proceso.
    foreach (['MAINTENANCE_FILE', 'DATABASE_UPDATE_FLAG'] as $filename) {
        if (file_exists(constant($filename))) {
            $result['info']['unlink'] = unlink(constant($filename));
            $result['status'] = true;
            $result['message'] = 'Se ha quitado el testigo para la actualización de la base de datos.';
        }
    }
    return $result;
}

/**
 * Descomprime el núcleo previamente descargado.
 *
 * El fichero json añadirá la siguiente información a la previa:
 *      - download_backup: 1 si ha tenido éxito y 0 si no.
 *      - download_installed: 1 si ha tenido éxito y 0 si no.
 *
 * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
 * @version 2022.0622
 *
 * @return mixed
 */
function actionDecompressCore($name)
{
    $pluginManager = new PluginManager();
    $result = json_decode(file_get_contents(constant('MAINTENANCE_FILE')));
    $result->status = true;
    $result->message = 'Núcleo descomprimido correctamente.';
    $status = $pluginManager->decompressCore($name);
    if (isset($status['error'])) {
        $result->status = false;
        $result->message = $status['error'];
    }
    return $result;
}

/**
 * Mueve los archivos descomprimidos previamente de la carpeta temporal a la ubicación
 * correcta en la carpeta principal.
 *
 * TODO: Este proceso es el que suele fallar por timeout
 *
 * @author  Rafael San José Tovar <rafael.sanjose@x-netdigital.com>
 * @version 2022.0624
 *
 * @return mixed
 */
function actionInstallCore($name)
{
    $pluginManager = new PluginManager();
    $result = json_decode(file_get_contents(constant('MAINTENANCE_FILE')));
    $result->status = true;
    $result->message = 'Núcleo instalado correctamente.';
    if (!$pluginManager->installCore($name)) {
        $result->status = false;
        $result->message = 'Error al instalar el núcleo';
        if (!$pluginManager->restoreCore($name)) {
            $result->message = 'Error al instalar el núcleo y al restaurar las carpetas';
        }
    }
    return $result;
}

/**
 * Devuelve una respuesta en formato JSON.
 * Se duplica aquí la función, porque al no cargarse el autoload, no está disponible Functions.php
 *
 * @author  Francesc Pineda Segarra <francesc.pineda@x-netdigital.com>
 * @version 2022.0902
 *
 * @param array|stdClass $data
 * @param null|int       $flags
 *
 * @return void
 */
function json_response($data, $flags = null): void
{
    if ($flags == null) {
        $flags = constant('FS_DEBUG') ? JSON_PRETTY_PRINT : 0;
    }
    header('Content-Type: application/json; charset=utf-8');
    echo json_encode($data, $flags);
    die();
}

$action = $_GET['action'];
switch ($action) {
    case 'set_updating':
        $result = actionSetUpdating();
        break;
    case 'unset_updating':
        $result = actionUnsetUpdating();
        break;
    case 'download_core':
        $result = actionDownloadCore($_GET['name']);
        break;
    case 'download_plugin':
        $result = actionDownloadPlugin($_GET['name']);
        break;
    case 'decompress_plugin':
        $result = actionDecompressPlugin($_GET['name']);
        break;
    case 'install_plugin':
        $result = actionInstallPlugin($_GET['name']);
        break;
    case 'set_database_update_flag':
        $result = actionSetDatabaseUpdateFlag();
        break;
    case 'unset_database_update_flag':
        $result = actionUnsetDatabaseUpdateFlag();
        break;
    case 'backup_core':
        $result = actionBackupCore($_GET['name']);
        break;
    case 'decompress_core':
        $result = actionDecompressCore($_GET['name']);
        break;
    case 'install_core':
        $result = actionInstallCore($_GET['name']);
        break;
    default:
        $result = new stdClass();
        $result->message = 'Unknown action: ' . $action;
}

json_response($result);
