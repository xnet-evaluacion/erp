# UPDATER
El módulo de actualización sirve para actualizar el núcleo y los módulos desde cualquier comunidad de X-NET.

La parte visual no está 100% operativa y nos hemos encontrados con el problema del gran tamaño que tiene el núcleo de MiFactura con Blade/Skote.

El proceso de actualización realiza los siguientes procesos:

## Lanzamiento del proceso de actualización

Al lanzar el proceso de actualización, lo primero que se hace es desactivar el botón de actualización y mostrar un modal con un textarea dónde se irá detallando el proceso.

**NOTA**: El navegador deja la página congelada hasta que finaliza el proceso.

## Actualización de la aplicación y los módulos

El primer paso de actualización, es la verificación de si hay actualización del núcleo. Si la hay:

- Se obtiene el fichero ZIP desde la comunidad correspondiente.
- Se renombran las carpetas de la instalación actual como *nombre_carpeta*_old
- Se descomprime el archivo ZIP en una carpeta temporal.
- Se mueve el contenido de esa carpeta temporal al sitio correspondiente.

Una vez instalado el núcleo, se procederá a instalar los plugins uno a uno.

- Se obtiene el fichero ZIP del primer plugin disponible.
- Se descomprime y se repite el proceso mientras que queden plugins por actualizar.

## Actualización de la base de datos y de los Wizard

Una vez descargados todos los plugins, se establece un testigo, y será el núcleo quién continuará con el proceso de instalación:

- Actualizará las bases de datos.
- Si procede, ejecutará los correspondientes wizard.

# NOTAS

A tener en cuenta para futuras versiones:

- Los archivos updater.php, updater_core.php y updater_maintenance.php, así como el contenido íntegro de la carpeta src/updater, deberían de ser independientes de la aplicación, y el mismo para Mifactura, XFS o cualquier otra aplicación de XNET.